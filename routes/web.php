<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
    Route::get('reset', function (){
       // Artisan::call('storage:link');
       Artisan::call('route:clear');
       Artisan::call('cache:clear');
       Artisan::call('config:clear');
       Artisan::call('config:cache');
       return "Cache clear successfully";
    });

    Route::get('/cache-clear', function() {
        Artisan::call('cache:clear');
        return "Cache is cleared";
    });
    Route::get('/config-cache', function() {
        Artisan::call('config:cache');
        return "config Cache is cleared";
    });
    Route::get('/config-clear', function() {    
        Artisan::call('config:clear');
        return "config is cleared";
    });

    Route::get('/', function () {
        return view('user.index');
    });
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('about', 'User\HomeController@About');
    Route::get('commitment', 'User\HomeController@Commitment');
    Route::get('faqs', 'User\HomeController@Faqs');
    Route::get('covid-19', 'User\HomeController@Covid19');
    Route::post('add_user', 'User\HomeController@AddUser');
    Route::post('add_contact_user', 'User\HomeController@addContactUser');
    Route::get('page/{title}', 'User\HomeController@CmsPage');

    #Admin Login Register   
	Route::get('/login/admin', 'Auth\LoginController@showAdminLoginForm');
    Route::post('/login/admin', 'Auth\LoginController@adminLogin');
    Route::post('/register/admin', 'Auth\RegisterController@createAdmin');
    Route::get('admin/forgot-password', 'Admin\LoginController@forgetPassword');
    Route::post('admin/sendforgetlink', 'Admin\LoginController@sendforgetlink');
    Route::get('admin/resetpassword', 'Admin\LoginController@showResetPassword'); 
    Route::post('admin/updateresetpassword', 'Admin\LoginController@updateResetPassword'); 

    
    #Seller login Register Route
    Route::get('/login/seller', 'Auth\LoginController@showSellerLoginForm');
    Route::get('/seller/signup', 'Auth\RegisterController@showSellerRegisterForm');    
    Route::get('/seller/signup/{id}', 'Auth\RegisterController@showSellerRegister');  
    Route::post('/login/seller', 'Auth\LoginController@sellerLogin');
    Route::get('seller/forgot-password', 'Seller\LoginController@ForgotPassword');
    Route::post('seller/sendforgetlink', 'Seller\LoginController@SendForgetLink');
    Route::get('seller/reset-password/{token}', 'Seller\LoginController@ResetPassword');
    Route::get('seller/resend_Otp', 'Seller\LoginController@resendOtp');
    Route::post('seller/updateresetpassword', 'Seller\LoginController@UpdateResetPassword');
    Route::post('/register/seller', 'Auth\RegisterController@createSeller');
    Route::get('seller/business-location/{id}', 'Auth\RegisterController@BusinessLocation');
    Route::get('seller/otp_verify/{id}', 'Auth\RegisterController@VerifyOtp');
    Route::post('seller/otp-match', 'Auth\RegisterController@MatchOtp');
    Route::get('seller/resend-otp/{id}', 'Auth\RegisterController@ResendOtp');
    Route::get('seller/business-details/{id}', 'Auth\RegisterController@Sellerbusiness');
    Route::post('seller/primary-business-location', 'Auth\RegisterController@PrimaryBusinessLocation');
    Route::post('seller/business-details-submit', 'Auth\RegisterController@BusinessDetailsSubmit');    
    Route::get('seller/getcountrylist/{id}', 'Auth\RegisterController@CountryList');
    Route::get('seller/GetoldAddress/{id}', 'Auth\RegisterController@GetOldAddress');
    Route::post('seller/add-business-hours', 'Auth\RegisterController@AddBusinessHour');
    Route::post('seller/update-business-hours', 'Auth\RegisterController@UpdateBusinessHour');
    Route::get('seller/business-hour/{id}', 'Auth\RegisterController@getBusinessHour');   
    Route::get('seller/check-one-day-exist', 'Auth\RegisterController@checkOneDayExist');
    Route::post('seller/add-businness-days', 'Auth\RegisterController@AddBusinnessDays');
    Route::post('seller/Edit-businness-days', 'Auth\RegisterController@EditBusinnessDays');

       

    //User Route 
    Route::get('user/forgot-password', 'User\HomeController@forgotpassword');
    Route::post('user/resetpassword', 'User\HomeController@userreset');
    Route::get('user/otpverify/{id}', 'User\HomeController@otpverification');
    Route::post('user/otpmatch', 'User\HomeController@otpmatch');
    Route::get('user/change-password/{id}', 'User\HomeController@changepassword');
    Route::post('user/passwordreset', 'User\HomeController@passwordreset');
  
    Auth::routes();
    Route::group(array('prefix' => 'user', 'middleware' => 'auth'), function() {
        Route::get('/otp', 'User\HomeController@sendotp');
        Route::post('/verify', 'User\HomeController@verify');
        Route::get('/create-profile', 'User\HomeController@createprofile');
        Route::post('/saveprofile', 'User\HomeController@saveprofile');
    });


	//Admin Route
	Route::group(array('prefix' => 'admin', 'middleware' => 'admin'), function () {		
        
        #Profile mgmtm
        Route::get('/profile', 'Admin\HomeController@Profile')->name('profile');
        Route::get('edit-profile', 'Admin\HomeController@editProfile')->name('edit-profile');
        Route::post('update-profile', 'Admin\HomeController@updateProfile')->name('update-profile');
        Route::get('change-password', 'Admin\HomeController@ChangePassword')->name('change-password');
        
        Route::post('change-password-admin', 'Admin\HomeController@ChangePasswordAdmin')->name('change-password-admin');
        Route::get('logout', 'Admin\LoginController@logout'); 
        Route::get('/dashboard', 'Admin\HomeController@index');  

        Route::get('dashboard-filter', 'Admin\HomeController@DashboardFilter');      
        #User Mgmt
        Route::get('/user-management', 'Admin\UserManagementController@userlist');
        Route::get('deleteUser/{id}', 'Admin\UserManagementController@DeleteUser');
        Route::get('blockUser/{id}/{type}', 'Admin\UserManagementController@blockUser');
        Route::get('user-gig-address/{id}', 'Admin\UserManagementController@UserGigAddress');
        Route::get('user-address/{id}', 'Admin\UserManagementController@UserAddress');
        #payment mgmt
        Route::get('payment-management', 'Admin\PaymentManagement@PaymentManagement');
        #Promocode Mgmt
        Route::get('promo-code-management', 'Admin\PaymentManagement@PromoCodeMannagement');
        Route::post('promo-code-management-filter', 'Admin\PaymentManagement@FilterPromocode');
        Route::post('promo-code-management-add','Admin\PaymentManagement@AddPromocode');
        Route::post('add-coupon','Admin\PaymentManagement@AddPromocode');
        Route::post('promo-code-management-edit','Admin\PaymentManagement@EditPromocode');
        Route::post('promo-code-management-delete','Admin\PaymentManagement@DeletePromocode');
        Route::post('promo-code-management-block-user','Admin\PaymentManagement@BlockPromoUser');
        #export Excel
        Route::get('export-user', 'Admin\UserManagementController@ExportExcel');
        Route::get('export-service-provider-excel', 'Admin\UserManagementController@ExportServiceProviderExcel');
        #crm mgmt 
        Route::post('add-cms-page', 'Admin\CrmManagement@AddCmsPage');
        Route::get('cms-management', 'Admin\CrmManagement@cmsMgmt');
        Route::post('add-about-us', 'Admin\CrmManagement@AddAbout');
        Route::post('termsconditions', 'Admin\CrmManagement@TermsConditions');
        Route::post('privacypolicy', 'Admin\CrmManagement@PrivacyPolicy');
        Route::post('faq', 'Admin\CrmManagement@AddFaq');
        Route::get('delete-faq/{id}', 'Admin\CrmManagement@DeleteFaq');
        #Store Mgmt
        Route::get('add-service-provider', 'Admin\UserManagementController@AddServiceProvider');
        Route::get('/services-provider-management', 'Admin\UserManagementController@storelist');
        Route::get('delete_store/{id}', 'Admin\UserManagementController@DeleteStore');
        Route::get('edit-service-provider/{id}', 'Admin\UserManagementController@EditServiceProvider');
        Route::post('save-service-provider', 'Admin\UserManagementController@SaveServiceProvider');
        Route::post('update-service-provider', 'Admin\UserManagementController@UpdateServiceceProvider');

        Route::get('blockVendor/{id}/{type}', 'Admin\UserManagementController@blockVendor');
        Route::get('venderVerify/{id}/{type}', 'Admin\UserManagementController@venderVerify');
        Route::post('rejectedReason', 'Admin\UserManagementController@RejectedReason');
        Route::get('user-mgmt-filter', 'Admin\UserManagementController@UserMgmtFilter');
        Route::get('seller-mgmt-filter', 'Admin\UserManagementController@SellerMgmtFilter');
        #Start Route service category        
        Route::get('services-category-management', 'Admin\ServiceCategoryController@ServiceCategoryList');
        Route::get('add-service-category', 'Admin\ServiceCategoryController@AddServiceCategory');
        Route::get('add-service-subcategory', 'Admin\ServiceCategoryController@AddServiceSubcategory');
        Route::post('add-subcategory', 'Admin\ServiceCategoryController@AddSubCategory');
        Route::post('add-category', 'Admin\ServiceCategoryController@AddService');
        Route::get('delete-service-category/{id}', 'Admin\ServiceCategoryController@DeleteServiceCategory');
        Route::get('service-category-filter', 'Admin\ServiceCategoryController@ServiceCargoryFilter');
        Route::get('blockcategory/{id}/{type}', 'Admin\ServiceCategoryController@BlockCategory');
        Route::get('edit-service-category/{id}', 'Admin\ServiceCategoryController@EditServiceCategory'); 
        Route::post('update-category', 'Admin\ServiceCategoryController@UpdateCategory');
        #end Route
        #Gig  Route
        Route::get('pending-gig', 'Admin\GigManagementController@PendingGig');
        Route::get('ongoing-gig', 'Admin\GigManagementController@OngoingGig');
        Route::get('completed-gig', 'Admin\GigManagementController@CompleateGig');
        Route::get('cancelled-gig', 'Admin\GigManagementController@CancelledGig');
        #end gig

        #Route Filter
        Route::get('ongoing-gig-filter', 'Admin\GigManagementController@OngoingGigFilter');
        Route::get('compleate-gig-filter', 'Admin\GigManagementController@CompleateGigFilter');
        Route::get('cancel-gig-filter', 'Admin\GigManagementController@CancelGigFilter');
        #endRoute 

        #Trip Management
        Route::get('trip-management', 'Admin\TripManagementController@TripManagement');
        Route::post('add_trip', 'Admin\TripManagementController@AddTrip');
        Route::post('update-trip', 'Admin\TripManagementController@UpdateTrip');
        Route::get('blockTrip/{id}/{status}', 'Admin\TripManagementController@BlockTrip');
        #end route 
    
        #Earning Management
        Route::get('earning-management', 'Admin\EarningManagement@EarningManagemnt');
        Route::post('add-service-tax', 'Admin\EarningManagement@AddServiceTax');
        Route::post('update-service-tax', 'Admin\EarningManagement@UpdateServiceTax');    
        Route::get('earning-mgmt-filter', 'Admin\EarningManagement@EariningFilter');       
        #endearning management

        #start Tax management        
        Route::get('tax-management', 'Admin\TaxManagementController@TaxManagement');
        Route::post('add_custom_tax', 'Admin\TaxManagementController@AddCustomTax');
        Route::post('update-tax', 'Admin\TaxManagementController@UpdateTax');
        #end tax management

        #rating review management
        Route::get('review-ratings-management', 'Admin\ReviewRatingManagement@ReviewRatingsManagement');
        Route::get('rating-filter', 'Admin\ReviewRatingManagement@RatingReview');
       
        #end review rating management
        Route::get('crm-management', 'Admin\HomeController@CrmManagement');
        Route::get('ongoingCity/{ProvinceName}', 'Admin\GigManagementController@OngoingCity');
        Route::get('UserCity/{provinceName}', 'Admin\UserManagementController@usercity');

        #tax mgmt
        Route::get('adminTax', 'Admin\EarningManagement@admintax');
        #end Route

        #Support Mgmt Route
        Route::get('users-support', 'Admin\SupportManagement@UserSupprt');
        Route::get('service-providers-support', 'Admin\SupportManagement@ServiceProvidersSupport');
        #endsupport mgmt

    });

    // Seller Route
	Route::group(array('prefix' => 'seller', 'middleware' => 'seller'), function() {

        Route::get('basic-information', 'Seller\LoginController@BasicInformation');
        Route::post('add-basic-information', 'Seller\LoginController@AddBasicInformation');
        Route::get('delete-multiple-images/{id}', 'Seller\LoginController@DeleteMultipleImage');
        Route::get('logout', 'Seller\LoginController@logout');  
		Route::get('/dashboard', 'Seller\HomeController@index');

        #Loyality Offers 
        Route::get('loyalty-offers', 'Seller\LoyalityOfrManagementController@LoyalityOfrDetails');
        Route::get('add-loyalty-offers', 'Seller\LoyalityOfrManagementController@AddLoyaltyOfr');
        Route::get('view-loyalty-offers/{id}', 'Seller\LoyalityOfrManagementController@ViewLoyalityOfr');
        Route::get('edit-loyalty-offers/{id}', 'Seller\LoyalityOfrManagementController@EditLoyaltyOfr');
        Route::get('past-edit-loyalty-offers/{id}', 'Seller\LoyalityOfrManagementController@pastEditLoyaltyOfr');
        Route::post('add_LoyaltyOfr', 'Seller\LoyalityOfrManagementController@AddLoyaltyOffers');
        Route::get('delete_loyality_offer/{id}', 'Seller\LoyalityOfrManagementController@DeleteLoyalityOffer');
        Route::post('update_Loyalty_Ofr', 'Seller\LoyalityOfrManagementController@UpdateLoyalityOffer');
        Route::post('past_update_Loyalty_Ofr', 'Seller\LoyalityOfrManagementController@pastUpdateLoyalityOffer');
        Route::get('past-loyaly-offer', 'Seller\LoyalityOfrManagementController@PastLoyaltyOffer');
        Route::get('active-offer-filter', 'Seller\LoyalityOfrManagementController@ActiveOfferFilter');        
        Route::get('past-offer-filter', 'Seller\LoyalityOfrManagementController@PastOfferFilter');
        Route::get('block-unblock-offer/{id}/{type}', 'Seller\LoyalityOfrManagementController@BlockUnblockOffer');
        #End Loyality Offers

        #subscription package 
        Route::get('subscription', 'Seller\SubscriptionManagementController@SubscriptionDetail');
        Route::post('add-card', 'Seller\SubscriptionManagementController@AddCard');
        Route::post('purchase-subscription', 'Seller\SubscriptionManagementController@AddPurchaseSubscription');
        Route::post('update-purchase-subscription', 'Seller\SubscriptionManagementController@UpdatePurchaseSubscription');
        Route::get('get-subscription-detail/{id}', 'Seller\SubscriptionManagementController@getSubscriptionDetail');
        Route::get('subscription-update-plan/{id}', 'Seller\SubscriptionManagementController@SubscriptionUpdate');
        #endsubscription route

        #notification Route 
        Route::get('notification', 'Seller\NotificationManagementController@NotificationList');
        Route::get('notification-display', 'Seller\NotificationManagementController@NotificationDisplay');
        
        Route::post('add-announcement', 'Seller\NotificationManagementController@AddNotification');
        #end notification

        #customer mgmt 
        Route::get('/customer-management', 'Seller\UsermanagementController@userlist');
        Route::get('/view-customer-details/{id}', 'Seller\UsermanagementController@ViewCustomerDetails'); 
        Route::get('activated-loyalty-offers/{id}', 'Seller\UsermanagementController@ActivatedLoyaltyOffers');  
        Route::get('/view-activated-loyalty-offers/{OfrID}/{UserID}', 'Seller\UsermanagementController@ViewActivatedLoyaltyOffers');
        Route::get('customer-mgmt-filter', 'Seller\UsermanagementController@CustomerMgmtFilter');
        #end customer mgmt
	});  

