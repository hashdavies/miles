<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('add-image', 'RegisterController@AddImage');

Route::post('add-data', 'RegisterController@AddData');
Route::post('add-data-update', 'RegisterController@addDataUpdate');
Route::post('add-data-delete', 'RegisterController@addDataDelete');

Route::get('splash', 'Api\CommonController@splash');


Route::post('save-token', 'Api\User\UserChatController@SaveToken');

Route::post('', 'API\User\HomeController@_sign');
Route::get('termsCondition', 'Api\User\HomeController@TermsCondition');
Route::get('privacy_policy', 'RegisterController@PrivacyPolicy');
Route::get('about_us', 'RegisterController@AboutUs');
Route::get('faq', 'RegisterController@Faq');
Route::get('dispute', 'RegisterController@Dispute');

// User Api 
Route::post('UserSignUp','RegisterController@UserSignUP');
Route::post('UserVerifyOtp','RegisterController@UserVerifyOtp');
Route::post('UserResendOtp','RegisterController@UserResendOtp');
Route::post('UserForgotPassword','RegisterController@UserForgotPassword');
Route::post('UserForgotResendOtp','RegisterController@UserForgotResendOtp');
Route::post('UserForgotVerifyOtp', 'RegisterController@userForgotOtpMatch');
Route::post('UserResetPassword','RegisterController@UserResetPassword');
Route::post('UserLogin','RegisterController@UserLogin');

Route::get('SubCategoryList','Api\User\HomeController@subcategorylist');

// Seller Api
Route::post('SellerSignUp','RegisterController@SellerSignUp');
Route::post('SellerSecoundStep', 'RegisterController@SellerSecoundStep');
Route::post('SellerLogin','RegisterController@SellerLogin');
Route::post('SellerForgotPassword','RegisterController@SellerForgotPassword');
Route::post('SellerVerifyOtp','RegisterController@SellerVerifyOtp');
Route::post('SellerResendOtp','RegisterController@SellerResendOtp');
Route::post('SellerResetPassword','RegisterController@SellerResetPassword');

Route::post('contactUs', 'RegisterController@ContactUs');


Route::group(['middleware'=>'UserCheck'],function(){

    Route::post('CityList', 'Api\User\HomeController@citylist');
    Route::post('UserSubCategory', 'Api\User\HomeController@usersubcategory');
    Route::post('AddCityCategorySubcategory', 'Api\User\HomeController@addcitycategorysubcategory');

    Route::post('pickupLocation', 'Api\User\HomeController@Pickuplocation');

    Route::post('ConfirmationGig', 'Api\User\HomeController@ConfirmationGig');

    Route::post('ServiceType', 'Api\User\HomeController@servicetype');
    
    Route::post('UserGigOfferdetails', 'Api\User\GigOfferController@usergigofferdetails');
    Route::post('UserGigOfferDecline', 'Api\User\GigOfferController@UserGigOfferDecline');
    Route::post('UserGigAccept', 'Api\User\GigOfferController@usergigaccept');    
    Route::post('UserGigOrder', 'Api\User\GigOfferController@usergigorder');

    Route::post('UserPendingGig', 'Api\User\GigOfferController@userpendinggig');

    Route::post('UserGigOngoing', 'Api\User\GigOfferController@usergigongoing');
    Route::post('UserGigCancel', 'Api\User\GigOfferController@usergigcancel');
    Route::post('UserPastGig', 'Api\User\GigOfferController@userpastgig');
    Route::post('UserViewLocation', 'Api\User\GigOfferController@userviewlocation');
    Route::post('UserOrderPending', 'Api\User\OrderManagementController@userorderpending');
    Route::post('UserOrderOngoing', 'Api\User\OrderManagementController@userorderongoing');
    Route::post('UserOrderPast', 'Api\User\OrderManagementController@userorderpast');
    Route::get('UserTripType', 'Api\User\OrderManagementController@usertriptype');
    Route::get('UserTermsConditions', 'Api\User\OrderManagementController@TermsConditions');

    Route::post('UserUpdateProfile', 'Api\User\OrderManagementController@userupdateprofile');
    Route::post('UserGiveRatingReview', 'Api\User\RatingReviewController@userratingreview');    
    Route::post('UserGigAddress', 'Api\User\AddressManagementController@UserGigAddress');
    Route::post('DeleteUserGigAddress', 'Api\User\AddressManagementController@DeleteUserGigAddress');

    
    Route::post('UserUpdateAddress', 'Api\User\AddressManagementController@userupdateaddress');
    Route::post('UserAddressList', 'Api\User\AddressManagementController@useraddresslist');
    Route::post('UserAddMoreAddress', 'Api\User\AddressManagementController@addmoreaddress');
    Route::post('DeleteUserAddress', 'Api\User\AddressManagementController@deleteuseraddress');

    Route::post('UserAdminChat', 'Api\User\UserChatController@useradminchat');
    Route::post('UserGetPromoCode', 'Api\User\PromoCodeController@UserGetPromoCode');
    Route::post('addVirtualCard', 'Api\User\PaymentController@AddVirtualCard');
    Route::post('viewCard', 'Api\User\PaymentController@ViewCard');
        
    Route::post('userConfirmGig', 'Api\User\PaymentController@userGigconfirmation');

    Route::post('gigOrderPayment', 'Api\User\PaymentController@GigOrderPayment');

    Route::post('ApplyCoupon', 'Api\User\PromoCodeController@UserGetPromoCode');
    
    Route::post('sendNotification', 'Api\User\PromoCodeController@sendNoti');
    Route::get('NotificationList', 'Api\User\PromoCodeController@NotificationList');

    Route::post('userOngoingGigPdf', 'Api\User\PdfController@userOngoingGigPdf');

    Route::post('userUnReadNotification', 'Api\User\PromoCodeController@UserUnReadNotification'); 
    Route::post('userReadNotification', 'Api\User\PromoCodeController@UserReadNotification');   
});

// Seller Authentication
Route::group(['middleware'=>'SellerCheck'],function(){

    Route::post('SellerPendingGig', 'Api\Seller\GigManagementController@sellerpendinggig');

    Route::post('SellerGigOrders', 'Api\Seller\GigManagementController@sellergigorder');    
    Route::post('SellerGigOffer', 'Api\Seller\GigManagementController@sellergigoffer');
    
    Route::post('SellerGigIgnore', 'Api\Seller\GigManagementController@sellergigignore');
    Route::post('SellerMakeOffer', 'Api\Seller\GigManagementController@sellermakeoffer');    
    Route::post('SellerStartGig', 'Api\Seller\GigManagementController@sellerstartgig');
    Route::post('SellerOngoingGig', 'Api\Seller\GigManagementController@sellerongoinggig'); 
    Route::post('SellerCompleateGig', 'Api\Seller\GigManagementController@sellercompleatedgig');
    Route::post('SellerPastGig', 'Api\Seller\GigManagementController@sellerpastgig');
    Route::post('SellerViewLocation', 'Api\Seller\GigManagementController@sellerviewlocation');

    Route::post('SellerUpdatePickupAddress', 'Api\Seller\UpdateGigAddressManagement@sellerupdatepickupaddress');

    Route::post('SellerUpdateTranslt', 'Api\Seller\UpdateGigAddressManagement@sellerupdatetranslt');
    Route::post('SellerUpdateCompleted', 'Api\Seller\UpdateGigAddressManagement@sellerupdatecompleted');
    Route::post('SellerUpdateProfile', 'Api\Seller\ProfileManagementController@sellerupdateprofile');
    Route::post('SellerGiveRatingReview', 'Api\Seller\AddressManagement@sellergiveratingreviewed');    
    Route::post('SellerAddMoreAddress', 'Api\Seller\AddressManagement@selleraddaddress');
    Route::post('SellerUpdateAddress', 'Api\Seller\AddressManagement@sellerupdateaddress');
    Route::post('SellerAddressList', 'Api\Seller\AddressManagement@selleraddresslist');
    Route::post('DeleteSellerAddress', 'Api\Seller\AddressManagement@deleteselleraddress');

    Route::post('SellerAdminChat', 'Api\Seller\SellerChatController@selleradminchat');

    Route::post('sellerEarning', 'Api\Seller\EarningMgmtController@SellerEarning');
    Route::post('sellerEarningHistory', 'Api\Seller\EarningMgmtController@SellerEarningHistory');

    Route::get('SellerNotificationList', 'Api\Seller\NotificationController@SellerNotificationList');
    
    Route::post('sellerOnDuty', 'Api\Seller\EarningMgmtController@SellerOnDuty');
    Route::post('sellerOffDuty', 'Api\Seller\EarningMgmtController@sellerOffDuty');

    Route::post('sellerUnReadNotification', 'Api\Seller\NotificationController@SellerUnReadNotification');

    Route::post('sellerReadNotification', 'Api\Seller\NotificationController@SellerReadNotification');
});