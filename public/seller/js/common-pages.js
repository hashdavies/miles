// "use strict";


$(document).on("change","#chooseImage",function(e2) {
  var img1 = e2.target.files[0];
  if(!iEdit.open(img1, true, function(res1){
    $("#imageFile").attr("src", res1);  
  })){
    alert("Whoops! That is not an image!");
  }
});
$(document).ready(function(){
  $('.theme-loader').animate({
      'opacity':'0',
  },1200);
  setTimeout(function(){
      $('.theme-loader').remove();
  },2000);

  // Header JS
  $('#mobile-collapse').click(function(){
    $('.pcoded.iscollapsed').toggleClass('expanded');
    $('.pcoded.iscollapsed').toggleClass('offcanvas');
  });
  $(".mobile-options").on("click",function(){
    $(".navbar-container .nav-right").slideToggle("slow")
  });
  $('.pcoded-submenu li a').on('click', function(){
    $('html, body').animate({
      scrollTop: 0
    }, 500);
  });
  if($(window).width() <= 767){
    $('.pcoded-submenu li a').on('click', function(){
      $('.pcoded.iscollapsed').toggleClass('expanded');
      $('.pcoded.iscollapsed').toggleClass('offcanvas');
    });
  }

  $('.lng-dropdown li a').click(function(){
    var aaa = this.innerHTML;
    $('.lng-dropdown #dropdown-active-item').html(aaa);
  }); 

  $("#more-details").on("click",function(){
    $(".more-details").slideToggle(500);
  });

  $(".blank").on("click",function(){
    $(this).closest('.input-group').find('input').val('');
  });

  $(".pcoded-hasmenu > a").on("click",function(e){
    if($(this).parent('.pcoded-hasmenu').hasClass('pcoded-trigger')){
      $(".pcoded-hasmenu").removeClass('pcoded-trigger active');
      $('.pcoded-hasmenu ul.pcoded-submenu').hide();
    }else{
      $(".pcoded-hasmenu").removeClass('pcoded-trigger');
      $('.pcoded-hasmenu ul.pcoded-submenu').hide();
      $(this).parent('.pcoded-hasmenu').addClass('pcoded-trigger active');
      $(this).parent('.pcoded-hasmenu').children('ul.pcoded-submenu').slideDown();
    }
  });

  $('.dataTable').dataTable({
    'searching': true,
    "info": true,
    "listing": true,
    "lengthChange": true,
    "paging": true,
    "columnDefs": [
        { "orderable": true, "targets": 'no-sort' }
    ]
  })

  //Show Password
  $('.form-group .showHidePass').on('click', function(){
    $(this).toggleClass('active');
    if($(this).hasClass('active')){
      $(this).prev('.password').prop('type', 'text');
    }else{
      $(this).prev('.password').prop('type', 'password');
    }
  });

// subscription expairy date
   
  $("#expairy_date").datepicker({
    orientation: 'bottom',
    format: "mm/yyyy",
    startDate: new Date(),
    autoclose: true
  }).on('changeDate', function(selected) {
    var startDate = new Date(selected.date.valueOf());
    $('#end_date').datepicker('setStartDate', startDate);
  });
  // Date Picker
  $(".date").datepicker({
    orientation: 'bottom',
    autoclose: true
  });

  $("#start_date").datepicker({
    orientation: 'bottom',
    format: "dd/mm/yyyy",
    startDate: new Date(),
    autoclose: true
  }).on('changeDate', function(selected) {
    var startDate = new Date(selected.date.valueOf());
    $('#end_date').datepicker('setStartDate', startDate);
  });
  
  $("#end_date").datepicker({
    orientation: 'bottom',
    format: "dd/mm/yyyy",
    autoclose: true
  }).on('changeDate', function(selected) {
    var startDate = new Date(selected.date.valueOf());
    $('#start_date').datepicker('setEndDate', startDate);
  });

  // Customer Mgmt
 
  $("#Cus_start_date").datepicker({
    orientation: 'bottom',
    format: "dd/mm/yyyy",
    endDate: new Date(),
    autoclose: true
  }).on('changeDate', function(selected) {
    var startDate = new Date(selected.date.valueOf());
    $('#Cus_end_date').datepicker('setStartDate', startDate);
  });
  
  $("#Cus_end_date").datepicker({
    orientation: 'bottom',
    format: "dd/mm/yyyy",
    autoclose: true
  }).on('changeDate', function(selected) {
    var startDate = new Date(selected.date.valueOf());
    $('#Cus_start_date').datepicker('setEndDate', startDate);
  });
  
  


  // 
  $('.numberOnly, .qtyInput').keypress(function(e){
    var key = e.which || e.keyCode || e.charCode;
    if (key >= 48 && key <= 57){
      return true;
    }else{
      return false;
    }
  });

  $(document).on('click', '.remove', function(){
    $(this).closest('.form-control.file').find('input[type=file]').val('');
    $(this).closest('span').html('');
  });
  $(document).on('click', '.del', function(){
    $(this).closest('.avatar').remove('');
  });

  // quantity
  // $(".qtyBtn").on('click', function(){     
  //   if ($(this).hasClass('fa-angle-up')){
  //     $(this).closest('.qty').find('.qtyInput').val(Number($(this).closest('.qty').find('.qtyInput').val())+1);
  //     if ($(this).closest('.qty').find('.qtyInput').val()== 5) {
  //       $(this).prop('disabled', true);
  //     }else{
  //       $(this).prop('disabled', false);        
  //     }
  //   }else{
  //     $(this).closest('.qty').find('.qtyInput').val(Number($(this).closest('.qty').find('.qtyInput').val())-1);
  //     if ($(this).closest('.qty').find('.qtyInput').val()== 1) {
  //       $(this).prop('disabled', true);
  //     }else{
  //       $(this).prop('disabled', false);        
  //     }
  //   }
  // });

  // Hours
  $(".qtyBtn.hr.fa-angle-up").on('click', function(){     
    if ($(this).closest('.qty').find('.qtyInput').val()< 24) {
      $(this).prop('disabled', false);        
      $(this).closest('.qty').find(".qtyBtn.hr.fa-angle-down").prop('disabled', false);        
      $(this).closest('.qty').find('.qtyInput').val(Number($(this).closest('.qty').find('.qtyInput').val())+1);
    }else{
      $(this).prop('disabled', true);
    }    
  });
  $(".qtyBtn.hr.fa-angle-down").on('click', function(){     
    if ($(this).closest('.qty').find('.qtyInput').val()> 0) {
      $(this).prop('disabled', false);        
      $(this).closest('.qty').find(".qtyBtn.hr.fa-angle-up").prop('disabled', false);        
      $(this).closest('.qty').find('.qtyInput').val(Number($(this).closest('.qty').find('.qtyInput').val())-1);
    }else{
      $(this).prop('disabled', true);
    }
  });

  // Minutes
  $(".qtyBtn.min.fa-angle-up").on('click', function(){     
    if ($(this).closest('.qty').find('.qtyInput').val()< 60) {
      $(this).prop('disabled', false);        
      $(this).closest('.qty').find(".qtyBtn.min.fa-angle-down").prop('disabled', false);        
      $(this).closest('.qty').find('.qtyInput').val(Number($(this).closest('.qty').find('.qtyInput').val())+1);
    }else{
      $(this).prop('disabled', true);
    }    
  });
  $(".qtyBtn.min.fa-angle-down").on('click', function(){     
    if ($(this).closest('.qty').find('.qtyInput').val()> 0) {
      $(this).prop('disabled', false);        
      $(this).closest('.qty').find(".qtyBtn.min.fa-angle-up").prop('disabled', false);        
      $(this).closest('.qty').find('.qtyInput').val(Number($(this).closest('.qty').find('.qtyInput').val())-1);
    }else{
      $(this).prop('disabled', true);
    }
  });


});
function fileInput(event){
  var filename = event.target.files[0].name
  console.log(filename);
  $(event.target).closest('.form-control.file').find('.text-dark').html(filename +'<i class="ti-close fs-10 ml-2 remove"></i>');
  // $(event.target).closest('.form-control.file').append('<span class="text-dark">'+filename+'<i class="ti-close fs-10 ml-1 remove"></i></span>')
}

single = new Array();
function singleFiles(event) {
  this.single = [];
  var singleFiles = event.target.files;
  if (singleFiles) {
    for (var file of singleFiles) {
      var singleReader = new FileReader();
      singleReader.onload = (e) => {
        this.single.push(e.target.result);
        $(event.target).closest('.avatar').find('img').attr('src', e.target.result)
      }
      singleReader.readAsDataURL(file);
    }
  }
}


$(function() {
  
  var imagesPreview = function(input, placeToInsertImagePreview) {
    if (input.files) {
      // debugger;
      var filesAmount = input.files.length;
      for (i = 0; i < filesAmount; i++) {
        var reader = new FileReader();
        reader.onload = function(event) {
          //for image
          var html = $('<li><span class="close">&times;</span><span>'+input.files[0].name+'</span><img src='+ event.target.result +' style="width:100%;height:135px;"></li>');
          //for video
          // var html = $('<li><span class="close">&times;</span><video controls=""><source src='+ event.target.result +' type="video/mp4"></video></li>');

          $(html).appendTo(placeToInsertImagePreview);
          $('.close').click(function(){
            $(this).closest('li').remove();
          });
        }
        reader.readAsDataURL(input.files[i]);
      }
    }
  };

  $('#multiImageSelet').on('change', function() {
    imagesPreview(this, 'ul.pictures');
  });
  
  var imagesPreview1 = function(input, placeToInsertImagePreview) {
    if (input.files) {
      // debugger;
      var filesAmount = input.files.length;
      for (i = 0; i < filesAmount; i++) {
        var reader = new FileReader();
        reader.onload = function(event) {
          //for image
          var html = $('<li><span class="close1">&times;</span><span>'+input.files[0].name+'</span><img src='+ event.target.result +' style="width:100%;height:135px;"></li>');
          //for video
          // var html = $('<li><span class="close">&times;</span><video controls=""><source src='+ event.target.result +' type="video/mp4"></video></li>');

          $(html).appendTo(placeToInsertImagePreview);
          $('.close1').click(function(){
            $(this).closest('li').remove();
          });
        }
        reader.readAsDataURL(input.files[i]);
      }
    }
  };

  $('#multiImageSelet1').on('change', function() {
    imagesPreview1(this, 'ul.pictures1');
  });
});

