$('document').ready(function() {

  $('.dataTable').dataTable({
    columnDefs: [
      { targets: 'no-sort', orderable: false }
    ]
  });

  $('.checkbox-select').multiselect({
    // positionMenuWithin: $('.position-menu-within'),
    placeholder: 'Filter',
    selectAll: true
  });

  $(document).on('click', '.remove', function(){
    $(this).closest('.appendBox').remove();
  })

  $('.table #selectAll').on('click', function(){
    if ($(this).is(':checked')){
      $(this).closest('.table').find('td:first-child input[type=checkbox]').prop('checked', true)
    }else{
      $(this).closest('.table').find('td:first-child input[type=checkbox]').prop('checked', false)
    }
  });

  $(".datepicker").datepicker({
    orientation: 'bottom',
    autoclose: true,
  });

  $("#logo-file").change(function(e){
    var img = e.target.files[0];
    if(!iEdit.open(img, true, function(res){
     $("#result").attr("src", res);      
   })){
      alert("Whoops! That is not an image!");
    }
  });
  $("#logo-file1").change(function(e){
    var img1 = e.target.files[0];
    if(!iEdit.open(img1, true, function(res1){
     $("#result1").attr("src", res1);      
   })){
      alert("Whoops! That is not an image!");
    }
  });
  $(document).on("click",".add-facility .plus-btn",function() {
    $('.add-facility .facility-box').append('\
      <div class="col-md-6">\
      <input id="" class="form-control" type="date">\
      <span class="del">x</span>\
      </div>');
    $(".add-facility .del").on('click',function(){
      $(this).parent('.col-md-6').remove();
    });
  });

  $('.qtyInput, .digit').keypress(function (e) {
    if (String.fromCharCode(e.keyCode).match(/[^0-9]/g)) return false;
  });

  $("#start_date").datepicker({
    orientation: 'bottom',
    autoclose: true
  }).on('changeDate', function(selected) {
    var startDate = new Date(selected.date.valueOf());
    $('#end_date').datepicker('setStartDate', startDate);
  });
  
  $("#end_date").datepicker({
    orientation: 'bottom',
    autoclose: true
  }).on('changeDate', function(selected) {
    var startDate = new Date(selected.date.valueOf());
    $('#start_date').datepicker('setEndDate', startDate);
  });

  $('.upld1-img').on('change','input', function(e){

    readURL(this);

  });

  function readURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      // console.log(reader);
      reader.onload = function (e) {  
        $(input).parents('.upld1-img').find('.upld-item').attr('src', e.target.result);
      }
      reader.readAsDataURL(input.files[0]);
    }
  }
});