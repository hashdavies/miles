$(document).ready(function() { 
  var input = document.querySelector("#phone"),
  errorMsg = document.querySelector("#error-msg"),
  validMsg = document.querySelector("#valid-msg");
  // here, the index maps to the error code returned from getValidationError - see readme
  var errorMap = ["Invalid number", "Invalid country code", "Too short", "Too long", "Invalid number"];

  // initialise plugin
  var iti = window.intlTelInput(input, {
    initialCountry: 'ch',
    separateDialCode: true,
    autoPlaceholder : false,
    utilsScript: "utils.js?1562189064761",
  });

  var reset = function() {
    input.classList.remove("error");
    errorMsg.innerHTML = "";
    errorMsg.classList.add("hide");
    validMsg.classList.add("hide");
  };

  // on blur: validate
  input.addEventListener('blur', function() {
    var dialCode1 = iti.getSelectedCountryData().dialCode;
    reset();
    if (input.value.trim()) {
      if (iti.isValidNumber()) {
        validMsg.classList.remove("hide");
        $('#country_code').val("+"+dialCode1);
      } else {
        input.classList.add("error");
        var errorCode = iti.getValidationError();
        errorMsg.innerHTML = errorMap[errorCode];
        errorMsg.classList.remove("hide");
      }
    }
  });

  // on keyup / change flag: reset
  input.addEventListener('change', reset);
  input.addEventListener('keyup', reset);

  $('#phone').keypress(function(e){
    var key = e.which || e.keyCode || e.charCode;
    if (key >= 48 && key <= 57){
      return true;
    }else{
      return false;
    }
  });
  var aaaa= $('.iti__selected-dial-code').html();
  $('#country_code').val(aaaa);
  $(document).on('click', '.iti__country', function(){
    var aaa= $(this).find('.iti__dial-code').html();
    $('#country_code').val(aaa);
  });
});