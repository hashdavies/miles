<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Pania</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui" />
  <link rel="icon" href="{{url('public/seller/images/favicon.png')}}" type="image/x-icon" />

  <link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600,700,800&display=swap" rel="stylesheet">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css" rel="stylesheet"/>
  <link rel="stylesheet" type="text/css" href="{{url('public/seller/css/bootstrap.min.css')}}" />
  <link rel="stylesheet" type="text/css" href="{{url('public/seller/css/font-awesome.min.css')}}" />  
  <link rel="stylesheet" type="text/css" href="{{url('public/seller/css/iEdit.css')}}" />
  <link rel="stylesheet" type="text/css" href="{{url('public/seller/css/jquery.dataTables.min.css')}}" />
  <link rel="stylesheet" type="text/css" href="{{url('public/seller/icon/themify-icons/themify-icons.css')}}" />
  <link rel="stylesheet" type="text/css" href="{{url('public/seller/icon/icofont/css/icofont.css')}}" />
  <link rel="stylesheet" type="text/css" href="{{url('public/seller/css/intlTelInput.css')}}" />
  <link rel="stylesheet" type="text/css" href="{{url('public/seller/css/owl.carousel.css')}}" />
  
  <link rel="stylesheet" type="text/css" href="{{url('public/seller/css/component.css')}}" />
  <link rel="stylesheet" type="text/css" href="{{url('public/seller/css/style.css')}}" />
  <link rel="stylesheet" type="text/css" href="{{url('public/seller/css/globle.css')}}" />
</head>
<body>
<div class="theme-loader">
  <div class="ball-scale">
    <div class='contain'>
      <div class="ring"><div class="frame"></div></div>
      <div class="ring"><div class="frame"></div></div>
      <div class="ring"><div class="frame"></div></div>
      <div class="ring"><div class="frame"></div></div>
      <div class="ring"><div class="frame"></div></div>
      <div class="ring"><div class="frame"></div></div>
      <div class="ring"><div class="frame"></div></div>
      <div class="ring"><div class="frame"></div></div>
      <div class="ring"><div class="frame"></div></div>
      <div class="ring"><div class="frame"></div></div>
    </div>
  </div>
</div>
<div id="pcoded" class="pcoded iscollapsed expanded">
  <div class="pcoded-overlay-box"></div>
    <div class="pcoded-container navbar-wrapper">

      @yield('content')

    </div>
</div>


<script type="text/javascript" src="{{url('public/seller/js/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{url('public/seller/js/jquery-ui.min.js')}}"></script>
<script type="text/javascript" src="{{url('public/seller/js/popper.min.js')}}"></script>
<script type="text/javascript" src="{{url('public/seller/js/bootstrap.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="{{url('public/seller/js/owl.carousel.min.js')}}"></script> 
<script type="text/javascript" src="{{url('public/seller/js/iEdit.js')}}"></script> 
<script type="text/javascript" src="{{url('public/seller/js/jquery.dataTables.min.js')}}"></script> 
<script type="text/javascript" src="{{url('public/seller/js/Chart.min.js')}}"></script> 
<script type="text/javascript" src="https://www.chartjs.org/samples/latest/utils.js"></script> 
<script type="text/javascript" src="{{url('public/seller/js/common-pages.js')}}"></script> 
</body>
</html>

<script type="text/javascript">
    function setInputFilter(textbox, inputFilter) {
    ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
      textbox.addEventListener(event, function() {
        if(this.value)
        {
          var value = $(this).val();
           value = value.replace(/^(0*)/,"");
          $(this).val(value);        
        }      
        if (inputFilter(this.value)) {
          this.oldValue = this.value;
          this.oldSelectionStart = this.selectionStart;
          this.oldSelectionEnd = this.selectionEnd;
        } else if (this.hasOwnProperty("oldValue")) {
          this.value = this.oldValue;
          this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
        } else {
          this.value = "";
        }
      });
    });
  } 
</script>

@yield('footer_script')