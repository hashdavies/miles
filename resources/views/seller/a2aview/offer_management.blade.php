@extends('seller.header')

@section('title', 'Offer Management')

@section('content')


  <div id="pcoded" class="pcoded">
    <div class="pcoded-overlay-box"></div>
    <div class="pcoded-container navbar-wrapper">
        
        @include('seller/navbar') 
        
        <div id="categoryPage"></div>
        <div class="pcoded-main-container">
            <div class="pcoded-wrapper">

              @include('seller/sidebar')

              <div class="pcoded-content">
                  <div class="pcoded-inner-content">
                      <div class="main-body">
                          <div class="page-wrapper">
                            <div class="page-header card">
                              <div class="row align-items-end">
                                <div class="col-sm-6 col-lg-6">
                                  <div class="page-header-title d-flex">
                                      <i class="ti-layout bg-c-green"></i>
                                      <div class="d-inline">
                                          <h4>Offers Management</h4>
                                      </div>
                                  </div>
                                </div>
                                <div class="col-sm-6 col-lg-6">
                                  <div class="page-header-breadcrumb">
                                    <div class="page-header-breadcrumb">
                                      <ul class="breadcrumb-title">
                                        <li class="breadcrumb-item">
                                          <a href="{{ url('admin/dashboard') }}">
                                          <i class="icofont icofont-home"></i>
                                          </a>
                                        </li>
                                        <li class="breadcrumb-item"><a href="#">Offers Management</a></li>
                                      </ul>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="page-body">

                            @if($errors->any())
                              <div class="alert alert-danger">
                              <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                                </ul>
                                </div>
                                @endif

                                @if(session()->has('success'))
                                <div class="alert alert-success">
                                  {{ session()->get('success') }}
                                </div>
                              @endif

                              <div class="card">
                                <div class="card-block">
                                   <div class="dt-plugin-buttons"></div>
                                     <div class="add-more-button m-b-15">
                                       <button type="button" data-target="#add-offer" data-toggle="modal" class="btn btn-sm btn-primary waves-effect"><i class="typcn typcn-plus-outline"></i>Add offer</button>
                                     </div>
                                      <div class="dt-responsive table-responsive mt-4">
                                        <table id="account-list" class="table table-striped table-bordered nowrap">
                                          <thead>
                                              <tr>
                                                  <th>Sr.No.</th>
                                                  <th>Offers Image</th>
                                                  <th>Offer Name</th>
                                                  <th>Discount</th>
                                                  <th>Description </th>      
                                                  <th>Status</th>
                                                  <th>Offer Verified</th>
                                                  <th>Action</th>
                                              </tr>
                                          </thead>
                                          <tbody>
                                            @foreach($getOffers as $key=>$row)
                                            <tr>
                                              <td>{{ $key+1}}</td>
                                              <td><img src="{{ url('/public/offersImages/' .$row->offer_Image)}}" width="30px;" height="30px;" class="img-radius mr-2" alt="User-Profile-Image"></td>
                                              <td>{{ $row->offerName }}</td>
                                              <td>{{ $row->discount}} %</td>
                                              <td>
                                                 <button class="btn btn-sm btn-primary" data-target="#viewdesc-modal" data-toggle="modal" title="View Descrption" onclick="viewDesc({{$row['id']}},1)"> View here</button>

                                              </td>

                                              <td class="text-success">  
                                                @if($row->status == 1)
                                                <span style="color: green">Active</span>
                                                @elseif($row->status == 0)
                                                <span style="color: red">Inactive</span>
                                                @endif
                                              </td>

                                              <td>
                                                @if($row->is_varified == 1)
                                                  <span style="color: green"> Verified</span>
                                                @elseif($row->is_varified == 0)
                                                  <span style="color: red"> Pending</span>
                                                @elseif($row->is_varified = 2)
                                                  <span style="color: red">Rejected</span>
                                                @endif
                                              </td>

                                              <td>        
                                               
                                                 <button onclick="fetch_offer_data(this.value)" value="{{$row['id']}}" class="btn btn-primary btn-sm btn-labeled" type="button" >edit</button>

                                                @if($row->status == 0)                                              
                                                  <button class="btn btn-sm btn-warning" data-target="#block-modal" data-toggle="modal" title="Block" onclick="blockUnblock({{$row['id']}},1)"> Activate</button>
                                                                                           
                                                @elseif($row->status == 1)

                                                  <button class="btn btn-sm btn-success" data-target="#block-modal" data-toggle="modal" title="Unblock" onclick="blockUnblock({{$row['id']}},0)"> Deactivate</button>

                                                @endif 
                                                

                                                <!-- <button data-toggle="modal" data-target="#unblock-modal" class="btn btn-sm btn-success mr-1 ml-1">Unblock</button>-->
                                                
                                              <button data-toggle="modal" data-target="#delete-modal" class="btn btn-sm btn-danger" onclick="deleteOffer({{$row->id}})">Delete</button> 
                                                
                                              </td>

                                              @endforeach
                                            </tr>
                                          </tbody>
                                        </table>
                                      </div>
                                </div>
                              </div>
                            </div>
                          </div>
                      </div>
                  </div>
              </div> 
            </div>
        </div>
    </div>    
  </div>          



<!-- block modal -->

  <div id="block-modal" class="modal fade" role="dialog">
      <div class="modal-dialog modal-sm">     
        <div class="modal-body text-center">
          <div class="row">
            <div class="col-md-12">
              <i class="text-warning fs-34 d-inline-block ti-link mb-4" style="font-size: 45px;"></i>
              <h6 id="tilteblock"></h6>
            </div>
          </div>
          <hr />
          <div class="row m-t-15">
            <div class="col-sm-12">
              <a class="btn btn-success btn-sm countinueblock" style="padding: 8px 18px 8px 18px;">Yes</a>
                  <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal" style="padding: 8px 18px 8px 18px;">No</button>
            </div>
          </div>
        </div>
      </div>
  </div>

  <div id="viewdesc-modal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm">      
      <div class="modal-body text-center">
        <div class="row">
          <div class="col-md-12">
            <h5>Description</h5><br>             
            <h6 id="description"></h6>
          </div>
        </div>
      </div>
    </div>
  </div>


<!-- unblock modal -->
<!-- <div id="unblock-modal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-sm">
    <div class="text-center">
      <img src="assets/images/auth/logo.png" alt="logo.png" />
    </div>
    <div class="modal-body text-center">
      <div class="row">
        <div class="col-md-12">
          <i class="text-success fs-34 d-inline-block ti-unlink mb-4"></i>
          <h6>Are you sure you want to unblock? </h6>
        </div>
      </div>
      <hr />
      <div class="row m-t-15">
        <div class="col-sm-12">
            <button type="button" class="btn btn-sm btn-success mr-1" data-target="#unblock-sccess-modal" data-toggle="modal" data-dismiss="modal">Unblock</button>
            <button type="button" class="btn btn-sm btn-danger ml-1" data-dismiss="modal">Cancel</button>
        </div>
      </div>
    </div>
  </div>
</div> -->
<!-- delete block -->


<div id="delete-modal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-sm">
    <div class="modal-body text-center">
      <div class="row">
        <div class="col-md-12">
          <i class="text-danger fs-34 d-inline-block ti-trash mb-4"></i>
          <h6>Are you sure you want to delete this Offer</h6>
        </div>
      </div>
      <hr />
      <div class="row m-t-15">
        <div class="col-sm-12">          
          <a class="btn btn-sm btn-success mr-1 countinuedelete" style="padding: 8px 18px 8px 18px; color: #fff">Delete</a>
           
          <button type="button" class="btn btn-sm btn-danger ml-1" data-dismiss="modal">Cancel</button>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- success modal -->
<div id="block-sccess-modal" class="modal fade">
  <div class="modal-dialog modal-sm" data-dismiss="modal">
    <div class="modal-body text-center">
      <div class="row">
        <div class="col-md-12">
          <i class="text-warning fs-34 d-inline-block icofont icofont-check-circled mb-4"></i>
          <h6>Block Successfully</h6>
        </div>
      </div>
    </div>
  </div>
</div>


<!-- success modal -->
<div id="unblock-sccess-modal" class="modal fade">
  <div class="modal-dialog modal-sm" data-dismiss="modal">
    <div class="modal-body text-center">
      <div class="row">
        <div class="col-md-12">
          <i class="text-success fs-34 d-inline-block icofont icofont-check-circled mb-4"></i>
          <h6>{{ session()->get('success_message') }}</h6>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- success modal -->


<div id="delete-sccess-modal" class="modal fade">
  <div class="modal-dialog modal-sm" data-dismiss="modal">
    <div class="modal-body text-center">
      <div class="row">
        <div class="col-md-12">
          <i class="text-danger fs-34 d-inline-block icofont icofont-check-circled mb-4"></i>
          <h6>{{ session()->get('error_message') }}</h6>
        </div>
      </div>
    </div>
  </div>
</div>



<div id="add-offer" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="titleText"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button> 
        </div>

        <div class="modal-body">
            <div class="model-content">               
               <form action="{{ url('admin/add_offers') }}" method="post" enctype="multipart/form-data">
                  @csrf

                  <div class="row">

                     <div class="col-md-6">
                        <div class="form-group">
                            <label>Offers Name</label>
                            <input type="text" class="form-control" name="offerName" id="edit_name" required="">
                            <span style="color: red">{{ $errors->first('offerName') }}</span>
                        </div>                         
                    </div>  

                    

                    <div class="col-md-6">
                      <div class="form-group">
                           <div class="img-box w-auto">
                              <label>Offers Images</label>
                              <label class="fileInput1">
                                  <input type="file" name="offer_Image" class="form-control" onchange="singleFiles(event)" required="">
                              </label>
                               <div class="img">
                                   <img class="imagefile" src="{{ url('public/admin/images/cover-image.png')}}" id="product_image" width="90px" height="90px">
                              </div>
                          </div>
                       </div>                     
                    </div> 

                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Vendor Name</label>
                            <select class="form-control" name="vendor_Id" id="Vendor">
                              @foreach($getVendor as $vendorRow)
                                <option value="{{$vendorRow->id}}">{{$vendorRow->first_name}} {{$vendorRow->last_name}}</option>
                              @endforeach                           
                            </select>
                            <span style="color: red">{{ $errors->first('status') }}</span>                          
                        </div> 
                    </div>                   

                    
                    <div class="col-md-12">                        
                        <div class="form-group"> 
                          <label class="fw-600">Product Name<span class="text-danger">*</span></label>
                          <input type="search" class="form-control" placeholder="Search.." id="myInput" onkeyup="search_check()">
                          <div class="city-wrap clearfix"></div>
                        </div>
                        <span style="color: red">{{ $errors->first('status') }}</span> 
                    </div>

                
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Discount (%)</label>
                            <input type="text" class="form-control" name="discount">
                             <span style="color: red">{{ $errors->first('discount') }}</span>
                        </div> 
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Valid To Date</label>                             
                            <div class="input-group date" id="datetimepicker">
                                <input type="text" class="form-control" id="start_date" name="valid_To" value="{{old('valid_To')}}">
                                <span class="input-group-addon ">
                                <span class="icofont icofont-ui-calendar"></span>
                                </span>             
                            </div>                            
                            <span style="color: red">{{ $errors->first('valid_To') }}</span>
                        </div>                         
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Select Time</label>                             
                            
                              <div class='input-group date' id=''>
                                <input type='time' class="form-control" name="valid_to_time"/>
                                <span class="input-group-addon">
                                  <span class="icofont icofont-time"></span>
                                </span>
                              </div>           
                                                  
                            <span style="color: red">{{ $errors->first('valid_To') }}</span>
                        </div>                         
                    </div>                      

                    <div class="col-md-6">
                        <div class="form-group">
                           <label>Valid From Date</label>                           
                           <div class="input-group date" id="datetimepicker10">
                              <input type="text" class="form-control" id="end_date" name="valid_From" value="{{old('product_to')}}">
                              <span class="input-group-addon ">
                              <span class="icofont icofont-ui-calendar"></span>
                              </span>              
                           </div>
                           <span style="color: red">{{ $errors->first('valid_From') }}</span>
                        </div> 
                    </div>  

                    <div class="col-md-6">
                        <div class="form-group">
                           <label>Select Time</label>                           
                           <div class='input-group date' id=''>
                                <input type='time' class="form-control" name="valid_from_time"/>
                                <span class="input-group-addon">
                                  <span class="icofont icofont-time"></span>
                                </span>
                              </div> 
                           <span style="color: red">{{ $errors->first('valid_From') }}</span>
                        </div> 
                    </div>  


                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Description</label>
                            <textarea class="form-control" rows="3" name="offerDescription" id="desc"></textarea>
                             <span style="color: red">{{ $errors->first('offerDescription') }}</span>
                        </div> 
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <button type="button" class="btn btn-sm btn-danger ml-1" data-dismiss="modal">Cancel</button>

                            <button type="submit" class="btn btn-sm btn-success mr-1">Save</button>

                         </div> 
                     </div>                                    
                  </div> 

               </form>               
            </div>  
         </div>
      </div>  
   </div>
</div>  


<!--Edit Category--->

<div id="edit-offer" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="titleText"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button> 
        </div>

        <div class="modal-body">
            <div class="model-content">               
               <form action="{{ url('admin/update_offers') }}" method="post" enctype="multipart/form-data">
                  @csrf
                  <div class="row">
                    <input type="hidden" name="id" id="offer_id">
                     <div class="col-md-6">
                        <div class="form-group">
                            <label>Offers Name</label>
                            <input type="text" class="form-control" name="offerName" id="offerName" required="">
                            <span style="color: red">{{ $errors->first('offerName') }}</span>
                        </div>                         
                    </div>  

                     


                    <div class="col-md-6">
                       <div class="form-group">
                           <div class="img-box w-auto">
                              <label>Offers Images</label>
                              <label class="fileInput1">
                                  <input type="file" name="offer_Image" class="form-control" onchange="singleFiles(event)">
                              </label>
                               <div class="img">
                                   <img class="imagefile" src="" id="edit_product_image" width="90px" height="90px" >
                              </div>
                               <span style="color: red">{{ $errors->first('product_image') }}</span>
                          </div>
                       </div>                                       
                    </div> 


                    <div class="col-md-6">                        
                        <div class="form-group"> 
                          <label class="fw-600">Product Name<span class="text-danger">*</span></label>
                          <input type="search" class="form-control" placeholder="Search.." id="myInput" onkeyup="search_check()">
                          <div class="city-wrap clearfix"></div>
                        </div>
                         
                        <span style="color: red">{{ $errors->first('status') }}</span> 
                    </div>
                            

                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Discount (%)</label>
                            <input type="text" class="form-control" name="discount" id="discount">
                             <span style="color: red">{{ $errors->first('discount') }}</span>
                        </div> 
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Valid To</label>                             
                            <div class="input-group date" id="datetimepicker10">
                                <input type="text" class="form-control" id="valid_To" name="valid_To" value="{{old('valid_To')}}">
                                <span class="input-group-addon ">
                                <span class="icofont icofont-ui-calendar"></span>
                                </span>             
                            </div>                            
                            <span style="color: red">{{ $errors->first('valid_To') }}</span>
                        </div>                         
                    </div>                      

                    <div class="col-md-6">
                        <div class="form-group">
                           <label>Valid From</label>                           
                           <div class="input-group date" id="datetimepicker10">
                              <input type="text" class="form-control" id="valid_From" name="valid_From" value="{{old('valid_From')}}">
                              <span class="input-group-addon ">
                              <span class="icofont icofont-ui-calendar"></span>
                              </span>              
                           </div>
                           <span style="color: red">{{ $errors->first('valid_From') }}</span>
                        </div> 
                    </div>  


                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Description</label>
                            <textarea class="form-control" rows="3" name="offerDescription" id="offerDescription"></textarea>
                             <span style="color: red">{{ $errors->first('offerDescription') }}</span>
                        </div> 
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <button type="button" class="btn btn-sm btn-danger ml-1" data-dismiss="modal">Cancel</button>

                            <button type="submit" class="btn btn-sm btn-success mr-1">Save</button>

                         </div> 
                     </div>                                    
                  </div>   

               </form>               
            </div>  
         </div>
      </div>  
   </div>
</div> 

<!-- @if ($message = session()->has('success_message'))      
  <div id="unblock-sccess-modal" class="modal fade">
    <div class="modal-dialog modal-sm" data-dismiss="modal">   
      <div class="modal-body text-center">
        <div class="row">
          <div class="col-md-12">
            <i class="text-success fs-34 d-inline-block icofont icofont-check-circled mb-4"></i>
            <h6> {{ session()->get('success_message') }} </h6>
          </div>
        </div>
      </div>
    </div>
  </div>
@endif -->


@endsection

@section('footerscript')

 <script type="text/javascript">
    
    function verifyoffer(id,type){
       window.location = "{{ url('seller/offerVerify')}}"+"/"+id+"/"+type;
    }

    function rejectedoffer(id,type){
      window.location = "{{url('seller/rejectoffer')}}"+"/"+id+"/"+type;
    }

 </script>

 <script type="text/javascript">
    
    function viewDesc(id){ 
      // alert(id);
        var globalarray = <?php echo json_encode($getOffers); ?>;
        var theIndex = -1;
        for (var i = 0; i < globalarray.length; i++) {
          if (globalarray[i].id == id) {
            theIndex = i;
            break;
          }
        }
        $("#editid").text(globalarray[theIndex].id);
        $("#description").text(globalarray[theIndex].offerDescription );            
    }  
  </script>


<script type="text/javascript">
  
   $('#account-list').DataTable({
    "columnDefs": [
        { "orderable": false, "targets": [3, 4, 5] },
        { "orderable": true, "targets": [0, 1, 2] }
    ]
});
</script>

<script>  
    
    $('#Vendor').on('change',function(){
        var vendorID = $(this).val(); 
        // alert(vendorID);    
        if(vendorID){
          $.ajax({
           type:"GET",
           url:"{{url('seller/vendorProductList')}}"+'/'+vendorID,
           success:function(res){  
              //console.log(res[0]);    
              if(res){
                 $('.city-wrap').html(res); 
                } 
             }
           });
        } 
    });

</script>

<script type="text/javascript"> 

    function getCountryId()
      {
        //alert("Cafsfsdfsd");
       $('#demo-select2-2').empty();
        var district_id = [];
        $.each($(".checkSelect option:selected"), function(){            
            district_id.push($(this).val());
        });
        myurl="{{url('seller/vendorProductList')}}";
         _token="{{csrf_token()}}";
         $.ajax({
            url:myurl,
            type:'POST',
            data:{_token:_token,dd:district_id},
            success:function(resp){
            $('.city-wrap').html(resp);            
            }
         });
    }

</script>

<script>

  function select_city(countryid){

    if($(".countryuncheck"+countryid).prop('checked')){
      $("#myUL"+countryid).find('li .select-check').prop('checked',true);
    }else{
      $("#myUL"+countryid).find('li .select-check').prop('checked',false);
    }

    // if($(".countryuncheck"+countryid).prop('checked')){
    //   $("#Myvariant"+producid).find('li .select-check').prop('checked',true);
    // }else{
    //   $("#Myvariant"+producid).find('li .select-check').prop('checked',false);
    // }      
  }


  // function select_city1(){
    
  //   if($(".countryuncheck1"+producid).prop('checked')){
  //     $("#Myvariant"+producid).find('li .select-check').prop('checked',true);
  //   }else{
  //     $("#Myvariant"+producid).find('li .select-check').prop('checked',false);
  //   }  
  // }


  function search_check() { 
    let input = document.getElementById('myInput').value 
    input=input.toLowerCase(); 
    let x = document.getElementsByClassName('list-check'); 
      
      for (i = 0; i < x.length; i++) {  
          if (!x[i].innerHTML.toLowerCase().includes(input)) { 
              x[i].style.display="none"; 
          } 
          else { 
              x[i].style.display="list-item";                  
          } 
      } 
  } 

</script>


<script>

  $('.select2').select2();
    $('.checkbox-select, .multi-select').multiselect({
    // positionMenuWithin: $('.position-menu-within'),
    placeholder: 'Filter',
    selectAll: true
  });

</script>


<script type="text/javascript">

  $(".alert").delay(3200).fadeOut(400);

      function fetch_offer_data(offer_id){   
      // alert(offer_id);    
         var myurl="{{url('seller/offer-edit')}}";
         $.ajax({
           url:myurl+'/'+offer_id,
           type:"get",
           success:function(getoffers)
           {           
           // console.log(getoffers[0]);

           var img="{{ url('public/offersImages')}}";
           var default_img="{{ url('public/admin/images/cover-image.png')}}";

           $('#offer_id').val(getoffers[0].id);
           $('#offerName').val(getoffers[0].offerName);

           $('#vendorId').val(getoffers[0].vendor_Id);

           $('#product_Id').val(getoffers[0].product_Id);
           $('#cat_Id').val(getoffers[0].cat_Id);
           $('#valid_To').val(getoffers[0].valid_To);
           $('#valid_From').val(getoffers[0].valid_From);
           $('#status').val(getoffers[0].status);

           $('#discount').val(getoffers[0].discount);
           $('#offerDescription').text(getoffers[0].offerDescription  );
           $('#offer_Image').val(getoffers[0].offer_Image);

           if(getoffers[0].offer_Image){
             $('#edit_product_image').attr('src',img+'/'+getoffers[0].offer_Image);
           } else {
             $('#edit_product_image').attr('src',default_img);
           }         
           
           }
         });

       $('#edit-offer').modal('show');     
      }
   

  </script>

 
  <script type="text/javascript">
    function blockUnblock(id, type){
        if(type == 1){
          $("#tilteblock").text("Are you sure you want to Block this Offer?");
        }
        else if(type == 0){
          $("#tilteblock").text("Are you sure you want to Unblock this Offer?");
        }
        $(".countinueblock").attr("href","{{url('seller/block_offer')}}"+"/"+id+"/"+type);
    }

  </script>
  
  <script type="text/javascript">
      function deleteOffer(id){
        $(".countinuedelete").attr("href","{{url('seller/delete_offer')}}"+"/"+id );
      }
  </script>

  <script type="text/javascript">
        $('#account-list').DataTable();
  </script>

  @if ($message = session()->has('success_message'))
   <script type="text/javascript">
     $(function() {
        $('#unblock-sccess-modal').modal('show');
        setTimeout(function() {$('#unblock-sccess-modal').modal('hide');}, 2000);
      });
  </script>
  @endif

  @if ($message = session()->has('error_message'))
   <script type="text/javascript">
     $(function() {
        $('#delete-sccess-modal').modal('show');
        setTimeout(function() {$('#delete-sccess-modal').modal('hide');}, 2000);
      });
   </script>
  @endif  
  
<script type="text/javascript">
  single = new Array();
      function singleFiles(event) {
        this.single = [];
        var singleFiles = event.target.files;
        if (singleFiles) {
          for (var file of singleFiles) {
            var singleReader = new FileReader();
            singleReader.onload = (e) => {
              this.single.push(e.target.result);
              $(event.target).closest('.img-box').find('.imagefile').attr('src', e.target.result)          
            }
            singleReader.readAsDataURL(file);
          }
        }
      }
</script>

<script type="text/javascript">
    
    $('#demo-datetimepicker').datetimepicker();
    $('#demo-datetimepicker1').datetimepicker1();

    // var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);
    // $("#start_date").datepicker({
      
    //   startDate: today, 
    //     opens: 'bottom',
    //     autoclose: true
    //   }).on('changeDate', function(selected){
    //     var startDate = new Date(selected.date.valueOf());
    //     $('#end_date').datepicker('setStartDate', startDate);
    //   });  

    //   $("#end_date").datepicker({
    //     opens: 'bottom',
    //     autoclose: true
    //   }).on('changeDate', function(selected){
    //       var startDate = new Date(selected.date.valueOf());
    //       $('#start_date').datepicker('setEndDate', startDate);
    //   });


    //   $(function () {
    //     $('#datetimepicker').datetimepicker({
    //       format: 'LT'
    //     });
    //   });
</script>

@endsection