@extends('seller.header')

@section('content')

    <div id="pcoded" class="pcoded">
        <div class="pcoded-overlay-box"></div>
        <div class="pcoded-container navbar-wrapper">
           
            @include('seller/navbar')

            <div id="accountPage"></div>
            <div class="pcoded-main-container">
                <div class="pcoded-wrapper">
                	
                	@include('seller/sidebar')

            <div class="pcoded-content">
              <div class="pcoded-inner-content">
                    <div class="main-body">
                      <div class="page-wrapper">
                        <div class="page-header card">
                          <div class="row align-items-end">
                            <div class="col-sm-6 col-lg-6">
                              <div class="page-header-title d-flex">
                                    <i class="ti-layout bg-c-green"></i>
                                    <div class="d-inline">
                                        <h4>Customer Management</h4>
                                    </div>
                              </div>
                            </div>
                            <div class="col-sm-6 col-lg-6">
                              <div class="page-header-breadcrumb">
                                <div class="page-header-breadcrumb">
                                  <ul class="breadcrumb-title">
                                    <li class="breadcrumb-item">
                                      <a href="{{ url('seller/dashboard')}}">
                                      <i class="icofont icofont-home"></i>
                                      </a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="#">Customer Management</a></li>
                                  </ul>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="page-body">
                          <div class="card">
                            <div class="card-block">
                              <div class="dt-plugin-buttons"></div>
                                <div class="row">
                                   <div class="mb-4  m-b-15 topForm">
                                      <label class="d-flex text-dark pull-left mr-3">
                                        From <input type="text" class="form-control ml-2 mr-2 wpx85" autocomplete="off" id="start_date"><i class="fa fa-calendar mt-1"></i>
                                      </label>
                                      <label class="d-flex text-dark pull-left ml-2 mr-3">
                                        To <input type="text" class="form-control ml-2 mr-2 wpx85" autocomplete="off" id="end_date"><i class="fa fa-calendar mt-1"></i>
                                      </label>
                                    </div>
                                </div>    
                              <div class="dt-responsive table-responsive">
                                <table id="account-list" class="table table-striped table-bordered nowrap">
                                  <thead>
                                    <tr>
                                        <th>S.No</th>
                                        <th>Name</th>
                                        
                                        <th>Email ID</th>
                                        <th>Mobile</th>
                                        <th>Registered Date</th>
                                        <th>Status</th>
                                        <th>Actions</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                  	@foreach($userData as $row)
                                    	<tr>              
                                     	  <td>{{ $row->id}}</td>
                                          <td>
                                          	@if(!empty($row->profile_image))
                                          		<img src="{{ asset('public/profilepic/'.$row->profile_image)}}" width="30px;" class="img-radius mr-2" alt="User-Profile-Image">
                                          	@else
                                          		<img src="{{ asset('public/seller/images/avatar-4.jpg')}}" width="30px;" class="img-radius mr-2" alt="User-Profile-Image">
                                          	@endif
                                          	{{ $row->username }}</td>
                                          
                                          <td>{{ $row->email_number }}</td>
                                          <td>{{ $row->mobile_number }}</td>
                                          <td>{{ $row->created_at }}</td>
                                          <td class="text-success">
                                          	@if($row->status == '0')
                                          	{{ "Active"}}
                                          	@else($row->status == '1')
                                          	{{ "Inactive" }}	
                                          	@endif
                                          </td>
                                          <td>

                                          	@if($row['status'] == 0)
						                    <button class="btn btn-sm btn-warning" data-target="#block-modal" data-toggle="modal" title="Block" onclick="blockUnblock({{$row['id']}},1)"> Block</button>
						                    @else
						                    <button class="btn btn-sm btn-success" data-target="#block-modal" data-toggle="modal" title="Unblock" onclick="blockUnblock({{$row['id']}},0)"> Unblock</button>
						                    @endif

                                           <!--  <button data-toggle="modal" data-target="#block-modal" class="btn btn-sm btn-warning">Block</button> -->
                                            <!-- <button data-toggle="modal" data-target="#unblock-modal" class="btn btn-sm btn-success mr-1 ml-1">Unblock</button>-->

                                            <button data-toggle="modal" data-target="#delete-modal" onclick="deleteUser({{$row->id }})" class="btn btn-sm btn-danger">Delete</button> 
                                          </td> 
                                    	</tr>
                                     @endforeach
                                  </tbody>
                                </table>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div> 
                </div>
            </div>
            <!-- success modal -->
            <div id="profile-success-modal" class="modal fade" role="dialog">
              <div class="modal-dialog modal-sm" data-dismiss="modal">
              
                <div class="modal-body text-center">
                  <div class="row">
                    <div class="col-md-12">
                      <i class="text-success fs-34 d-inline-block icofont icofont-check-circled mb-4"></i>
                      <h6>Saved Successfully</h6>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>    
    </div>          
          


	<div id="block-modal" class="modal fade" role="dialog">
	  <div class="modal-dialog modal-sm">
	 
	    <div class="modal-body text-center">
	      <div class="row">
	        <div class="col-md-12">
	          <i class="text-warning fs-34 d-inline-block ti-link mb-4"></i>
	          <h6 id="tilteblock"></h6>
	        </div>
	      </div>
	      <hr />
	      <div class="row m-t-15">
	        <div class="col-sm-12">
	        	<a class="btn btn-success btn-sm countinueblock" >Yes</a>
                <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">No</button>
	        </div>
	      </div>
	    </div>
	  </div>
	</div>
	<!-- unblock modal -->
	<div id="unblock-modal" class="modal fade" role="dialog">
	  <div class="modal-dialog modal-sm">
	    <div class="text-center">
	      <img src="assets/images/auth/logo.png" alt="logo.png" />
	    </div>
	    <div class="modal-body text-center">
	      <div class="row">
	        <div class="col-md-12">
	          <i class="text-success fs-34 d-inline-block ti-unlink mb-4"></i>
	          <h6>Are you sure you want to unblock? </h6>
	        </div>
	      </div>
	      <hr />
	      <div class="row m-t-15">
	        <div class="col-sm-12">
	          <button type="button" class="btn btn-sm btn-success mr-1" data-target="#unblock-sccess-modal" data-toggle="modal" data-dismiss="modal">Unblock</button>
	          <button type="button" class="btn btn-sm btn-danger ml-1" data-dismiss="modal">Cancel</button>
	        </div>
	      </div>
	    </div>
	  </div>
	</div>


	<!-- delete block -->
	<div id="delete-modal" class="modal fade" role="dialog">
	  <div class="modal-dialog modal-sm">	   
	    <div class="modal-body text-center">
	      <div class="row">
	        <div class="col-md-12">
	          <i class="text-danger fs-34 d-inline-block ti-trash mb-4"></i>
	          <h6>Are you sure you want to delete</h6>
	        </div>
	      </div>
	      <hr />
	      <div class="row m-t-15">
	        <div class="col-sm-12">
	          <a class="btn btn-success btn-sm continuedelete" >Yes</a>	          
	          <button type="button" class="btn btn-sm btn-danger ml-1" data-dismiss="modal">Cancel</button>
	        </div>
	      </div>
	    </div>
	  </div>
	</div>
	<!-- success modal -->


	<div class="modal fade" id="unblock" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-body text-center">
                <h2><i class="fa fa-trash-o text-danger"></i></h2>
                <h6 id="tilteblock"></h6>
                <a class="btn btn-success btn-sm countinueblock" >Yes</a>
                <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">No</button>
            </div>
        </div>
    </div>
</div>

	<div id="block-sccess-modal" class="modal fade">
	  <div class="modal-dialog modal-sm" data-dismiss="modal">
	   
	    <div class="modal-body text-center">
	      <div class="row">
	        <div class="col-md-12">
	          <i class="text-warning fs-34 d-inline-block icofont icofont-check-circled mb-4"></i>
	          <h6 >Block Successfully</h6>
	        </div>
	      </div>
	    </div>
	  </div>
	</div>
	<!-- success modal -->
	<div id="unblock-sccess-modal" class="modal fade">
	  <div class="modal-dialog modal-sm" data-dismiss="modal">
	 
	    <div class="modal-body text-center">
	      <div class="row">
	        <div class="col-md-12">
	          <i class="text-success fs-34 d-inline-block icofont icofont-check-circled mb-4"></i>
	          <h6>Unblock Successfully</h6>
	        </div>
	      </div>
	    </div>
	  </div>
	</div>
	<!-- success modal -->
	<div id="delete-sccess-modal" class="modal fade">
	  <div class="modal-dialog modal-sm" data-dismiss="modal">
	   
	    <div class="modal-body text-center">
	      <div class="row">
	        <div class="col-md-12">
	          <i class="text-danger fs-34 d-inline-block icofont icofont-check-circled mb-4"></i>
	          <h6>Delete Successfully</h6>
	        </div>
	      </div>
	    </div>
	  </div>
	</div>
@endsection

@section('footerscript')
	
	<script type="text/javascript">		
		function blockUnblock(id,type){
		    if(type == 0){
		      $("#tilteblock").text("Are you sure you want to unblock this User?");
		    }
		    else if(type == 1){
		      $("#tilteblock").text("Are you sure you want to block this User?");
		    }
		    $(".countinueblock").attr("href","{{url('admin/blockUser')}}"+"/"+id+"/"+type);
		}
		
	</script>

	<script type="text/javascript">

		function deleteUser(id){
			$(".continuedelete").attr("href", "{{url('admin/deleteUser')}}"+"/"+id);
		}
		function deleteData(id){
	      $(".continuedelete").attr("href","{{url('admin/deleteCategory')}}"+"/"+id);
	    }	    
	</script>

	<script type="text/javascript">
	      $('#account-list').DataTable();
	</script>
	<!-- block modal -->
@endsection