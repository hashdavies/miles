@extends('seller.header')

@section('content')
    <div id="pcoded" class="pcoded">
        <div class="pcoded-overlay-box"></div>
        <div class="pcoded-container navbar-wrapper">
      
            @include('seller.navbar')

            <div id="accountPage"></div>
            <div class="pcoded-main-container">
                <div class="pcoded-wrapper">
                   
                     @include('seller.sidebar')

                        <div class="pcoded-content">
                          <div class="pcoded-inner-content">
                                <div class="main-body">
                                  <div class="page-wrapper">
                                    <div class="page-header card">
                                      <div class="row align-items-end">
                                        <div class="col-sm-6 col-lg-6">
                                          <div class="page-header-title d-flex">
                                                <i class="ti-layout bg-c-green"></i>
                                                <div class="d-inline">
                                                    <h4>Vendor Management</h4>
                                                </div>
                                          </div>
                                        </div>
                                        <div class="col-sm-6 col-lg-6">
                                          <div class="page-header-breadcrumb">
                                            <div class="page-header-breadcrumb">
                                              <ul class="breadcrumb-title">
                                                <li class="breadcrumb-item">
                                                  <a href="{{ url('seller/dashboard')}}">
                                                  <i class="icofont icofont-home"></i>
                                                  </a>
                                                </li>
                                                <li class="breadcrumb-item"><a href="#">Vendor Management</a></li>
                                              </ul>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="page-body">
                                      <div class="card">
                                        <div class="card-block">
                                           <div class="dt-plugin-buttons"></div>
                                             <div class="add-more-button m-b-15">
                                               <button type="button"  onclick="location.href='{{ url('seller/add-vendor')}}';" class="btn btn-sm btn-primary waves-effect"><i class="typcn typcn-plus-outline"></i> Add Vendor</button>
                                             </div>
                                            <div class="">
                                                <div class="mb-4 m-b-15 topForm">
                                                  <label class="d-flex text-dark pull-left mr-3">
                                                    From <input type="text" class="form-control ml-2 mr-2 wpx85" autocomplete="off" id="start_date"><i class="fa fa-calendar mt-1"></i>
                                                  </label>
                                                  <label class="d-flex text-dark pull-left ml-2 mr-3">
                                                    To <input type="text" class="form-control ml-2 mr-2 wpx85" autocomplete="off" id="end_date"><i class="fa fa-calendar mt-1"></i>
                                                  </label>
                                                </div>
                                            </div>    
                                          <div class="dt-responsive table-responsive mt-4">
                                            <table id="account-list" class="table table-striped table-bordered nowrap">
                                              <thead>
                                                <tr>
                                                  <th>S.No</th>
                                                  <th>Name</th>
                                                  <th>Email ID</th>
                                                  <th>Mobile</th>
                                                  <th>Registered Date</th>
                                                  <th>Address</th>
                                                  <th>Status</th>
                                                  <th>Varification</th>
                                                  <th>Actions</th>
                                                </tr>
                                              </thead>
                                              <tbody>
                                                <tr>
                                                  <td>01</td>
                                                  <td><img src="files/assets/images/avatar-4.jpg" width="30px;" class="img-radius mr-2" alt="User-Profile-Image">Anshu</td>
                                                  <td>anshu@gmail.com</td>
                                                  <td>+91 9876540321</td>
                                                  <td>22 nov</td>
                                                  <td>India</td>
                                                  <td class="text-success">Active</td>
                                                  <td>pending</td>
                                                  <td>
                                                    <button data-toggle="modal" data-target="#view-model" class="btn btn-sm btn-primary">View</button>
                                                    <button data-toggle="modal" data-target="#block-modal" class="btn btn-sm btn-warning">Block</button>
                                                    <!-- <button data-toggle="modal" data-target="#unblock-modal" class="btn btn-sm btn-success mr-1 ml-1">Unblock</button>-->
                                                    <button data-toggle="modal" data-target="#delete-modal" class="btn btn-sm btn-danger">Delete</button> 
                                                  </td>
                                                </tr>
                                              </tbody>
                                            </table>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                          </div>
                        </div> 
                </div>
            </div>
            <!-- success modal -->
            <div id="profile-success-modal" class="modal fade" role="dialog">
              <div class="modal-dialog modal-sm" data-dismiss="modal">
              
                <div class="modal-body text-center">
                  <div class="row">
                    <div class="col-md-12">
                      <i class="text-success fs-34 d-inline-block icofont icofont-check-circled mb-4"></i>
                      <h6>Saved Successfully</h6>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>    
    </div>          
          
    <!-- block modal -->
  <div id="block-modal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm">
   
      <div class="modal-body text-center">
        <div class="row">
          <div class="col-md-12">
            <i class="text-warning fs-34 d-inline-block ti-link mb-4"></i>
            <h6>Are you sure you want to block?</h6>
          </div>
        </div>
        <hr />
        <div class="row m-t-15">
          <div class="col-sm-12">
            <button type="button" class="btn btn-sm btn-success mr-1" data-target="#block-sccess-modal" data-toggle="modal" data-dismiss="modal">Block</button>
            <button type="button" class="btn btn-sm btn-danger ml-1" data-dismiss="modal">Cancel</button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- unblock modal -->
  <div id="unblock-modal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="text-center">
        <img src="assets/images/auth/logo.png" alt="logo.png" />
      </div>
      <div class="modal-body text-center">
        <div class="row">
          <div class="col-md-12">
            <i class="text-success fs-34 d-inline-block ti-unlink mb-4"></i>
            <h6>Are you sure you want to unblock? </h6>
          </div>
        </div>
        <hr />
        <div class="row m-t-15">
          <div class="col-sm-12">
            <button type="button" class="btn btn-sm btn-success mr-1" data-target="#unblock-sccess-modal" data-toggle="modal" data-dismiss="modal">Unblock</button>
            <button type="button" class="btn btn-sm btn-danger ml-1" data-dismiss="modal">Cancel</button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- delete block -->
  <div id="delete-modal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm">
     
      <div class="modal-body text-center">
        <div class="row">
          <div class="col-md-12">
            <i class="text-danger fs-34 d-inline-block ti-trash mb-4"></i>
            <h6>Are you sure you want to delete</h6>
          </div>
        </div>
        <hr />
        <div class="row m-t-15">
          <div class="col-sm-12">
            <button type="button" class="btn btn-sm btn-success mr-1" data-target="#delete-sccess-modal" data-toggle="modal" data-dismiss="modal">Delete</button>
            <button type="button" class="btn btn-sm btn-danger ml-1" data-dismiss="modal">Cancel</button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- success modal -->
  <div id="block-sccess-modal" class="modal fade">
    <div class="modal-dialog modal-sm" data-dismiss="modal">
     
      <div class="modal-body text-center">
        <div class="row">
          <div class="col-md-12">
            <i class="text-warning fs-34 d-inline-block icofont icofont-check-circled mb-4"></i>
            <h6>Block Successfully</h6>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- success modal -->
  <div id="unblock-sccess-modal" class="modal fade">
    <div class="modal-dialog modal-sm" data-dismiss="modal">
   
      <div class="modal-body text-center">
        <div class="row">
          <div class="col-md-12">
            <i class="text-success fs-34 d-inline-block icofont icofont-check-circled mb-4"></i>
            <h6>Unblock Successfully</h6>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- success modal -->
  <div id="delete-sccess-modal" class="modal fade">
    <div class="modal-dialog modal-sm" data-dismiss="modal">
     
      <div class="modal-body text-center">
        <div class="row">
          <div class="col-md-12">
            <i class="text-danger fs-34 d-inline-block icofont icofont-check-circled mb-4"></i>
            <h6>Delete Successfully</h6>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div id="view-model" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Vendor Information</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
          </button> 
        </div>
        <div class="modal-body">
            <div class="model-content">
              <div class="row">
                <div class="col-md-12">
                    <h6 class="fw-600 fs-18">Store Information</h6>
                     <div class="row mt-2"> 
                         <div class="col-md-6">
                            <h6 class="fw-500">Store Name:</h6>
                         </div>
                          <div class="col-md-6">
                             <h6>data</h6>
                         </div>
                      </div> 
                       <div class="row"> 
                         <div class="col-md-6">
                            <h6 class="fw-500">Mobile Number:

                            </h6>
                         </div>
                          <div class="col-md-6">
                             <h6>data</h6>
                         </div>
                      </div> 
                      <div class="row"> 
                         <div class="col-md-6">
                            <h6 class="fw-500">Address:
                            </h6>
                         </div>
                          <div class="col-md-6">
                             <h6>data</h6>
                         </div>
                      </div>
                       <div class="row"> 
                         <div class="col-md-6">
                            <h6 class="fw-500">City:
                            </h6>
                         </div>
                          <div class="col-md-6">
                             <h6>data</h6>
                         </div>
                      </div>
                         <div class="row"> 
                         <div class="col-md-6">
                            <h6 class="fw-500">Country Name:
                            </h6>
                         </div>
                          <div class="col-md-6">
                             <h6>data</h6>
                         </div>
                      </div>
                </div>  
                <div class="col-md-12 mt-3">
                    <h6 class="fw-600 fs-18">Bank Account Information</h6>
                     <div class="row mt-2"> 
                         <div class="col-md-6">
                            <h6 class="fw-500">Account Number:</h6>
                         </div>
                          <div class="col-md-6">
                             <h6>data</h6>
                         </div>
                      </div> 
                       <div class="row"> 
                         <div class="col-md-6">
                            <h6 class="fw-500">Account Holder Name:

                            </h6>
                         </div>
                          <div class="col-md-6">
                             <h6>data</h6>
                         </div>
                      </div> 
                      <div class="row"> 
                         <div class="col-md-6">
                            <h6 class="fw-500">Swift/Bank Code:
                            </h6>
                         </div>
                          <div class="col-md-6">
                             <h6>data</h6>
                         </div>
                      </div>
                </div>
                <div class="col-md-12 mt-3">
                    <h6 class="fw-600 fs-18">Tax Details</h6>
                     <div class="row mt-2"> 
                         <div class="col-md-6">
                            <h6 class="fw-500">Tax Registration Number:</h6>
                         </div>
                          <div class="col-md-6">
                             <h6>data</h6>
                         </div>
                      </div> 
                       <div class="row"> 
                         <div class="col-md-6">
                            <h6 class="fw-500">Tax Registration Date:

                            </h6>
                         </div>
                          <div class="col-md-6">
                             <h6>data</h6>
                         </div>
                      </div> 
                </div>                  
            </div>
        </div>  
        </div>
      </div>  
    </div>
@endsection

@section('footerscript')
  <script type="text/javascript">
      $('#account-list').DataTable();
  </script>
@endsection