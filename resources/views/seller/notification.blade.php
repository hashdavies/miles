 @extends('seller.header')    
    @section('title', 'Loyality Offers')
    @section('content')

      <div class="pcoded-content">
        <div class="pcoded-inner-content">
          <div class="page-wrapper">
            <div class="row gutter-xs">
              <div class="col-12 col-sm-8 col-md-8 col-lg-9 mb-3">
                <h6>Send Notification</h6>
              </div>
              <div class="col-12 col-sm-4 col-md-4 col-lg-3 mb-3">
                <a href="{{url('seller/notification-display')}}" class="btn btn-md btn-primary btn-block">Display Notifications</a>
              </div>
              <!--  <div class="col-12 col-sm-12 col-md-12 form-group">
                <input type="text" class="form-control form-control-lg rounded-20 search" placeholder="Search by ID or Name" />
              </div> -->
            </div>
            <div class="card">
              <div class="card-body">
                <div class="row gutter-sm">
                  <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                    <h6>Filter</h6>
                  </div>
                  <div class="col-12 col-sm-9 col-md-9 col-lg-9">
                    <div class="d-flex align-items-center filter">
                      <div class="d-flex align-items-center">
                        From<input type="text" class="form-control plr-65 wpx-100 ml-1" placeholder="--/--/----" id="start_date"/>
                        <i class="ml-2 mr-3"><img src="{{url('public/seller/images/calendar.png')}}"></i>
                        To<input type="text" class="form-control plr-65 wpx-100 ml-1" placeholder="--/--/----" id="end_date"/>
                        <i class="ml-2 mr-3"><img src="{{url('public/seller/images/calendar.png')}}"></i>
                      </div>
                      <div class="d-flex align-items-center">
                        <select class="form-control wpx-140 m-r-10">
                          <option value="" hidden="">Country</option>
                          <option value="Australia">Australia</option>
                          <option value="India">India</option>
                          <option value="USA">USA</option>
                        </select>
                        <select class="form-control wpx-140">
                          <option value="" hidden="">City</option>
                          <option value="Delhi">Delhi</option>
                          <option value="Mumbai">Mumbai</option>
                          <option value="Noida">Noida</option>
                        </select>
                      </div>
                    </div>                
                  </div>
                  <div class="col-12 col-sm-3 col-md-3 col-lg-3">
                    <button type="button" class="btn btn-sm btn-primary pull-right">Apply</button>
                  </div>
                </div>
              </div>
            </div>
            <div class="card">
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table table-bordered dataTable m-0">
                    <thead>
                      <tr>
                        <th>Notification ID</th>
                        <th>Topic</th>
                        <th>Country</th>
                        <th>City</th>
                        <th>Start Date</th>
                        <th>End Date</th>
                        <th>Interwal</th>
                        <th class="no-sort wpx-50">Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>#2436678985</td>
                        <td><span class="wpx-260">Lorem ipsum is dummy text</span></td>
                        <td>America</td>
                        <td>New York</td>
                        <td>01-01-2020</td>
                        <td>04-01-2020</td>
                        <td>Twice per week</td>
                        <td>
                          <!-- <a href="notification-view.php"><img src="assets/images/show.png"/></a>
                          <a href="notification-edit.php" class="ml-2"><img src="assets/images/edit1.png"/></a> -->
                          <a class="ml-2" data-target="#delete-modal" data-toggle="modal"><img src="{{url('public/seller/images/delete.png')}}"/></a>
                        </td>
                      </tr>
                      
                    </tbody>
                  </table>                
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <!-- Delete Modal -->
      <div class="modal fade" id="delete-modal">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-body">
              <h6 class="fs-18">Are you sure you want cancel this notification?</h6>
              <div class="text-right mt-4">
                <button type="button" data-dismiss="modal" class="btn btn-outline-primary btn-sm wpx-100">No</button>
                <button type="button" data-dismiss="modal" data-target="#success-modal" data-toggle="modal" class="btn btn-primary btn-sm wpx-100">Yes</button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- success-modal -->
      <div class="modal fade" id="success-modal">
        <div class="modal-dialog modal-sm" data-dismiss="modal">
          <div class="modal-content">
            <div class="modal-body pt-4 text-center">
              <img src="assets/images/confirm.png" class="mb-3"/>
              <h6>Your notification has been deleted successfully!</h6>
            </div>
          </div>
        </div>
      </div>

      @endsection()

      @section('footer_script') 


      @endsection