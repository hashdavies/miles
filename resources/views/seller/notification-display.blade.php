 @extends('seller.header')    
    @section('title', 'Loyality Offers')
    @section('content')

      <div class="pcoded-content">
        <div class="pcoded-inner-content">
          <div class="page-wrapper">
            <div class="row gutter-xs">
              <div class="col-10 col-sm-8 col-md-8 col-lg-9 mb-3">
                <h6><span class="text-gray">Notifications</span> <i class="fa fa-angle-right ml-1 mr-1"></i> Dispaly for Users</h6>
              </div>
              <div class="col-2 col-sm-4 col-md-4 col-lg-3 mb-3">
                <a href="{{url('seller/notification')}}" class="text-primary pull-right"><i class="fa fa-long-arrow-left mr-1"></i> back</a>
              </div>
            </div>
            <div class="card p-3">
              <div class="card-body">


                @if(!empty($notificationData))
                   <form action="{{url('seller/add-announcement')}}" class="w-100 form-control-bottom" autocomplete="off" method="post" id="AnnouncementForm">             
                @csrf

                   <div class="row gutter-sm">
                    @php $i = '1' @endphp
                    @foreach($notificationallData as $notificationData)
                    
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6">
                      <div class="form-group">
                        <label>                         
                              {{ $i++ }}           
                        .</label>
                        <input type="text" class="form-control" name="announcement[]" id="announcement1" value="{{$notificationData->announcement}}">
                        <span class="text-danger" id="announcementError" style="display: none">Plz select atleast one on notification</span>
                      </div>
                    </div>

                    @endforeach
                     

                    <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                      <div class="notification-btn text-right">
                        <button type="submit" class="btn btn-sm btn-primary">Save</button>
                      </div>  
                    </div>         
                  </div>

                </form>

                @else


                <form action="{{url('seller/add-announcement')}}" class="w-100 form-control-bottom" autocomplete="off" method="post" id="AnnouncementForm">             
                @csrf

                  <div class="row gutter-sm">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6">
                      <div class="form-group">
                        <label>1.</label>
                        <input type="text" class="form-control" name="announcement[]" id="announcement1">
                        <span class="text-danger" id="announcementError" style="display: none">Plz select atleast one on notification</span>
                      </div>
                    </div>
                     <div class="col-12 col-sm-12 col-md-12 col-lg-6">
                      <div class="form-group">
                        <label>2.</label>
                        <input type="text" class="form-control" name="announcement[]" id="announcement2">
                      </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6">
                      <div class="form-group">
                        <label>3.</label>
                        <input type="text" class="form-control" name="announcement[]" id="announcement3">
                      </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6">
                      <div class="form-group">
                        <label>4.</label>
                        <input type="text" class="form-control" name="announcement[]" id="announcement4">
                      </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6">
                      <div class="form-group">
                        <label>5.</label>
                        <input type="text" class="form-control" name="announcement[]" id="announcement5">
                      </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6">
                      <div class="form-group">
                        <label>6.</label>
                        <input type="text" class="form-control" name="announcement[]" id="announcement6">
                      </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6">
                      <div class="form-group">
                        <label>7.</label>
                        <input type="text" class="form-control" name="announcement[]" id="announcement7">
                      </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6">
                      <div class="form-group">
                        <label>8.</label>
                        <input type="text" class="form-control" name="announcement[]" id="announcement8">
                      </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6">
                      <div class="form-group">
                        <label>9.</label>
                        <input type="text" class="form-control" name="announcement[]" id="announcement9">
                      </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6">
                      <div class="form-group">
                        <label>10.</label>
                        <input type="text" class="form-control" name="announcement[]" id="announcement10">
                      </div>
                    </div> 
                    <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="notification-btn text-right">
                      <button type="submit" class="btn btn-sm btn-primary">Save</button>
                    </div>  
                    </div>         
                  </div>

                </form>
                @endif

              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="modal fade" id="success-modal">
        <div class="modal-dialog modal-sm" data-dismiss="modal">
          <div class="modal-content">
            <div class="modal-body pt-4 text-center">
              <img src="{{url('public/seller/images/confirm.png')}}" class="mb-3"/>
              <h6>{{ session()->get('success_message') }}</h6>
            </div>
          </div>
        </div>
      </div>

      @endsection()

      @section('footer_script') 
        <script type="text/javascript">
          $('#AnnouncementForm').on('submit',function(e) {  
            $('#announcementError').hide();
            
            var announcement1 = $('#announcement1').val();
            var announcement2 = $('#announcement2').val();
            var announcement3 = $('#announcement3').val();
            var announcement4 = $('#announcement4').val();
            var announcement5 = $('#announcement5').val();
            var announcement6 = $('#announcement6').val();
            var announcement7 = $('#announcement7').val();
            var announcement8 = $('#announcement8').val();
            var announcement9 = $('#announcement9').val();
            var announcement10 = $('#announcement10').val();
           
            if((announcement1 == '' && announcement2 == '' && announcement3 == '' && announcement4 ==  '' && announcement5 == '' && announcement6 == ''  && announcement7 == '' && announcement8 == '' && announcement9 == '' && announcement10 == '')){

                $('#announcementError').show();
                e.preventDefault();
            }
          });
        </script>

        @if ($message = session()->has('success_message'))
          <script type="text/javascript">
             $(function() {
                $('#success-modal').modal('show');
                setTimeout(function() {$('#success-modal').modal('hide');}, 2000);
              });
          </script>
        @endif

      @endsection