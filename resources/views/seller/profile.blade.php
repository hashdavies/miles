  @extends('seller.header')

  @section('content')	
      
      <div class="pcoded-content">
        <div class="pcoded-inner-content">
          <div class="page-wrapper">
            <div class="row">
              <div class="col-12 col-sm-12 col-md-12 col-lg-12 mb-2">
                <h5>Profile</h5>
              </div>
              <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                <div class="card">
                  <div class="card-body">
                    <div class="row">
                      <div class="col-12 col-sm-12 col-md-12 col-lg-12 mb-4">
                        <ul class="nav-tab-group">                        
                          <li class="active"><a href="{{url('seller/profile')}}">Profile Details</a></li>
                          <li><a href="profile-business-details.php">Business Details</a></li>
                          <li><a href="my-payment.php">My Payment</a></li>
                        </ul>
                      </div>
                      <div class="col-12 col-sm-12 col-md-6">
                        <h6>Personal Details</h6>
                        <table class="w-100">
                          <tr>
                            <td class="pt-2 pb-2">Name</td>
                            <td class="pt-2 pb-2">
                              {{ $sellerProfile->first_name}} {{ $sellerProfile->last_name}}
                            </td>
                          </tr>
                          <tr>
                            <td class="pt-2 pb-2">Email Address</td>
                            <td class="pt-2 pb-2">
                              {{ $sellerProfile->first_name}}
                            </td>
                          </tr>
                          <tr>
                            <td class="pt-2 pb-2">Mobile Number</td>
                            <td class="pt-2 pb-2">49870720783</td>
                          </tr>
                          <tr>
                            <td class="pt-2 pb-2">Store Name</td>
                            <td class="pt-2 pb-2">Zara</td>
                          </tr>
                        </table>
                      </div>
                      <div class="col-12 col-sm-12 col-md-6">
                        <h6>Store Address</h6>
                        <table class="w-100">
                          <tr>
                            <td class="pt-2 pb-2">Pincode</td>
                            <td class="pt-2 pb-2">110077</td>
                          </tr>
                          <tr>
                            <td class="pt-2 pb-2">Address</td>
                            <td class="pt-2 pb-2">Lorem ipsum is my new address</td>
                          </tr>
                          <tr>
                            <td class="pt-2 pb-2">City</td>
                            <td class="pt-2 pb-2">New York City</td>
                          </tr>
                          <tr>
                            <td class="pt-2 pb-2">State</td>
                            <td class="pt-2 pb-2">New York</td>
                          </tr>
                        </table>
                      </div>
                      <div class="col-12 mt-3">
                        <h6>Social Media Link</h6>
                        <div class="mt-3">
                          <a href="#" class="mr-1"><img src="{{url('public/seller/images/facebook.png')}}"></a>
                          <a href="#" class="mr-1"><img src="{{url('public/seller/images/twitter.png')}}"></a>
                        </div>
                      </div>
                      <div class="col-12 text-right mt-3">
                        <button type="button" class="btn btn-primary btn-md wpx-180" href="add-card.php">Edit Details</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

  @endsection  