<nav class="navbar header-navbar pcoded-header" header-theme="theme1" pcoded-header-position="fixed">
  <div class="navbar-wrapper">
    <div class="navbar-logo" logo-theme="theme1">
      <a class="mobile-menu" id="mobile-collapse" href="javascript:void(0)">
        <i class="ti-menu"></i>
      </a>
      <a class="mobile-search morphsearch-search" href="javascript:void(0)">
        <i class="ti-search"></i>
      </a>
      <a href="{{url('seller/dashboard')}}" class="navbar-brand">
        <img class="img-fluid" src="{{url('public/seller/images/logo.png')}}" alt="Theme-Logo" />
      </a>
      <a class="mobile-options">
        <i class="ti-more"></i>
      </a>
    </div>
    <div class="navbar-container container-fluid">
      <ul class="nav-left">
        <li>
          <div class="sidebar_toggle"><a href="javascript:void(0)"><i class="ti-menu"></i></a></div>
        </li>
        <!-- <li>
          <a class="main-search morphsearch-search" href="javascript:void(0)">
          <i class="ti-search"></i>
          </a>
        </li>
        <li>
          <a href="javascript:void(0)" onclick="javascript:toggleFullScreen()">
            <i class="ti-fullscreen"></i>
          </a>
        </li>
        <li class="mega-menu-top">
          <a href="javascript:void(0)">
          Mega
            <i class="ti-angle-down"></i>
          </a>
          <ul class="show-notification row">
            <li class="col-sm-3">
              <h6 class="mega-menu-title">Popular Links</h6>
              <ul class="mega-menu-links">
                <li><a href="/form-elements-component">Form Elements</a></li>
                <li><a href="/button">Buttons</a></li>
                <li><a href="/map-google">Maps</a></li>
                <li><a href="/user-card">Contact Cards</a></li>
                <li><a href="/user-profile">User Information</a></li>
                <li><a href="/auth-lock-screen">Lock Screen</a></li>
              </ul>
            </li>
            <li class="col-sm-3">
              <h6 class="mega-menu-title">Mailbox</h6>
              <ul class="mega-mailbox">
                <li>
                  <a href="index.php" class="media">
                    <div class="media-left">
                      <i class="ti-folder"></i>
                    </div>
                    <div class="media-body">
                      <h5>Data Backup</h5>
                      <small class="text-muted">Store your data</small>
                    </div>
                  </a>
                </li>
                <li>
                  <a href="index.php" class="media">
                    <div class="media-left">
                      <i class="ti-headphone-alt"></i>
                    </div>
                    <div class="media-body">
                      <h5>Support</h5>
                      <small class="text-muted">24-hour support</small>
                    </div>
                  </a>
                </li>
                <li>
                  <a href="index.php" class="media">
                    <div class="media-left">
                      <i class="ti-dropbox"></i>
                    </div>
                    <div class="media-body">
                      <h5>Drop-box</h5>
                      <small class="text-muted">Store large amount of data in one-box only
                      </small>
                    </div>
                  </a>
                </li>
                <li>
                  <a href="index.php" class="media">
                    <div class="media-left">
                      <i class="ti-location-pin"></i>
                    </div>
                    <div class="media-body">
                      <h5>Location</h5>
                      <small class="text-muted">Find Your Location with ease of use</small>
                    </div>
                  </a>
                </li>
              </ul>
            </li>
            <li class="col-sm-3">
              <h6 class="mega-menu-title">Gallery</h6>
              <div class="row m-b-20">
                <div class="col-sm-4"><img class="img-fluid img-thumbnail" src="assets/images/avatar-2.jpg" alt="Gallery-1" /></div>
                <div class="col-sm-4"><img class="img-fluid img-thumbnail" src="assets/images/avatar-3.jpg" alt="Gallery-2" /></div>
                <div class="col-sm-4"><img class="img-fluid img-thumbnail" src="assets/images/avatar-4.jpg" alt="Gallery-3" /></div>
              </div>
              <div class="row m-b-20">
                <div class="col-sm-4"><img class="img-fluid img-thumbnail" src="assets/images/avatar-3.jpg" alt="Gallery-4" /></div>
                <div class="col-sm-4"><img class="img-fluid img-thumbnail" src="assets/images/avatar-4.jpg" alt="Gallery-5" /></div>
                <div class="col-sm-4"><img class="img-fluid img-thumbnail" src="assets/images/avatar-2.jpg" alt="Gallery-6" /></div>
              </div>
              <button class="btn btn-primary btn-sm btn-block">Browse Gallery</button>
            </li>
            <li class="col-sm-3">
              <h6 class="mega-menu-title">Contact Us</h6>
              <div class="mega-menu-contact">
                <div class="form-group row">
                  <label for="example-text-input" class="col-3 col-form-label">Name</label>
                  <div class="col-9">
                    <input class="form-control" type="text" placeholder="Artisanal kale" id="example-text-input" />
                  </div>
                </div>
                <div class="form-group row">
                  <label for="example-search-input" class="col-3 col-form-label">Email</label>
                  <div class="col-9">
                    <input class="form-control" type="email" placeholder="Enter your E-mail Id" id="example-search-input1" />
                  </div>
                </div>
                <div class="form-group row">
                  <label for="example-search-input" class="col-3 col-form-label">Contact</label>
                  <div class="col-9">
                    <input class="form-control" type="number" placeholder="+91-9898989898" id="example-search-input" />
                  </div>
                </div>
                <div class="form-group row">
                  <label for="exampleTextarea" class="col-3 col-form-label">Message</label>
                  <div class="col-9"> <textarea class="form-control" id="exampleTextarea" rows="3"></textarea> </div>
                </div>
                <div class="form-group row">
                  <div class="col-12 text-right"> <button class="btn btn-primary">Submit</button> </div>
                </div>
              </div>
            </li>
          </ul>
        </li> -->
      </ul>
      <ul class="nav-right">
        <li class="header-notification lng-dropdown">
          <a href="javascript:void(0)" id="dropdown-active-item">
            <img src="{{url('public/seller/images/flags/ENGLISH.jpg')}}" alt="" /> English
          </a>
          <ul class="show-notification">
            <li>
              <a href="javascript:void(0)" data-lng="en">
                <img src="{{url('public/seller/images/flags/ENGLISH.jpg')}}" alt="" /> English
              </a>
            </li>
            <li>
              <a href="javascript:void(0)" data-lng="es">
              <img src="{{url('public/seller/images/flags/SPAIN.jpg')}}" alt="" /> Spanish
              </a>
            </li>
            <li>
              <a href="javascript:void(0)" data-lng="pt">
              <img src="{{url('public/seller/images/flags/PORTO.jpg')}}" alt="" /> Portuguese
              </a>
            </li>
            <li>
              <a href="javascript:void(0)" data-lng="fr">
              <img src="{{url('public/seller/images/flags/FRANCE.jpg')}}" alt="" /> French
              </a>
            </li>
          </ul>
        </li>
        <!-- <li class="header-notification">
          <a href="index.php" class="displayChatbox fs-16 text-primary">
            <i class="ti-comments"></i>
            <span class="whr-8 mr-1 bg-primary"></span> Active
          </a>
        </li> -->
        <li class="header-notification">
          <a href="/notification">
            <img src="{{url('public/seller/images/notification.png')}}"/>
            <!-- <i class="ti-bell fs-18"></i> -->
            <!-- <span class="badge bg-c-pink"></span> -->
          </a>
          <ul class="show-notification">
            <li>
              <h6>Notifications</h6>
              <!-- <label class="label label-danger">New</label> -->
            </li>
            <div class="scroll">
              <li>
                <div class="media">
                  <!-- <img class="d-flex align-self-center img-radius" src="assets/images/avatar-1.jpg"/> -->
                  <div class="media-body">
                    <h5 class="notification-user">John Doe</h5>
                    <p class="notification-msg">Lorem ipsum dolor sit amet, consectetuer elit.</p>
                    <span class="notification-time">30 minutes ago</span>
                  </div>
                </div>
              </li>
              <li>
                <div class="media">
                  <!-- <img class="d-flex align-self-center img-radius" src="assets/images/avatar-1.jpg"/> -->
                  <div class="media-body">
                    <h5 class="notification-user">John Doe</h5>
                    <p class="notification-msg">Lorem ipsum dolor sit amet, consectetuer elit.</p>
                    <span class="notification-time">30 minutes ago</span>
                  </div>
                </div>
              </li>
              <li>
                <div class="media">
                  <!-- <img class="d-flex align-self-center img-radius" src="assets/images/avatar-1.jpg"/> -->
                  <div class="media-body">
                    <h5 class="notification-user">John Doe</h5>
                    <p class="notification-msg">Lorem ipsum dolor sit amet, consectetuer elit.</p>
                    <span class="notification-time">30 minutes ago</span>
                  </div>
                </div>
              </li>
              <li>
                <div class="media">
                  <!-- <img class="d-flex align-self-center img-radius" src="assets/images/avatar-1.jpg"/> -->
                  <div class="media-body">
                    <h5 class="notification-user">Joseph William</h5>
                    <p class="notification-msg">Lorem ipsum dolor sit amet, consectetuer elit.</p>
                    <span class="notification-time">30 minutes ago</span>
                  </div>
                </div>
              </li>
              <li>
                <div class="media">
                  <!-- <img class="d-flex align-self-center img-radius" src="assets/images/avatar-1.jpg"/> -->
                  <div class="media-body">
                    <h5 class="notification-user">Sara Soudein</h5>
                    <p class="notification-msg">Lorem ipsum dolor sit amet, consectetuer elit.</p>
                    <span class="notification-time">30 minutes ago</span>
                  </div>
                </div>
              </li>
            </div>
            <li class="p-0">
              <a href="#" class="d-block p-3 border-top-1 text-center">Seel All</a>
            </li>
          </ul>
        </li>
       
        <li class="user-profile header-notification">
          <a href="javascript:void(0)">
            <img src="{{url('public/admin/images/avatar-1.jpg')}}" class="img-radius" alt="User-Profile-Image" />
            
            <span id="more-details" class="text-gray">{{ Auth::guard('seller')->user()->emailAddress }}</span>
            <i class="ti-angle-down"></i>
          </a>
          <ul class="profile-notification">
            <li>
              <!-- <a href="{{url('seller/profile')}}">
                <i class="ti-user"></i> My Profile
              </a> -->
              <a href="{{url('seller/basic-information')}}">
                <i class="ti-user"></i> Basic Information
              </a>
            </li>
           <!--  <li>
              <a href="#">
                <i class="ti-settings"></i> Settings
              </a>
            </li> -->
            <li>             
              <a href="{{ url('seller/logout') }}">
                <i class="ti-layout-sidebar-left"></i> Logout
              </a>
            </li>
          </ul>
        </li>

        <!-- <li class="header-notification">
          <a href="index.php" class="displayChatbox">
            <img src="assets/images/setting.png"/>
            <i class="ti-comments"></i>
            <span class="badge bg-c-green"></span>
          </a>
        </li> -->
        <!-- <li class="user-profile header-notification">
          <a href="javascript:void(0)">
            <img src="assets/images/avatar-1.jpg" class="img-radius" alt="User-Profile-Image" />
            <span>John Doe</span>
            <i class="ti-angle-down"></i>
          </a>
          <ul class="show-notification profile-notification">
            <li>
              <a href="/profile">
                <i class="ti-user"></i> My Profile
              </a>
            </li>
            <li>
              <a href="/setting">
                <i class="ti-settings"></i> Settings
              </a>
            </li>
            <li>
              <a href="javascript:void(0)"(click)='logout()'>
                <i class="ti-layout-sidebar-left"></i> Logout
              </a>
            </li>
          </ul>
        </li> -->
      </ul>
      <!-- <div id="morphsearch" class="morphsearch">
        <div class="form morphsearch-form">
          <input class="morphsearch-input" type="search" placeholder="Search..." />
          <button class="morphsearch-submit" type="submit">Search</button>
        </div>
        <div class="morphsearch-content">
          <div class="dummy-column">
            <h2>People</h2>
            <a class="dummy-media-object img-radius" href="javascript:void(0)">
              <img src="assets/images/avatar-4.jpg" class="img-radius" alt="Sara Soueidan" />
              <h3>Sara Soueidan</h3>
            </a>
            <a class="dummy-media-object img-radius" href="javascript:void(0)">
              <img src="assets/images/avatar-2.jpg" class="img-radius" alt="Shaun Dona" />
              <h3>Shaun Dona</h3>
            </a>
          </div>
          <div class="dummy-column">
            <h2>Popular</h2>
            <a class="dummy-media-object img-radius" href="javascript:void(0)">
              <img src="assets/images/avatar-3.jpg" class="img-radius" alt="PagePreloadingEffect" />
              <h3>Page Preloading Effect</h3>
            </a>
            <a class="dummy-media-object img-radius" href="javascript:void(0)">
              <img src="assets/images/avatar-4.jpg" class="img-radius" alt="DraggableDualViewSlideshow" />
              <h3>Draggable Dual-View Slideshow</h3>
            </a>
          </div>
          <div class="dummy-column">
            <h2>Recent</h2>
            <a class="dummy-media-object img-radius" href="javascript:void(0)">
              <img src="assets/images/avatar-2.jpg" class="img-radius" alt="TooltipStylesInspiration" />
              <h3>Tooltip Styles Inspiration</h3>
            </a>
            <a class="dummy-media-object img-radius" href="javascript:void(0)">
              <img src="assets/images/avatar-3.jpg" class="img-radius" alt="NotificationStyles" />
              <h3>Notification Styles Inspiration</h3>
            </a>
          </div>
        </div>
        <span class="morphsearch-close"><i class="icofont icofont-search-alt-1"></i></span>
      </div> -->
    </div>
  </div>
</nav>
<!-- <div id="sidebar" class="users p-chat-user showChat">
  <div class="had-container">
    <div class="card card_main p-fixed users-main">
      <div class="user-box">
        <div class="card-block">
          <div class="right-icon-control">
            <input type="text" class="form-control  search-text" placeholder="Search Friend" id="search-friends" />
            <div class="form-icon">
              <i class="icofont icofont-search"></i>
            </div>
          </div>
        </div>
        <div class="main-friend-list">
          <div class="media userlist-box" data-id="1" data-status="online" data-username="Josephin Doe" data-toggle="tooltip" data-placement="left" title="Josephin Doe">
            <a class="media-left" href="javascript:void(0)">
              <img class="media-object img-radius img-radius" src="assets/images/avatar-3.jpg" alt="Generic placeholder image " />
              <div class="live-status bg-success"></div>
            </a>
            <div class="media-body">
              <div class="f-13 chat-header">Josephin Doe</div>
            </div>
          </div>
          <div class="media userlist-box" data-id="2" data-status="online" data-username="Lary Doe" data-toggle="tooltip" data-placement="left" title="Lary Doe">
            <a class="media-left" href="javascript:void(0)">
              <img class="media-object img-radius" src="assets/images/avatar-2.jpg" alt="Generic placeholder image" />
              <div class="live-status bg-success"></div>
            </a>
            <div class="media-body">
              <div class="f-13 chat-header">Lary Doe</div>
            </div>
          </div>
          <div class="media userlist-box" data-id="3" data-status="online" data-username="Alice" data-toggle="tooltip" data-placement="left" title="Alice">
            <a class="media-left" href="javascript:void(0)">
              <img class="media-object img-radius" src="assets/images/avatar-4.jpg" alt="Generic placeholder image" />
              <div class="live-status bg-success"></div>
            </a>
            <div class="media-body">
              <div class="f-13 chat-header">Alice</div>
            </div>
          </div>
          <div class="media userlist-box" data-id="4" data-status="online" data-username="Alia" data-toggle="tooltip" data-placement="left" title="Alia">
            <a class="media-left" href="javascript:void(0)">
              <img class="media-object img-radius" src="assets/images/avatar-3.jpg" alt="Generic placeholder image" />
              <div class="live-status bg-success"></div>
            </a>
            <div class="media-body">
              <div class="f-13 chat-header">Alia</div>
            </div>
          </div>
          <div class="media userlist-box" data-id="5" data-status="online" data-username="Suzen" data-toggle="tooltip" data-placement="left" title="Suzen">
            <a class="media-left" href="javascript:void(0)">
              <img class="media-object img-radius" src="assets/images/avatar-2.jpg" alt="Generic placeholder image" />
              <div class="live-status bg-success"></div>
            </a>
            <div class="media-body">
              <div class="f-13 chat-header">Suzen</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="showChat_inner">
  <div class="media chat-inner-header">
    <a class="back_chatBox">
    <i class="icofont icofont-rounded-left"></i> Josephin Doe
    </a>
  </div>
  <div class="media chat-messages">
    <a class="media-left photo-table" href="javascript:void(0)">
    <img class="media-object img-radius img-radius m-t-5" src="assets/images/avatar-3.jpg" alt="Generic placeholder image" />
    </a>
    <div class="media-body chat-menu-content">
      <div class="">
        <p class="chat-cont">I'm just looking around. Will you tell me something about yourself?</p>
        <p class="chat-time">8:20 a.m.</p>
      </div>
    </div>
  </div>
  <div class="media chat-messages">
    <div class="media-body chat-menu-reply">
      <div class="">
        <p class="chat-cont">I'm just looking around. Will you tell me something about yourself?</p>
        <p class="chat-time">8:20 a.m.</p>
      </div>
    </div>
    <div class="media-right photo-table">
      <a href="javascript:void(0)">
      <img class="media-object img-radius img-radius m-t-5" src="assets/images/avatar-4.jpg" alt="Generic placeholder image" />
      </a>
    </div>
  </div>
  <div class="chat-reply-box p-b-20">
    <div class="right-icon-control">
      <input type="text" class="form-control search-text" placeholder="Share Your Thoughts" />
      <div class="form-icon">
        <i class="icofont icofont-paper-plane"></i>
      </div>
    </div>
  </div>
</div> -->

<div class="pcoded-main-container">
  <div class="pcoded-wrapper">
    <nav class="pcoded-navbar">
      <div class="sidebar_toggle"><a href="javascript:void(0)"><i class="icon-close icons"></i></a></div>
      <div class="pcoded-inner-navbar main-menu">
        <div class="">
          
        <!-- <div class="pcoded-search">
          <span class="searchbar-toggle"> </span>
          <div class="pcoded-search-box ">
            <input type="text" placeholder="Search" />
            <span class="search-icon"><i class="ti-search" aria-hidden="true"></i></span>
          </div>
        </div> -->
        <!-- <div class="pcoded-navigatio-lavel h6">Navigation</div> -->
        <ul class="pcoded-item pcoded-left-item left-menu">
          <!-- <li id="dashboardNav" class="pcoded-hasmenu">
            <a href="javascript:void(0)">
              <span class="pcoded-micon"><i class="ti-home"></i></span>
              <span class="pcoded-mtext">Dashboard</span>
              <span class="pcoded-mcaret"></span>
            </a>
             <ul class="pcoded-submenu">
              <li class="active">
                <a href="">
                  <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                  <span class="pcoded-mtext" data-i18n="nav.dash.default">Default</span>
                  <span class="pcoded-mcaret"></span>
                </a>
              </li>
            </ul>
          </li>  -->
          <!-- <li id="dashboardNav">
            <a href="profile">
              <span class="pcoded-micon"><i class="ti-user"></i></span>
              <span class="pcoded-img">
                <img src="assets/images/dashboard.png"/>
              </span>
              <span class="pcoded-mtext">Dashboard</span>
              <span class="pcoded-mcaret"></span>
            </a>
          </li> -->
          <li id="dashboardNav">
            <a href="{{url('seller/dashboard')}}">
              <span class="pcoded-img">
                <img src="{{url('public/seller/images/dashboard.png')}}"/>
              </span>
              <span class="pcoded-mtext">Dashboard</span>
            </a>
          </li>
          <li id="customermanagementNav">
            <a href="{{url('seller/customer-management')}}">
              <span class="pcoded-img">
                <img src="{{url('public/seller/images/customer_management.png')}}"/>
              </span>
              <span class="pcoded-mtext">Customer Management</span>
            </a>
          </li>
          <li id="loyaltyoffersNav">
            <a href="{{url('seller/loyalty-offers')}}">
              <span class="pcoded-img">
                <img src="{{url('public/seller/images/loyalty_offers.png')}}"/>
              </span>
              <span class="pcoded-mtext">Loyality Offers</span>
            </a>
          </li>
          <li id="subscriptionNav">
            <a href="{{url('seller/subscription')}}">
              <span class="pcoded-img">
                <img src="{{url('public/seller/images/subscription.png')}}"/>
              </span>
              <span class="pcoded-mtext">Subscription</span>
            </a>
          </li>
          <li id="mystorecardsNav">
            <a href="my-store-cards.php">
              <span class="pcoded-img">
                <img src="{{url('public/seller/images/my_store_cards.png')}}"/>
              </span>
              <span class="pcoded-mtext">My Store Cards</span>
            </a>
          </li>
          <li id="notificationNav">
            <a href="{{url('seller/notification')}}">
              <span class="pcoded-img">
                <img src="{{url('public/seller/images/notification_management.png')}}"/>
              </span>
              <span class="pcoded-mtext">Notification Management</span>
            </a>
          </li>
          <li id="settingsNav">
            <a href="settings.php">
              <span class="pcoded-img">
                <img src="{{url('public/seller/images/settings.png')}}"/>
              </span>
              <span class="pcoded-mtext">Settings</span>
            </a>
          </li>
        </ul>
      </div>
    </nav>