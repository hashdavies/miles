<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Pania | Forgot Password</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui" />
  <link rel="icon" href="{{url('public/seller/images/favicon.png')}}" type="image/x-icon" />
  <link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600,700,800&display=swap" rel="stylesheet">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css" rel="stylesheet"/>
  <link rel="stylesheet" type="text/css" href="{{url('public/seller/css/bootstrap.min.css')}}" />
  <link rel="stylesheet" type="text/css" href="{{url('public/seller/css/font-awesome.min.css')}}" />
  <link rel="stylesheet" type="text/css" href="{{url('public/seller/css/iEdit.css')}}" />
  <link rel="stylesheet" type="text/css" href="{{url('public/seller/css/jquery.dataTables.min.css')}}" />
  <link rel="stylesheet" type="text/css" href="{{url('public/seller/icon/themify-icons/themify-icons.css')}}" />
  <link rel="stylesheet" type="text/css" href="{{url('public/seller/icon/icofont/css/icofont.css')}}" />
  <link rel="stylesheet" type="text/css" href="{{url('public/seller/css/intlTelInput.css')}}" />
  <link rel="stylesheet" type="text/css" href="{{url('public/seller/css/owl.carousel.css')}}" />
  <link rel="stylesheet" type="text/css" href="{{url('public/seller/css/component.css')}}" />
  <link rel="stylesheet" type="text/css" href="{{url('public/seller/css/style.css')}}" /> 
  <link rel="stylesheet" type="text/css" href="{{url('public/seller/css/globle.css')}}" />
</head>
<body onLoad="noBack();" onpageshow="if (event.persisted) noBack();" onUnload="">
<div class="theme-loader">
  <div class="ball-scale">
    <div class='contain'>
      <div class="ring"><div class="frame"></div></div>
      <div class="ring"><div class="frame"></div></div>
      <div class="ring"><div class="frame"></div></div>
      <div class="ring"><div class="frame"></div></div>
      <div class="ring"><div class="frame"></div></div>
      <div class="ring"><div class="frame"></div></div>
      <div class="ring"><div class="frame"></div></div>
      <div class="ring"><div class="frame"></div></div>
      <div class="ring"><div class="frame"></div></div>
      <div class="ring"><div class="frame"></div></div>
    </div>
  </div>
</div>
<div id="pcoded" class="pcoded iscollapsed expanded">
  <div class="pcoded-overlay-box"></div>
  <div class="pcoded-container navbar-wrapper">
    <section class="login">
      <div class="container">
        <div class="row">
          <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <a href="{{url('seller/dashboard')}}">
              <img src="{{url('public/seller/images/logo.png')}}"/>
            </a>
          </div>
        </div>
        <div class="row">
          <div class="col-12 col-sm-12 col-md-6 col-lg-7 col-xl-7 full-hieght login-left">
            <div class="login-card">
              <h1>Become a <span class="text-primary">Pania</span> partner</h1>
              <h6>Sign in or create account to start selling</h6>
              <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam,</p>
            </div>
          </div>
          <div class="col-12 col-sm-12 col-md-6 col-lg-5 col-xl-5 full-hieght login-right">
            <div class="login-card">
              <div class="m-b-30">
                <h4>Forgot Password</h4>
                <p>Enter your valid mobile number we will send a OTP on  your mobile number.</p>
              </div>
              @if (session('success_message'))
                  <div class="alert alert-success">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> {{ session('success_message') }}
                  </div>
              @endif
              @if (session('error_message'))
                  <span style="color: red">{{ session('error_message') }}</span>
              @endif              
              @if (session('email'))
                  <div class="alert alert-danger">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> {{ session('email') }}
                  </div>
              @endif
              <form class="w-100 form-control-bottom" method="POST" action="{{url('seller/sendforgetlink')}}" autocomplete="off">
                {{ csrf_field() }}
                <div class="m-t-10">
                  <div class="form-group">
                    <label>Mobile Number</label>

                    <input type="text" id="country_code" value="+41" name="countryCode" hidden>

                    <input id="phone" type="tel" autocomplete="off" data-intl-tel-input-id="0" name="mobileNumber" class="form-control" value="{{ old('mobileNumber') }}"/>
                    <span id="valid-msg" class="hide valid">✓ Valid</span>
                    <span id="error-msg" class="hide valid"></span>                   
                    <span style="color: red">{{$errors->first('mobileNumber')}}</span>                     
                  </div>
                  <!-- @error('email')
                      <small id="email-error" class="has-error help-block" style="color: red"> </small>
                  @enderror
 -->
                  <div class="form-group m-t-10">                    
                    <button type="submit" class="btn btn-primary btn-md btn-block waves-effect text-center m-b-20">SEND OTP</button>
                  </div>
                </div>
              </form>              
      
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  </div>
  
  <script type="text/javascript">
    window.history.forward();
    function noBack()
    {
        window.history.forward();
    }
  </script> 
 
  <script type="text/javascript" src="{{url('public/seller/js/jquery.min.js')}}"></script>
  <script type="text/javascript" src="{{url('public/seller/js/jquery-ui.min.js')}}"></script>
  <script type="text/javascript" src="{{url('public/seller/js/popper.min.js')}}"></script>
  <script type="text/javascript" src="{{url('public/seller/js/bootstrap.min.js')}}"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>
  <script type="text/javascript" src="{{url('public/seller/js/owl.carousel.min.js')}}"></script> 
  <script type="text/javascript" src="{{url('public/seller/js/iEdit.js')}}"></script> 
  <script type="text/javascript" src="{{url('public/seller/js/jquery.dataTables.min.js')}}"></script> 
  <script type="text/javascript" src="{{url('public/seller/js/Chart.min.js')}}"></script> 
  <script type="text/javascript" src="https://www.chartjs.org/samples/latest/utils.js"></script> 
  <script type="text/javascript" src="{{url('public/seller/js/common-pages.js')}}"></script> 

  <script type="text/javascript" src="{{url('public/seller/js/intlTelInput.js')}}"></script> 
  <script type="text/javascript" src="{{url('public/seller/js/isValidNumber.js')}}"></script> 
  <script type="text/javascript" src="{{url('public/seller/js/utils.js')}}"></script> 

  <script type="text/javascript">
    $('#phone').keypress(function(e){
      var key = e.which || e.keyCode || e.charCode;
      if (key >= 48 && key <= 57){
        return true;
      }else{
        return false;
      }
    });
  </script>  

</body>
</html>
