    @extends('seller.header')    
    @section('title', 'Store Dashboard')
    @section('content')   
       <div class="pcoded-content">
          <div class="pcoded-inner-content">
            <div class="main-body">
              <div class="page-wrapper">
                <div class="page-body">
                  <div class="row gutter-sm">
                    <div class="col-12 col-sm-5 col-md-5 col-lg-4 form-group">
                      <span class="form-control">Total Followers
                        <span class="text-primary pull-right">250</span>
                      </span>
                    </div>
                    <div class="col-12 col-sm-7 col-md-7 col-lg-8 form-group">
                      <div class="d-flex align-items-center pull-right">
                        <div class="d-flex align-items-center">
                          From<input type="text" class="form-control plr-65 wpx-100 ml-1" placeholder="--/--/----" id="start_date"/>
                          <i class="ml-2 mr-3"><img src="{{url('public/seller/images/calendar.png')}}"></i>
                          To<input type="text" class="form-control plr-65 wpx-100 ml-1" placeholder="--/--/----" id="end_date"/>
                          <i class="ml-2 mr-3"><img src="{{url('public/seller/images/calendar.png')}}"></i>
                        </div>
                        <select class="form-control wpx-100">
                          <option value="Daily">Daily</option>
                          <option value="Weekly">Weekly</option>
                          <option value="Monthly">Monthly</option>
                          <option value="Yearly">Yearly</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="row gutter-sm">
                    <div class="col-12 col-sm-4 col-md-4 full-height">
                      <div class="card bg-c-pink text-white widget-visitor-card">
                        <div class="card-block-small">
                          <h3 class="d-inline-block mb-0 mr-2">250</h3>
                          <span class="d-inline-block text-uppercase">Active Loyalty Offers</span>
                          <img src="{{url('public/seller/images/new_orders.png')}}" class="courner"/>
                        </div>
                      </div>
                    </div>
                    <div class="col-12 col-sm-4 col-md-4 full-height">
                      <div class="card bg-c-blue-light text-white widget-visitor-card">
                        <div class="card-block-small">
                          <h3 class="d-inline-block mb-0 mr-2">20</h3>
                          <span class="d-inline-block text-uppercase">Expired Loyalty Offers</span>
                          <img src="{{url('public/seller/images/scheduled_orders.png')}}" class="courner"/>
                        </div>
                      </div>
                    </div>
                    <div class="col-12 col-sm-4 col-md-4 full-height">
                      <div class="card bg-c-yellow text-white widget-visitor-card">
                        <div class="card-block-small">
                          <h3 class="d-inline-block mb-0 mr-2">10</h3>
                          <span class="d-inline-block text-uppercase">Loyalty Cards Redeemed</span>
                          <img src="{{url('public/seller/images/meal_plans.png')}}" class="courner"/>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-12 col-sm-4 col-md-4 full-height">
                      <div class="card">
                        <div class="card-block-small">
                          <span class="d-inline-block">Average Age Of Customer</span>
                          <h6 class="pull-right mb-0 ml-2 text-primary">22yrs</h6>
                        </div>
                      </div>
                    </div>
                    <div class="col-12 col-sm-4 col-md-4 full-height">
                      <div class="card">
                        <div class="card-block-small">
                          <span class="d-inline-block">Use who earned min. 1 point in past visit to store</span>
                          <h6 class="pull-right mb-0 ml-2 text-primary">575</h6>
                        </div>
                      </div>
                    </div>
                    <div class="col-12 col-sm-4 col-md-4 full-height">
                      <div class="card">
                        <div class="card-block-small">
                          <span class="d-inline-block">Use who earned 2-3 points in past visit to store</span>
                          <h6 class="pull-right mb-0 ml-2 text-primary">575</h6>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-12 col-sm-6 col-md-6 full-height">
                      <div class="card">
                        <div class="card-block-small">
                          <div class="row gutter-xs">
                            <div class="col-12 mb-3">
                              <h6 class="text-capitalize">Men to women ratio visiting the store</h6>
                            </div>
                            <div class="col-12 mb-3">
                              <p class="text-capitalize">Total visitors
                                <span class="text-success ml-2">500</span>
                                <select class="form-control form-control-sm wpx-90 fs-12 pull-right">
                                  <option value="Daily">Daily</option>
                                  <option value="Weekly">Weekly</option>
                                  <option value="Monthly">Monthly</option>
                                  <option value="Yearly">Yearly</option>
                                </select>
                              </p>
                            </div>
                            <div class="col-12 col-sm-7 col-md-7 col-lg-7">
                              <canvas id="chart-area1"></canvas>
                            </div>
                            <div class="col-12 col-sm-5 col-md-5 col-lg-5">
                              <ul class="chart-list">
                                <li>Male Visitors<small>125</small></li>
                                <li>Female Visitors<small>175</small></li>
                              </ul>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 full-height">
                      <div class="card">
                        <div class="card-block-small">
                          <div class="row gutter-xs">
                            <div class="col-12 mb-3">
                              <h6 class="text-capitalize">Redeem Ratio</h6>
                            </div>
                            <div class="col-12 mb-3">
                              <p class="text-capitalize">Total visitors
                                <span class="text-success ml-2">500</span>
                                <select class="form-control form-control-sm wpx-90 fs-12 pull-right">
                                  <option value="Daily">Daily</option>
                                  <option value="Weekly">Weekly</option>
                                  <option value="Monthly">Monthly</option>
                                  <option value="Yearly">Yearly</option>
                                </select>
                              </p>
                            </div>
                            <div class="col-12 col-sm-7 col-md-7 col-lg-7">
                              <canvas id="chart-area2"></canvas>
                            </div>
                            <div class="col-12 col-sm-5 col-md-5 col-lg-5">
                              <ul class="chart-list">
                                <li>User who activates offer but doesn't qualify for redemption<small>125</small></li>
                                <li>User who activates offer and qualify to redeem offer<small>145</small></li>
                              </ul>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 full-height">
                      <div class="card">
                        <div class="card-block-small">
                          <div class="row gutter-xs">
                            <div class="col-12 mb-3">
                              <h6 class="text-capitalize">Age Groups Visiting The Store</h6>
                            </div>
                            <div class="col-12 mb-3">
                              <p class="text-capitalize">Total visitors
                                <span class="text-success ml-2">500</span>
                                <select class="form-control form-control-sm wpx-90 fs-12 pull-right">
                                  <option value="Daily">Daily</option>
                                  <option value="Weekly">Weekly</option>
                                  <option value="Monthly">Monthly</option>
                                  <option value="Yearly">Yearly</option>
                                </select>
                              </p>
                            </div>
                            <div class="col-12 col-sm-7 col-md-7 col-lg-7">
                              <canvas id="chart-area3"></canvas>
                            </div>
                            <div class="col-12 col-sm-5 col-md-5 col-lg-5">
                              <ul class="chart-list success">
                                <li>Age 16 - 30<small>125</small></li>
                                <li>Age 30 - 45<small>245</small></li>
                                <li>Age 45 and above<small>475</small></li>
                              </ul>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 full-height">
                      <div class="card">
                        <div class="card-block-small">
                          <div class="row gutter-xs">
                            <div class="col-12 mb-3">
                              <h6 class="text-capitalize">Loyalty Card Activated vs Redeemed Ratio</h6>
                            </div>
                            <div class="col-12 mb-3">
                              <p class="text-capitalize">Total visitors
                                <span class="text-success ml-2">500</span>
                                <select class="form-control form-control-sm wpx-90 fs-12 pull-right">
                                  <option value="Daily">Daily</option>
                                  <option value="Weekly">Weekly</option>
                                  <option value="Monthly">Monthly</option>
                                  <option value="Yearly">Yearly</option>
                                </select>
                              </p>
                            </div>
                            <div class="col-12 col-sm-7 col-md-7 col-lg-7">
                              <canvas id="chart-area4"></canvas>
                            </div>
                            <div class="col-12 col-sm-5 col-md-5 col-lg-5">
                              <ul class="chart-list success">
                                <li>User who have activated their loyalty offers<small>125</small></li>
                                <li>User who have activated and redeemed their loyalty offers<small>175</small></li>
                              </ul>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-12 col-sm-12 col-md-12 full-height">
                      <div class="card">
                        <div class="card-body">
                          <div class="w-100 pull-left">
                            <div class="wpx-100 pull-right">
                              <select class="form-control">
                                <option value="Daily">Daily</option>
                                <option value="Today">Today</option>
                                <option value="Weekly">Weekly</option>
                                <option value="Monthly">Monthly</option>
                                <option value="Yearly">Yearly</option>
                              </select>
                            </div>
                          </div>
                          <canvas id="canvas"></canvas>
                        </div>
                      </div>                      
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="modal fade" id="success-modal">
          <div class="modal-dialog modal-sm" data-dismiss="modal">
            <div class="modal-content">
              <div class="modal-body pt-4 text-center">
                <img src="{{url('public/seller/images/confirm.png')}}" class="mb-3"/>
                <h6>{{ session()->get('success_message') }}</h6>
              </div>
            </div>
          </div>
        </div>
    @endsection

    @section('footer_script') 

         @if ($message = session()->has('success_message'))
         
          <script type="text/javascript">
             $(function() {
                $('#success-modal').modal('show');
                setTimeout(function() {$('#success-modal').modal('hide');}, 2000);
              });
          </script>
        @endif

        <script>
          //Chart  1
          var config1 = {
            type: 'pie',
            data: {
              datasets: [{
                data: [ '62', '38', ],
                backgroundColor: [ '#a3a1fb', '#edecfe', ],
                label: false,
                borderWidth: 0
              }],
              labels: [ 'Female Visitors', 'Male Visitors', ]
            },
            options: {
              legend: { display: false },
              responsive: true
            }
          };
          //Chart 2
          var config2 = {
            type: 'pie',
            data: {
              datasets: [{
                data: [ '62', '38', ],
                backgroundColor: [ '#a3a1fb', '#edecfe', ],
                label: false,
                borderWidth: 0
              }],
              labels: [ 'Female Visitors', 'Male Visitors', ]
            },
            options: {
              legend: { display: false },
              responsive: true
            }
          };
          //Chart 3
          var config3 = {
            type: 'pie',
            data: {
              datasets: [{
                data: [ '52', '22', '26', ],
                backgroundColor: [ '#32bca5', '#4ad9c1', '#c9fff6', ],
                label: false,
                borderWidth: 0
              }],
              labels: [ 'Age 16-30', 'Age 30-45', 'Age 45 and above', ]
            },
            options: {
              legend: { display: false },
              responsive: true
            }
          };
          //Chart 4
          var config4 = {
            type: 'pie',
            data: {
              datasets: [{
                data: [ '62', '38', ],
                backgroundColor: [ '#4ad9c1', '#c9fff6', ],
                label: false,
                borderWidth: 0
              }],
              labels: [ 'Female Visitors', 'Male Visitors', ]
            },
            options: {
              legend: { display: false },
              responsive: true
            }
          };
          //Line Chart
          var configg = {
            type: 'line',
            data: {
              labels: [ 'Today' ],
              datasets: [{
                label: 'My First dataset',
                backgroundColor: '#ccc',
                borderColor: '#f00',
                fill: false,
                data: [
                  '25', '65', '95', '55'
                ],
              }]
            },
            options: {
              legend: { display: false },
              responsive: true
            }
          };          
          window.onload = function() {
            // Chart 1
            var ctx1 = document.getElementById('chart-area1').getContext('2d');
            window.myPie = new Chart(ctx1, config1);

            // Chart 2
            var ctx2 = document.getElementById('chart-area2').getContext('2d');
            window.myPie = new Chart(ctx2, config2);

            // Chart 3
            var ctx3 = document.getElementById('chart-area3').getContext('2d');
            window.myPie = new Chart(ctx3, config3);

            // Chart 4
            var ctx4 = document.getElementById('chart-area4').getContext('2d');
            window.myPie = new Chart(ctx4, config4);
            
            //Line Chart
            var ctxg = document.getElementById('canvas').getContext('2d');
            window.myLine = new Chart(ctxg, configg);
          };
        </script>
    @endsection                   