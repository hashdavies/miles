<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Basic Page Needs -->
    <meta charset="UTF-8">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <!-- <title>Myles || Home Page</title> -->
    <!-- Mobile Specific Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!-- Favicon and touch icons  -->
    <link href="{{url('public/website/images/favicon.png')}}" rel="shortcut icon">
    <!-- REVOLUTION LAYERS STYLES -->
    <link rel="stylesheet" type="text/css" href="{{url('public/website/revolution/css/layers.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('public/website/revolution/css/settings.css')}}">
    <!-- Boostrap style -->
    <link rel="stylesheet" type="text/css" href="{{url('public/website/stylesheet/bootstrap.css')}}">
    <!-- Icommon icon -->
    <link rel="stylesheet" type="text/css" href="{{url('public/website/stylesheet/icommon.css')}}">
    <!-- Carousel -->
    <link rel="stylesheet" type="text/css" href="{{url('public/website/stylesheet/owl.carousel.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('public/website/stylesheet/owl.theme.default.min.css')}}">
    <!-- Flipster -->
    <link rel="stylesheet" type="text/css" href="{{url('public/website/stylesheet/flipsternavtabs.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('public/website/stylesheet/jquery.flipster.min.css')}}">
    <!-- Animation Style -->
    <link rel="stylesheet" type="text/css" href="{{url('public/website/stylesheet/animate.css')}}">
    <!-- FancyBox -->
    <link rel="stylesheet" type="text/css" href="{{url('public/website/fancybox/jquery.fancybox.css')}}" media="screen"/>
    <!-- Theme style -->
    <link rel="stylesheet" type="text/css" href="{{url('public/website/stylesheet/style.css')}}">
    <!-- Colors -->
    <link rel="stylesheet" type="text/css" href="{{url('public/website/stylesheet/colors/color1.css')}}" id="colors"> 
    <!-- Responsive -->
    <link rel="stylesheet" type="text/css" href="{{url('public/website/stylesheet/responsive.css')}}">


    <script id="mcjs">!function(c,h,i,m,p){m=c.createElement(h),p=c.getElementsByTagName(h)[0],m.async=1,m.src=i,p.parentNode.insertBefore(m,p)}(document,"script","https://chimpstatic.com/mcjs-connected/js/users/23b99bf4a8281263aec99f25b/06c98d1e4f7c4a2570a5c5ece.js");</script>

</head>

<body class="home header_sticky onepage">

<div class="boxed">



    <div id="preloader">

        <div id="preloader-status"></div>

    </div>



    <div class="header-wrap">

        <header id="header" class="header clearfix">

            <div class="container big">

                <div class="row">

                    <div class="col-md-12">

                        <div class="logo logo-top" >                  

                            <a href="{{url('/')}}" title="Landing Page">

                                <!-- <img class="site-logo one" src="images/logo.png" alt="logo"/> -->
                                <img class="site-logo"  src="{{url('public/website/images/logo-default-slim.png')}}" alt="logo"/>

                            </a>

                        </div><!-- /.logo -->

                        <!-- <a href="#" class="button-menu center">Contact Us</a> -->

                        <div class="nav-wrap">

                            <nav id="mainnav" class="mainnav">

                                <ul class="menu main-menu" id="top-menu">

                                    <li class="menu-item">

                                    <a href="{{url('/')}}#home" class="active">Home</a>

                                    <!-- <ul class="sub-menu">

                                    <li class="menu-item"><a href="index.html" class="active">Home 01</a></li>

                                    <li class="menu-item"><a href="index-v2.html">Home 02</a></li>

                                    </ul> -->

                                    </li>

                                    <li class="menu-item">

                                    <a href="{{url('/')}}#service">Service Providers</a>

                                    </li>

                                    <li class="menu-item">

                                    <a href="{{url('/')}}#features">Features</a>

                                    </li>

                                    <li class="menu-item">

                                    <a href="{{url('/')}}#how-it-works">How It Works</a>

                                    </li>

                                    <li class="menu-item">

                                    <a href="{{url('/')}}#contact">Contact Us</a>

                                    </li>

                                    <!-- <li class="menu-item">

                                    <a href="#download">Download</a>

                                    </li> -->

                                    <!-- <li class="menu-item">

                                    <a href="#">Blog</a>

                                    <ul class="sub-menu">

                                    <li><a href="blog-list.html">Blog List</a>

                                    <li><a href="blog-list-small.html">Blog List Small</a>

                                    <li><a href="blog-single-s1.html">Blog Single Style 01</a>

                                    </li>

                                    <li><a href="blog-single-s2.html">Blog Single Style 02</a>

                                    </li>

                                    </ul>

                                    </li> -->

                                </ul>

                            </nav><!-- #site-navigation -->  

                            <div class="btn-menu">

                                <span></span>

                            </div><!-- //mobile menu button -->

                        </div><!-- /.nav-wrap -->



                    </div><!-- /.col-sm-12 -->

                </div><!-- /.row -->

            </div><!-- /.container -->

        </header><!-- /.header -->   

    </div> <!-- /.header-wrap -->


    @yield('content')

        <footer class="flat-call-back text-center">

        <div class="container">

            <div class="row">

                <div class="col-lg-12">
                    <!-- <h3 class="text-white mb-20">Add Link For</h3> -->
                    <ul>
                        <li><a href="{{url('about')}}">About Us</a></li>
                        <li><a href="{{url('commitment')}}">Our Commitment</a></li>
                        <li><a href="{{url('faqs')}}">FAQs</a></li>
                        <li><a href="{{url('covid-19')}}">COVID-19 Statement</a></li>
                        @foreach($CmsPageCustom as $rowPage)
                           <li><a href="{{url('page/'.$rowPage->id)}}">{{$rowPage->title}}</a></li>
                        @endforeach
                    </ul>

                </div><!-- /.col-md-12 -->



            </div><!-- /.row -->

        </div><!-- /.container -->

    </footer><!-- /.flat-call-back -->

   

    <div class="bottom copy-right">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-4 col-sm-12">
                    <div id="logo-footer" class="logo-ft" >                  
                        <a href="index.html"  title="Template">
                           <img class="site-logo-ft" src="{{url('public/website/images/logo-default-slim.png')}}" alt="image" data-retina="images/logo-default-slim.png"/>
                        </a>
                    </div>
                </div><!-- /.col-lg-4 col-sm-12 -->
                <div class="col-lg-4 col-sm-12">
                    <div class="copyright">
                        <p>© <a href="#">Myles</a> 2020. All rights reserved.</p>
                    </div>
                </div><!-- /.col-lg-4 col-sm-12 -->
                <div class="col-lg-4 col-sm-12">
                    <ul class="flat-socials">
                        <li class="facebook">
                            <a href="https://www.facebook.com/Myles-App-113434033826968/" target="_blank" rel="alternate" title="Facebook">
                                <i class="fa fa-facebook"></i>
                            </a>
                        </li>
                        <li class="instagram">
                            <a href="https://www.instagram.com/myles.app" target="_blank" rel="alternate" title="Instagram">
                                <i class="fa fa-instagram"></i>
                            </a>
                        </li>
                        <li class="twitter">
                            <a href="https://www.twitter.com/myles_app" target="_blank" rel="alternate" title="Twitter">
                                <i class="fa fa-twitter"></i>
                            </a>
                        </li>
                        <li class="linkedin">
                            <a href="https://www.linkedin.com/company/myles-app" target="_blank" rel="alternate" title="Linkedin"> <i class="fa fa-linkedin"></i>
                            </a>
                        </li>
                    </ul><!-- /.flat-socials -->      
                </div><!-- /.col-lg-4 col-sm-12 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.bottom -->
    <div class="go-top" title="Go top">
        <i class="fa fa-chevron-up" aria-hidden="true"></i>
    </div><!-- /.go top -->
    </div><!-- /.boxed -->

    <script src="{{url('public/website/js/jquery.min.js')}}"></script>
   
    <!-- <script src="{{url('public/website/js/counter.js')}}"></script> -->
       
    <script src="{{url('public/website/js/popper.js')}}"></script>
    <script src="{{url('public/website/js/bootstrap.min.js')}}"></script>

    <!-- Fancybox -->
    <script  src="{{url('public/website/fancybox/jquery.fancybox.js')}}"></script>
    <script  src="{{url('public/website/fancybox/jquery.fancybox.pack.js')}}"></script>
    <script  src="{{url('public/website/js/jquery.easing.js')}}"></script>
    <!--script  src="js/switcher.js"></script-->
    <script  src="{{url('public/website/js/jquery.cookie.js')}}"></script>
    <script  src="{{url('public/website/js/main.js')}}"></script>
    <!-- Counter Number -->
    <script src="{{url('public/website/js/jquery.waypoints.min.js')}}"></script>
    <!-- <script src="{{url('public/website/js/jquery.counterup.js')}}"></script> -->
    <!-- flipster -->
    <script src="{{url('public/website/js/jquery.flipster.min.js')}}"></script>
    <!-- Revolution Slider -->
    <script src="{{url('public/website/revolution/js/jquery.themepunch.tools.min.js')}}"></script>
    <script src="{{url('public/website/revolution/js/jquery.themepunch.revolution.min.js')}}"></script>
    <!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
    <script  src="{{url('public/website/revolution/js/extensions/revolution.extension.actions.min.js')}}"></script>
    <script  src="{{url('public/website/revolution/js/extensions/revolution.extension.carousel.min.js')}}"></script>
    <script  src="{{url('public/website/revolution/js/extensions/revolution.extension.kenburn.min.js')}}"></script>
    <script  src="{{url('public/website/revolution/js/extensions/revolution.extension.layeranimation.min.js')}}"></script>
    <script  src="{{url('public/website/revolution/js/extensions/revolution.extension.migration.min.js')}}"></script>
    <script  src="{{url('public/website/revolution/js/extensions/revolution.extension.navigation.min.js')}}"></script>
    <script  src="{{url('public/website/revolution/js/extensions/revolution.extension.parallax.min.js')}}"></script>
    <script  src="{{url('public/website/revolution/js/extensions/revolution.extension.slideanims.min.js')}}"></script>
    <script  src="{{url('public/website/revolution/js/extensions/revolution.extension.video.min.js')}}"></script> 
    <script src="{{url('public/website/js/rev-slider.js')}}"></script>
    <!-- Carousel -->
    <script src="{{url('public/website/js/owl.carousel.min.js')}}"></script>
    <!-- <script src="{{url('public/website/js/owl-carousel.js')}}"></script> -->

    @yield('footerscript')
     <script type="text/javascript">
        var lastId,
        topMenu = $("#top-menu"),
        topMenuHeight = topMenu.outerHeight()+250,
        menuItems = topMenu.find("a"),
        scrollItems = menuItems.map(function(){
        var item = $($(this).attr("href"));
        if (item.length) { return item; }
        });

        menuItems.click(function(e){
        var href = $(this).attr("href"),
        offsetTop = href === "#" ? 0 : $(href).offset().top-topMenuHeight+1;
        $('html, body').stop().animate({
        scrollTop: offsetTop
        }, 300);
        e.preventDefault();
        });

        $(window).scroll(function(){
        var fromTop = $(this).scrollTop()+topMenuHeight;
        var cur = scrollItems.map(function(){
        if ($(this).offset().top < fromTop)
        return this;
        });
        cur = cur[cur.length-1];
        var id = cur && cur.length ? cur[0].id : "";
        
        var myurl="{{ url('/')}}";
        
        var finalurl = myurl+'#'+id;
        // console.log(myurl+'#'+id+);

        if (lastId !== id) {
        lastId = id;
        menuItems
        .parent().removeClass("active")

        .end().filter("[href='+finalurl+']").parent().addClass("active");
        }
        });
        </script>
  
</body>
</html>