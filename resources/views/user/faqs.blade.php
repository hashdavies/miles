@extends('user.header')
  @section('content')
   <title>Myles | FAQs</title>  
     <div class="content-page">
	<div class="container">	
		<h3>FAQs</h3>
        @foreach($faq as $rowfaq)
    		<h4>Q: {{$rowfaq->question}}</h4>
            A: <p>{!!$rowfaq->answer!!} </p>
        @endforeach
	</div>
</div>
@endsection