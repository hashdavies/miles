@extends('user.header')
  @section('content')

    <title>Myles | Home</title>
    <style type="text/css">
        .joinwhitlist{
            color: #ffb636;
        }
    </style>
     <div class="page-wrap home-1" id="home">

        <div id="rev_slider_1_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-source="gallery">

            <!-- START REVOLUTION SLIDER 5.4.7.3 fullwidth mode -->

            <div id="rev_slider_1_1" class="rev_slider fullwidthabanner font-eina" data-version="5.4.7.3">

                <ul>    <!-- SLIDE  -->
                    <li data-index="rs-1" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="300"  data-rotate="0"  data-saveperformance="off"  data-title="Slide">
                       <!-- MAIN IMAGE -->
                        <img src="{{url('public/website/images/slide.jpg')}}"  alt="image" title="slide"  width="1920" height="115vh" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
                        <!-- LAYERS -->
                        <!-- LAYER NR. 1 -->
                        <div class="tp-caption tp-resizeme title-revslider" 
                             id="slide-1-layer-1" 
                             data-x="center" data-hoffset="1" 
                             data-y="center" data-voffset="[-252,-202,-202,-100]" data-width="['auto']"
                            data-height="['auto']" data-type="text" data-responsive_offset="on" 
                            data-frames='[{"delay":10,"speed":1120,"frame":"0","from":"y:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                            data-textAlign="['center','center','center','center']"
                            data-paddingtop="[0,0,0,0]"
                            data-paddingright="[0,0,0,0]"
                            data-paddingbottom="[0,0,0,0]"
                            data-paddingleft="[0,0,0,0]">
                            An on-demand marketplace <br>where items can be posted for speedy <br> and efficient dispatch </div>
                        <div class="tp-caption rev-btn rev-withicon  tp-resizeme button btn-slider" 
                             id="apple-btn" 
                             data-x="center" data-hoffset="[-88,-88,-88,-88]" 
                             data-y="center" data-voffset="[-75,-13,-13,70]" 
                            data-width="['auto']"
                            data-height="['auto']"
                            data-type="button" 
                            data-responsive_offset="on" 
                            data-frames='[{"delay":1540,"speed":1500,"frame":"0","from":"x:[-175%];y:0px;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:1;","mask":"x:[100%];y:0;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeOut"},
                            {"delay":"wait","speed":1000,"frame":"999","to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"},

                           {"frame":"hover","speed":"300","ease":"Power3.easeInOut","to":"o:1;rX:0;rY:0;rZ:0;z:0;"}]'
                            data-textAlign="['center','center','center','center']"
                            data-paddingtop="[0]"
                            data-paddingright="[0]"
                            data-paddingbottom="[0]"
                            data-paddingleft="[0]">
                            <img src="{{url('public/website/images/playstore.png')}}" class="wpx-145"  data-toggle="modal" data-target="#join-modal"/>
                        </div>
                        <!-- LAYER NR. 4 -->
                        <div class="tp-caption rev-btn rev-withicon  tp-resizeme button btn-slider" 
                             id="android-btn" 
                             data-x="center" data-hoffset="[84,84,84,84]" 
                             data-y="center" data-voffset="[-75,-13,-13,70]" 
                            data-width="['auto']"
                            data-height="['auto']"
                            data-type="button" 
                            data-responsive_offset="on" 
                            data-frames='[{"delay":1540,"speed":1500,"frame":"0","from":"x:[175%];y:0px;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:1;","mask":"x:[-100%];y:0;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeOut"},
                            {"delay":"wait","speed":1000,"frame":"999","to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"},
                            {"frame":"hover","speed":"300","ease":"Power3.easeInOut","to":"o:1;rX:0;rY:0;rZ:0;z:0;"}]'
                            data-textAlign="['center','center','center','center']"
                            data-paddingtop="[0]"
                            data-paddingright="[0]"
                            data-paddingbottom="[0]"
                            data-paddingleft="[0]">
                            <img src="{{url('public/website/images/appstore.png')}}" class="wpx-145"  data-toggle="modal" data-target="#join-modal"/>
                        </div>
                        <!-- LAYER NR. 5 -->
                        <div class="tp-caption   tp-resizeme" 
                             id="slide-1-layer-3" 
                             data-x="center" data-hoffset="9" 
                             data-y="520" 
                            data-width="['none','none','none','none']"
                            data-height="['none','none','none','none']"
                            data-type="image" 
                            data-responsive_offset="on" 
                            data-frames='[{"delay":2290,"speed":1650,"frame":"0","from":"y:bottom;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                            data-textAlign="['inherit','inherit','inherit','inherit']"
                            data-paddingtop="[0,0,0,0]"
                            data-paddingright="[0,0,0,0]"
                            data-paddingbottom="[0,0,0,0]"
                            data-paddingleft="[0,0,0,0]">
                            <!-- <div class="text-center">
                                <button type="button" class="btn btn-md btn-primary mb-4 joinwhitlist" data-toggle="modal" data-target="#join-modal" style="padding: 6px 10px;font-size: 18px"><strong>Join The Waitlist</strong></button>
                            </div> 
  -->
                            <div class="text-center">
                                <button type="button" class="btn btn-md btn-primary mb-4 joinwhitlist joinwhitlistBtn" data-toggle="modal" data-target="#join-modal"><strong>Join The Waitlist</strong></button>

                            </div> 

                            <img src="{{url('public/website/images/mobile-slide.png')}}" alt="mobile image" data-ww="577px" data-hh="519px" width="577" height="519" data-no-retina> 
                        </div>
                    </li>
                </ul>
            </div>
        </div><!-- END REVOLUTION SLIDER -->
        <section class="mobile-feature mf1">
            <div class="container">
                <div class="row flat-row">
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 section-left">
                        <figure>
                           <img src="{{url('public/website/images/home-02.png')}}" alt="image">
                        </figure> 
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 section-right">
                        <div class="title-box">
                            <div class="icon-title">
                                <span class="icon-noun_173935_cc"></span>
                            </div>
                            <div class="title-section style1">

                                <h2 class="title">

                                    Services on the app include

                                </h2>

                            </div>

                            <div class="title-content">
                                <h4 class="bold text-purpal">Junk removal</h4>
                                <p>Haul items such as broken electronics, outdated electronics, unwanted household items to the nearest eco station</p>
                                <h4 class="bold text-purpal">Delivery</h4>
                                <p>Pick up and delivery of anything, anywhere, must be legal of course</p>
                                <h4 class="bold text-purpal">Moving</h4>
                                <p>Pack up your stuff and move from one location to another</p>
                                <h4 class="bold text-purpal">Others</h4>
                                <p>Get bulk items from costco, get towed, get grandma's casserole, bring a new pet home, get cargo from the airport, etc.</p>
                                <!-- <a class="button style2" href="#">See Case Study</a> -->

                            </div>

                        </div><!-- /.title-box -->     

                    </div><!-- /.col-sm-5 -->

                </div><!-- /.row -->

            </div><!-- /.container -->

        </section><!-- /.mobile-feature -->



        <section id="service" class="mobile-feature mf2">

            <div class="container">

                <div class="row flat-row">

                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 section-left">

                        <div class="title-box">

                            <div class="icon-title">

                                <span class="icon-noun_581945_cc"></span>

                            </div>

                            <div class="title-section style1">

                                <h2 class="title">

                                    Register as a Service Provider

                                </h2>

                            </div>

                            <div class="title-content">
                                <ul class="listing">
                                    <li>To pick up gigs and earn extra cash</li>
                                    <li>To turn your truck/van into a revenue generating tool</li>
                                    <li>To keep busy during your spare time</li>
                                    <li>To work around your current reality</li>
                                </ul>
                                <a id="learn-more" href="#">Learn more</a>

                            </div>

                        </div><!-- /.title-box -->

                     

                      
                    </div>

                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 section-right">

                        <figure>

                            <img src="{{url('public/website/images/mobile-slide-2.png')}}" alt="image">

                        </figure>

                    </div><!-- /.col-sm-6 -->

                </div><!-- /.row -->

            </div><!-- /.container -->

        </section><!-- /.mobile-feature -->



        <section id="features" class="big-feature big-ft-1">

            <div class="container-fluid">

                <div class="row ">

                    <div class="col-sm-12 col-lg-6 section-left">

                        <img class="right" src="{{url('public/website/images/home-03.png')}}" alt="image">

                    </div>



                    <div class="col-sm-12 col-lg-6 section-right color-white">

                        <div class="section-wrap">

                            <div class="title-section style2">

                                <h2 class="title">Where it all 'appens.</h2>

                            </div>

                            

                            <div class="iconbox">

                                <div class="iconbox-icon">

                                    <div class="icon gas-icon">

                                        <span class=""><img src="{{url('public/website/images/post_gig.svg')}}"/></span>

                                    </div>

                                </div>

                                <div class="iconbox-content">

                                    <h3 class="sub-title color-white">Post a Gig</h3>

                                    <p>Users post gigs with descriptions, pictures, and their budget; it could be for junk removal, moving, or delivery sevices, etc.</p>

                                </div>

                            </div><!-- /.iconbox -->

                            <div class="iconbox">

                                <div class="iconbox-icon">

                                    <div class="icon" >

                                        <span class=""><img src="{{url('public/website/images/assign_gig.svg')}}"/></span>

                                    </div>

                                </div>

                                <div class="iconbox-content">

                                    <h3 class="sub-title color-white"> Assign Gig</h3>

                                    <p>Users assign gigs to Service Providers that best meet their needs i.e. vehicle type, cost, delivery time, etc.</p>

                                </div>

                            </div><!-- /.iconbox -->

                            <div class="iconbox">

                                <div class="iconbox-icon">

                                    <div class="icon">

                                        <span class=""><img src="{{url('public/website/images/rating_review.svg')}}"/></span>

                                    </div>

                                </div>

                                <div class="iconbox-content">

                                    <h3 class="sub-title color-white">Review and Ratings</h3>

                                    <p>Users and Service Providers exchange reviews to guide future actions of the Myles community members</p>

                                </div>

                            </div><!-- /.iconbox -->

                        </div><!-- /.section-wrapper -->

                    </div> <!-- /.col-md-6 -->



                </div><!-- /.row-->

            </div><!-- /.container-fluid -->

        </section><!-- /.big-feature -->



        <section id="how-it-works" class="section-video bg-gray">

            <div class="container">

                <div class="row">

                    <div class="col-sm-12">

                        <div class="title-section center">

                            <h2 class="title">

                                See how it works

                            </h2>

                        </div><!-- /.title-section -->

                    </div>

                    <div class=" col-12 col-sm-6 text-center mb-3">

                        <div class="video-wrap">                        

                            <a  class="fancybox" data-type="iframe" href="{{url('public/website/images/User-Video.mp4')}}"><img  src="{{url('public/website/images/video-1.png')}}" width="1170" height="650" alt="image"/>
                                <div class="icon-play">
                                   <span class="icon-noun_498864_cc"></span>
                                </div>
                            </a>

                        </div><!-- /.section-video --> 

                        <h4 class="text-purpal bold">

                            Users

                        </h4>

                    </div><!-- /.col-md-12 -->

                    <div class=" col-12 col-sm-6 text-center mb-3">

                        <div class="video-wrap">                        

                            <!-- <a  class="fancybox" target="_blanck" data-type="iframe" href="https://www.youtube.com/embed/LHO9BdVDJVM?autoplay=1"><img  src="images/video.jpg" width="1170" height="650" alt="image"/>

                                <div class="icon-play">

                                   <span class="icon-noun_498864_cc"></span>

                                </div>

                            </a> -->                             

                            <a  class="fancybox" data-type="iframe" href="{{url('public/website/images/Service-Provider-Video.mp4')}}"><img  src="{{url('public/website/images/video-2.png')}}" width="1170" height="650" alt="image"/>
                                <div class="icon-play">
                                   <span class="icon-noun_498864_cc"></span>
                                </div>
                            </a>

                        </div><!-- /.section-video --> 

                        <h4 class="text-purpal bold">

                            Service Providers

                        </h4>

                    </div><!-- /.col-md-12 -->

                </div><!-- /.row -->

            </div><!-- /.container -->

        </section><!-- /.section-video -->

        <section id="contact" class="section-conatct">

            <div class="container">

                <div class="row">

                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 text-center mb-4">
                        <h2 class="text-white bold mb-3">Contact Us</h2>
                    </div>
                    <div class="col-12 col-sm-12 col-md-8 col-lg-8 mx-auto">
                        
                        <form action="{{url('add_contact_user')}}" class="row" method="post" id="AddContactData" enctype="multipart/form-data">
                            @csrf
                        
                            <div class="form-group col-12 col-sm-6 col-md-6">
                                <input type="text" name="conname" id="conname" class="form-control" placeholder="Full Name">
                                <span style="color: red; display:none;" id="connameError">Enter name</span>
                            </div>
                            <div class="form-group col-12 col-sm-6 col-md-6">
                                <input type="email" name="conemail" id="conemail" class="form-control" placeholder="Email Address">
                                <span style="color: red; display:none;" id="conemailError">Enter email address</span>
                            </div>
                            <div class="form-group col-12 col-sm-6 col-md-6">
                                <input type="tel" name="conmobileno" id="conmobileno" class="form-control" placeholder="Mobile Number">
                                <span style="color: red; display:none;" id="conmobilenoError">Enter mobile number</span>
                            </div>
                            <div class="form-group col-12 col-sm-6 col-md-6">
                                <input type="text" name="concity" id="concity" class="form-control" placeholder="City, Province">
                                <span style="color: red; display:none;" id="concityError">Enter city</span>
                            </div>
                            <div class="form-group col-12 col-sm-12 col-md-12">
                                <textarea rows="4" name="conmessage" id="conmessage" class="form-control" placeholder="Type Your Message Here..."></textarea>
                                <span style="color: red; display:none;" id="conmessageError">Enter message</span>
                            </div>
                            <div class="col-12 col-sm-12 col-md-12 text-center">
                                <button type="submit" class="btn btn-lg btn-white pl-5 pr-5">Submit</button>
                            </div>
                        </form>
                    </div>

                </div><!-- /.row -->

            </div><!-- /.container -->

        </section><!-- /.section-video -->   

    </div>

    
<!-- join-modal -->
<div class="modal fade" id="join-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body p-4">
                
                
                <form action="{{url('add_user')}}" method="post" id="AddUser" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <input class="form-control" type="text" name="name" id="name" placeholder="Name">
                        <span style="color: red; display:none;" id="nameError">Enter Name</span>
                    </div>
                    <div class="form-group">
                        <input class="form-control" type="email" name="email" id="email" placeholder="Email">
                        <span style="color: red; display:none;" id="emailError">Enter Email address</span>
                    </div>
                    <div class="form-group">
                        <input class="form-control" type="text" name="city" id="city" placeholder="City/Town">
                        <span style="color: red; display:none;" id="cityError">Enter City</span>
                    </div>
                    <div class="form-group">
                        <select class="form-control" name="role" id="role">
                            <option value="">Are You a</option>
                            <option value="Service Provider">Service Provider</option>
                            <option value="User">User</option>
                            <option value="Both">Both</option>
                        </select>
                        <span style="color: red; display:none;" id="roleError">Enter your role</span>
                    </div>
                    <div class="text-center mb-2">
                        <button type="submit" class="btn btn-primary btn-md btn-round">SUBMIT</button>
                        <button type="button" class="btn btn-danger ml-2 btn-md btn-round" data-dismiss="modal">CANCEL</button>
                    </div>
                </form>
            
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="message-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body p-4">
                <h5 class="text-center mb-3">{{ session()->get('success_message')}}</h5>
            </div>
        </div>
    </div>
</div>

@endsection

    @section('footerscript')
        
    <script type="text/javascript">      
         
          $('#AddContactData').on('submit', function(e){        
              $('#connameError').hide();
              $('#conemailError').hide();
              $('#conmobilenoError').hide();
              $('#concityError').hide();
              $('#conmessageError').hide();

              var conname = $('#conname').val();
              var conemail = $('#conemail').val();
              var conmobileno = $('#conmobileno').val();
              var concity = $('#concity').val();
              var conmessage = $('#conmessage').val();
                       
              if(conname == ''){
                $('#connameError').show();
                e.preventDefault();
              }
              if(conemail == ''){
                $('#conemailError').show();
                e.preventDefault();
              }
              if(conmobileno == ''){         
                $('#conmobilenoError').show();
                e.preventDefault();
              } 
              if(concity == ''){         
                $('#concityError').show();
                e.preventDefault();
              }
              if(conmessage == ''){         
                $('#conmessageError').show();
                e.preventDefault();
              }          
          });
    </script>

    <script type="text/javascript">      
          
          $('#AddUser').on('submit', function(e){        
              $('#nameError').hide();
              $('#emailError').hide();
              $('#cityError').hide();
              $('#roleError').hide();

              var name = $('#name').val();
              var email = $('#email').val();
              var city = $('#city').val();
              var role = $('#role').val();
             
              if(name == ''){
                $('#nameError').show();
                e.preventDefault();
              }
              if(email == ''){
                $('#emailError').show();
                e.preventDefault();
              }
              if(city == ''){         
                $('#cityError').show();
                e.preventDefault();
              } 
              if(role == ''){         
                $('#roleError').show();
                e.preventDefault();
              }          
          });
      </script>

      @if ($message = session()->has('success_message'))
        <script type="text/javascript">
           $(function() {
              $('#message-modal').modal('show');
              setTimeout(function() {$('#message-modal').modal('hide');}, 2000);
            });
        </script>
      @endif
    
   
        <script type="text/javascript">
          $(document).ready(function(){
            $('.onepage .mainnav > ul > li > a').on('click',function() {     
              console.log('has click');      
              var anchor = $(this).attr('href').split('#')[1];
              var largeScreen = matchMedia('only screen and (min-width: 992px)').matches;
              var headerHeight = 0;
              headerHeight = $('.header').height();
              if ( anchor ) {
                if ( $('#'+anchor).length > 0 ) {
                  if ( $('.upscrolled').length > 0 && largeScreen ) {
                    headerHeight = headerHeight;
                  } else {
                    headerHeight = 0;
                  }                   
                  var target = $('#'+anchor).offset().top - headerHeight;
                  $('html,body').animate({scrollTop: target}, 1000, 'easeInOutExpo');
                }
              }
              return false;
            });

            $('.onepage .mainnav ul > li > a').on( 'click', function() {
              $( this ).addClass('active').parent().siblings().children().removeClass('active');
              $(this).parents('#mainnav-mobi').hide();            
            });
          });
        </script>

@endsection