@extends('user.header')
  @section('content')

<title>Myles | About Us</title>
<div class="content-page">
	<div class="container">	
		<h3>{{$page->title}}</h3>

		 
	       <p>{!! @$page->content !!}</p>
	    	
	</div>
</div>
@endsection