@extends('layouts.header')

@section('content')

<!--testimonial section area  start-->
<section class="section-self change-password" style="background-image:url({{ asset('public/frontend/assets/img/login/bg.png') }}); background-position:top center; background-size:cover;">
  <div class="testimonial_inner">
    <div class="container">
      <div class="row">
        <div class="col-lg-7 col-md-7"></div>
        <div class="col-lg-4 col-md-5">
         <div class="row">
          <div class="otp_form otp-box">
             <div class="col-md-12 mb-4">
              <div class="otp-white">
                <h2>Enter Verification Code</h2>
              <p>We have sent you a 4 digit verification code on your registered number</p>
              </div>
              </div>
              
              

              @if(!empty($id))
                 <form method="POST" action="{{ url('user/otpmatch') }}">   
                 <input type="hidden" name="id" value="@if(!empty($id)) {{ $id }} @endif">  

              @else
                 <form method="POST" action="{{ url('user/verify') }}">              
              @endif

             
              @csrf            
              <div class="col-md-12">
                <div class="count-down">
                  <p id="demo"></p>
                </div>
                <div class="otp">
                    <ul>                       
                        <li class="otp-labl"><input class="otp1 form-control" name="otp1" type="text" placeholder="0" maxlength="1"></li>
                        <li class="otp-labl"><input class="otp1 form-control" name="otp2" type="text" placeholder="0" maxlength="1"></li>
                        <li class="otp-labl"><input class="otp1 form-control" name="otp3" type="text" placeholder="0" maxlength="1"></li>
                        <li class="otp-labl"><input class="otp1 form-control" name="otp4" type="text" placeholder="0" maxlength="1"></li>
                    </ul>                     
                </div>
                <span style="color: red">{{ $errors->first('otp1') }}</span><br>
                <span style="color: red">{{ $errors->first('otp2') }}</span><br>
                <span style="color: red">{{ $errors->first('otp3') }}</span><br>
                <span style="color: red">{{ $errors->first('otp4') }}</span>

                @if(session()->has('msg'))
                    <div class="alert alert-danger">
                        {{ session()->get('msg') }}
                    </div>
                @endif
              </div>
              <div class="col-md-12 mt-4">
                <div class="login_submit">
                  <button type="submit d-block" onclick="window.location.href='change-password.php';">Verified
                  </button>
                </div>             
              </div>
            </form>

             <div class="resend-otp">
                <a href="javascript:void(0);">Resend OTP</a>
              </div>
            </div>
          </div>
        </div>
    </div>
  </div>
</section>
<!--testimonial section area  end-->
@endsection


@section('footer_script')
<!-- JS
============================================ -->
<!-- jQuery JS -->
<script src="{{ asset('frontend/assets/js/vendor/jquery-1.12.4.min.js') }}"></script>
<!-- Popper JS -->
<script src="{{ asset('frontend/assets/js/popper.min.js') }}"></script>
<!-- Bootstrap JS -->
<script src="{{ asset('frontend/assets/js/bootstrap.min.js') }}"></script>
<!-- Plugins JS -->
<script src="{{ asset('frontend/assets/js/plugins.js') }}"></script>
<!-- Ajax Mail -->
<script src="{{ asset('frontend/assets/js/ajax-mail.js') }}"></script>
<!-- Main JS -->
<script src="{{ asset('frontend/assets/js/main.js') }}"></script>
<script>
// Set the date we're counting down to
var countDownDate = new Date("Jan 5, 2021 15:37:25").getTime();

// Update the count down every 1 second
var x = setInterval(function() {

  // Get today's date and time
  var now = new Date().getTime();
    
  // Find the distance between now and the count down date
  var distance = countDownDate - now;
    
  // Time calculations for days, hours, minutes and seconds
  var days = Math.floor(distance / (1000 * 60 * 60 * 24));
  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60*60)+3);
  var seconds = Math.floor((distance % (1000 * 60)) / 1000);
    
  // Output the result in an element with id="demo"
  document.getElementById("demo").innerHTML = "0" +minutes + "<span>:" + seconds+"</span>";
    
  // If the count down is over, write some text 
  if (distance < 0) {
    clearInterval(x);
    document.getElementById("demo").innerHTML = "EXPIRED";
  }
}, 1000);
</script>
<script type="text/javascript">
    $('.otp .otp-labl .otp1').keyup(function (e) {
      var key = e.which || e.keyCode || e.charCode;
      if (key == 8 || key == 46) {
        let is_first_child = $($(this).parent('.otp-labl')).is(':first-child');
        if (!is_first_child)
          $(this).parent('.otp-labl').prev('.otp-labl').children('.otp1').trigger("select");
        return;
      }
    
      $(this).parent('.otp-labl').next('.otp-labl').children('.otp1').trigger("select");
      
      if (String.fromCharCode(e.keyCode).match(/[^0-9]/g)) return false;    
    });
</script>

@endsection 