<?php include "include/header-login.php"?>
<!--header area end-->
<!--testimonial section area  start-->
<section class="section-self register " style="background-image:url(assets/img/login/bg.png); background-position:top center; background-size:cover;">
  <div class="testimonial_inner">
    <div class="container">
      <div class="row">
        <div class="col-lg-7 col-md-7"></div>
        <div class="col-lg-4 col-md-5">
          <div class="account_form">
            <form action="#">
              <h2 class="text-center">Create an Account</h2>
              <div class="row mt-4">
                <div class="col-lg-12 mb-20">
                  <label>Username <span>*</span></label>
                  <div class="input-container">
                    <i class="fa fa-user icon"></i>
                    <input type="text" placeholder="Enter your name">
                  </div>
                </div>
                <div class="col-lg-12 mb-20">
                  <label>E-mail Address/Mobile Number <span>*</span></label>
                  <div class="input-container">
                    <i class="fa fa-envelope-o icon"></i>
                    <input type="email" placeholder="Enter emaid id/mobile number">
                  </div>
                </div>
                <div class="col-12 mb-20">
                  <label>Password</label>
                  <div class="input-container">
                    <i class="fa fa-lock icon"></i>
                    <input type="password" placeholder="Enter password" id="myInput">
                    <button type="button" class="my-eye" onclick="myFunction()"><i class="fa fa-eye"></i></button>
                  </div>
                </div>
                <div class="col-12 mb-20">
                  <label for="country">Country<span>*</span></label>
                  <div class="input-container">
                    <i class="fa fa-map-marker icon"></i>
                    <select class="niceselect_option" name="cuntry" id="country">
                      <option value="2">bangladesh</option>
                      <option value="3">Algeria</option>
                      <option value="4">Afghanistan</option>
                      <option value="5">Ghana</option>
                      <option value="6">Albania</option>
                      <option value="7">Bahrain</option>
                      <option value="8">Colombia</option>
                      <option value="9">Dominican Republic</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-12 login_submit mt-3">
                  <button type="submit d-block">Register</button>
                </div>
              </div>
            </form>
          </div>
        </div>
        <div class="col-md-1"></div>
      </div>
    </div>
  </div>
</section>
<!--testimonial section area  end-->
<!-- JS
============================================ -->
<!-- jQuery JS -->
<script src="assets/js/vendor/jquery-1.12.4.min.js"></script>
<!-- Popper JS -->
<script src="assets/js/popper.min.js"></script>
<!-- Bootstrap JS -->
<script src="assets/js/bootstrap.min.js"></script>
<!-- Plugins JS -->
<script src="assets/js/plugins.js"></script>
<!-- Ajax Mail -->
<script src="assets/js/ajax-mail.js"></script>
<!-- Main JS -->
<script src="assets/js/main.js"></script>

</body>

</html>
