<!doctype html>
<html class="no-js" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>A2A eCommerce</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">  
  <link rel="stylesheet" href="assets/css/bootstrap.min.css">
  <link rel="stylesheet" href="assets/css/plugins.css">
  <link rel="stylesheet" href="assets/css/style.css">
  <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
  <link rel="stylesheet" href="assets/css/owl.theme.default.min.css">
  <script src="assets/js/vendor/modernizr-2.8.3.min.js"></script>
  <script src="assets/js/owl.carousel.js"></script>
</head>
<body>
  <!--header area start-->
  <header class="header_area header_three">
    <div class="header_inner">
      <!--header middel css here-->
      <div class="header_login">
        <div class="container">
          <div class="row align-items-center">
            <div class="col-lg-2 col-md-3">
              <div class="logo">
                <a href="index.php"><img src="assets/img/login/logo.png" alt=""></a>
              </div>
            </div>
            <div class="col-lg-10 col-md-9">
              <div class="row">
                <div class="col-lg-12">
                  <div class="social-link-list">
                    <ul>
                      <li>Log in with <span><button class="btn-facebook" onclick="window.location.href='https://www.facebook.com';"><img src="assets/img/icon/fb.png">Facebook</button></span></li>
                      <li>or <span><button class="btn-google" onclick="window.location.href='https://accounts.google.com/signin/v2/identifier?continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&service=mail&sacu=1&rip=1&flowName=GlifWebSignIn&flowEntry=ServiceLogin';"><img src="assets/img/icon/google-icon.png">Google</button></span></li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="row less-mgn">
                <div class="col-lg-2">
                  <h6 class="have-account">Already have an Account?</h6>
                </div>
                <div class="col-lg-4">
                  <div class="form-group">
                    <input type="text" class="form-control border-primary" placeholder="Mobile number/Email-address"/>
                  </div>
                </div>
                <div class="col-lg-4">
                  <div class="form-group">
                    <input type="password" class="form-control border-primary" placeholder="Password"/>
                  </div>
                </div>
                <div class="col-lg-2">
                  <button class="btn btn-signin btn-block" onclick="window.location.href='create-profile.php';">Sign In</button>
                </div>
              </div>
              <div class="row">
                <div class="col-lg-2"></div>
                <div class="col-lg-4">
                  <div class="remeber-me">
                    <label><input type="checkbox"/><span>Remember Me</span></label>
                  </div>
                </div>
                <div class="col-lg-4">
                  <div class="remeber-me">
                   <a href="register-number.php">Forgotten your Password ?</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!--header middel css here-->
    </div>
  </header>
  <script>
function myFunction() {
  var x = document.getElementById("myInput");
  if (x.type === "password") {
    x.type = "text";
  } else {
    x.type = "password";
  }
}
</script>
  <!--header area end-->
