@extends('layouts.header')
<!--header area end-->

@section('content')
<!--testimonial section area  start-->
<section class="section-self change-password" style="background-image:url({{ asset('frontend/assets/img/login/bg.png') }}); background-position:top center; background-size:cover;">
  <div class="testimonial_inner">
    <div class="container">
      <div class="row">
        <div class="col-lg-7 col-md-7"></div>
        <div class="col-lg-4 col-md-5">
         <div class="row">
          <div class="otp_form otp-box">           
            <div class="register-white">
             <div class="col-md-12 mb-4 text-center">
              <p>Enter your registered mobile number</p>
              </div>
            
            <form method="POST" action="{{ url('user/resetpassword') }}">
            @csrf
            <div class="col-md-12">
               <div class="form-group">
                <input type="text" class="text-center" name="email_number">
                </div>
             </div>
             <span style="color: red">{{ $errors->first('email_number') }}</span>

              @if(session()->has('msg'))
                  <div class="alert alert-danger">
                      {{ session()->get('msg') }}
                  </div>
              @endif
            </div>
            <div class="theme-button">
                <button type="submit">Send</button>
              </div>
            </div>

            <div class="col-md-12 mt-4">
              
            </div>
          </div>
        </div>
         <div class="col-lg-1 col-md-2">
        
      </div>
    </div>
  </div>
</section>
<!--testimonial section area  end-->
@endsection

@section('footer_script')
<!-- JS
============================================ -->
<!-- jQuery JS -->
<script src="{{ asset('frontend/assets/js/vendor/jquery-1.12.4.min.js') }}"></script>
<!-- Popper JS -->
<script src="{{ asset('frontend/assets/js/popper.min.js') }}"></script>
<!-- Bootstrap JS -->
<script src="{{ asset('frontend/assets/js/bootstrap.min.js') }}"></script>
<!-- Plugins JS -->
<script src="{{ asset('frontend/assets/js/plugins.js') }}"></script>
<!-- Ajax Mail -->
<script src="{{ asset('frontend/assets/js/ajax-mail.js') }}"></script>
<!-- Main JS -->
<script src="{{ asset('frontend/assets/js/main.js') }}"></script>
@endsection