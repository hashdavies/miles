@extends('layouts.header')
<!--header area end-->

@section('content')
<!--testimonial section area  start-->
<section class="section-self register create-profile" style="background-image:url({{ asset('public/frontend/assets/img/login/bg.png') }}); background-position:top center; background-size:cover;">
  <div class="testimonial_inner">
    <div class="container">
      <div class="row">
        <div class="col-lg-7 col-md-7"></div>
        <div class="col-lg-4 col-md-5">
          <div class="account_form">

            <form action="{{ url('/user/saveprofile') }}" method="post" class="form-horizontal" enctype="multipart/form-data">
              @csrf
              <input type="hidden" name="user_id" value="{{ $profiledata->id }}">
              <h2 class="text-center">Profile Creation</h2>
              <div class="row mt-3">
                <div class="col-md-12">
                 <div class="input-upload">
                 <input type="file" onChange="singleFiles(event)" name="profile_image">
                  <div class="profile-image">
                  <img src="{{ asset('public/frontend/assets/img/home/profile-pic.jpg') }}" id="prof">
                  </div>
                   <i class="fa fa-long-arrow-up upload-icon"  aria-hidden="true"></i>
                   </div> 
                </div>
                <div class="col-lg-12 mb-20">

                  <label>Username <span>*</span></label>
                  <div class="input-container">
                    <i class="fa fa-user icon"></i>
                    <input type="text" placeholder="Enter your name" value="@if(!empty($profiledata)) {{ $profiledata->username }} @endif" name="username">
                  </div>
                  <span style="color: red">{{ $errors->first('username') }} </span>
                </div>
                <div class="col-lg-12 mb-20">
                  <label>E-mail Address/Mobile Number <span>*</span></label>
                  <div class="input-container">
                    <i class="fa fa-envelope-o icon"></i>
                    <input type="text" placeholder="Enter emaid id/mobile number" value="@if(!empty($profiledata)) {{ $profiledata->email_number }} @endif" name="email_number">
                  </div>
                  <span style="color: red">{{ $errors->first('email_number') }} </span>
                </div>
                
                <!--<div class="col-12 mb-20">
                  <label>Password</label>
                  <div class="input-container">
                    <i class="fa fa-lock icon"></i>
                    <input type="password" placeholder="Enter password" id="myInput">
                    <button type="button" class="my-eye" onclick="myFunction()"><i class="fa fa-eye"></i></button>
                  </div>
                </div> -->

                <div class="col-12 mb-20">
                  <label for="country">Country<span>*</span></label>
                  <div class="input-container">
                    <i class="fa fa-map-marker icon"></i>
                    <select class="niceselect_option" name="country" id="country">       

                      <option value="Afghanistan" @if(!empty($profiledata->country) == 'Afghanistan') selected="select" @endif>Afghanistan</option>

                      <option value="Algeria" @if(!empty($profiledata->country) =='Algeria') selected="select" @endif>Algeria</option>

                      <option value="Albania" @if(!empty($profiledata->country) =='Albania') selected="select" @endif>Albania</option> 

                      <option value="Bangladesh" @if(!empty($profiledata->country) =='Bangladesh') selected="select" @endif>Bangladesh</option>

                      <option value="Colombia" @if(!empty($profiledata->country) =='Colombia') selected="select" @endif>Colombia</option>

                      <option value="Bahrain" @if(!empty($profiledata->country) =='Bahrain') selected="select" @endif>Bahrain</option>                                   

                      <option value="Ghana" @if(!empty($profiledata->country) =='Ghana') selected="select" @endif>Ghana</option>

                      <option value="Dominican_Republic" @if(!empty($profiledata->country) =='Dominican_Republic') selected="select" @endif>Dominican Republic</option>
                       
                    </select>
                  </div>
                  <span style="color: red">{{ $errors->first('country') }} </span>
                </div>
                <div class="col-12 mb-20">
                  <label>Town / City <span>*</span></label>
                  <div class="input-container">
                    <i class="fa fa fa-building icon"></i>
                    <input type="text" placeholder="Enter town / city" name="city">
                  </div>
                  <span style="color: red">{{ $errors->first('city') }} </span>
                </div>
                <div class="col-md-12 text-center text-black mb-3 mt-1">
                  <h4>Shipping Address</h4>
                </div>
                <div class="col-12 mb-20">
                  <label>Mobile Number (Whatsapp Optional) <span>*</span></label>
                  <div class="input-container">
                    <i class="fa fa-phone icon"></i>
                    <input placeholder="Enter mobile number" type="text" name="mobile_number">
                  </div>
                </div>
                <div class="col-12 mb-20">
                  <label>Alternative Number (Whatsapp Optional)</label>
                  <div class="input-container">
                    <i class="fa fa-phone icon"></i>
                    <input placeholder="Enter mobile number" type="text" name="alt_mobile_number">
                  </div>
                </div>
                <div class="col-12 mb-20">
                  <label>House/Flat/Building Number <span>*</span></label>
                  <div class="input-container">
                    <i class="fa fa fa-building icon"></i>
                    <input type="text" placeholder="Enter house/flat/building number" name="shipping_address">
                  </div>
                  <span style="color: red">{{ $errors->first('shipping_address') }} </span>
                </div>
                <div class="col-lg-12 mb-20">
                  <label>Street Number<span>*</span></label>
                  <div class="input-container">
                    <i class="fa fa fa-building icon"></i>
                    <input type="text" placeholder="Enter street number" name="street">
                  </div>
                  <span style="color: red">{{ $errors->first('street') }} </span>
                </div>
                <div class="col-lg-12 mb-20">
                  <label> Zip Code <span>*</span></label>
                  <div class="input-container">
                    <i class="fa fa fa-building icon"></i>
                    <input type="text" placeholder="Enter zip code" name="zip_code">
                  </div>
                  <span style="color: red">{{ $errors->first('zip_code') }} </span>
                </div>
                <div class="col-md-12 login_submit mt-3">
                  <button type="submit d-block">Save </button>
                </div>
              </div>
            </form>

          </div>
        </div>
        <div class="col-md-1"></div>
      </div>
    </div>
  </div>
</section>
<!--testimonial section area  end-->
@endsection


@section('footer_script')
<!-- JS
============================================ -->
<!-- jQuery JS -->
<script src="{{ asset('frontend/assets/js/vendor/jquery-1.12.4.min.js') }}"></script>
<!-- Popper JS -->
<script src="{{ asset('frontend/assets/js/popper.min.js') }}"></script>
<!-- Bootstrap JS -->
<script src="{{ asset('frontend/assets/js/bootstrap.min.js') }}"></script>
<!-- Plugins JS -->
<script src="{{ asset('frontend/assets/js/plugins.js') }}"></script>
<!-- Ajax Mail -->
<script src="{{ asset('frontend/assets/js/ajax-mail.js') }}"></script>
<!-- Main JS -->
<script src="{{ asset('frontend/assets/js/main.js') }}"></script>
<script>
  single = new Array();
  function singleFiles(event) {
    this.single = [];
    var singleFiles = event.target.files;
    if (singleFiles) {
      for (var file of singleFiles) {
        var singleReader = new FileReader();
        singleReader.onload = (e) => {
          this.single.push(e.target.result);
          $('#prof').attr('src', e.target.result)          
        }
        singleReader.readAsDataURL(file);
      }
    }
  }
</script>
@endsection