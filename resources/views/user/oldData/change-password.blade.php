@extends('layouts.header')

<!--header area end-->

@section('content')
<!--testimonial section area  start-->
<section class="section-self change-password" style="background-image:url({{ asset('frontend/assets/img/login/bg.png') }}); background-position:top center; background-size:cover;">
  <div class="testimonial_inner">
    <div class="container">
      <div class="row">
        <div class="col-lg-7 col-md-7"></div>
        <div class="col-lg-4 col-md-5">
         <div class="row">
          <div class="account_form">

 
            <form action="{{ url('user/passwordreset') }}" method="post">
             @csrf
             <input type="hidden" name="id" value="@if(!empty($id)) {{ $id }} @endif">
             <div class="col-md-12 mb-4 text-center">
              <h2>Create new Password</h2>
              <p>we'll ask for this password whenever you sign in</p>
              </div>
            <div class="col-md-12">
               <div class="form-group">
                <label>New Password <span>*</span></label>
                <input type="password" name="password">
                </div>
                <span style="color: red">{{ $errors->first('password') }}</span>
             </div>
             <div class="col-md-12">
                <div class="form-group">
                <label>Confirm Password <span>*</span></label>
                <input type="password" name="c_password">
               </div>
               <span style="color: red"> {{ $errors->first('c_password') }} </span>
             </div>
               <div class="col-md-12 mt-4">
              <div class="login_submit">
                <button type="submit d-block" onclick="window.location.href='register.php';">Save Password
                </button>
                 </div></div>
            </form>


            </div>
          </div>
        </div>
    </div>
  </div>
</section>
@endsection


@section('footer_script')
<!--testimonial section area  end-->
<!-- JS
============================================ -->
<!-- jQuery JS -->
<script src="{{ asset('frontend/assets/js/vendor/jquery-1.12.4.min.js') }}"></script>
<!-- Popper JS -->
<script src="{{ asset('frontend/assets/js/popper.min.js') }}"></script>
<!-- Bootstrap JS -->
<script src="{{ asset('frontend/assets/js/bootstrap.min.js') }}"></script>
<!-- Plugins JS -->
<script src="{{ asset('frontend/assets/js/plugins.js') }}"></script>
<!-- Ajax Mail -->
<script src="{{ asset('frontend/assets/js/ajax-mail.js') }}"></script>
<!-- Main JS -->
<script src="{{ asset('frontend/assets/js/main.js') }}"></script>
@endsection