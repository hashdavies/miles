@extends('user.header')
  @section('content')

<title>Myles | About Us</title>
<div class="content-page">
	<div class="container">	
		<h3>ABOUT US</h3>
		@foreach(@$about as $rowabout)
	       <p>{!! @$rowabout->help !!}</p>
	    @endforeach		
	</div>
</div>
@endsection