<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Myles Invoice {{ date('Y-m-d') }}</title>
   
    <style>

    p.heading {
        font-size: 16px;
        color: #000;
        font-weight: 600;
    }
    tr.items td {
    text-align: left !important;
    }

    tr.total td {
    text-align: left !important;
}
    .invoice-box {
        max-width: 800px;
        margin: auto;
        padding: 0px;
        border: 1px solid #eee;
        box-shadow: 0 0 10px rgba(0, 0, 0, .15);
        font-size: 12px;
        line-height: 24px;
        font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
        color: #000;
    }
    
    .invoice-box table {
        width: 100%;
        line-height: inherit;
        text-align: left;
    }
    
    .invoice-box table td {
        padding: 5px;
        vertical-align: top;
    }
    
    .invoice-box table tr td:nth-child(2) {
        text-align: right;
    }
    
    .invoice-box table tr.top table td {
        padding-bottom: 20px;
    }
    
    .invoice-box table tr.top table td.title {
        font-size: 45px;
        line-height: 45px;
        color: #333;
    }
    
    .invoice-box table tr.information table td {
        padding-bottom: 40px;
    }
    
    .invoice-box table tr.heading td {
        background: #eee;
        border-bottom: 1px solid #ddd;
        font-weight: bold;
        text-align: left;
    }
    
    .invoice-box table tr.details td {
        padding-bottom: 20px;
    }
    
    .invoice-box table tr.item td{
        border-bottom: 1px solid #eee;
    }
    
    .invoice-box table tr.item.last td {
        border-bottom: none;
    }
    
    .invoice-box table tr.total td:nth-child(2) {
        border-top: 2px solid #eee;
        font-weight: bold;
    }
    
    @media only screen and (max-width: 600px) {
        .invoice-box table tr.top table td {
            width: 100%;
            display: block;
            text-align: center;
        }
        
        .invoice-box table tr.information table td {
            width: 100%;
            display: block;
            text-align: center;
        }
    }
    
    /** RTL **/
    .rtl {
        direction: rtl;
        font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
    }
    
    .rtl table {
        text-align: right;
    }
    
    .rtl table tr td:nth-child(2) {
        text-align: left;
    }
    </style>
</head>

<body>
    <div class="invoice-box">
        <table cellpadding="0" cellspacing="0">
            <tr class="top">
                <td colspan="2">
                    <table>
                        <tr>
                            <td class="title">
                                <img src="http://dev.business-startup.in/img/logo.png">
                            </td>
                            
                            <td class="heading" style="color: red">
                                (Original for Recipient)
                            </td>

                        </tr>
                    </table>
                </td>
            </tr>
            
            <tr class="information">
                <td colspan="2">
                    <table>
                        <tr>
                            <td>
                                <p class="heading">Sold By :</p>
                                {{$storeName}}<br>
                                {{$company_email}}<br>
                                {{$phoneno}}<br>
                                {{$physicaladdress}},{{$city}},{{$country}}
                            </td>
                            
                            <td>
                                <p class="heading">Shipping Address :</p>
                                {{$customerName}}<br>
                                {{$customerMobile}},
                                {{$altcustomerMobile}}<br>
                                @php echo wordwrap($deliveryAddress,50,"<br>", true) @endphp <br>
                                PinCode : {{$pincode}}<br>
                                Landmark : {{$landmark}}<br>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            </table>
       
            <table cellpadding="0" cellspacing="0">
            <tr class="heading">
                <td>Sl.No</td>
                <td>Description</td>
                <td>Unit Price</td>
                <td>Qty</td>
                <td>Discount</td>
                <td>Tax Type</td>
                <td>Tax Amount</td>
                <td>Total Amount</td>
            </tr>
            
            @foreach($items as $key=>$row)
            <tr class="items">
                <td>{{$key+1}}</td>
                <td> @php echo wordwrap($row->title,40,"<br>", true) @endphp
                    @if($row->attribute) 
                    <span style="color: red"> {{$row->attribute}} </span>
                    @endif
                    @if($row->subattribute) 
                    <span style="color: red"> - {{$row->subattribute}} </span>
                    @endif
                    <br>
                    <sm class="vatNote"> (Inclusive of VAT)</sm></td>
                <td>$ {{$row->unitPrice}} </td>
                <td>{{$row->qty}}</td>
                <td>$ {{$row->discount}}</td>
                <td>{{$row->taxType}}</td>
                <td>$ {{$row->taxAmt}}</td> 
                <td>$ {{$row->salePrice}}</td> 
            </tr>
            @endforeach
            
            @if($type =='vendor')
             <tr class="total">
                <td colspan="7"></td>
                
                <td width="100px">
                   Total: $ {{$paidAmt}}
                </td>
            </tr>
            @endif
            
            @if($type =='admin' || $type =='user')
            <tr class="total">
                <td colspan="7"></td>
                <td width="120px">
                   Subtotal: $ {{$orderTotal}}
                </td>
            </tr>


            @if($couponDiscount != 'null')
            <tr class="total">
                <td colspan="7"></td>
                <td width="120px">
                   Coupon: -$ {{$couponDiscount}}
                </td>
            </tr>
            @endif
            <tr class="total">
                <td colspan="7"></td>
                
                <td width="120px">
                   Shipping: $ {{$shipping}}
                </td>
            </tr>
            <tr class="total">
                <td colspan="7"></td>
                
                <td width="100px">
                   Total: $ {{$paidAmt}}
                </td>
            </tr>
            @endif
        </table>
   
    </div>
      <div class="information" style="position: absolute; bottom: 0;background: #e27070;color: #fff;padding: 10px;">
            <table width="100%">
                <tr>
                     <td align="left">
                        &copy; {{ date('Y') }} <a href="{{ config('app.url') }}" target="_blank" style="color: #fff;"> - All rights reserved.<a>
                    </td>
                    <td align="right" style="width: 50%;">
                        {{config('app.name')}}
                    </td>
                </tr>

            </table>
        </div>
</body>
</html>