<!doctype html>
<html class="no-js" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>A2A eCommerce</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Favicon -->
 <!-- <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.ico">-->
  <link rel="stylesheet" href="{{ url('public/frontend/assets/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ url('public/frontend/assets/css/plugins.css') }}">
  <link rel="stylesheet" href="{{ url('public/frontend/assets/css/style.css') }}">
  <link rel="stylesheet" href="{{ url('public/frontend/assets/css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ url('public/frontend/assets/css/owl.theme.default.min.css') }}">

  <script src="{{ url('public/frontend/assets/js/vendor/modernizr-2.8.3.min.js') }}"></script>
  <script src="{{ url('public/frontend/assets/js/owl.carousel.js') }}"></script>
</head>
<body>
  <!--header area start-->
  <header class="header_area header_three">
    <div class="header_inner">
      <!--header top css here-->
      <div class="header_top">
        <div class="container">
          <div class="row">
            <div class="col-lg-3 col-md-3">
              <div class="language_select">
                <ul>
                  <li class="language"><a href="#">English <i class="fa fa-angle-down"></i></a>
                    <ul class="dropdown_language">
                      <li><a href="#">French</a></li>
                      <li><a href="#">German</a></li>
                    </ul>
                  </li>
                </ul>
              </div>
            </div>
            <div class="col-lg-9 col-md-9">
              <div class="header_top_menu text-right">
                <ul>
                  <li><a href="#">SAVE MORE ON APP</a></li>
                  <li><a href="#">SELL ON A2AEXPRESS</a></li>
                  <li><a href="#">CUSTOMER CARE </a></li>
                  <li><a href="#">TRACK MY ORDER</a></li>

                  @if (Route::has('login'))
                    
                        @auth
                          <li><a href="#"> {{ Auth::user()->username }} </a></li>                            
                        @else
                            <li><a href="{{ route('login') }}">Login</a></li>
                            <li><a href="{{ route('register') }}">Register</a></li>
                        @endauth
                    
                  @endif
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!--header top css end-->
      <!--header middel css here-->
      <div class="header_middel">
        <div class="container">
          <div class="row align-items-center">
            <div class="col-lg-3 col-md-3">
              <div class="logo">
                <a href="{{ url('/') }}"><img src="{{ asset('public/frontend/assets/img/login/logo.png') }}" alt=""></a>
              </div>
            </div>
            <div class="col-lg-6 col-md-6">
              <div class="header_search">
                <form action="#">
                  <input placeholder="Search in A2AEXPRESS..." type="text">
                  <button type="submit"><i class="fa fa-search"></i></button>
                  <div class="img-search">
                    <img src="{{ asset('public/frontend/assets/img/home/camera.png') }}">
                  </div>
                </form>
              </div>
            </div>
            <div class="col-lg-3 col-md-3">
              <div class="shipping_cart">
                <a href="#">
                  <img src="{{ asset('public/frontend/assets/img/home/chat.png') }}">
                </a>
                <a href="#">
                  <span class="cart_icon"><img src="{{ asset('public/frontend/assets/img/home/cart.png') }}"></span>
                </a>
                <!--mini cart-->
                <div class="mini_cart">
                  <div class="cart_item">
                    <div class="cart_img">
                      <a href="product-details.html"><img src="{{ asset('public/frontend/assets/img/s_product/product1.jpg') }}" alt=""></a>
                    </div>
                    <div class="cart_info">
                      <a href="product-details.html">Etiam molestie</a>
                      <span class="cart_price">£95.00</span>
                      <div class="cart_remove">
                        <a title="Remove this item" href="#"><i class="fa fa-times-circle"></i></a>
                      </div>
                    </div>
                  </div>
                  <div class="cart_item">
                    <div class="cart_img">
                      <a href="product-details.html"><img src="{{ asset('public/frontend/assets/img/s_product/product2.jpg') }}" alt=""></a>
                    </div>
                    <div class="cart_info">
                      <a href="product-details.html">Congue lectus</a>
                      <span class="cart_price">£115.00</span>
                      <div class="cart_remove">
                        <a title="Remove this item" href="#"><i class="fa fa-times-circle"></i></a>
                      </div>
                    </div>
                  </div>
                  <div class="total_price">
                    <span> Subtotal: </span>
                    <span class="prices"> $160.00 </span>
                  </div>
                  <div class="cart_button">
                    <a class="button" href="checkout.html"> Check out</a>
                  </div>
                </div>
                <!--mini cart end-->
                <a href="#">
                  <img src="{{ asset('public/frontend/assets/img/home/user.png') }}">
                </a>
                <div class="mini_cart user">
                  <div class="cart_item">
                    <button class="btn btn-danger d-block w-100">Sign In</button>
                    <div class="text-center">
                      <a href="#" class="d-block mt-2">New Customer? Start here</a>
                    </div>
                  </div>
                  <div class="cart_item">
                    <a href="#">My Account</a>
                  </div>
                  <div class="cart_item">
                    <a href="#">Settings</a>
                  </div>
                  <div class="cart_item">
                    <a href="#">Change Password</a>
                  </div>
                  <div class="cart_item">
                    <a href="#">Terms & Conditions</a>
                  </div>
                  <div class="cart_item">
                    <a href="#">Help Support</a>
                  </div>
                  <div class="cart_item">
                    <a href="#">Legal</a>
                  </div>
                  <div class="cart_item">
                    <a href="#">Feedback</a>
                  </div>
                  <div class="cart_item">
                    <a href="#">Invite Friends </a>
                  </div>
                  <div class="cart_item">
                    <a href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                    
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!--header middel css here-->
    </div>
  </header>
  <script>
function myFunction() {
  var x = document.getElementById("myInput");
  if (x.type === "password") {
    x.type = "text";
  } else {
    x.type = "password";
  }
}


</script>


  <!--header area end-->

  @yield('content')


  @yield('footer_script')
</body>
</html>