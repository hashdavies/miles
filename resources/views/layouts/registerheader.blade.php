<!doctype html>
<html class="no-js" lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>A2A eCommerce</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Favicon -->
 <!-- <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.ico">-->
  <link rel="stylesheet" href="{{ url('public/frontend/assets/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ url('public/frontend/assets/css/plugins.css') }}">
  <link rel="stylesheet" href="{{ url('public/frontend/assets/css/style.css') }}">
  <link rel="stylesheet" href="{{ url('public/frontend/assets/css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ url('public/frontend/assets/css/owl.theme.default.min.css') }}">

  <script src="{{ url('public/frontend/assets/js/vendor/modernizr-2.8.3.min.js') }}"></script>
  <script src="{{ url('public/frontend/assets/js/owl.carousel.js') }}"></script>
</head>
<body>
  <!--header area start-->
  <header class="header_area header_three">
    <div class="header_inner">
      <!--header middel css here-->
      <div class="header_login">
        <div class="container">
          <div class="row align-items-center">
            <div class="col-lg-2 col-md-3">
              <div class="logo">
                <a href="index.php"><img src="{{ url('public/frontend/assets/img/login/logo.png') }}" alt=""></a>
              </div>
            </div>
            <div class="col-lg-10 col-md-9">
              <div class="row">
                <div class="col-lg-12">
                  <div class="social-link-list">
                    <ul>
                      <li>Log in with <span><button class="btn-facebook" onclick="window.location.href='https://www.facebook.com';"><img src="{{ asset('public/frontend/assets/img/icon/fb.png') }}">Facebook</button></span></li>
                      <li>or <span><button class="btn-google" onclick="window.location.href='https://accounts.google.com/signin/v2/identifier?continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&service=mail&sacu=1&rip=1&flowName=GlifWebSignIn&flowEntry=ServiceLogin';"><img src="{{ asset('public/frontend/assets/img/icon/google-icon.png') }}">Google</button></span></li>
                    </ul>
                    
                  </div>
                </div>
              </div>


            <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
              @csrf
              <div class="row less-mgn">
                <div class="col-lg-2">
                  <h6 class="have-account">Already have an Account?</h6>
                </div>            
                <div class="col-lg-4">
                  <div class="form-group">
                    <input type="text" class="form-control border-primary" name="email_number" value="{{ old('email_number') }}" placeholder="Mobile number/Email-address"/>
                     <span style="color: red">{{ $errors->first('email_number') }}</span>
                  </div>
                </div>
                <div class="col-lg-4">
                  <div class="form-group">
                    <input type="password" class="form-control border-primary" name="password" placeholder="Password"/>
                    <span style="color: red">{{ $errors->first('password') }}</span>
                  </div>
                </div>
                <div class="col-lg-2">
                  <button class="btn btn-signin btn-block">Sign In</button>
                </div>
              </div>
              <div class="row">
                <div class="col-lg-2"></div>
                <div class="col-lg-4">
                  <div class="remeber-me">
                    <label><input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}><span>Remember Me</span></label>
                  </div>
                </div>
                <div class="col-lg-4">
                  <div class="remeber-me">
                      <a class="btn btn-link" href="{{ url('user/forgot-password') }}">
                          {{ __('Forgotten your Password ?') }}
                      </a>
                  
                  </div>
                </div>
              </div>
            </form>
            

            </div>
          </div>
        </div>
      </div>
      <!--header middel css here-->
    </div>
  </header>
  <script>
function myFunction() {
  var x = document.getElementById("password");
  if (x.type === "password") {
    x.type = "text";
  } else {
    x.type = "password";
  }
}
</script>
  <!--header area end-->

  @yield('content')


  @yield('footer_script')
</body>
</html>
