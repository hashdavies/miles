@extends('admin.header')
@section('title', 'Support Management')
@section('content')
  @include('admin/navbar')

     <div class="responsiveTbPage ExplorerListPage">
  <div class="layout-content">
    <div class="layout-content-body">
      <div class="title-bar">
        <h1 class="title-bar-title">
          <span class="d-ib">Support Management</span>
        </h1>
      </div>
      <div class="row">
        <div class="col-xs-12">
          <div class="card">
            <div class="card-body">
              <form class="form-group formIcons">
                <div class="col-12 position-relative">
                  <input type="text" name="search" class="form-control pl-5 bg-search" placeholder="Search">
                </div>
              </form>
              <div class="row">
                <div class="col-12 text-center">
                  <button class="btn btn-primary wpx-195">Users</button>
                  <button class="btn active wpx-195 text-dark" onclick="window.location='{{url('admin/service-providers-support')}}'">Service Providers</button>
                </div>
              </div>
              <div class="col-12 support_box">
                <div class="row">
                  <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                    <div class="full-height">
                      <div class="card m-0">
                        <div class="p-10">
                          <div class="media">
                            <div class="media-middle media-left">
                              <span class="bg-silver circle sq-48">
                                <span class="icon icon-user"></span>
                              </span>
                            </div>
                            <div class="media-middle media-body">
                              <h5 class="media-heading mt-3">User Name</h5>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="full-height">
                      <div class="card m-0">
                        <div class="p-10">
                          <div class="media">
                            <div class="media-middle media-left">
                              <span class="bg-silver circle sq-48">
                                <span class="icon icon-user"></span>
                              </span>
                            </div>
                            <div class="media-middle media-body">
                              <h5 class="media-heading mt-3">User Name</h5>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="full-height">
                      <div class="card m-0">
                        <div class="p-10">
                          <div class="media">
                            <div class="media-middle media-left">
                              <span class="bg-silver circle sq-48">
                                <span class="icon icon-user"></span>
                              </span>
                            </div>
                            <div class="media-middle media-body">
                              <h5 class="media-heading mt-3">User Name</h5>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="full-height">
                      <div class="card m-0">
                        <div class="p-10">
                          <div class="media">
                            <div class="media-middle media-left">
                              <span class="bg-silver circle sq-48">
                                <span class="icon icon-user"></span>
                              </span>
                            </div>
                            <div class="media-middle media-body">
                              <h5 class="media-heading mt-3">User Name</h5>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div><div class="full-height">
                      <div class="card m-0">
                        <div class="p-10">
                          <div class="media">
                            <div class="media-middle media-left">
                              <span class="bg-silver circle sq-48">
                                <span class="icon icon-user"></span>
                              </span>
                            </div>
                            <div class="media-middle media-body">
                              <h5 class="media-heading mt-3">User Name</h5>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div><div class="full-height">
                      <div class="card m-0">
                        <div class="p-10">
                          <div class="media">
                            <div class="media-middle media-left">
                              <span class="bg-silver circle sq-48">
                                <span class="icon icon-user"></span>
                              </span>
                            </div>
                            <div class="media-middle media-body">
                              <h5 class="media-heading mt-3">User Name</h5>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
                    <div class="card">
                      <div class="card-header">
                        <a href="javascript:void(0);">
                          <i class="icon icon-user pr-1 text-muted"></i>
                        </a>  
                        <span class="user-name heading-font-family fw-400">User Name</span> 
                          <button type="button" class="close pull-right" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                      </div>
                      <!-- /.card-header -->
                      <div class="card-body">
                        <div class="widget-chat-activity">
                          <div class="scrollbar-enabled">
                           

                            <div class="message media reply">
                              <figure class="thumb-xs">
                                <a href="#">
                                  <img src="https://cdn.pixabay.com/photo/2017/05/13/23/05/img-src-x-2310895_960_720.png" class="rounded-circle" alt="">
                                </a>
                              </figure>
                              <div class="message-body media-body">
                                <p>Epic Cheeseburgers come in all kind of styles.</p>
                              </div>
                              <!-- /.message-body -->
                            </div>
                            <!-- /.message -->
                        
                             
                            <!-- /.message -->
                            <div class="message media">
                              <figure class="thumb-xs">
                                <a href="#">
                                  <img src="https://cdn.pixabay.com/photo/2017/05/13/23/05/img-src-x-2310895_960_720.png" class="rounded-circle" alt="">
                                </a>
                              </figure>
                              <div class="message-body media-body">
                                <p>If you are a vegan, we are sorry for you loss.</p>
                              </div>
                              <!-- /.message-body -->
                            </div>
                            <!-- /.message -->


                          </div>
                          <!-- /.messages -->
                        </div>
                        <!-- /.widget-chat-acitvity -->
                      </div>
                      <!-- /.card-body -->
                      <form action="#" class="card-footer m-0 p-3" method="post">
                        <div class="d-flex justify-content-end">
                          <button type="button" class="btn-primary border-0 rounded-left"><i class="fa fa-plus-circle"></i></button>
                          <button type="button" class="btn-secondary border-0"><i class="fa fa-picture-o"></i></button>
                          <button type="button" class="btn-warning border-0"><i class="fa fa-smile-o"></i></button>

                          <input class="form-control p-1 pr-2 pl-2 rounded-0" placeholder="Type your message here"/>

                          <button type="submit" class="btn-primary border-0 rounded-right"><i class="fa fa-paper-plane"></i></button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
 
<!-- Block Modal -->
<div class="modal fade" id="block-modal">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-body text-center">
        <h2><i class="fa fa-toggle-off text-success"></i></h2>
        <h5>Are you sure you want to block this user?</h5>
        <div class="text-center">
          <button type="button" data-dismiss="modal" class="btn btn-success btn-sm">
            Yes
          </button>
          <button type="button" data-dismiss="modal" class="btn btn-danger btn-sm">
            No
          </button>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Unblock Modal -->
<div class="modal fade" id="unblock-modal">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-body text-center">
        <h2><i class="fa fa-toggle-on text-danger"></i></h2>
        <h5>Are you sure you want to unblock this user?</h5>
        <div class="text-center">
          <button type="button" data-dismiss="modal" class="btn btn-success btn-sm">
            Yes
          </button>
          <button type="button" data-dismiss="modal" class="btn btn-danger btn-sm">
            No
          </button>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Delete Modal -->
<!-- <div class="modal fade" id="delete-modal">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-body text-center">
        <h2><i class="fa fa-trash-o text-danger"></i></h2>
        <h5>Are you sure you want to delete this user?</h5>
        <div class="text-center">
          <button type="button" data-dismiss="modal" class="btn btn-success btn-sm">
            Yes
          </button>
          <button type="button" data-dismiss="modal" class="btn btn-danger btn-sm">
            No
          </button>
        </div>
      </div>
    </div>
  </div>
</div> -->

<div class="modal fade" id="set-taxes">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header bg-primary">
        <h5 class="modal-title">Set Taxes</h5>
        <button type="button" data-dismiss="modal" class="close">&times;</button>
      </div>
      <div class="modal-body">
        <form>
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
               <label>Select Province</label>
               <select class="form-control">
                <option>choose</option>
                <option>choose</option>
                <option>choose</option>
              </select>
            </div> 
          </div>  
          <div class="col-md-12">
            <div class="form-group">
             <label>Set Tax % on offer amount</label>
             <select class="form-control">
              <option>choose</option>
              <option>choose</option>
              <option>choose</option>
            </select>
          </div> 
        </div>  
        <div class="col-md-12">
          <div class="form-group">
           <label>Set Manually</label>
           <input type="text" class="form-control" name="">
         </div> 
       </div>  
       <div class="col-md-12">
        <div class="form-group">
         <button type="button" data-dismiss="modal" class="btn btn-success btn-sm">
          Confirm
        </button>
      </div> 
    </div>  
  </div>
</form> 
</div>
</div>
</div>
</div>
<div class="modal fade" id="set-service">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header bg-primary">
        <h5 class="modal-title">Set Service Charges</h5>
        <button type="button" data-dismiss="modal" class="close">&times;</button>
      </div>
      <div class="modal-body">
        <form>
          <div class="row">


            <div class="col-md-12">
              <div class="form-group">
               <label>Set Service Charge %</label>
               <input type="text" class="form-control" name="">
             </div> 
           </div>  
           <div class="col-md-12">
            <div class="form-group">
             <button type="button" data-dismiss="modal" class="btn btn-success btn-sm">
              Set
            </button>
          </div> 
        </div>  
      </div>
    </form> 
  </div>
</div>
</div>
</div>

<!-- Rating Modal -->
<div class="modal fade" id="rating-modal">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-body text-center bg-primary">
        <h4 class="text-left mb-0"><strong>RATINGS & REVIES</strong> given by user</h4>
      </div>
      <div class="modal-body text-center">
        <table class="table table-striped table-bordered no-footer" >
          <thead>
            <tr>
              <th class="h5">ORDER ID</th>
              <td class="text-left h5">ASD324</td>
            </tr>
            <tr>
              <th class="h5">RATINGS</th>
              <td class="text-left h5">
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star-half-o" aria-hidden="true"></i>
                <i class="fa fa-star-o" aria-hidden="true"></i>
                <i class="fa fa-star-o" aria-hidden="true"></i>
              </td>
            </tr>
            <tr>
              <th class="h5">REVIEW</th>
              <td></td>
            </tr>
          </thead>
        </table>
        <p class="text-left">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
          tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
          quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
          consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
            
      </div>
      
      <div class="modal-body text-center mt-0">
        <h4 class="text-left"><strong>RATING & REVIEW </strong> given by service provider</h4>
        <table class="table table-striped table-bordered no-footer" >
          <thead>
            <tr>
              <th class="h5">ORDER ID</th>
              <td class="text-left h5">ASD324</td>
            </tr>
            <tr>
              <th class="h5">RATINGS</th>
              <td class="text-left h5">
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star-half-o" aria-hidden="true"></i>
                <i class="fa fa-star-o" aria-hidden="true"></i>
                <i class="fa fa-star-o" aria-hidden="true"></i>
              </td>
            </tr>
            <tr>
              <th class="h5">REVIEW</th>
              <td></td>
            </tr>
          </thead>
        </table>
        <p class="text-left">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
          tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
          quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
          consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
        cillum dolore eu fugiat nulla pariatur.</p>
      </div>
    </div>
  </div>
</div>
<!-- Rating Modal -->

    @endsection

    @section('footerscript')

      <script>
          
          const messaging = firebase.messaging();

          messaging.getToken({vapidKey: "BN5HpCbpsXLPciWZ37UXtk6EWFNNFMH4JEt-cTNsanl4ylRkanpI86umHfrdCJe-PaeDkN5NfVqCEaXOTwKc9UE"});

          function sendTokenToServer(fcm_token){
            const admin_id = '{{ Auth::guard('admin')->user()->id }}';
            console.log(admin_id);
            axios.post('api/save-token', {
              fcm_token,admin_id
            })
            .then(function (response) {
              console.log(response);
            });
          
          }

          messaging.getToken().then((currentToken) => {
            if (currentToken) {
              sendTokenToServer(currentToken);
              // updateUIForPushEnabled(currentToken);
            } else {
              alert('you should allow notification');
              
              // Show permission request.
              // console.log('No registration token available. Request permission to generate one.');
              // Show permission UI.
              // updateUIForPushPermissionRequired();
              // setTokenSentToServer(false);
            
            }
          }).catch((err) => {
            console.log('An error occurred while retrieving token. ', err);
            // showToken('Error retrieving registration token. ', err);
            // setTokenSentToServer(false);
          });

      </script>
       
    @endsection