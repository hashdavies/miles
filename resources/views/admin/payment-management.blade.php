@extends('admin.header')
@section('title', 'Payment Management')
@section('content')
@include('admin/navbar')

<div class="responsiveTbPage ExplorerListPage">
  <div class="layout-content">
    <div class="layout-content-body">
      <div class="title-bar">
        <h1 class="title-bar-title">
          <span class="d-ib">Payment Management</span>
        </h1>
      </div>
      <div class="row">
        <div class="col-xs-12">
          <div class="card">
            <div class="card-header pb-1">
              <div class="row gutter-xs">
                <!-- <div class="col-xs-12 col-sm-12 mb-1">
                  <div class="card-actions">
                    <button type="button" class="btn btn-success btn-sm mb-1">Import</button>
                    <button type="button" class="btn btn-primary btn-sm mb-1" data-target="#add-modal" data-toggle="modal">Add New User</button>
                  </div>
                  <strong>User List</strong>
                </div> -->
                
                 <form action="{{url('admin/payment-management')}}" method="get" class="">

                <div class="col-xs-6 col-sm-2 col-md-3 mb-2">
                  <label class="wpx-50">From</label>
                  <input type="text" id="start_date" name="start_date" class="form-control" placeholder="mm/dd/yy">
                </div>
                <div class="col-xs-6 col-sm-2 col-md-3 mb-2">
                  <label class="wpx-50">To</label>
                  <input type="text" placeholder="mm/dd/yy" id="end_date" name="end_date" class="form-control" onkeypress="myfun()">
                </div>
                
                <!-- <div class="col-xs-6 col-sm-2 col-md-2 mb-2">
                  <label>Province</label>
                  <select class="form-control" name="province" onchange="myfun()" id="province">
                    <option value="">Choose province</option>
                     
                  </select>
                </div>    
                <div class="col-xs-6 col-sm-2 col-md-2 mb-2">
                  <label>City</label>
                  <select class="form-control" name="city" id="city" onchange="myfun()">
                    <option value="">Choose City</option>
                     
                  </select>
                </div>   -->              
                <div class="col-xs-6 col-sm-4 col-md-3 mb-2">
                  <label>Sort By</label>
                  <select class="form-control" name="status" onchange="myfun()">
                    <option value="">Choose Status</option>
                    <option value="1">Completed</option>
                    <option value="0">Incompleted</option>
                  </select>
                </div>

                <div class="col-xs-6 col-sm-4 col-md-2 mb-2">
                  <br>
                  <a href="{{url('admin/payment-management')}}" class="btn btn-sm btn-primary pull-right" style="">Reset</a>
                     <button type="submit" class="btn btn-sm btn-primary pull-right " style="margin-right: 12px;">Apply</button>
                     
                </div> 
               </form> 

              </div>
            </div>


            <div class="card-body">
              <div class="table-responsive">
                <table class="table dataTable table-striped table-bordered">
                  <thead>                  
                    <tr>
                      <th>Gig ID</th>
                      <th class="wpx-180">Transection Id</th>
                      <th>User Id</th>
                      <th>User Name</th>
                      <th>Type</th>
                      <th>Date & Time</th>
                      <th>Service Provider</th>
                      <th>Payment Method</th>
                       <th>Amount</th>
                      <th class="wpx-80 no-sort">Status</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($data as $key=>$rowData)
                      <tr>
                        <td>#{{$rowData->gig_id}}</td>
                        <td>{{$rowData->tranzectionId}}</td>
                        <td>{{ '@$rowData->CouponName->promo_name' ? @$rowData->CouponName->promo_name : 'N/A'}}</td>
                        <td>{{@$rowData->UserName->firstName}} {{@$rowData->UserName->lastName}}</td>
                        <td>{{@$rowData->UserGigAddressDetails->additional_services}}</td>
                        <td>{{$rowData->PaymentDate}}</td>
                        <td>{{@$rowData->SellerName->firstName}}  {{@$rowData->SellerName->lastName}} </td>
                        <td>{{$rowData->payment_mode}}</td>
                        <td>{{$rowData->subtotal_price}}</td>
                        <td>
                          @if($rowData->paymentStatus == '1')
                            <span class="text-success">Completed</span>
                          @elseif($rowData->paymentStatus == '0')
                            <span class="text-danger">Pending</span>
                          @endif
                            
                        </td>
                      </tr>
                    @endforeach
                       
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Block Modal -->
<div class="modal fade" id="block-modal">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-body text-center">
        <h2><i class="fa fa-toggle-off text-success"></i></h2>
        <h5>Are you sure you want to block this user?</h5>
        <div class="text-center">
          <button type="button" data-dismiss="modal" class="btn btn-success btn-sm">
            Yes
          </button>
          <button type="button" data-dismiss="modal" class="btn btn-danger btn-sm">
            No
          </button>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Unblock Modal -->
<div class="modal fade" id="unblock-modal">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-body text-center">
        <h2><i class="fa fa-toggle-on text-danger"></i></h2>
        <h5>Are you sure you want to unblock this user?</h5>
        <div class="text-center">
          <button type="button" data-dismiss="modal" class="btn btn-success btn-sm">
            Yes
          </button>
          <button type="button" data-dismiss="modal" class="btn btn-danger btn-sm">
            No
          </button>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Delete Modal -->
<!-- <div class="modal fade" id="delete-modal">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-body text-center">
        <h2><i class="fa fa-trash-o text-danger"></i></h2>
        <h5>Are you sure you want to delete this user?</h5>
        <div class="text-center">
          <button type="button" data-dismiss="modal" class="btn btn-success btn-sm">
            Yes
          </button>
          <button type="button" data-dismiss="modal" class="btn btn-danger btn-sm">
            No
          </button>
        </div>
      </div>
    </div>
  </div>
</div> -->

<!-- View Modal -->
<div class="modal fade" id="view-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header bg-primary">
        <h5 class="modal-title">Basic Information</h5>
        <button type="button" data-dismiss="modal" class="close">&times;</button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center mb-3">
            <img src="assets/img/user.jpg" class="whpx-80 circle mb-3"/>
            <h5>John Deo</h5>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <table class="table">
              <tr>
                <th>Service Provider Id</th>
                <td>data</td>
              </tr>
              <tr>
                
                <th>Owner Name</th>
                <td>data</td>
              </tr>
              <tr>
                <th>Email</th>
                 <td>data</td>
              </tr>
              <tr>
                <th>Mobile Number</th>
                 <td>data</td>
              </tr>
              <tr>
                <th>City</th>
                 <td>data</td>
              </tr>
               <tr>
                <th>Service Type</th>
                 <td>data</td>
              </tr>
               <tr>
                <th>No. of Services Till Date</th>
                 <td>data</td>
              </tr>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection