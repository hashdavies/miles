@extends('admin.header')
@section('title', 'Earning Management')
@section('content')
@include('admin/navbar')
  <div class="dashboardPage"></div>
  <div class="layout-content">
    <div class="layout-content-body">
      <div class="title-bar">
        <h1 class="title-bar-title">
          <span class="d-ib">Earning Management</span>
        </h1>
      </div>
       <div class="col-xs-12 col-sm-12 mb-1">
                  <div class="card-actions">
                    <!-- <button type="button" data-target="#set-taxes" data-toggle="modal" class="btn btn-success btn-sm mb-1">Set Taxes</button> -->
                    <a href="{{url('admin/tax-management')}}" class="btn btn-success btn-sm mb-1">Set Taxes</a>
                    <button type="button" onclick="getTax()" class="btn btn-primary btn-sm mb-1">Set Service Charge</button>
                  </div>
                </div>


      <!-- <div class="title-bar">
        <div class="row">
          <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
            <h1 class="title-bar-title">
              <span class="d-ib">Dashboard</span>
            </h1>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
            <div class="form-group pull-lg-right wpx-280">
              <div class="d-flex">
                <label class="white-space-nowrap pr-2 align-self-center">Fiter : Dropdown</label>
                <select class="form-control">
                  <option>A</option>
                  <option>B</option>
                  <option>C</option>
                </select>
              </div>
            </div>
          </div>
        </div>
      </div> -->
      <div class="row gutter-xs height-box">

        <!-- <div class="full-height col-xs-12 col-sm-3 col-md-3 col-lg-3">
          <div class="card">
            <div class="card-body">
              <div class="media">
                <div class="media-middle media-left">
                  <span class="bg-primary circle sq-48">
                    <span class="icon icon-check"></span>
                  </span>
                </div>
                <div class="media-middle media-body">
                  <h5 class="media-heading">Total Successful Gigs</h5>
                  <h4 class="media-heading">
                    <span class="fw-l">200</span>
                  </h4>
                </div>
              </div>
            </div>
          </div>
        </div> -->

        <!-- <div class="full-height col-xs-12 col-sm-3 col-md-3 col-lg-3">
          <div class="card">
            <div class="card-body">
              <div class="media">
                <div class="media-middle media-left">
                  <span class="bg-warning circle sq-48">
                    <span class="icon icon-truck"></span>
                  </span>
                </div>
                <div class="media-middle media-body">
                  <h5 class="media-heading">Total Ongoing Gigs</h5>
                  <h4 class="media-heading">
                    <span class="fw-l">2</span>
                  </h4>
                </div>
              </div>
            </div>
          </div>
        </div> -->

        <!-- <div class="full-height col-xs-12 col-sm-3 col-md-3 col-lg-3">
          <div class="card">
            <div class="card-body">
              <div class="media">
                <div class="media-middle media-left">
                  <span class="bg-info circle sq-48">
                    <span class="icon icon-times"></span>
                  </span>
                </div>
                <div class="media-middle media-body">
                  <h5 class="media-heading">Total Cancelled Gigs</h5>
                  <h4 class="media-heading">
                    <span class="fw-l">20</span>
                  </h4>
                </div>
              </div>
            </div>
          </div>
        </div> -->

        <!-- <div class="full-height col-xs-12 col-sm-3 col-md-3 col-lg-3">
          <div class="card">
            <div class="card-body">
              <div class="media">
                <div class="media-middle media-left">
                  <span class="bg-success circle sq-48">
                    <span class="icon icon-money"></span>
                  </span>
                </div>
                <div class="media-middle media-body">
                  <h5 class="media-heading">Total Revenue</h5>
                  <h4 class="media-heading">
                    <span class="fw-l">$200</span>
                  </h4>
                </div>
              </div>
            </div>
          </div>
        </div> -->

        
        
        
        
      </div>
      <div class="row gutter-xs height-box">
        <div class="full-height col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">

         
          <div class="card">
            <div class="card-body">
              <div class="media">
                <div class="media-middle media-left">
                  <span class="bg-success circle sq-48">
                    <span class="icon icon-check"></span>
                  </span>
                </div>
                <div class="media-middle media-body">
                  <h5 class="media-heading">Total number of successful services delivered</h5>
                  <h4 class="media-heading">
                    <span class="fw-l">1200</span>
                  </h4>
                </div>
              </div>
            </div>
          </div>
        

          <div class="card">
            <div class="card-body">
              <!-- <h5 class="pt-2 pb-2">Successful Services Delivered <span class="pull-right cs_Graph">1200</span></h5> -->
              <div class="card-chart">
                <canvas id="demo-visitors" data-chart="bar" data-animation="false" data-labels='["Aug 1", "Aug 2", "Aug 3", "Aug 4", "Aug 5", "Aug 6", "Aug 7", "Aug 8", "Aug 9", "Aug 10", "Aug 11", "Aug 12", "Aug 13", "Aug 14", "Aug 15", "Aug 16", "Aug 17", "Aug 18", "Aug 19", "Aug 20", "Aug 21", "Aug 22", "Aug 23", "Aug 24", "Aug 25", "Aug 26", "Aug 27", "Aug 28", "Aug 29", "Aug 30", "Aug 31"]' data-values='[{"label": "Visitors", "backgroundColor": "#35cf76", "borderColor": "#35cf76",  "data": [29432, 20314, 17665, 22162, 31194, 35053, 29298, 36682, 45325, 39140, 22190, 28014, 24121, 39355, 36064, 45033, 42995, 30519, 20246, 42399, 37536, 34607, 33807, 30988, 24562, 49143, 44579, 43600, 18064, 36068, 41605]}]' data-hide='["legend", "scalesX"]' height="150"></canvas>
              </div>
              <div class="row gutter-xs mt-2">
                <span class="col-xs-6 col-sm-6 col-md-6">Overall : 70.78%</span>
                <span class="col-xs-6 col-sm-6 col-md-6">Monthly : 24.66%</span>
              </div>
            </div>
          </div>
        </div>
        <div class="full-height col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">

          <div class="card">
            <div class="card-body">
              <div class="media">
                <div class="media-middle media-left">
                  <span class="bg-warning circle sq-48">
                    <span class="fa fa-clock-o"></span>
                  </span>
                </div>
                <div class="media-middle media-body">
                  <h5 class="media-heading">Total number of pending service bookings</h5>
                  <h4 class="media-heading">
                    <span class="fw-l">600</span>
                  </h4>
                </div>
              </div>
            </div>
          </div>

          <div class="card">
            <div class="card-body">
              <!-- <h5 class="pt-2 pb-2">Pending Service Booking <span class="pull-right cs_Graph">600</span></h5> -->
              <div class="card-chart">
                <canvas id="demo-visitors" data-chart="bar" data-animation="false" data-labels='["Aug 1", "Aug 2", "Aug 3", "Aug 4", "Aug 5", "Aug 6", "Aug 7", "Aug 8", "Aug 9", "Aug 10", "Aug 11", "Aug 12", "Aug 13", "Aug 14", "Aug 15", "Aug 16", "Aug 17", "Aug 18", "Aug 19", "Aug 20", "Aug 21", "Aug 22", "Aug 23", "Aug 24", "Aug 25", "Aug 26", "Aug 27", "Aug 28", "Aug 29", "Aug 30", "Aug 31"]' data-values='[{"label": "Visitors", "backgroundColor": "#e67e22", "borderColor": "#e67e22",  "data": [29432, 20314, 17665, 22162, 31194, 35053, 29298, 36682, 45325, 39140, 22190, 28014, 24121, 39355, 36064, 45033, 42995, 30519, 20246, 42399, 37536, 34607, 33807, 30988, 24562, 49143, 44579, 43600, 18064, 36068, 41605]}]' data-hide='["legend", "scalesX"]' height="150"></canvas>
              </div>
              <div class="row gutter-xs mt-2">
                <span class="col-xs-6 col-sm-6 col-md-6">Overall : 70.78%</span>
                <span class="col-xs-6 col-sm-6 col-md-6">Monthly : 24.66%</span>
              </div>
            </div>
          </div>
        </div>
        <div class="full-height col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">


          <div class="card">
            <div class="card-body">
              <div class="media">
                <div class="media-middle media-left">
                  <span class="bg-primary circle sq-48">
                    <span class="fa fa-calendar"></span>
                  </span>
                </div>
                <div class="media-middle media-body">
                  <h5 class="media-heading">Total Revenue Till Date</h5>
                  <h4 class="media-heading">
                    <span class="fw-l"><i>Date</i></span>
                  </h4>
                </div>
              </div>
            </div>
          </div>


          <div class="card">
            <div class="card-body">
              <!-- <h5 class="pt-2 pb-2">Total Revenue Till Date <span class="pull-right cs_Graph">date</span></h5> -->
              <div class="card-chart">
                <canvas id="demo-visitors" data-chart="bar" data-animation="false" data-labels='["Aug 1", "Aug 2", "Aug 3", "Aug 4", "Aug 5", "Aug 6", "Aug 7", "Aug 8", "Aug 9", "Aug 10", "Aug 11", "Aug 12", "Aug 13", "Aug 14", "Aug 15", "Aug 16", "Aug 17", "Aug 18", "Aug 19", "Aug 20", "Aug 21", "Aug 22", "Aug 23", "Aug 24", "Aug 25", "Aug 26", "Aug 27", "Aug 28", "Aug 29", "Aug 30", "Aug 31"]' data-values='[{"label": "Visitors", "backgroundColor": "#6B52AB", "borderColor": "#6B52AB",  "data": [29432, 20314, 17665, 22162, 31194, 35053, 29298, 36682, 45325, 39140, 22190, 28014, 24121, 39355, 36064, 45033, 42995, 30519, 20246, 42399, 37536, 34607, 33807, 30988, 24562, 49143, 44579, 43600, 18064, 36068, 41605]}]' data-hide='["legend", "scalesX"]' height="150"></canvas>
              </div>
              <div class="row gutter-xs mt-2">
                <span class="col-xs-6 col-sm-6 col-md-6">Overall : 70.78%</span>
                <span class="col-xs-6 col-sm-6 col-md-6">Monthly : 24.66%</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="set-service">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header bg-primary">
        <h5 class="modal-title">Set Service Charges</h5>
        <button type="button" data-dismiss="modal" class="close">&times;</button>
      </div>
      <div class="modal-body">
        <form action="{{url('admin/update-service-tax')}}" id="add_servicetax" method="post">
          @csrf
          <input type="hidden" id="editId" name="editId">
          <div class="row">
              <div class="col-md-12">
                  <div class="form-group">
                       <label>Set Service Charge %</label>
                        <input type="text" class="form-control" name="servicetax" id="servicetax">
                        <span style="display: none; color: red" id="Errorservicetax">Required </span>
                  </div> 
              </div>  
              <div class="col-md-12">
                  <div class="form-group">
                     <button type="submit" class="btn btn-success btn-sm">
                      Set
                    </button>
                   </div> 
              </div>  
          </div>
        </form> 
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="set-taxes">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header bg-primary">
        <h5 class="modal-title">Set Taxes</h5>
        <button type="button" data-dismiss="modal" class="close">&times;</button>
      </div>
      <div class="modal-body">
        <form>
          <div class="row">
              <div class="col-md-12">
                  <div class="form-group">
                       <label>Select Province</label>
                       <select class="form-control">
                         <option>choose</option>
                       </select>
                   </div> 
              </div>  
              <div class="col-md-12">
                  <div class="form-group">
                       <label>Set Tax % on offer amount</label>
                        <select class="form-control">
                          @foreach($servicetax as $rowtax)
                            <option>{{$rowtax->serviceTax}}</option>
                          @endforeach
                       </select>
                   </div> 
              </div>  
              <div class="col-md-12">
                  <div class="form-group">
                       <label>Set Manually</label>
                        <input type="text" class="form-control" name="">
                   </div> 
              </div>  
              <div class="col-md-12">
                  <div class="form-group">
                     <button type="button" data-dismiss="modal" class="btn btn-success btn-sm">
                      Confirm
                    </button>
                   </div> 
              </div>  
          </div>
        </form> 
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="success-modal">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-body text-center">
          <h2><i class="fa fa-link text-success"></i></h2>
          <h5>Service Charge set successfully!</h5>
        </div>
      </div>
    </div>
  </div>

@endsection

@section('footerscript')
  
  @if ($message = session()->has('success_message'))
    <script type="text/javascript">
      $(function() {
        $('#success-modal').modal('show');
         setTimeout(function() {$('#success-modal').modal('hide');}, 2000);
      });
    </script>
  @endif

  <script type="text/javascript">
    
      function getTax(){
        $.ajax({
           type:"GET",
           url:"{{url('admin/adminTax')}}",
           success:function(data){  
             // console.log(data[0].serviceTax); 
             $('#editId').val(data[0].id);
             $('#servicetax').val(data[0].serviceTax);
           }
        });
        $('#set-service').modal('show');
      }

  </script>
  

  <script type="text/javascript">
    $('#add_servicetax').on('submit', function(e){
      
      $('#Errorservicetax').hide();
      var servicetax = $('#servicetax').val();
      if(servicetax == ''){
        $('#Errorservicetax').show();
        e.preventDefault();
      }
    });
  </script>

  <script>
        $(document).ready(function(){
          // $("input").keydown(function(){
          //  alert("dcfdsfdas");
          // });
          $("input").click(function(){
            
            $("#showSubmitButton").html(`<a href="{{url('admin/earning-management')}}" class="btn btn-sm btn-primary pull-right" style="">Reset</a>
                    <button type="submit" class="btn btn-sm btn-primary pull-right " style="margin-right: 12px;">Apply</button>`);

          });
        });
      </script>

      <script type="text/javascript">
        
        function myfun(){  
          
          $("#showSubmitButton").html(`<a href="{{url('admin/earning-management')}}" class="btn btn-sm btn-primary pull-right" style="">Reset</a>
                    <button type="submit" class="btn btn-sm btn-primary pull-right " style="margin-right: 12px;">Apply</button>`);
          
        } 

      </script>
      <script type="text/javascript">
          $('#province').on('change',function(){
          // alert();
          var countryID = $("#province").find(":selected").data("id");;
          //var countryID = $(this).val(); 
          // alert(countryID);  
          if(countryID){
              $.ajax({
                 type:"GET",
                 url:"{{url('admin/UserCity')}}"+'/'+countryID,
                 success:function(res){  
                 //console.log(res[0]); 
                  if(res){

                      $("#city").empty();

                      $.each(res,function(key,value){
                          $("#city").append('<option value="'+value.city+'">'+value.city+'</option>');
                      });
                 
                  }else{
                     $("#city").empty();
                  }
                 }
              });
          }else{
              $("#city").empty();
         }
       });
      </script>


@endsection