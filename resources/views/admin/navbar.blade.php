<div class="layout-sidebar">
        <div class="layout-sidebar-backdrop"></div>
        <div class="layout-sidebar-body">
          <div class="custom-scrollbar">
            <nav id="sidenav" class="sidenav-collapse collapse">
              <ul class="sidenav">                
                <li class="sidenav-heading">Navigation</li>
                <li class="sidenav-item dashboardNav">
                  <a href="{{url('admin/dashboard')}}">
                    <span class="sidenav-icon icon icon-home"></span>
                    <span class="sidenav-label">Dashboard</span>
                  </a>
                </li>
                <li class="sidenav-item userNav">
                  <a href="{{url('admin/user-management')}}">
                    <span class="sidenav-icon icon icon-user"></span>
                    <span class="sidenav-label">User Mgmt.</span>
                  </a>
                </li>
                <li class="sidenav-item servicesProviderNav">
                  <a href="{{url('admin/services-provider-management')}}">
                    <span class="sidenav-icon icon icon-users"></span>
                    <span class="sidenav-label">Service Provider Mgmt.</span>
                  </a>
                </li>
                <li class="sidenav-item servicesCategoryNav">
                  <a href="{{url('admin/services-category-management')}}">
                    <span class="sidenav-icon icon icon-list"></span>
                    <span class="sidenav-label">Service Category Mgmt.</span>
                  </a>
                </li>
                <li class="sidenav-item has-subnav gigNav">
                  <a href="#">
                    <span class="sidenav-icon icon icon-star"></span>
                    <span class="sidenav-label">Gig Mgmt.</span>
                  </a>
                  <ul class="sidenav-subnav collapse">                    
                    <li class="ratingNav-a"><a href="{{url('admin/pending-gig')}}">Pending</a></li>
                    <li class="ratingNav-a"><a href="{{url('admin/ongoing-gig')}}">Ongoing</a></li>
                    <li class="ratingNav-c"><a href="{{url('admin/cancelled-gig')}}">Cancelled</a></li>
                    <li class="ratingNav-b"><a href="{{url('admin/completed-gig')}}">Completed</a></li>
                  </ul>
                </li>
                <li class="sidenav-item tripNav">
                  <a href="{{url('admin/trip-management')}}">
                    <span class="sidenav-icon fa fa-bus"></span>
                    <span class="sidenav-label">Trip Mgmt.</span>
                  </a>
                </li>  
                <li class="sidenav-item tripNav">
                  <a href="{{url('admin/tax-management')}}">
                    <span class="sidenav-icon fa fa-bus"></span>
                    <span class="sidenav-label">Tax Mgmt.</span>
                  </a>
                </li>                
                <li class="sidenav-item earningNav">
                  <a href="{{url('admin/earning-management')}}">
                    <span class="sidenav-icon icon icon-money"></span>
                    <span class="sidenav-label">Earnings Mgmt.</span>
                  </a>
                </li>
                <li class="sidenav-item reviewRratingsNav">
                  <a href="{{url('admin/review-ratings-management')}}">
                    <span class="sidenav-icon fa fa-star"></span>
                    <span class="sidenav-label">Review & Ratings Mgmt.</span>
                  </a>
                </li>
                
                <li class="sidenav-item has-subnav supportNav">
                  <a href="#">
                    <span class="sidenav-icon fa fa-phone"></span>
                    <span class="sidenav-label">Support Mgmt.</span>
                  </a>
                  <ul class="sidenav-subnav collapse">
                    <li class="supportNav-b"><a href="{{url('admin/users-support')}}">Users</a></li>
                    <li class="supportNav-c"><a href="{{url('admin/service-providers-support')}}">Service Providers</a></li>
                  </ul>
                </li>
                <li class="sidenav-item cmsNav">
                  <a href="{{url('admin/cms-management')}}">
                    <span class="sidenav-icon fa fa-dashboard"></span>
                    <span class="sidenav-label">CMS Mgmt.</span>
                  </a>
                </li>

                <li class="sidenav-item reviewRratingsNav">
                  <a href="{{url('admin/crm-management')}}">
                    <span class="sidenav-icon fa fa-star"></span>
                    <span class="sidenav-label">CRM Mgmt.</span>
                  </a>
                </li>
                <li class="sidenav-item paymentNav">
                  <a href="{{url('admin/payment-management')}}">
                    <span class="sidenav-icon icon icon-money"></span>
                    <span class="sidenav-label">Payment Mgmt.</span>
                  </a>
                </li>
                
                <li class="sidenav-item promoCodeNav">
                  <a href="{{url('admin/promo-code-management')}}">
                    <span class="sidenav-icon icon icon-code"></span>
                    <span class="sidenav-label">Promo Code Mgmt.</span>
                  </a>
                </li>
                
                <!-- <li class="sidenav-item reportNav">
                  <a href="{{url('admin/report-management')}}">
                    <span class="sidenav-icon fa fa-line-chart"></span>
                    <span class="sidenav-label">Report Mgmt.</span>
                  </a>
                </li> -->
                
                <li class="sidenav-item has-subnav settingNav">
                  <a href="#">
                    <span class="sidenav-icon icon icon-cog"></span>
                    <span class="sidenav-label">Setting</span>
                  </a>
                  <ul class="sidenav-subnav collapse">
                    <li class="cmsNav-a"><a href="{{ route('profile') }}">Profile</a></li>
                    <li class="cmsNav-b"><a href="{{ route('change-password') }}">Change Password</a></li>
                    <li class="cmsNav-c"><a href="{{ url('admin/logout') }}">Sign Out</a></li>
                  </ul>
                </li>
              </ul>
            </nav>
          </div>
        </div>
      </div>