 @extends('admin.header')
 @section('title', 'Pending Management')
 @section('content')
 @include('admin/navbar')

 <div class="profilePage"></div>
 <div class="layout-content">
  <div class="row gutter-xs">
    <div class="profile-box">
      <div class="card text-center">
        <div class="card-image">
          <div class="overlay">
            <div class="overlay-gradient">
              <img class="card-img-top img-responsive" src="{{url('public/admin/img/bg2.jpg')}}" alt="Instagram App">
            </div>
          </div>
        </div>
        <div class="card-avatar">
          <a class="card-thumbnail rounded sq-100" href="#">
            <?php if(count($adminProfile)>0){ ?>
             <img class="img-responsive" src="{{ url('public/admin/img/') }}<?php echo '/'.$adminProfile['profile_image']; ?>" id="result" alt="Instagram">
           <?php }else{ ?>
            <img class="img-responsive" src="{{ url('public/admin/img/user1.jpg') }}" alt="Instagram">
          <?php } ?>
        </a>
      </div>
      <div class="card-body">
        <h3 class="card-title">Name of Admin</h3>

        <p class="card-text">
          <span class="app-users">
           <span class="icon icon-envelope"></span>
           <strong>Email</strong>: <?php if(count($adminProfile)>0){ echo $adminProfile['emailAddress']; }else{ echo "admin@gmail.com"; } ?> 
         </span>
       </p>
       <p class="card-text">
        <span class="app-users">
         <span class="icon icon-phone"></span>
         <strong>Phone</strong>: <?php if(count($adminProfile)>0){ echo $adminProfile['mobile']; }else{ echo "9999999999"; } ?> 
       </span>
     </p>
          <!-- <p class="card-text">
            <span class="app-users">
             <span class="icon icon-map-marker"></span>
            <strong>Address</strong>: Delhi, India

           </span>
         </p> -->

         <p class="card-text">
          <span class="app-users">
           <span class="icon icon-globe"></span>
           <strong>Region</strong>: <?php if(count($adminProfile)>0){ echo $adminProfile['country']; }else{ echo "USA"; } ?> 
         </span>
       </p>
       <div class="col-md-12 text-center">
        <a href="{{ route('edit-profile') }}" class="btn btn-primary btn-sm">Edit Profile</a>
        <!-- <a href="changePassword.php" class="btn btn-primary tn-sm">Change Password</a> -->
      </div>
    </div>
  </div>
</div>
</div>
</div>

</div> 
@endsection


@section('footerscript')

@if ($message = session()->has('successpassword'))
<script type="text/javascript">
  $(function() {
   $('#success-modal').modal('show');
   setTimeout(function() {$('#success-modal').modal('hide');}, 2000);
 });
</script>
@endif

@if ($message = session()->has('wrongpassword'))
<script>
 $(function() {
   $('#profile-success-modal').modal('show');
   setTimeout(function() {$('#profile-success-modal').modal('hide');}, 2000);
 });
</script>

@endif


<script type="text/javascript">

  $('.edit-info, .change-pass').hide();
      // var b=$(this).find("i");
      // var edit_class=b.attr('class');
      $('#edit-btn').on('click',function(){ 
        $('.view-info').hide();
        $('.edit-info').show(); 
        $('.change-pass').hide();
        $('#change-pass-btn').show();
        $(this).hide();  
      });
      $('.edit-info .edit-cancel').click(function(){
        $('.view-info').show();
        $('.edit-info').hide();
        $('#edit-btn').show();
      });
      $('#change-pass-btn').click(function(){
        $('.view-info').hide();
        $('.edit-info').hide();
        $('.change-pass').show(); 
        $('#edit-btn').show(); 
        $(this).hide();
      });
      $('.change-pass .edit-cancel').click(function(){
        $('.view-info').show();
        $('.change-pass').hide();
        $('#change-pass-btn').show();
      });
    </script>
    @endsection