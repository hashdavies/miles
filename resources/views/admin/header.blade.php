<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>    
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui" />
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}" id="token">
  <title>Myles | @yield('title')</title>
  <!-- css -->     
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>    
  <link rel="mask-icon" href="safari-pinned-tab.svg" color="#6B52AB">
  <meta name="theme-color" content="#ffffff">
  <link rel="icon" href="{{url('public/admin/img/favicon.png')}}" type="image/png" sizes="16x16">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700">
  <link rel="stylesheet" href="{{url('public/admin/fonts/font-awesome/css/font-awesome.min.css')}}">
  <link rel="stylesheet" href="{{url('public/admin/css/jquery.multiselect.css')}}">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" integrity="sha512-mSYUmp1HYZDFaVKK//63EcZq4iFWFjxSL+Z3T/aCt4IO9Cejm03q3NKKYN6pFQzY0SBOr8h+eCIAZHPXcpZaNw==" crossorigin="anonymous" />
  <link rel="stylesheet" href="{{url('public/admin/css/vendor.min.css')}}">
  <link rel="stylesheet" href="{{url('public/admin/css/elephant.min.css')}}">
  <link rel="stylesheet" href="{{url('public/admin/css/application.min.css')}}">
    <!-- <link rel="stylesheet" href="assets/css/profile.min.css">
    <link rel="stylesheet" href="assets/css/product.min.css">
    <link rel="stylesheet" href="assets/css/demo.min.css"> -->
    <link rel="stylesheet" href="{{url('public/admin/css/iEdit.css')}}">
    <link rel="stylesheet" href="{{url('public/admin/css/style.css')}}">
    <!-- <link rel="stylesheet" href="assets/css/signup-2.min.css"> -->
    <!-- The core Firebase JS SDK is always required and must be listed first -->

    <link rel="stylesheet" href="{{url('public/admin/build/css/intlTelInput.css')}}">
    <!-- <link rel="stylesheet" href="{{url('public/admin/build/css/demo.css')}}">  -->
    
    <script src="{{url('public/admin/build/js/intlTelInput.js')}}"></script>
    
    <script src="https://cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>


    <script src="https://www.gstatic.com/firebasejs/8.2.0/firebase-app.js"></script>
    <script src="https://www.gstatic.com/firebasejs/8.1.2/firebase-messaging.js"></script>

    <script>
      var firebaseConfig = {
        apiKey: "AIzaSyCqTIHlSFRhQZFMARCEbsF2IS_A8VU1R9c",
        authDomain: "myles-fdd94.firebaseapp.com",
        projectId: "myles-fdd94",
        storageBucket: "myles-fdd94.appspot.com",
        messagingSenderId: "969678702715",
        appId: "1:969678702715:web:3bf4f73f8679652832bade",
        measurementId: "G-S5XDJHYPH2"
      };
      firebase.initializeApp(firebaseConfig);
      firebase.analytics();
    </script>

    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>

  </head>
  <body onLoad="noBack();" onpageshow="if (event.persisted) noBack();" onUnload="">
    <body class="layout layout-header-fixed">
      <div class="layout-header">
        <div class="navbar navbar-default">
          <div class="navbar-header">
            <a class="navbar-brand navbar-brand-center" href="{{url('/')}}">
              <img class="navbar-brand-logo" src="{{url('public/admin/img/logo.png')}}" alt="Elephant">
            </a>
            <button class="navbar-toggler visible-xs-block collapsed" type="button" data-toggle="collapse" data-target="#sidenav">
              <span class="sr-only">Toggle navigation</span>
              <span class="bars">
                <span class="bar-line bar-line-1 out"></span>
                <span class="bar-line bar-line-2 out"></span>
                <span class="bar-line bar-line-3 out"></span>
              </span>
              <span class="bars bars-x">
                <span class="bar-line bar-line-4"></span>
                <span class="bar-line bar-line-5"></span>
              </span>
            </button>
            <button class="navbar-toggler visible-xs-block collapsed" type="button" data-toggle="collapse" data-target="#navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="arrow-up"></span>
              <span class="ellipsis ellipsis-vertical">
                <img class="ellipsis-object" width="32" height="32" src="assets/img/user1.jpg" alt="Teddy Wilson">
              </span>
            </button>
          </div>
          <div class="navbar-toggleable">
            <nav id="navbar" class="navbar-collapse collapse">
              <button class="sidenav-toggler hidden-xs" title="Collapse sidenav ( [ )" type="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="bars">
                  <span class="bar-line bar-line-1 out"></span>
                  <span class="bar-line bar-line-2 out"></span>
                  <span class="bar-line bar-line-3 out"></span>
                  <span class="bar-line bar-line-4 in"></span>
                  <span class="bar-line bar-line-5 in"></span>
                  <span class="bar-line bar-line-6 in"></span>
                </span>
              </button>
              <ul class="nav navbar-nav navbar-right">             
                <li class="dropdown">
                  <a class="dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                    <span class="icon-with-child hidden-xs">
                      <span class="icon icon-bell-o icon-lg"></span>
                      <span class="badge badge-danger badge-above right">7</span>
                    </span>
                    <span class="visible-xs-block">
                      <span class="icon icon-bell icon-lg icon-fw"></span>
                      <span class="badge badge-danger pull-right">7</span>
                      Notifications
                    </span>
                  </a>
                  <div class="dropdown-menu dropdown-menu-right dropdown-menu-lg">
                    <div class="dropdown-header">                   
                      <h5 class="dropdown-heading">Recent Notifications</h5>
                    </div>
                    <div class="dropdown-body">
                      <div class="list-group list-group-divided custom-scrollbar">
                        <a class="list-group-item" href="#">
                          <div class="notification">
                            <div class="notification-media">
                              <span class="icon icon-exclamation-triangle bg-warning rounded sq-40"></span>
                            </div>
                            <div class="notification-content">
                              <small class="notification-timestamp">35 min</small>
                              <h5 class="notification-heading">Update Status</h5>
                              <p class="notification-text">
                                <small class="truncate">Failed to get available update data. To ensur the proper functioning of your application, update now.</small>
                              </p>
                            </div>
                          </div>
                        </a>
                        <a class="list-group-item" href="#">
                          <div class="notification">
                            <div class="notification-media">
                              <span class="icon icon-flag bg-success rounded sq-40"></span>
                            </div>
                            <div class="notification-content">
                              <small class="notification-timestamp">43 min</small>
                              <h5 class="notification-heading">Account Contact Change</h5>
                              <p class="notification-text">
                                <small class="truncate">A contact detail associated with your account teddy.wilson, has recently changed.</small>
                              </p>
                            </div>
                          </div>
                        </a>
                        <a class="list-group-item" href="#">
                          <div class="notification">
                            <div class="notification-media">
                              <span class="icon icon-exclamation-triangle bg-warning rounded sq-40"></span>
                            </div>
                            <div class="notification-content">
                              <small class="notification-timestamp">1 hr</small>
                              <h5 class="notification-heading">Failed Login Warning</h5>
                              <p class="notification-text">
                                <small class="truncate">There was a failed login attempt from "192.98.19.164" into the account teddy.wilson.</small>
                              </p>
                            </div>
                          </div>
                        </a>
                        <a class="list-group-item" href="#">
                          <div class="notification">
                            <div class="notification-media">
                              <img class="rounded" width="40" height="40" src="assets/img/user2.jpg" alt="Daniel Taylor">
                            </div>
                            <div class="notification-content">
                              <small class="notification-timestamp">4 hr</small>
                              <h5 class="notification-heading">Daniel Taylor</h5>
                              <p class="notification-text">
                                <small class="truncate">Like your post: "Everything you know about Bootstrap is wrong".</small>
                              </p>
                            </div>
                          </div>
                        </a>
                        <a class="list-group-item" href="#">
                          <div class="notification">
                            <div class="notification-media">
                              <img class="rounded" width="40" height="40" src="assets/img/user1.jpg" alt="Emma Lewis">
                            </div>
                            <div class="notification-content">
                              <small class="notification-timestamp">8 hr</small>
                              <h5 class="notification-heading">Emma Lewis</h5>
                              <p class="notification-text">
                                <small class="truncate">Like your post: "Everything you know about Bootstrap is wrong".</small>
                              </p>
                            </div>
                          </div>
                        </a>
                        <a class="list-group-item" href="#">
                          <div class="notification">
                            <div class="notification-media">
                              <img class="rounded" width="40" height="40" src="assets/img/user2.jpg" alt="Sophia Evans">
                            </div>
                            <div class="notification-content">
                              <small class="notification-timestamp">8 hr</small>
                              <h5 class="notification-heading">Sophia Evans</h5>
                              <p class="notification-text">
                                <small class="truncate">Like your post: "Everything you know about Bootstrap is wrong".</small>
                              </p>
                            </div>
                          </div>
                        </a>
                        <a class="list-group-item" href="#">
                          <div class="notification">
                            <div class="notification-media">
                              <img class="rounded" width="40" height="40" src="assets/img/user1.jpg" alt="Teddy Wilson">
                            </div>
                            <div class="notification-content">
                              <small class="notification-timestamp">9 hr</small>
                              <h5 class="notification-heading">Teddy Wilson</h5>
                              <p class="notification-text">
                                <small class="truncate">Publisheda new post: "Everything you know about Bootstrap is wrong".</small>
                              </p>
                            </div>
                          </div>
                        </a>
                      </div>
                    </div>
                    <div class="dropdown-footer">
                      <a class="dropdown-btn" href="#">See All</a>
                    </div>
                  </div>
                </li>

                <li class="dropdown hidden-xs">
                  <button class="navbar-account-btn" data-toggle="dropdown">
                   @if(!empty($adminProfile)>0)
                     <img class="rounded" width="36" height="36" src="{{ url('public/admin/img/') }}<?php echo '/'.@$adminProfile['profile_image']; ?>" alt="Teddy Wilson">
                   @else
                    <img class="rounded" width="36" height="36" src="{{ url('public/admin/img/user2.jpg') }}" alt="Teddy Wilson">
                   @endif

                  {{ Auth::guard('admin')->user()->emailAddress }}
                  <span class="caret"></span>
                </button>
                <ul class="dropdown-menu dropdown-menu-right">
                  <li><a href="{{ url('admin/profile') }}">Profile</a></li>
                  <li><a href="{{ url('admin/change-password') }}">Change Password</a></li>
                  <li><a href="{{ url('admin/logout') }}">Sign out</a></li>
                </ul>
              </li>
              <li class="visible-xs-block">
                <a href="{{url('admin/profile')}}">
                  <span class="icon icon-user icon-lg icon-fw"></span>
                  Profile
                </a>
              </li>
              <li class="visible-xs-block">
                <a href="#">
                  <span class="icon icon-lock icon-lg icon-fw"></span>
                  Change Password
                </a>
              </li>
              <li class="visible-xs-block">
                <a href="{{ url('admin/logout') }}">
                  <span class="icon icon-power-off icon-lg icon-fw"></span>
                  Sign out
                </a>
              </li>
            </ul>
          </nav>
        </div>
      </div>
    </div>
    <div class="layout-main">
      @include('admin.navbar')
      @yield('content')
      <div class="layout-footer">
        <div class="layout-footer-body">
          <small class="copyright">2020 &copy; Myles App</small>
        </div>
      </div>
    </div>
    
   <!--  <script type="text/javascript">
      window.history.forward();
      function noBack()
      {
          window.history.forward();
      }
    </script> -->
    
    <script src="{{url('public/admin/js/jquery.min.js')}}"></script>
    <script src="{{url('public/admin/js/vendor.min.js')}}"></script>
    <script src="{{url('public/admin/js/elephant.min.js')}}"></script>
    <script src="{{url('public/admin/js/application.min.js')}}"></script>
    <script src="{{url('public/admin/js/jquery.multiselect.js')}}"></script>
    <script src="{{url('public/admin/js/iEdit.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js" integrity="sha512-T/tUfKSV1bihCnd+MxKD0Hm1uBBroVYBOYSk1knyvQ9VyZJpc/ALb4P0r6ubwVPSGB2GvjeoMAJJImBG12TiaQ==" crossorigin="anonymous"></script>
    <script src="{{url('public/admin/js/custom.js')}}"></script>
    <script src="{{url('public/admin/js/step-form.js')}}"></script>
    <!-- <script src="assets/js/profile.min.js"></script> -->
    <!-- <script src="assets/js/demo.min.js"></script> -->
    <script type="text/javascript">
      $(window).load(function(){
      // For Driver Page
      if ( $('.profilePage').length ) {
        $('.sidenav-item').removeClass("active");
        $('.profilePageNav').addClass("active");
      }
      if ( $('.dashboardPage').length ) {
        $('.sidenav-item').removeClass("active");
        $('.dashboardPageNav').addClass("active");
      }
    });
    // $(window).scroll(function (){
    //     var window_top = $(window).scrollTop();
    //     var div_top = $('.tabs-new').position().top;
    //     console.log(window_top);
    //     console.log(div_top);
    //     if (window_top > div_top) {
   //         $('.tabs-new').addClass('stick');
    //         $('.tabs-new').parents('.layout-content-body').removeClass('layout-content-body').addClass('layout-content-body-dummy');
    //     } else {
    //         $('.tabs-new').parents('.layout-content-body').removeClass('layout-content-body-dummy').addClass('layout-content-body')
    //     }
    //     if ( window_top < 74 ) {
    //       $('.tabs-new').removeClass('stick');
    //     }
    // });
    $('.datepicker').datepicker({
      format: 'mm/dd/yyyy',
      startDate: '-3d'
    });



  </script>
</body>
</html>

@yield('footerscript')  