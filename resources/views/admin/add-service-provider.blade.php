@extends('admin.header')
  @section('title', 'Service Provider Management')
  @section('content')
  @include('admin/navbar')
  
<!-- <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?libraries=places&sensor=false&key=YourApiKey"></script> -->

<script src="https://maps.google.com/maps/api/js?key=AIzaSyDxTV3a6oL6vAaRookXxpiJhynuUpSccjY&amp;libraries=places&amp;callback=initAutocomplete" type="text/javascript"></script>


<script type="text/javascript">
    google.maps.event.addDomListener(window, 'load', function () {
        var places = new google.maps.places.Autocomplete(document.getElementById('txtPlace'));
        google.maps.event.addListener(places, 'place_changed', function () {
            var place = places.getPlace();
            var address = place.formatted_address;
            var latitude = place.geometry.location.lat();
            var longitude = place.geometry.location.lng();
            var latlng = new google.maps.LatLng(latitude, longitude);
            var geocoder = geocoder = new google.maps.Geocoder();
            geocoder.geocode({ 'latLng': latlng }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        var address = results[0].formatted_address;
                        var pin = results[0].address_components[results[0].address_components.length - 1].long_name;

                        var country = results[0].address_components[results[0].address_components.length - 2].long_name;
                        
                        var state = results[0].address_components[results[0].address_components.length - 3].long_name;
                        var city = results[0].address_components[results[0].address_components.length - 4].long_name;
                        document.getElementById('txtCountry').value = country;
                        document.getElementById('txtState').value = state;
                        document.getElementById('txtCity').value = city;
                        document.getElementById('txtZip').value = pin;
                        document.getElementById('txtlat').value = latitude;
                        document.getElementById('txtlong').value = longitude;
                        
                    }
                }
            });
        });
    });
</script>



<div class="responsiveTbPage ExplorerListPage">
  <div class="layout-content">
    <div class="layout-content-body">
      <div class="title-bar">
        <h1 class="title-bar-title">
          <span class="d-ib">Add Service Provider</span>
        </h1>
      </div>
      <div class="row">
        <div class="col-xs-12">
          <div class="card">
            <div class="card-header pb-1">

            <form action="{{url('admin/save-service-provider')}}" id="add_service_provider" method="post" enctype="multipart/form-data">
              @csrf
              <div class="row gutter-xs">
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 col-xl-4 mb-3">
                  <label class="">Business Name</label>
                  <input type="text" class="form-control" name="businessName" id="businessName">
                  <span style="display: none; color: red" id="ErrorbusinessName">Enter Business Name </span>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 col-xl-4 mb-3">
                  <label class="">First Name</label>
                  <input type="text" class="form-control" name="firstName" id="firstName">
                  <span style="display: none; color: red" id="ErrorfirstName">Enter First Name </span>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 col-xl-4 mb-3">
                  <label class="">Last Name</label>
                  <input type="text" class="form-control" name="lastName" id="lastName">
                  <span style="display: none; color: red" id="ErrorlastName">Enter Last Name </span>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 col-xl-4 mb-3">
                  <label class="">Email</label>
                  <input type="email" class="form-control" name="email" id="email">
                  <span style="display: none; color: red" id="Erroremail">Enter Email</span>
                </div>
                
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 col-xl-4 mb-3">
                  <label class="">Service Type</label>
                  <select class="form-control checkbox-select" name="service[]" id="service" multiple="">
                    @foreach($service as $key=>$rowservice)
                      <option value="{{$rowservice->id}}">{{$rowservice->category_name}}</option>
                    @endforeach
                  </select>
                  <span style="display: none; color: red" id="Errorservice">Select Service</span>
                </div>

                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 col-xl-4 mb-3">
                  <label class="d-block">Mobile No</label>

                  <input type="text" id="country_code" value="+41" name="countryCode" hidden>
                    
                  <input id="phone" type="tel" autocomplete="off" data-intl-tel-input-id="0" value="" name="phone" class="form-control mobilr" value="@if(!empty($preData->mobileNumber)) {{$preData->mobileNumber}} @else {{ old('mobileNumber') }} @endif">
                  <!-- <input id="phone" class="form-control w-100" value="+91" name="phone" type="tel"> -->                 
                <span style="display: none; color: red" id="Errormobilephone">Enter Mobile No</span>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 col-xl-4 mb-3">
                  <label class="">Dl Number</label>
                  <input type="text" class="form-control" name="dlNumber" id="dlNumber">
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 col-xl-4 mb-3">
                  <label class="">Business Logo</label>
                  <input type="file" class="form-control" name="businessLogo" id="businessLogo">
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 col-xl-4 mb-3">
                  <label class="">Service Provider Type</label>
                  <select class="form-control" name="serviceProviderType" id="serviceProviderType">
                    <option value="business">Business</option>
                    <option value="freelancer">Freelancer</option>
                  </select>
                  <span style="display: none; color: red" id="ErrorserviceProviderType">Service Provider Type</span>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 col-xl-4 mb-3">
                  <label class="">Transist Number</label>
                  <input type="text" class="form-control" name="transitNumber" id="transitNumber">
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 col-xl-4 mb-3">
                  <label class="">Institution Number</label>
                  <input type="text" class="form-control" name="institutionNumber" id="institutionNumber">
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 col-xl-4 mb-3">
                  <label class="">Account Number</label>
                  <input type="text" class="form-control" name="accountNumber" id="accountNumber">
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 col-xl-4 mb-3">
                  <label class="">Address</label>
      
                  <input type="text" class="form-control" name="address" id="txtPlace" placeholder="Enter a location" />
                  <input type="hidden" id="txtCountry" />
                  <!--  <input type="text" id="txtState" />
                  <input type="text" id="txtCity" />-->
                  <input type="hidden" name="postalcode" id="txtZip" />               
                  <input type="hidden" name="address_latitude" id="txtlat" />
                  <input type="hidden" name="address_longitude" id="txtlong" />                
                  <span style="display: none; color: red" id="ErrortxtPlace">Enter Address</span>

                </div>
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 col-xl-4 mb-3">
                  <label class="">Province/State</label>
                  <input type="text" class="form-control" name="province" id="txtState">
                  <span style="display: none; color: red" id="ErrortxtState">Enter Province/State</span>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 col-xl-4 mb-3">
                  <label class="">City</label>
                  <input type="text" class="form-control" name="city" id="txtCity">
                  <span style="display: none; color: red" id="ErrortxtCity">Enter City</span>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 col-xl-4 mb-3">
                  <label class="">Profile Image</label>
                  <input type="file" class="form-control" name="profileImage" id="profileImage">
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 col-xl-4 mb-3">
                  <label class="">Id Proof</label>
                  <input type="file" class="form-control" name="IdProof" id="IdProof">
                </div>
                <!-- <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 col-xl-4 mb-3">
                  <label class="">Profile Image</label>
                  <input type="file" class="form-control" name="">
                </div> -->
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 col-xl-4 mb-3">
                  <label class="">Password</label>
                  <input type="password" placeholder="******" class="form-control" name="password" id="password">
                  <span style="display: none; color: red" id="Errorpassword">Enter Password</span>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 mt-3">
                  <div class="text-center">
                    <button class="btn btn-success wpx-80">Save</button>
                  </div>
                </div>
              </div>
            </form>
            </div>
            
          </div>
        </div>
      </div>
    </div>
  </div>
</div> 

<!-- Block Modal -->
<div class="modal fade" id="block-modal">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-body text-center">
        <h2><i class="fa fa-toggle-off text-success"></i></h2>
        <h5>Are you sure you want to block this user?</h5>
        <div class="text-center">
          <button type="button" data-dismiss="modal" class="btn btn-success btn-sm">
            Yes
          </button>
          <button type="button" data-dismiss="modal" class="btn btn-danger btn-sm">
            No
          </button>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Unblock Modal -->
<div class="modal fade" id="unblock-modal">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-body text-center">
        <h2><i class="fa fa-toggle-on text-danger"></i></h2>
        <h5>Are you sure you want to unblock this user?</h5>
        <div class="text-center">
          <button type="button" data-dismiss="modal" class="btn btn-success btn-sm">
            Yes
          </button>
          <button type="button" data-dismiss="modal" class="btn btn-danger btn-sm">
            No
          </button>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Delete Modal -->
<!-- <div class="modal fade" id="delete-modal">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-body text-center">
        <h2><i class="fa fa-trash-o text-danger"></i></h2>
        <h5>Are you sure you want to delete this user?</h5>
        <div class="text-center">
          <button type="button" data-dismiss="modal" class="btn btn-success btn-sm">
            Yes
          </button>
          <button type="button" data-dismiss="modal" class="btn btn-danger btn-sm">
            No
          </button>
        </div>
      </div>
    </div>
  </div>
</div> -->

<!-- View Modal -->
<div class="modal fade" id="view-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header bg-primary">
        <h5 class="modal-title">Basic Information</h5>
        <button type="button" data-dismiss="modal" class="close">&times;</button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center mb-3">
            <img src="assets/img/user.jpg" class="whpx-80 circle mb-3"/>
            <h5>John Deo</h5>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <table class="table">
              <tr>
                <th>Service Provider Id</th>
                <td>data</td>
              </tr>
              <tr>
                
                <th>Owner Name</th>
                <td>data</td>
              </tr>
              <tr>
                <th>Email</th>
                 <td>data</td>
              </tr>
              <tr>
                <th>Mobile Number</th>
                 <td>data</td>
              </tr>
              <tr>
                <th>City</th>
                 <td>data</td>
              </tr>
               <tr>
                <th>Service Type</th>
                 <td>data</td>
              </tr>
               <tr>
                <th>No. of Services Till Date</th>
                 <td>data</td>
              </tr>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection

@section('footerscript')

  

<!-- <script src="https://maps.google.com/maps/api/js?key=AIzaSyDxTV3a6oL6vAaRookXxpiJhynuUpSccjY&amp;libraries=places&amp;callback=initAutocomplete" type="text/javascript"></script> -->

<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCg4rES-cHvkyowz2QjYSS1aQi1vBJaYYM&libraries=places&callback=initAutocomplete" async defer></script> -->

 


<script type="text/javascript">
  $('#add_service_provider').on('submit', function(e){
    
     $('#ErrorbusinessName').hide();
     $('#ErrorfirstName').hide();
     $('#ErrorlastName').hide();
     $('#Erroremail').hide();
     $('#Errorservice').hide();
     $('#Errormobilephone').hide();
     $('#ErrorserviceProviderType').hide();
     $('#ErrortxtPlace').hide();
     $('#ErrortxtState').hide();
     $('#ErrortxtCity').hide();
     $('#Errorpassword').hide();       

     var businessName = $('#businessName').val();
     var firstName = $('#firstName').val();
     var lastName = $('#lastName').val();
     var email = $('#email').val();
     var service = $('#service').val();
     var mobilr = $('.mobilr').val();
     var dlNumber = $('#dlNumber').val();
     var serviceProviderType = $('#serviceProviderType').val();
     var txtPlace = $('#txtPlace').val();
     var txtState = $('#txtState').val();
     var txtCity = $('#txtCity').val();
     var password = $('#password').val();
       
    if(businessName == ''){
      $('#ErrorbusinessName').show();
      e.preventDefault();
    }
    if(firstName == ''){
      $('#ErrorfirstName').show();
      e.preventDefault();
    }
    if(lastName == ''){
      $('#ErrorlastName').show();
      e.preventDefault();
    }
    if(email == ''){
      $('#Erroremail').show();
      e.preventDefault();
    }    
    if(service == null){
      // alert(service);
      $('#Errorservice').show();
      e.preventDefault();
    }
    
    if(mobilr == ''){
      $('#Errormobilephone').show();
      e.preventDefault();      
    }
    if(serviceProviderType == ''){
      $('#ErrorserviceProviderType').show();
      e.preventDefault();
    }
    if(txtPlace == ''){
      $('#ErrortxtPlace').show();
      e.preventDefault();
    }
    if(txtState == ''){
      $('#ErrortxtState').show();
      e.preventDefault();
    }
    if(txtCity == ''){
      $('#ErrortxtCity').show();
      e.preventDefault();
    }
    if(password == ''){
      $('#Errorpassword').show();
      e.preventDefault();
    }


  });
</script>

    <script type="text/javascript" src="{{url('public/seller/js/intlTelInput.js')}}"></script> 
    <script type="text/javascript" src="{{url('public/seller/js/isValidNumber.js')}}"></script> 
    <script type="text/javascript" src="{{url('public/seller/js/utils.js')}}"></script> 
    <script type="text/javascript">
      $('#phone').keypress(function(e){
        var key = e.which || e.keyCode || e.charCode;
        if (key >= 48 && key <= 57){
          return true;
        }else{
          return false;
        }
      });
    </script>

<script>
    $(document).ready(function(){
      $(".iti.iti--allow-dropdown").css('width','100%');
    })

    var input = document.querySelector("#phone");
    window.intlTelInput(input, {
      // allowDropdown: false,
      // autoHideDialCode: false,
      // autoPlaceholder: "off",
      // dropdownContainer: document.body,
      // excludeCountries: ["us"],
      // formatOnDisplay: false,
      // geoIpLookup: function(callback) {
      //   $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
      //     var countryCode = (resp && resp.country) ? resp.country : "";
      //     callback(countryCode);
      //   });
      // },
      // hiddenInput: "full_number",
      // initialCountry: "auto",
      // localizedCountries: { 'de': 'Deutschland' },
      // nationalMode: false,
      // onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
      // placeholderNumberType: "MOBILE",
      // preferredCountries: ['cn', 'jp'],
      // separateDialCode: true,
      utilsScript: "build/js/utils.js",
    });
  </script>
@endsection