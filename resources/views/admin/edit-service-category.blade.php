@extends('admin.header')
  @section('title', 'Service Provider Management')
  @section('content')
  @include('admin/navbar')

  <div class="responsiveTbPage ExplorerListPage">
  <div class="layout-content">
    <div class="layout-content-body">
      <div class="title-bar">
        <h1 class="title-bar-title">
          <span class="d-ib">Service Category Management</span>
        </h1>
      </div>
      <div class="row">
        <div class="col-md-8">
          <div class="card">
            <div class="card-body">
              <form action="{{url('admin/update-category')}}" method="post" id="add_category" enctype="multipart/form-data">                
                 @csrf
                 <input type="hidden" name="editID" value="{{$data->id}}">
                 <div class="row">
                   <!--  <div class="col-md-4">
                      <div class="form-group add-pic">
                          <div class="pic-box">
                            <div class="logoImg">
                               <img src="{{url('http://mylesimages.s3.ap-south-1.amazonaws.com/Category/'.$data->category_image)}}" id="result">
                               <input id="logo-file" name="service_category_image" type="file" class="hide">
                               <input type="hidden" name="service_category_image" value="{{$data->category_image}}">
                               <label for="logo-file" class="btn btn-large"></label>
                            </div>                                   
                          </div>                          
                      </div>
                      <span style="color: red; display: none;" id="ImageError">Please attact Image</span>
                    </div>  -->
                  </div>
                  <div class="row">
                      <div class="col-md-12">
                          <div class="form-group">
                            <label class="black fw-600">Service Category Name</label>
                            <input class="form-control" id="categoryName" name="categoryName" value="{{$data->category_name}}" type="text">
                        </div>
                        <span style="color: red; display: none;" id="categoryNameError">Category name</span>
                      </div>  
                      <div class="col-md-12">
                          <div class="form-group">
                               <button type="button" onclick="window.location.href='{{url('admin/add-service-subcategory')}}'" class="margin-bottom-15 float-right btn btn-success btn-sm">Edit Sub Category</button>
                           </div>                           
                       </div> 
                       <div class="col-md-12">
                          <div class="form-group">
                            <label class="black fw-600">Alloted Sub Services Name  </label>                            
                               <div class="allot-category">
                                      <ul>
                                        @foreach($subcategory as $rowsabcate)
                                          <li>
                                            
                                            <div class="checkbox check-success">
                                               
                                                  <input type="checkbox" id="checkboxagree" name="subcategory[]" value="{{$rowsabcate->id}}" @if(in_array($rowsabcate->id, $subcategoryselected)) checked @endif>
                                                 <label for="checkbox-agree">{{$rowsabcate->sub_category_name}}
                                              </label>
                                            </div>

                                          </li>
                                        @endforeach
                                      </ul>
                                 </div>
                                 <span style="color: red; display:none;" id="subserviceError">Select subcategory</span>
                              </div>
                          </div> 
                          <div class="col-md-12">
                              <div class="m-t-10 text-center">
                                  <button class="btn btn-md btn-primary">Save & Continue</button>
                              </div>
                          </div> 
                      </div>
                  </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Block Modal -->
<div class="modal fade" id="block-modal">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-body text-center">
        <h2><i class="fa fa-toggle-off text-success"></i></h2>
        <h5>Are you sure you want to block this user?</h5>
        <div class="text-center">
          <button type="button" data-dismiss="modal" class="btn btn-success btn-sm">
            Yes
          </button>
          <button type="button" data-dismiss="modal" class="btn btn-danger btn-sm">
            No
          </button>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Unblock Modal -->
<div class="modal fade" id="unblock-modal">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-body text-center">
        <h2><i class="fa fa-toggle-on text-danger"></i></h2>
        <h5>Are you sure you want to unblock this user?</h5>
        <div class="text-center">
          <button type="button" data-dismiss="modal" class="btn btn-success btn-sm">
            Yes
          </button>
          <button type="button" data-dismiss="modal" class="btn btn-danger btn-sm">
            No
          </button>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Delete Modal -->
<!-- <div class="modal fade" id="delete-modal">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-body text-center">
        <h2><i class="fa fa-trash-o text-danger"></i></h2>
        <h5>Are you sure you want to delete this user?</h5>
        <div class="text-center">
          <button type="button" data-dismiss="modal" class="btn btn-success btn-sm">
            Yes
          </button>
          <button type="button" data-dismiss="modal" class="btn btn-danger btn-sm">
            No
          </button>
        </div>
      </div>
    </div>
  </div>
</div> -->

<!-- View Modal -->
@endsection

@section('footerscript')
  
  
  <script type="text/javascript">      
      
      $('#add_category').on('submit', function(e){

          // var categoryImage = $('#logo-file').val();
          var categoryName = $('#categoryName').val();
          var subservice = $('#checkboxagree').val();
          var servicecategory = $('#servicecategory').val();
          
          // alert(subservice);

          // if(categoryImage == ''){
          //   $('#ImageError').show();
          //   e.preventDefault();
          // }
          if(categoryName == ''){
            $('#categoryNameError').show();
            e.preventDefault();
          }
          if($('#subservice').prop("checked") == false){
            $('#subserviceError').show();
            e.preventDefault();
          }
          if(servicecategory == ''){
            $('#servicecategoryError').show();
            e.preventDefault();
          }

      });
  </script>
@endsection