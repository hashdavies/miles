<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>    
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui" />
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}" id="token">
    <title>Myles | Forgot Password</title>
    <!-- css -->     
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="mask-icon" href="safari-pinned-tab.svg" color="#6B52AB">
    <meta name="theme-color" content="#ffffff">
    <link rel="icon" href="{{url('public/admin/img/favicon.png')}}" type="image/png" sizes="16x16">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,400italic,500,700">
    <link rel="stylesheet" href="{{url('public/admin/css/vendor.min.css')}}">
    <link rel="stylesheet" href="{{url('public/admin/css/elephant.min.css')}}">
    <link rel="stylesheet" href="{{url('public/admin/css/application.min.css')}}">
    <link rel="stylesheet" href="{{url('public/admin/css/login-2.min.css')}}">
  </head>
  <body>
  <div class="login">
      <div class="login-body">
        <a class="login-brand" href="#">
          <img class="img-responsive" src="{{url('public/admin/img/logo.png')}}" alt="logo">
        </a>
        <div class="login-form">
          <h5>Forgot Password</h5>
          @if (session('email'))
            <span style="color: red">{{ session('email') }}</span>
          @endif 
          <form id="form-login" method="POST" action="{{url('admin/sendforgetlink')}}" >
          @csrf
            <div class="form-group">
              <label for="email">Enter Email Address</label>
              <input id="email" class="form-control" type="email" name="email" spellcheck="false" value="{{old('email')}}" autocomplete="off" data-msg-required="Please enter your email address." required>
              @if($errors->first('email'))
                <p style="color: red; text-align: left; margin-top: -20px;
margin-bottom: 9px;">{{ $errors->first('email') }}</p>
              @endif 
            </div>
            <button class="btn btn-primary btn-block mb-2" type="submit">Send link</button>
            <div class="text-center"><a href="{{url('login/admin')}}">Back to Sign In</a></div>                     
          </form>
        </div>
      </div>
    </div>
    <script src="{{url('public/admin/js/vendor.min.js')}}"></script>
    <script src="{{url('public/admin/js/elephant.min.js')}}"></script>
    <script src="{{url('public/admin/js/application.min.js')}}"></script>
    <!-- Success Modal -->
    <div class="modal fade" id="success-modal">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
          <div class="modal-body text-center">
            <h2><i class="fa fa-check text-success"></i></h2>
            <h5>Link Send Successfully.</h5>
          </div>
        </div>
      </div>
    </div>  
</body>
</html> 

@if ($message = session()->has('success_message'))
  <script type="text/javascript">
     $(function() {
        $('#success-modal').modal('show');
        setTimeout(function() {$('#success-modal').modal('hide');}, 2000);
      });
  </script>
@endif

@if ($message = session()->has('error_message'))
  <script type="text/javascript">
     $(function() {
        $('#success-modal').modal('show');
        setTimeout(function() {$('#error-modal').modal('hide');}, 2000);
      });
  </script>
@endif