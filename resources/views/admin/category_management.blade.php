  @extends('admin.header')
  @section('title', 'Store Category')
  @section('content')
      @include('admin/navbar') 
      
      <div id="categoryPage"></div>    
        <div class="pcoded-content">
          <div class="pcoded-inner-content">
            <div class="main-body">
              <div class="page-wrapper">
                <div class="page-header card">
                  <div class="row align-items-end">
                    <div class="col-sm-6 col-lg-6">
                      <div class="page-header-title d-flex">
                        <i class="icofont icofont-pay bg-c-lite-green"></i>
                        <div class="d-inline">
                          <h4>Store Category</h4>
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-6 col-lg-6">
                      <div class="page-header-breadcrumb">
                        <div class="page-header-breadcrumb">
                          <ul class="breadcrumb-title">
                            <li class="breadcrumb-item">
                              <a href="home.php">
                              <i class="icofont icofont-home"></i>
                              </a>
                            </li>
                            <li class="breadcrumb-item"><a>Food Category</a></li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>               
                <div class="page-body">
                  <div class="row">
                    <div class="col-sm-12 col-md-12 text-right mb-4">
                      <button class="btn btn-md btn-primary" data-toggle="modal" data-target="#add">Add New Category</button>
                    </div>
                    <div class="col-sm-12 col-md-12">
                      <div class="card">
                        <div class="card-body">
                          <table class="table table-striped table-bordered complete-dataTable">
                            <thead>
                              <tr>
                                <th>Category Name</th>
                                <th>Category Image</th>
                                <th class="no-sort wpx-110">Action</th>
                              </tr>
                            </thead>
                            <tbody>
                              @foreach($cateData as $key=>$row)
                              <tr>
                                <td>{{$row->category_name}}</td>
                                <td>
                                  <img src="{{url($row->cate_image)}}" alt="your image" class="img" height="80px" />
                                </td>
                                <td>
                                  <button data-toggle="modal" data-target="#unblock"   class="btn btn-sm btn-success" onclick="viewDetails({{$row->id}})"><i class="ti-pencil fs-13"></i></button>   
                                  <button data-toggle="modal" data-target="#delete" class="btn btn-sm btn-danger" onclick="deleteCategory({{$row->id}})"><i class="ti-trash fs-13"></i></button>
                                </td>
                              </tr>
                              @endforeach
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- delete block -->
        <div id="delete" class="modal fade">
          <div class="modal-dialog modal-sm">
            <div class="text-center">           
            </div>
            <div class="modal-body text-center">
              <div class="row">
                <div class="col-md-12">
                  <i class="text-danger fs-34 d-inline-block ti-trash mb-4"></i>
                  <h6>Are you sure want to delete this category?</h6>
                </div>
              </div>
              <hr />
              <div class="row m-t-15">
                <div class="col-sm-12">
                 <a class="btn btn-sm btn-success mr-1 continuedelete">Yes</a>                 
                  <button type="button" class="btn btn-sm btn-danger ml-1" data-dismiss="modal">Cancel</button>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- success modal -->
        <div id="delete-sccess-modal" class="modal fade">
          <div class="modal-dialog modal-sm" data-dismiss="modal">
            <div class="modal-body text-center">
              <div class="row">
                <div class="col-md-12">
                  <i class="text-danger fs-34 d-inline-block icofont icofont-check-circled mb-4"></i>
                  <h6>@if(session()->get('error_message')){{ session()->get('error_message') }} @elseif(session()->get('success_message')) {{ session()->get('success_message') }} @endif</h6>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div id="add" class="modal fade">
          <div class="modal-dialog modal-md">
            <div class="modal-content">
              <div class="modal-header bg-primary">
                <h6 class="modal-title">Add Category</h6>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
              </div>
              <div class="modal-body">
                <form action="{{ url('admin/add_category') }}" method="post" enctype="multipart/form-data" id="addCaregory">
                @csrf
                  <div class="form-group">
                    <label>Category Name</label>
                     <input type="text" class="form-control" name="name" id="name">
                  </div>
                  <span class="text-danger" id="categorynameError" style="display: none;">Enter category name</span>

                  <div class="form-group">
                    <label>Upload Image(Size will be 300*300) <span class="text-danger">* </span></label>
                     <div class="single-img">
                      <div class="img-box">
                      <img id="image_preview_container" src="{{url('public/admin/images/avatar-3.jpg')}}" alt="your image" class="img" />
                      <label class="file1"> Upload Image 
                      <input type='file' id="image" name="cate_image" onchange="readURL(this);" /> 
                    </label>
                    <span class="text-danger" id="imageError" style="display: none;">Please attach Image </span>

                     </div>
                    </div>
                  </div>

                  <div class="form-group text-center">
                    <button type="submit" class="btn btn-md btn-success">Save</button>
                    <button class="btn btn-md btn-danger" data-dismiss="modal">Cancel</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <div id="edit" class="modal fade">
          <div class="modal-dialog modal-md">
            <div class="modal-content">
              <div class="modal-header bg-primary">
                <h6 class="modal-title">Edit Category</h6>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
              </div>
              <div class="modal-body">
                 <form action="{{ url('admin/update-category') }}" method="post" enctype="multipart/form-data" id="updateCategory">
                 @csrf
                    <input type="hidden" name="editId" id="editId">
                    <div class="form-group">
                      <label>Category Name</label>
                       <input type="text" class="form-control" name="name" id="category_name">
                   </div>
                   <span class="text-danger" id="updatecategorynameError" style="display: none;">Enter category name</span>

                   <div class="form-group">
                    <label>Upload Image(Size will be 300*300) <span class="text-danger">* </span></label>
                     <div class="single-img">
                      <div class="img-box">
                      <img id="EditOffer_image" src="{{url('public/admin/images/avatar-3.jpg')}}" alt="your image" class="img" />
                      <label class="file1"> Upload Image 
                      <input type='file' id="Editimage" name="image" onchange="readURL(this);" />                      
                       <input type="hidden" id="Editimage" name="image">
                    </label>
                    <span class="text-danger" id="Editimageerror" style="display: none;">Please attach Image </span>
                      </div>
                    </div>
                  </div>

                    <div class="form-group text-center">
                      <button type="submit" class="btn btn-md btn-success">Save</button>
                      <button class="btn btn-md btn-danger" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
              </div>
            </div>
          </div>          
          <div id="unblock-sccess-modal" class="modal fade" role="dialog">
            <div class="modal-dialog modal-sm" data-dismiss="modal">
              <div class="text-center"></div> 
              <div class="modal-body text-center">
                <div class="row">
                  <div class="col-md-12">
                    <i class="text-success fs-34 d-inline-block icofont icofont-check-circled mb-4"></i>
                    <h6>Unblock Successfully</h6>
                  </div>
                </div>
              </div>
            </div>
          </div>          
    @endsection

    @section('footerscript')

      <script type="text/javascript">
          $('#addCaregory').on('submit',function(e) {
              
              $('#categorynameError').hide();
              $('#imageError').hide();
              var catename = $('#name').val();
              var image = $('#image').val();

              if(catename == ''){
                $('#categorynameError').show();
                e.preventDefault();
              }
              if(image == ''){
                $('#imageError').show();
                e.preventDefault();
              }
          });
      </script>
      <script type="text/javascript">
          $('#updateCategory').on('submit',function(e) {
              
              $('#updatecategorynameError').hide();
              var categoryname = $('#category_name').val();              
              if(categoryname == ''){
                // alert("sdfdsfds");
                $('#updatecategorynameError').show();
                e.preventDefault();
              }              
          });
      </script>
      
      <script type="text/javascript">
          $('#Editimage').change(function(){          
            let reader = new FileReader();
            reader.onload = (e) => { 
              $('#EditOffer_image').attr('src', e.target.result); 
            }
            reader.readAsDataURL(this.files[0]);  
        }); 
      </script>

      @if ($message = session()->has('success_message'))
        <script type="text/javascript">
           $(function() {
              $('#delete-sccess-modal').modal('show');
              setTimeout(function() {$('#delete-sccess-modal').modal('hide');}, 2000);
            });
        </script>
      @endif

      @if ($message = session()->has('error_message'))
        <script type="text/javascript">
           $(function() {
              $('#delete-sccess-modal').modal('show');
              setTimeout(function() {$('#delete-sccess-modal').modal('hide');}, 2000);
            });
        </script>
      @endif
      
      <script type="text/javascript">
        $('#account-list').DataTable();      
        function deleteCategory(id){
          $(".continuedelete").attr("href","{{url('admin/delete_category')}}"+"/"+id);
        }

        function viewDetails(id){           
           var myurl="{{url('admin/category-edit')}}";
           $.ajax({
             url:myurl+'/'+id,
             type:"get",
             success:function(data)
             {
             // console.log(data[0]);
             var img="{{ url('/')}}";
             var default_img="{{ url('public/admin/images/avatar-3.jpg')}}";
             
             $('#editId').val(data[0].id);
             $('#category_name').val(data[0].category_name);

             $('#cate_image').val(data[0].cate_image);
             if(data[0].cate_image){
               $('#EditOffer_image').attr('src',img+'/'+data[0].cate_image);
             } else {
               $('#EditOffer_image').attr('src',default_img);
             }

             }
           }); 
         $('#edit').modal('show');     
        }
      </script>

      <script type="text/javascript">
        
        $('#image').change(function(){          
            let reader = new FileReader();
            reader.onload = (e) => { 
              $('#image_preview_container').attr('src', e.target.result); 
            }
            reader.readAsDataURL(this.files[0]);  
        }); 

      </script>
    @endsection