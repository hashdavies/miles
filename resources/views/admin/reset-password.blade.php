<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>    

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui" />
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}" id="token">
    <title>Myles | Forgot Password</title>
    <!-- css -->     
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    
    <meta name="theme-color" content="#ffffff">
    <link rel="icon" href="assets/img/favicon.png" type="image/png" sizes="16x16">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,400italic,500,700">
    <link rel="stylesheet" href="{{url('public/admin/css/vendor.min.css')}}">
    <link rel="stylesheet" href="{{url('public/admin/css/elephant.min.css')}}">
    <link rel="stylesheet" href="{{url('public/admin/css/application.min.css')}}">
    <link rel="stylesheet" href="{{url('public/admin/css/login-2.min.css')}}">

    <link rel="stylesheet" type="text/css" href="{{url('public/admin/css/globle.css')}}" />
  </head>
  <body onLoad="noBack();" onpageshow="if (event.persisted) noBack();" onUnload="">
    <div class="login">
      <div class="login-body">
        <a class="login-brand" href="#">
          <img class="img-responsive" src="{{url('public/admin/img/logo.png')}}" alt="logo">
        </a>
        <div class="login-form">
          <h5>Reset Password</h5>
              @if (session('error'))
                <div class="alert alert-danger">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> 
                @foreach(session('error') as $err)
                  <p align="left">{{$err}}</p>    
                @endforeach                            
                </div>
              @endif

              <form method="post" data-toggle="validator" action="{{ url('admin/updateresetpassword') }}">
                @csrf
                <input type="hidden" name="email" value="@if($email) {{ $email }} @elseif($getopt->mobile_no) {{ $getopt->mobile_no }} @endif">                
                <div class="form-group">
                  <label for="password">Password</label>
                  <input id="password" class="form-control" type="password" name="password" minlength="6" data-msg-minlength="Password must be 6 characters or more." data-msg-required="Please enter your password." required>                  
                </div>
                @if($errors->first('password'))
                     <p style="color: red; text-align: left; margin-top: -7px;
margin-bottom: 9px;">{{ $errors->first('password') }}</p>
                  @endif
                <div class="form-group">
                  <label for="password">Confirm Password</label>
                  <input id="password" class="form-control" type="password" name="confirm_password" minlength="6" data-msg-minlength="Password must be 6 characters or more." data-msg-required="Confirm password." required>
                  
                </div>
                @if($errors->first('confirm_password'))
                     <p style="color: red; text-align: left; margin-top: -7px;
margin-bottom: 9px;">{{ $errors->first('confirm_password') }}</p>
                  @endif
                <button class="btn btn-primary btn-block mb-2" type="submit">Sign in</button>
                <div class="text-center"><a href="{{url('admin/login')}}">Back to Sign In</a></div>
              </form>
          </div>
       </div>
    </div>
    <script src="{{url('public/admin/js/vendor.min.js')}}"></script>
    <script src="{{url('public/admin/js/elephant.min.js')}}"></script>
    <script src="{{url('public/admin/js/application.min.js')}}"></script>

    <!-- Success Modal -->
    <div class="modal fade" id="success-modal">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
          <div class="modal-body text-center">
            <h2><i class="fa fa-link text-danger"></i></h2>
            <h5>New Password Set Successfully!</h5>
          </div>
        </div>
      </div>
    </div>

    <script type="text/javascript">
        window.history.forward();
        function noBack()
        {
            window.history.forward();
        }
    </script>
</body>
</html>