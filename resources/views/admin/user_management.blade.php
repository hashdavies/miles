@extends('admin.header')
@section('title', 'Customer Management')
@section('content')
  @include('admin/navbar')
  <div class="responsiveTbPage ExplorerListPage">
	<div class="layout-content">
      <div class="layout-content-body">
       <div class="title-bar">
          <h1 class="title-bar-title">
            <span class="d-ib">User Management</span>
          </h1>
      </div>
      <div class="row">
        <div class="col-xs-12">
          <div class="card">
            <div class="card-header pb-1">
              <div class="row gutter-xs">
               <form action="{{url('admin/user-mgmt-filter')}}" method="get" class="">

                <div class="col-xs-6 col-sm-2 col-md-2 mb-2">
                  <label class="wpx-50">From</label>
                  <input type="text" id="start_date" name="start_date" class="form-control" placeholder="mm/dd/yy" onkeypress="myfun()">
                </div>
                <div class="col-xs-6 col-sm-2 col-md-2 mb-2">
                  <label class="wpx-50">To</label>
                  <input type="text" placeholder="mm/dd/yy" id="end_date" name="end_date" class="form-control" onkeypress="myfun()">
                </div>
                
                <div class="col-xs-6 col-sm-2 col-md-2 mb-2">
                  <label>Province</label>
                  <select class="form-control" name="province" onchange="myfun()" id="province">
                    <option value="">Choose province</option>
                    @foreach($province as $rowprovince)
                    	<option data-id="{{$rowprovince->province}}" value="{{$rowprovince->province}}">{{$rowprovince->province}}</option>
                    @endforeach
                  </select>
                </div>    
                <div class="col-xs-6 col-sm-2 col-md-2 mb-2">
                  <label>City</label>
                  <select class="form-control" name="city" id="city" onchange="myfun()">
                    <option value="">Choose City</option>
                     
                  </select>
                </div>                
                <div class="col-xs-6 col-sm-2 col-md-2 mb-2">
                  <label>Sort By</label>
                  <select class="form-control" name="status" onchange="myfun()">
                    <option value="">Choose Status</option>
                    <option value="1">Active</option>
                    <option value="0">Inactive</option>
                  </select>
                </div>

                <div class="col-xs-6 col-sm-2 col-md-2 mb-2">
                	<br>
                	<a href="{{url('admin/user-management')}}" class="btn btn-sm btn-primary pull-right" style="">Reset</a>
	                  <button type="submit" class="btn btn-sm btn-primary pull-right " style="margin-right: 12px;">Apply</button>
	                  
                </div> 
              <form>              

              </div>
            </div>
 			<div class="card-body">
              <div class="table-responsive">
              	
              	<div class="clearfix"></div>
              	<a href="{{url('admin/export-user')}}" class="btn btn-primary pull-right mb-2">Export Excel</a>
                <div class="clearfix"></div>

                <table class="table dataTable table-striped table-bordered">
                  <thead>
                    <tr>
                      <th>User ID</th>
                      <th>User Name</th>
                      <th>Email ID</th>
                      <th>Phone No.</th>
                      <th>Joining Date</th>
                      <th>Address</th>
                      <th>Status</th>
                      <th class="wpx-100 no-sort">Action</th>
                    </tr>
                  </thead>
                  <tbody>

	              	@foreach($userData as $key=>$row)
	                	<tr>              
	                 	  <td>{{ $key+1 }}</td>
	                      <td>{{ $row->firstName }} {{ $row->lastName}}</td>
	                      <td>{{ $row->emailAddress }}</td>
	                      <td>{{ $row->countryCode}}-{{ $row->mobileNo }}</td>
	                      <td>{{ $row->registerDate }}</td>
	                      <td>{{ $row->address }}</td>
	                      <td>
	                      	@if($row['status'] == 0)
	                      		<span style="color: red">Deactivate</span>
	                      	@elseif($row['status'] == 1)
	                      		<span style="color: green">Active</span>
	                      	@endif
	                      </td>
	                      <td>

	                      	<button data-toggle="modal" data-target="#view" class="btn btn-sm btn-info" title="View" onclick="viewData({{$row['id']}})"><i class="fa fa-eye"></i></button>

	                      	@if($row['status'] == 0)

		                    <button data-toggle="modal" data-target="#block-modal" class="btn btn-sm btn-danger" title="Inactive" onclick="blockUnblock({{$row['id']}},1)"><i class="fa fa-toggle-off"></i></button>

		                    @else

		                    <button data-toggle="modal" data-target="#block-modal" class="btn btn-sm btn-success" title="Active" onclick="blockUnblock({{$row['id']}},0)"><i class="fa fa-toggle-on"></i></button>
		                    
		                    @endif
		                   
	                      </td> 
	                	</tr>
	                @endforeach
                    </tbody>
	                </table>
	              </div>
	            </div>
	          </div>
	        </div>
	      </div>
	    </div>
	  </div>
	</div>

	<!-- block modal -->
	<div class="modal fade" id="block-modal">
	  <div class="modal-dialog modal-sm">
	    <div class="modal-content">
	      <div class="modal-body text-center">
	        <h2><i class="fa fa-toggle-off text-success"></i></h2>
	        <h5 id="tilteblock"></h5>
	        <div class="text-center">
	          <a class="btn btn-success btn-sm countinueblock">Yes</a>
	          <button type="button" data-dismiss="modal" class="btn btn-danger btn-sm">
	            No
	          </button>
	        </div>
	      </div>
	    </div>
	  </div>
	</div>
	<!-- unblock modal -->

				<div id="unblock-modal" class="modal fade" role="dialog">
				  <div class="modal-dialog modal-sm">
				    <div class="text-center">  
				    </div>
				    <div class="modal-body text-center">
				      <div class="row">
				        <div class="col-md-12">
				          <i class="text-success fs-34 d-inline-block ti-unlink mb-4"></i>
				          <h6>Are you sure want to unblock this person?</h6>
				        </div>
				      </div>
				      <hr />
				      <div class="row m-t-15">
				        <div class="col-sm-12">
				          <button type="button" class="btn btn-sm btn-success mr-1" data-target="#unblock-sccess-modal" data-toggle="modal" data-dismiss="modal">Unblock</button>
				          <button type="button" class="btn btn-sm btn-danger ml-1" data-dismiss="modal">Cancel</button>
				        </div>
				      </div>
				    </div>
				  </div>
				</div>
				<!-- delete block -->
				<div id="delete-modal" class="modal fade" role="dialog">
				  <div class="modal-dialog modal-sm">
				    <div class="text-center"> 
				    </div>
				    <div class="modal-body text-center">
				      <div class="row">
				        <div class="col-md-12">
				          <i class="text-danger fs-34 d-inline-block ti-trash mb-4"></i>
				          <h6>Are you sure want to delete this User?</h6>
				        </div>
				      </div>
				      <hr />
				      <div class="row m-t-15">
				        <div class="col-sm-12">
				          <a class="btn btn-sm btn-success mr-1 continuedelete">Yes</a>
				          <button type="button" class="btn btn-sm btn-danger ml-1" data-dismiss="modal">Cancel</button>
				        </div>
				      </div>
				    </div>
				  </div>
				</div>
				<!-- success modal -->
				<div id="block-sccess-modal" class="modal fade" role="dialog">
				  <div class="modal-dialog modal-sm" data-dismiss="modal">
				    <div class="text-center">
				    </div>
				    <div class="modal-body text-center">
				      <div class="row">
				        <div class="col-md-12">
				          <i class="text-warning fs-34 d-inline-block icofont icofont-check-circled mb-4"></i>
				          <h6>Block Successfully</h6>
				        </div>
				      </div>
				    </div>
				  </div>
				</div>
				<!-- success modal -->
				<div id="unblock-sccess-modal" class="modal fade" role="dialog">
				  <div class="modal-dialog modal-sm" data-dismiss="modal">
				    <div class="text-center"></div> 
				    <div class="modal-body text-center">
				      <div class="row">
				        <div class="col-md-12">
				          <i class="text-success fs-34 d-inline-block icofont icofont-check-circled mb-4"></i>
				          <h6>Unblock Successfully</h6>
				        </div>
				      </div>
				    </div>
				  </div>
				</div>
				<!-- success modal -->
				<div id="delete-sccess-modal" class="modal fade" role="dialog">
				  <div class="modal-dialog modal-sm" data-dismiss="modal">
				    <div class="text-center">
				    </div>
				    <div class="modal-body text-center">
				      <div class="row">
				        <div class="col-md-12">
				          <i class="text-danger fs-34 d-inline-block icofont icofont-check-circled mb-4"></i>
				          <h6>Delete Successfully</h6>
				        </div>
				      </div>
				    </div>
				  </div>
				</div>


		<!-- View Modal -->
		<div class="modal fade" id="view">
		  <div class="modal-dialog">
		    <div class="modal-content">
		      <div class="modal-header bg-primary">
		        <h4 class="modal-title">User Information</h4>
		         <button type="button" data-dismiss="modal" class="close">&times;</button>
		        
		      </div>
		      <div class="modal-body">
		        <div class="row">
		          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center mb-3">
		            <img id="profileImage" class="whpx-80 circle mb-3"/>
		            <h5 id="fullName"></h5>
		          </div>
		          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		            <div style="background-color:#c7c4c463; color:#000;padding: 36px; padding-bottom: 11px;">
		            <h4>General Information</h4>
		           
		            <div class="form-group d-flex">
		              <label class="pr-2">Email :</label>
		              <div id="email"></div>
		            </div>
		            <div class="form-group d-flex">
		              <label class="pr-2">Mobile Number :</label>
		              <div id="mobileNumber"></div>
		            </div>
		            <div class="form-group d-flex">
		              <label class="pr-2">Joining Date :</label>
		              <div id="registerDate"></div>
		            </div>
		            <div class="form-group d-flex">
		              <label class="pr-2">Address :</label>
		              <div id="address"></div>
		            </div>
		            
		            <h4>Other Information</h4>
		            <div class="form-group d-flex">
		              <label class="pr-2">Ongoing Gig :</label>
		              <div id="email">1</div>
		            </div>
		            <div class="form-group d-flex">
		              <label class="pr-2">Total Cancelled Gigs :</label>
		              <div id="email">2</div>
		            </div>
		            <div class="form-group d-flex">
		              <label class="pr-2">Total Gigs Placed :</label>
		              <div id="email">9</div>
		            </div>
		            <div class="form-group d-flex">
		              <label class="pr-2">Last Gig Placed :</label>
		              <div id="email">10 Days ago</div>
		            </div>
		            <div class="form-group d-flex">
		              <label class="pr-2">Total Gigs Completed :</label>
		              <div id="email">10</div>
		            </div>
		          </div>
		          <br>	            
		            <div id="useraddres">
		            </div><br>
		            <div id="table">		            	
		            </div>
		          </div>

		        </div>
		      </div>
		    </div>
		  </div>
		</div>          
	@endsection

	@section('footerscript')

	  <script>
	      $(document).ready(function(){
	        // $("input").keydown(function(){
	        //  alert("dcfdsfdas");
	        // });
	        $("input").click(function(){
	          
	          $("#showSubmitButton").html(`<a href="{{url('admin/user-management')}}" class="btn btn-sm btn-primary pull-right" style="">Reset</a>
	                  <button type="submit" class="btn btn-sm btn-primary pull-right " style="margin-right: 12px;">Apply</button>`);

	        });
	      });
    </script>

        <script type="text/javascript">
          
          function myfun(){  
            
            $("#showSubmitButton").html(`<a href="{{url('admin/user-management')}}" class="btn btn-sm btn-primary pull-right" style="">Reset</a>
                      <button type="submit" class="btn btn-sm btn-primary pull-right " style="margin-right: 12px;">Apply</button>`);
            
          } 

        </script>

		@if ($message = session()->has('deleteuser'))
		  <script type="text/javascript">
		     $(function() {
		        $('#delete-sccess-modal').modal('show');
		        setTimeout(function() {$('#sdelete-sccess-modal').modal('hide');}, 2000);
		      });
		  </script>
		@endif			 		

		<script type="text/javascript"> 
			
		    function viewData(id){
		      if(id>0){
		      		
		          var globalarray = <?php echo json_encode($userData); ?>;
		          var theIndex = -1;
		          for (var i = 0; i < globalarray.length; i++) {
		            if (globalarray[i].id == id) {
		              theIndex = i;
		              break;
		            }
		          }		          

		          $("#editid").text(globalarray[theIndex].id);
		          // $("#fullName").text(globalarray[theIndex].firstName);	
		          
		          // console.log(globalarray[theIndex].gigAddress);	
		          // var gigAddress = val(globalarray[theIndex].gigAddress);

		          $("#fullName").html(`<h5>`+globalarray[theIndex].firstName+` `+globalarray[theIndex].lastName+`</h5>`);
		          $("#mobileNumber").html(`<div>`+globalarray[theIndex].countryCode+`-`+globalarray[theIndex].mobileNo+`</div>`);
		          $("#email").text(globalarray[theIndex].emailAddress);
		          $("#dob").text(globalarray[theIndex].dob);

		          $("#registerDate").text(globalarray[theIndex].registerDate);
		          
		          // $('#address').text(globalarray[theIndex].address);
		          $('#address').html(`<div>`+globalarray[theIndex].address+`</div>`);
		          // $('#city').text(globalarray[theIndex].city);
		          
		          var img="{{ url('https://mylesimages.s3.ap-south-1.amazonaws.com/Serviceprovider')}}";
		          var default_img="{{ url('public/admin/images/Group-user.jpg')}}";
		          if(globalarray[theIndex].profileImage){
		            $('#profileImage').attr('src',img+'/'+globalarray[theIndex].profileImage);
		          } else {
		            $('#profileImage').attr('src',default_img);
		          }
		          
		          $.ajax({
                     type:"GET",
                     url:"{{url('admin/user-gig-address')}}"+'/'+globalarray[theIndex].id,
                     success:function(res){  
                     // console.log(res);
                        if(res){
                            $("#table").empty();
                            $.each(res,function(key,value){
                               $("#table").append('<div style="background-color:#c7c4c4; color:#000;padding: 36px; padding-bottom: 11px;"><h4>User Gig Address '+(key+1)+'</h4><div class="form-group d-flex"><label class="pr-2">Pickup Address: </label><div>'+value.pickUpAddress+'</div></div><div class="form-group d-flex"><label class="pr-2">Pickup Address Type: </label><div>'+value.pickUpAddressType+'</div></div><div class="form-group d-flex"><label class="pr-2">Drop Address: </label><div>'+value.dropAddress+'</div></div><div class="form-group d-flex"><label class="pr-2">Drop Address Type: </label><div>'+value.dropAddressType+'</div></div></div><br>');
                            });
                        }else{
                           // $("#table").empty();
                           $("#table").text('NO');
                        }
                     }
                  });


		          $.ajax({
                     type:"GET",
                     url:"{{url('admin/user-address')}}"+'/'+globalarray[theIndex].id,
                     success:function(res){  
                     // console.log(res);
                        if(res){
                            $("#useraddres").empty();
                            $.each(res,function(key,value1){
                               $("#useraddres").append('<div style="background-color:#c1d8ef; color:#000;padding: 36px; padding-bottom: 11px;"><h4>User Address '+(key+1)+'</h4><div class="form-group d-flex"><label class="pr-2">Pickup Address:  </label><div>'+value1.address+'</div></div><div class="form-group d-flex"><label class="pr-2">City: </label><div>'+value1.city+'</div></div><div class="form-group d-flex"><label class="pr-2">Province: </label><div>'+value1.province+'</div></div></div><br>');
                            });

                        }else{
                           $("#useraddres").empty();
                        }
                     }
                  });

		    	  // $('#useraddres').html(`<div>`+userAddress+`</div>`);
		          // $('#table').html(`<div>`+newItem+`</div>`);

		          $("#titleText").text("Edit Category");
		      }
		      else
		      {
		          $("#editid").val("");
		          $("#name").val("");
		          $("#titleText").text("Add Category");
		      }
		  }
		  </script>

		  <script type="text/javascript">		
			function blockUnblock(id,type){
			    if(type == 0){
			      $("#tilteblock").text("Are you sure you want to Deactivate User?");
			    }
			    else if(type == 1){
			      $("#tilteblock").text("Are you sure you want to Active this User?");
			    }
			    $(".countinueblock").attr("href","{{url('admin/blockUser')}}"+"/"+id+"/"+type);
			}
			
		</script>

		<script type="text/javascript">
			function deleteUser(id){
				$(".continuedelete").attr("href", "{{url('admin/deleteUser')}}"+"/"+id);
			}
			function deleteData(id){
		      $(".continuedelete").attr("href","{{url('admin/deleteCategory')}}"+"/"+id);
		    }	    
		</script>

		<script type="text/javascript">
		      $('#account-list').DataTable();
		</script>
		<!-- block modal -->

		<script type="text/javascript">
	        $('#province').on('change',function(){
	        // alert();
	        var countryID = $("#province").find(":selected").data("id");;
	        //var countryID = $(this).val(); 
	        // alert(countryID);  
	        if(countryID){
	            $.ajax({
	               type:"GET",
	               url:"{{url('admin/UserCity')}}"+'/'+countryID,
	               success:function(res){  
	               //console.log(res[0]); 
	                if(res){

	                    $("#city").empty();

	                    $.each(res,function(key,value){
	                        $("#city").append('<option value="'+value.city+'">'+value.city+'</option>');
	                    });
	               
	                }else{
	                   $("#city").empty();
	                }
	               }
	            });
	        }else{
	            $("#city").empty();
	       }
	     });
	    </script>
	@endsection