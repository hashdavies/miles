@extends('admin.header')
@section('title', 'Review & Rating Management')
@section('content')
@include('admin/navbar')

<!-- <script src="https://maps.google.com/maps/api/js?key=AIzaSyDxTV3a6oL6vAaRookXxpiJhynuUpSccjY&amp;libraries=places&amp;callback=initAutocomplete" type="text/javascript"></script> -->

<script src="https://maps.google.com/maps/api/js?key=AIzaSyDxTV3a6oL6vAaRookXxpiJhynuUpSccjY&amp;libraries=places&amp;callback=initAutocomplete" type="text/javascript"></script>

<script type="text/javascript">
    google.maps.event.addDomListener(window, 'load', function () {
        var places = new google.maps.places.Autocomplete(document.getElementById('txtPlace'));
        google.maps.event.addListener(places, 'place_changed', function () {
            var place = places.getPlace();
            var address = place.formatted_address;
            var latitude = place.geometry.location.lat();
            var longitude = place.geometry.location.lng();
            var latlng = new google.maps.LatLng(latitude, longitude);
            var geocoder = geocoder = new google.maps.Geocoder();
            geocoder.geocode({ 'latLng': latlng }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        var address = results[0].formatted_address;
                        var pin = results[0].address_components[results[0].address_components.length - 1].long_name;

                        var country = results[0].address_components[results[0].address_components.length - 3].long_name;

                        var country1 = results[0].address_components[results[0].address_components.length - 4].long_name;

                        var city = results[0].address_components[results[0].address_components.length - 4].long_name;
                        document.getElementById('txtCountry').value = country;
                        
                        document.getElementById('txtCountry1').value = country1;
                        
                    }
                }
            });
        });
    });
</script>


<div class="responsiveTbPage ExplorerListPage">
  <div class="layout-content">
    <div class="layout-content-body">
      <div class="title-bar">
        <h1 class="title-bar-title">
          <span class="d-ib">Rating Management</span>
        </h1>
      </div>
      <div class="row">
        <div class="col-xs-12">
          <div class="card">
            <div class="card-header pb-1">
            
            <form action="{{url('admin/rating-filter')}}" method="get" id="add_filter">
              <div class="row gutter-xs">
                <div class="col-xs-6 col-sm-2 col-md-2 mb-2">
                  <label class="wpx-50">From</label>
                  <input type="text" id="start_date" name="from" class="form-control" placeholder="mm/dd/yyyy" onkeypress="myfun()">
                </div>
                <div class="col-xs-6 col-sm-2 col-md-2 mb-2">
                  <label class="wpx-50">To</label>
                  <input type="text" id="end_date" name="to" class="form-control" placeholder="mm/dd/yyyy" onkeypress="myfun()">
                </div>


                <div class="col-xs-6 col-sm-2 col-md-2 mb-2">
                  <label>Province</label>

                  <input type="text" class="form-control" name="province" id="txtPlace" placeholder="Enter a location" />
                  <input type="hidden" name="province" id="txtCountry" />                  
                  <!--  <input type="text" id="txtState" />-->
                  <!-- <input type="text" id="txtCountry1" /> -->
                

                  <!-- <select class="form-control" name="province" onchange="myfun()" id="province">
                    <option value="">Choose Province</option>
                    @foreach($province as $rowprovince)
                      <option value="{{$rowprovince->province}}" data-id="{{$rowprovince->province}}">{{$rowprovince->province}}</option>
                    @endforeach
                  </select> -->
                </div>
                 <div class="col-xs-6 col-sm-3 col-md-2 mb-2">
                  <label>City</label>
                  <input type="text" class="form-control" name="city" id="txtCountry1" />
                  <!-- <select class="form-control" name="city" id="city" onchange="myfun()">
                    <option value="">Choose City</option>
                  </select> -->
                </div>                
                <div class="col-xs-6 col-sm-2 col-md-2 mb-2">
                  <label>Sort</label>
                  <select class="form-control" name="sort" onchange="myfun()">
                    <option value="">Select here</option>
                    <option value="desc">H. to L Rating</option>
                    <option value="1">1-2 star</option>
                    <option value="3">3-4 star</option>
                    <option value="5">5 star</option>
                  </select>
                </div>
                <div class="col-xs-6 col-sm-2 col-md-2 mb-2">
                  <br>                  
                  <a href="{{url('admin/review-ratings-management')}}" class="btn btn-sm btn-primary pull-right" style="">Reset</a>
                    <button type="submit" class="btn btn-sm btn-primary pull-right " style="margin-right: 12px;">Apply</button>
                  <!-- <div id="showSubmitButton">
                        
                    </div> -->
                </div> 
              </div>
            </form>

            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table dataTable table-striped table-bordered">
                  <thead>
                    <tr>
                      <th>Gig ID</th>
                      <th>User Name</th>
                      <th>Service Provider Name</th>
                      <th>Date</th>
                      <th>Time</th>
                      <th class="wpx-80 no-sort">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($RatingReview as $rowrating)
                    <tr>
                      <td>#{{$rowrating->gig_id}}</td>
                      <td>{{@$rowrating->UserName->firstName}}</td>
                      <td>{{@$rowrating->SellerName->firstName}}</td>
                      <td>{{$rowrating->ratingRegisterDate}}</td>
                      <td>{{$rowrating->registerTime}}</td>                      
                      <td>
                        <div class="d-flex">
                          <button type="button" onclick="viewRating({{$rowrating->id}})" class="btn btn-success btn-sm mb-1">View Ratings</button>

                        </div>  
                      </td>
                    </tr>
                    @endforeach                    
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>

<!-- Block Modal -->
<div class="modal fade" id="block-modal">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-body text-center">
        <h2><i class="fa fa-toggle-off text-success"></i></h2>
        <h5>Are you sure you want to block this user?</h5>
        <div class="text-center">
          <button type="button" data-dismiss="modal" class="btn btn-success btn-sm">
            Yes
          </button>
          <button type="button" data-dismiss="modal" class="btn btn-danger btn-sm">
            No
          </button>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Unblock Modal -->
<div class="modal fade" id="unblock-modal">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-body text-center">
        <h2><i class="fa fa-toggle-on text-danger"></i></h2>
        <h5>Are you sure you want to unblock this user?</h5>
        <div class="text-center">
          <button type="button" data-dismiss="modal" class="btn btn-success btn-sm">
            Yes
          </button>
          <button type="button" data-dismiss="modal" class="btn btn-danger btn-sm">
            No
          </button>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Delete Modal -->
<!-- <div class="modal fade" id="delete-modal">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-body text-center">
        <h2><i class="fa fa-trash-o text-danger"></i></h2>
        <h5>Are you sure you want to delete this user?</h5>
        <div class="text-center">
          <button type="button" data-dismiss="modal" class="btn btn-success btn-sm">
            Yes
          </button>
          <button type="button" data-dismiss="modal" class="btn btn-danger btn-sm">
            No
          </button>
        </div>
      </div>
    </div>
  </div>
</div> -->

<div class="modal fade" id="set-taxes">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header bg-primary">
        <h5 class="modal-title">Set Taxes</h5>
        <button type="button" data-dismiss="modal" class="close">&times;</button>
      </div>
      <div class="modal-body">
        <form>
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
               <label>Select Province</label>
               <select class="form-control">
                <option>choose</option>
                <option>choose</option>
                <option>choose</option>
              </select>
            </div> 
          </div>  
          <div class="col-md-12">
            <div class="form-group">
             <label>Set Tax % on offer amount</label>
             <select class="form-control">
              <option>choose</option>
              <option>choose</option>
              <option>choose</option>
            </select>
          </div> 
        </div>  
        <div class="col-md-12">
          <div class="form-group">
           <label>Set Manually</label>
           <input type="text" class="form-control" name="">
         </div> 
       </div>  
       <div class="col-md-12">
        <div class="form-group">
         <button type="button" data-dismiss="modal" class="btn btn-success btn-sm">
          Confirm
        </button>
      </div> 
    </div>  
  </div>
</form> 
</div>
</div>
</div>
</div>
<div class="modal fade" id="set-service">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header bg-primary">
        <h5 class="modal-title">Set Service Charges</h5>
        <button type="button" data-dismiss="modal" class="close">&times;</button>
      </div>
      <div class="modal-body">
        <form>
          <div class="row">


            <div class="col-md-12">
              <div class="form-group">
               <label>Set Service Charge %</label>
               <input type="text" class="form-control" name="">
             </div> 
           </div>  
           <div class="col-md-12">
            <div class="form-group">
             <button type="button" data-dismiss="modal" class="btn btn-success btn-sm">
              Set
            </button>
          </div> 
        </div>  
      </div>
    </form> 
  </div>
</div>
</div>
</div>

<!-- Rating Modal -->
<div class="modal fade" id="rating-modal">
  <div class="modal-dialog modal-md">
    
    <div class="modal-content">
     
      <div class="modal-body text-center bg-primary">
        <h4 class="text-left mb-0"><strong>RATINGS & REVIEW</strong> </h4>
      </div>
      


      <div class="modal-body text-center mt-0"> 
        <h4 class="text-left"><strong>RATING & REVIEW </strong> given by user</h4> 
        <table class="table table-striped table-bordered no-footer" > 
          <thead>
            <tr> 
              <th class="h5">ORDER ID</th> 
              <td class="text-left h5" id="userorderid"><div class="stars-inner"></div></td> 
            </tr> 
            <tr> 
              <th class="h5">RATINGS</th> 
              <td class="text-left h5" id="ratings"> 
                
              </td> 
            </tr> 
          </thead> 
        </table>
        <div class="d-flex">
          <h5 class="text-left mb-0 pl-2">REVIEW :- </h5> <p class="text-left mb-0 pl-1" id="review"></p>
        </div>
               
      </div>


      <div id="serviceproviderrating"></div>

    </div>
  </div>
</div>
<!-- Rating Modal -->
@endsection

@section('footerscript')
    <script type="text/javascript">        
        function viewRating(id){

           $("#ratings").html('');
          // alert(id);
           if(id>0){
              var globalarray = <?php echo json_encode($RatingReview); ?>;
              var theIndex = -1;
              for (var i = 0; i < globalarray.length; i++) {
                if (globalarray[i].id == id) {
                  theIndex = i;
                  break;
                }
              }

              $("#userorderid").html(`<td>`+globalarray[theIndex].gig_id+`</td>`);
              // $("#rating").html(`<td>`+globalarray[theIndex].rating+`</td>`);
              $("#review").html(`<p>`+globalarray[theIndex].review+`</p>`);
              //$("#ratings").text(globalarray[theIndex].rating);
              // console.log(globalarray[theIndex].rating);

              for (var j = 0; j < globalarray[theIndex].rating; j++) {
                 $("#ratings").append('<i class="fa fa-star" aria-hidden="true"></i>');
              }

              for (var j = 0; j < (5 - globalarray[theIndex].rating); j++) {
                 $("#ratings").append('<i class="fa fa-star-o" aria-hidden="true"></i>');
              }
                  
              // const starTotal = 5;
              // alert(ratings);
              // for(var rating in ratings) { 
              // for (var i = 0; i < rating; i++) {
              //    // var starPercentage = (rating / starTotal) * 100;
              //    // var starPercentageRounded = `${(Math.round(starPercentage / 10) * 10)}%`;
              //    $("#ratings").html('<i class="fa fa-star" aria-hidden="true"></i>');
              //    // alert("sadsad");
              //    // var rating = "5";
              // }
              // $("#ratinger").html(ratings);
              // alert(ratings);
              // $('#rating').val(rating);

              if(globalarray[theIndex].sellerData){
                  
                 $("#serviceproviderrating").html('<div class="modal-body text-center mt-0"> <h4 class="text-left"><strong>RATING & REVIEW </strong> given by service provider</h4> <table class="table table-striped table-bordered no-footer" > <thead><tr> <th class="h5">ORDER ID</th> <td class="text-left h5">'+globalarray[theIndex].sellerData['seller_gigId']+'</td> </tr> <tr> <th class="h5">RATINGS</th> <td class="text-left h5" id="sellerratings"> </td> </tr> </thead> </table><div class="d-flex"><h5 class="text-left mb-0 pl-2">REVIEW :- </h5> <p class="text-left mb-0 pl-1"> '+globalarray[theIndex].sellerData['seller_review']+' </p></div></div>');

                 for (var i = 0; i < globalarray[theIndex].sellerData['sellerRating']; i++) {
                   var sellerratings = $("#sellerratings").append('<i class="fa fa-star" aria-hidden="true"></i>');
                 }

                 for (var i = 0; i < (5 - globalarray[theIndex].sellerData['sellerRating']); i++) {
                    var sellerratings =  $("#sellerratings").append('<i class="fa fa-star-o" aria-hidden="true"></i>');
                 }

              }else{
                 $('#serviceproviderrating').html('');
              }

              // if(globalarray[theIndex].sellerData['sellerRating']){
              //   $("#sellerRating").html(`<td>`+globalarray[theIndex].sellerData['sellerRating']+`</td>`);
              // }else{
              //    $('#sellerRating').html(`<td>test</td>`);
              // }
              
              // if(globalarray[theIndex].sellerData['seller_review']){
              //   $("#sellerReview").html(`<td>`+globalarray[theIndex].sellerData['seller_review']+`</td>`);
              // }else{

              // }
              
              // if(globalarray[theIndex].sellerData['seller_gigId']){
              //   $("#sellerorderid").html(`<td>#`+globalarray[theIndex].sellerData['seller_gigId']+`</td>`);
              // }else{

              // }
              
              
            }       
          $('#rating-modal').modal('show');
        }
    </script>
    
    <script>
        $(document).ready(function(){
          // $("input").keydown(function(){
          //  alert("dcfdsfdas");
          // });
          $("input").click(function(){
            
            $("#showSubmitButton").html(`<a href="{{url('admin/review-ratings-management')}}" class="btn btn-sm btn-primary pull-right" style="">Reset</a>
                    <button type="submit" class="btn btn-sm btn-primary pull-right " style="margin-right: 12px;">Apply</button>`);

          });
        });
      </script>

      <script type="text/javascript">
        
        function myfun(){  
          
          $("#showSubmitButton").html(`<a href="{{url('admin/review-ratings-management')}}" class="btn btn-sm btn-primary pull-right" style="">Reset</a>
                    <button type="submit" class="btn btn-sm btn-primary pull-right " style="margin-right: 12px;">Apply</button>`);
          
        } 

      </script>
      <script type="text/javascript">
          $('#province').on('change',function(){
          // alert();
          var countryID = $("#province").find(":selected").data("id");;
          //var countryID = $(this).val(); 
          // alert(countryID);  
          if(countryID){
              $.ajax({
                 type:"GET",
                 url:"{{url('admin/UserCity')}}"+'/'+countryID,
                 success:function(res){  
                 //console.log(res[0]); 
                  if(res){

                      $("#city").empty();

                      $.each(res,function(key,value){
                          $("#city").append('<option value="'+value.city+'">'+value.city+'</option>');
                      });
                 
                  }else{
                     $("#city").empty();
                  }
                 }
              });
          }else{
              $("#city").empty();
         }
       });
      </script>

@endsection