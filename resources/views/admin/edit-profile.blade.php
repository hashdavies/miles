 @extends('admin.header')
 @section('title', 'Pending Management')
 @section('content')
 @include('admin/navbar')

 <div class="profilePage"></div>
 <div class="layout-content">
  <div class="layout-content-body">
    <div class="title-bar">
      <h1 class="title-bar-title">
        <span class="d-ib">Edit Admin Profile /</span>
        <a href="profile.php">Back to list</a>
      </h1>
    </div>

    @if ($message = Session::get('message'))
      <div class="alert alert-success alert-block">
          <button type="button" class="close" data-dismiss="alert">×</button> 
          <strong>{{ $message }}</strong>
      </div>
      @endif

        @if ($error = Session::get('error'))
      <div class="alert alert-danger alert-block">
          <button type="button" class="close" data-dismiss="alert">×</button> 
          <strong>{{ $error }}</strong>
      </div>
      @endif

    <div class="row gutter-xs">
      <div class="col-sm-12 col-md-12 col-lg-8">
        <div class="card panel-body" style="box-shadow: 0px 0px 14px #999;">
          <div class="demo-form-wrapper">
            <form class="form" method="post" action="{{ route('update-profile') }}" enctype="multipart/form-data" >
              @csrf
              <div class="col-xs-12 col-md-6">
                <div class="form-group">
                  <label class=" control-label">Name</label>
                  <input id="" class="form-control" placeholder="Name" value="<?php if(count($adminProfile)>0){ echo $adminProfile['first_name']; }else{ echo ""; } ?>" name="first_name" type="text" required="">
                </div>
                <div class="form-group">
                  <label class=" control-label">Mobile</label>
                  <input id="" class="form-control" name="mobile" value="<?php if(count(@$adminProfile)>0){ echo @$adminProfile['mobile']; }else{ echo ""; } ?>" placeholder="Mobile" type="text" required="">
                </div>
                <div class="form-group">
                  <label class=" control-label">Email ID</label>
                  <input id="" class="form-control" placeholder="Email ID" value="<?php if(count($adminProfile)>0){ echo $adminProfile['emailAddress']; }else{ echo ""; } ?>" name="emailAddress" type="text" required="">
                </div>
                <div class="form-group">
                  <label class=" control-label">Region</label>
                  <input id="" class="form-control" name="country" value="<?php if(count($adminProfile)>0){ echo $adminProfile['country']; }else{ echo ""; } ?>" placeholder="Region" type="text" required="">
                </div>
              </div>
              <div class="col-xs-12 col-md-6">
                <!-- <div class="form-group">
                  <label for="about" class=" control-label">Description</label>
                  <textarea id="" class="form-control" rows="3"></textarea>
                </div> -->
                <div class="form-group add-pic">
                  <label class=" control-label">Add Images</label>
                  <div class="pic-box">
                    <div class="logoImg">
                      <?php if(count($adminProfile)>0){ ?>
                     <img src="{{ url('public/admin/img/') }}<?php echo '/'.$adminProfile['profile_image']; ?>" id="result">
                     <?php }else{ ?>
                      <img src="{{ url('public/admin/img/user.jpg') }}" id="result">
                     <?php } ?>
                     <input id="logo-file" type="file" name="profile_image" class="hide">
                     <label for="logo-file" class="btn btn-large"></label>
                   </div>
                 </div>
               </div>
             </div>
             <div class="form-group col-sm-12 mt-2">
              <button class="btn btn-primary wpx-120" type="submit">Submit</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
</div>



@endsection


@section('footerscript')

@if ($message = session()->has('successpassword'))
<script type="text/javascript">
  $(function() {
   $('#success-modal').modal('show');
   setTimeout(function() {$('#success-modal').modal('hide');}, 2000);
 });
</script>
@endif

@if ($message = session()->has('wrongpassword'))
<script>
 $(function() {
   $('#profile-success-modal').modal('show');
   setTimeout(function() {$('#profile-success-modal').modal('hide');}, 2000);
 });
</script>

@endif


<script type="text/javascript">

  $('.edit-info, .change-pass').hide();
      // var b=$(this).find("i");
      // var edit_class=b.attr('class');
      $('#edit-btn').on('click',function(){ 
        $('.view-info').hide();
        $('.edit-info').show(); 
        $('.change-pass').hide();
        $('#change-pass-btn').show();
        $(this).hide();  
      });
      $('.edit-info .edit-cancel').click(function(){
        $('.view-info').show();
        $('.edit-info').hide();
        $('#edit-btn').show();
      });
      $('#change-pass-btn').click(function(){
        $('.view-info').hide();
        $('.edit-info').hide();
        $('.change-pass').show(); 
        $('#edit-btn').show(); 
        $(this).hide();
      });
      $('.change-pass .edit-cancel').click(function(){
        $('.view-info').show();
        $('.change-pass').hide();
        $('#change-pass-btn').show();
      });
    </script>
    @endsection