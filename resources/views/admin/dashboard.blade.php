@extends('admin.header')    
    @section('title', 'Admin Dashboard')
    @section('content')   
     
  <div class="dashboardPage"></div>
  <div class="layout-content">
    <div class="layout-content-body">      
      <div class="row">
          <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 col-xl-3">
            <h1 class="title-bar-title">
              <span class="d-ib">Dashboard</span>
            </h1>
          </div>
          <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9 col-xl-9">

            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 col-xl-3">
              <div class="form-group pull-lg-right wpx-280">
                <div class="d-flex">
                  @php 
                   if(!empty($_GET['day'])){ 
                       $day = $_GET['day'];
                   }                        
                  @endphp
                      
                  <label class="white-space-nowrap pr-2 align-self-center">Fiter :</label>
                  <select class="form-control" name="dashboardFilter">
                    <option value="">Select </option>
                    <option value="1" {{ old('dashboardFilter', @$day == '1') ? 'selected' : '' }}>Today</option>
                    <option value="7" {{ old('dashboardFilter', @$day == '7') ? 'selected' : '' }}>This Week</option>
                    <option value="30" {{ old('dashboardFilter', @$day == '30') ? 'selected' : '' }}>This Month</option>
                    <!-- <option value="All" {{ old('dashboardFilter', @$day == 'All') ? 'selected' : '' }}>All</option> -->

                  </select>
                </div>
              </div>
            </div>
            <form action="{{url('admin/dashboard')}}" method="get">
              @csrf
              <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 col-xl-3">
                <div class="d-flex">
                  <label class="white-space-nowrap pr-2 align-self-center">From :</label>
                  <input type="text" required="" id="start_date" name="start_date" class="form-control" placeholder="mm/dd/yy" onkeypress="myfun()">
                </div>
              </div>
              <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 col-xl-3">
                <div class="d-flex">
                  <label class="white-space-nowrap pr-2 align-self-center">To</label>
                  <input type="text" required="" placeholder="mm/dd/yy" id="end_date" name="end_date" class="form-control" onkeypress="myfun()">
                </div>
              </div>
              <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 col-xl-3">
                <a href="{{url('admin/dashboard')}}" class="btn btn-sm btn-primary pull-right" style="">Reset</a>
                  <button type="submit" class="btn btn-sm btn-primary pull-right " style="margin-right: 12px;">Apply</button>
              </div>
            </form>

          </div>
        </div>
      </div>

      <div class="row gutter-xs height-box">
        
        <!-- <div class="row">
          <div class="col-lg-2 col-md-2">
            <div class="form-group">
              @php if(!empty($_GET['yrs'])){ $yrs=$_GET['yrs'];}else{ $yrs=date('Y'); } 
                   $yrsCurrent=date('Y')
              @endphp
              <select class="form-control">
                  @for($i = 1990; $i<=$yrsCurrent+1 ; $i++)
                  <option value="{{$i}}" @if($i == $yrs) selected @endif>{{ $i }}</option>
                  @endfor
              </select>
            </div>
          </div>  
        </div> -->

        <div class="full-height col-xs-12 col-sm-3 col-md-3 col-lg-3">
          <div class="card">
            <div class="card-body">
              <div class="media">
                <div class="media-middle media-left">
                  <span class="bg-primary circle sq-48">
                    <span class="icon icon-user"></span>
                  </span>
                </div>
                <div class="media-middle media-body">
                  <h5 class="media-heading">Total no of User Register</h5>
                  <h4 class="media-heading">
                    <span class="fw-l">{{$Totaluser}}</span>
                  </h4>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="full-height col-xs-12 col-sm-3 col-md-3 col-lg-3">
          <div class="card">
            <div class="card-body">
              <div class="media">
                <div class="media-middle media-left">
                  <span class="bg-info circle sq-48">
                    <span class="icon icon-user-plus"></span>
                  </span>
                </div>
                <div class="media-middle media-body">
                  <h5 class="media-heading">Total number of users recently joined</h5>
                  <h4 class="media-heading">
                    <span class="fw-l">{{$Totaluserrecenty}}</span>
                  </h4>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="full-height col-xs-12 col-sm-3 col-md-3 col-lg-3">
          <div class="card">
            <div class="card-body">
              <div class="media">
                <div class="media-middle media-left">
                  <span class="bg-warning circle sq-48">
                    <span class="icon icon-user"></span>
                  </span>
                </div>
                <div class="media-middle media-body">
                  <h5 class="media-heading">Total number of Service Providers</h5>
                  <h4 class="media-heading">
                    <span class="fw-l">{{$TotalServiceProvider}}</span>
                  </h4>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="full-height col-xs-12 col-sm-3 col-md-3 col-lg-3">
          <div class="card">
            <div class="card-body">
              <div class="media">
                <div class="media-middle media-left">
                  <span class="bg-primary circle sq-48">
                    <span class="icon icon-user-plus"></span>
                  </span>
                </div>
                <div class="media-middle media-body">
                  <h5 class="media-heading">Total number of Service Providers recently joined</h5>
                  <h4 class="media-heading">
                    <span class="fw-l">{{$TotalServiceProviderrecenty}}</span>
                  </h4>
                </div>
              </div>
            </div>
          </div>
        </div>


        <div class="full-height col-xs-12 col-sm-3 col-md-3 col-lg-3">
          <div class="card">
            <div class="card-body">
              <div class="media">
                <div class="media-middle media-left">
                  <span class="bg-primary circle sq-48">
                    <span class="icon icon-check"></span>
                  </span>
                </div>
                <div class="media-middle media-body">
                  <h5 class="media-heading">Total Successful Gigs</h5>
                  <h4 class="media-heading">
                    <span class="fw-l">{{$TotalSuccessGig}}</span>
                  </h4>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="full-height col-xs-12 col-sm-3 col-md-3 col-lg-3">
          <div class="card">
            <div class="card-body">
              <div class="media">
                <div class="media-middle media-left">
                  <span class="bg-warning circle sq-48">
                    <span class="icon icon-truck"></span>
                  </span>
                </div>
                <div class="media-middle media-body">
                  <h5 class="media-heading">Total Ongoing Gigs</h5>
                  <h4 class="media-heading">
                    <span class="fw-l">{{$TotalOngoingGig}}</span>
                  </h4>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="full-height col-xs-12 col-sm-3 col-md-3 col-lg-3">
          <div class="card">
            <div class="card-body">
              <div class="media">
                <div class="media-middle media-left">
                  <span class="bg-info circle sq-48">
                    <span class="icon icon-times"></span>
                  </span>
                </div>
                <div class="media-middle media-body">
                  <h5 class="media-heading">Total Cancelled Gigs</h5>
                  <h4 class="media-heading">
                    <span class="fw-l">{{$TotalCancelGig}}</span>
                  </h4>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="full-height col-xs-12 col-sm-3 col-md-3 col-lg-3">
          <div class="card">
            <div class="card-body">
              <div class="media">
                <div class="media-middle media-left">
                  <span class="bg-success circle sq-48">
                    <span class="icon icon-money"></span>
                  </span>
                </div>
                <div class="media-middle media-body">
                  <h5 class="media-heading">Total Revenue</h5>
                  <h4 class="media-heading">
                    <span class="fw-l">${{$TotalrevenueGig}}</span>
                  </h4>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="full-height col-xs-12 col-sm-3 col-md-2 col-lg-3">
          <div class="card">
            <div class="card-body">
              <div class="media">
                <div class="media-middle media-left">
                  <span class="bg-success circle sq-48">
                    <span class="icon icon-star"></span>
                  </span>
                </div>
                <div class="media-middle media-body">
                  <h5 class="media-heading">Total Review & Ratings</h5>
                  <h4 class="media-heading">
                    <span class="fw-l">{{$totalReview}}</span>
                  </h4>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="full-height col-xs-12 col-sm-3 col-md-2 col-lg-3">
          <div class="card">
            <div class="card-body">
              <div class="media">
                <div class="media-middle media-left">
                  <span class="bg-info circle sq-48">
                    <span class="icon icon-film"></span>
                  </span>
                </div>
                <div class="media-middle media-body">
                  <h5 class="media-heading">Most requested services</h5>
                  <h4 class="media-heading">
                    <span class="fw-l">Moving</span>
                  </h4>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="full-height col-xs-12 col-sm-3 col-md-2 col-lg-3">
          <div class="card">
            <div class="card-body">
              <div class="media">
                <div class="media-middle media-left">
                  <span class="bg-danger circle sq-48">
                    <span class="icon icon-film"></span>
                  </span>
                </div>
                <div class="media-middle media-body">
                  <h5 class="media-heading">Least requested services</h5>
                  <h4 class="media-heading">
                    <span class="fw-l">Junk</span>
                  </h4>
                </div>
              </div>
            </div>
          </div>
        </div>
        
        <!-- <div class="full-height col-xs-12 col-sm-3 col-md-2 col-lg-3">
          <div class="card">
            <div class="card-body">
              <div class="media">
                <div class="media-middle media-left">
                  <span class="bg-danger circle sq-48">
                    <span class="icon icon-film"></span>
                  </span>
                </div>
                <div class="media-middle media-body">
                  <h5 class="media-heading">Requested services</h5>
                  <h4 class="media-heading">
                    <span class="fw-l">Others</span>
                  </h4>
                </div>
              </div>
            </div>
          </div>
        </div> -->

      </div>
      <div class="row gutter-xs height-box">
        <div class="full-height col-xs-12 col-sm-12 col-md-12">
          <div class="card">
            <div class="card-body">
              <h5>Growth</h5>
              <div class="card-chart">
                <canvas id="demo-visitors" data-chart="bar" data-animation="false" data-labels='["Aug 1", "Aug 2", "Aug 3", "Aug 4", "Aug 5", "Aug 6", "Aug 7", "Aug 8", "Aug 9", "Aug 10", "Aug 11", "Aug 12", "Aug 13", "Aug 14", "Aug 15", "Aug 16", "Aug 17", "Aug 18", "Aug 19", "Aug 20", "Aug 21", "Aug 22", "Aug 23", "Aug 24", "Aug 25", "Aug 26", "Aug 27", "Aug 28", "Aug 29", "Aug 30", "Aug 31"]' data-values='[{"label": "Visitors", "backgroundColor": "#6B52AB", "borderColor": "#6B52AB",  "data": [29432, 20314, 17665, 22162, 31194, 35053, 29298, 36682, 45325, 39140, 22190, 28014, 24121, 39355, 36064, 45033, 42995, 30519, 20246, 42399, 37536, 34607, 33807, 30988, 24562, 49143, 44579, 43600, 18064, 36068, 41605]}]' data-hide='["legend", "scalesX"]' height="150"></canvas>
              </div>
              <!-- <div class="row gutter-xs mt-2">
                <span class="col-xs-6 col-sm-6 col-md-6">Overall : 70.78%</span>
                <span class="col-xs-6 col-sm-6 col-md-6">Monthly : 24.66%</span>
              </div> -->
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>   
  @endsection   
  @section('footerscript')

    <script type="text/javascript">
       $( "select" ).change(function () {    
          window.location = "{{url('admin/dashboard')}}"+"?day="+this.value;  
       }); 
    </script>

  @endsection 