@extends('admin.header')
  @section('title', 'Banner Management')
  @section('content')           
    @include('admin/navbar')
    <div id="bannerPage"></div>
    <div class="pcoded-content">
      <div class="pcoded-inner-content">
        <div class="main-body">
          <div class="page-wrapper">
            <div class="page-header card">
              <div class="row align-items-end">
                <div class="col-sm-6 col-lg-6">
                  <div class="page-header-title d-flex">
                    <i class="icofont icofont-pay bg-c-lite-green"></i>
                    <div class="d-inline">
                      <h4>Banner Management</h4>
                      <!-- <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span> -->
                    </div>
                  </div>
                </div>
                <div class="col-sm-6 col-lg-6">
                  <div class="page-header-breadcrumb">
                    <div class="page-header-breadcrumb">
                      <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                          <a href="home.php">
                          <i class="icofont icofont-home"></i>
                          </a>
                        </li>
                        <li class="breadcrumb-item"><a>Banner Management</a></li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
           
            <div class="page-body">
              <div class="card">
                <div class="card-block">
                  <div class="col-sm-12 col-md-12 mb-3 p-0">
                    <div class="row">                      
                      <div class="offset-md-6 col-sm-12 col-md-12 col-lg-6 text-right">
                        <label class="text-nowrap">                          
                            <button class="btn btn-md btn-danger" data-toggle="modal" data-target="#add-banner">Add Banner ({{$activeBanner}})</button>                      
                        </label>
                      </div>
                    </div>
                  </div>
                  <div class="table-responsive" id="ajaxBannerList">
                    <table class="table table-striped table-bordered complete-dataTable">                    
                      <thead>
                          <tr>
                            <th class="no-sort wpx-50">Sr. No</th>
                            <th class="no-sort wpx-200">Banner</th>
                            <th>Associated Seller</th>
                            <th>Upper Content</th>
                            <th>Lower Content</th>
                            <th class="no-sort wpx-200 text-center">Actions</th>
                          </tr>
                        </thead>
                      <tbody>
                        @foreach($bnrList as $key=>$rowList)
                        <tr>
                          <td>{{$key+1}}</td>  
                          <td><img src="{{url($rowList->Offer_image)}}" class="img-responsive" alt="User-Profile-Image" width="130px;" height="180px;"></td>
                          <td>@if(!empty($rowList->StoreName->fullName))
                            {{$rowList->StoreName->fullName}}
                            @else
                            N/A
                            @endif  
                          </td>  
                          <td>
                            @if(!empty($rowList->upper_content))
                            {{$rowList->upper_content}}
                            @else
                            N/A
                            @endif
                          </td>
                          <td>
                            @if(!empty($rowList->lower_content))
                            {{$rowList->lower_content}}
                            @else
                            N/A
                            @endif
                          </td>
                          <td class="text-center">
                            <button class="btn btn-sm btn-success edit" id="{{$rowList->id}}"><i class="ti-pencil fs-13"></i></button>
                            <button id="{{$rowList->id}}" class="btn btn-sm btn-danger delete"><i class="ti-trash fs-13"></i></button>
                            <label class="switch">
                              @if($rowList->status == 0 && $rowList->seller_status == 0)
                                <span class="slider round" onclick="seller_block()" style="top: 18px; bottom: -17px;"></span>
                              @elseif($rowList->status == 0)
                                @if($activeBanner >= 10)
                                  <span class="slider round" onclick="limitOver()" style="top: 18px; bottom: -17px;"></span>
                                @else
                                  <input type="checkbox" id="unblockUnblock" onclick="BannerUnBlockUnblock({{$rowList->id}}, {{$activeBanner}})">
                                  <span class="slider round"></span>
                                @endif
                              @elseif($rowList->status == 1)
                                <input type="checkbox" id="blockUnblock" onclick="BannerBlockUnblock({{$rowList->id}})" checked>
                                <span class="slider round"></span>
                              @endif                              
                            </label>

                          </td>               
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- delete block -->
    <div id="delete-modal" class="modal fade">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
          <div class="modal-body text-center">
            <i class="text-danger fs-34 d-inline-block ti-trash mb-4"></i>
            <h6>Are you sure want to delete this Banner ?</h6>
            <hr />
            <button type="button" class="btn btn-md btn-success mr-1" id="ok_button" data-dismiss="modal">Delete</button>
            <button type="button" class="btn btn-md btn-danger ml-1" data-dismiss="modal">Cancel</button>
          </div>
        </div>
      </div>
    </div>
    <!-- success modal -->
    
    <div id="limitOver-modal" class="modal fade">
      <div class="modal-dialog modal-sm" data-dismiss="modal">
        <div class="modal-body text-center">
          <i class="text-danger fs-34 d-inline-block icofont icofont-check-circled mb-4"></i>
          <h6>Your add banner limit is full so please block atleast one banner first then you can try</h6>
        </div>
      </div>
    </div>

    <div id="seller-block-modal" class="modal fade">
      <div class="modal-dialog modal-sm" data-dismiss="modal">
        <div class="modal-body text-center">
          <i class="text-danger fs-34 d-inline-block icofont icofont-check-circled mb-4"></i>
          <h6>Seller already blocked by admin please contact admin</h6>
        </div>
      </div>
    </div>    

    <div id="delete-sccess-modal" class="modal fade">
      <div class="modal-dialog modal-sm" data-dismiss="modal">
        <div class="modal-body text-center">
          <i class="text-danger fs-34 d-inline-block icofont icofont-check-circled mb-4"></i>
          <h6>Delete Successfully</h6>
        </div>
      </div>
    </div>
    
    <div id="block-sccess-modal" class="modal fade">
      <div class="modal-dialog modal-sm" data-dismiss="modal">
        <div class="modal-body text-center">
          <i class="text-danger fs-34 d-inline-block icofont icofont-check-circled mb-4"></i>
          <h6>Unblock Successfully</h6>
        </div>
      </div>
    </div>
    <div id="unblock-sccess-modal" class="modal fade">
      <div class="modal-dialog modal-sm" data-dismiss="modal">
        <div class="modal-body text-center">
          <i class="text-danger fs-34 d-inline-block icofont icofont-check-circled mb-4"></i>
          <h6>Block Successfully</h6>
        </div>
      </div>
    </div>

    <div id="add-success-banner" class="modal fade">
      <div class="modal-dialog modal-sm" data-dismiss="modal">
        <div class="modal-body text-center">
          <i class="text-danger fs-34 d-inline-block icofont icofont-check-circled mb-4"></i>
          <h6>Banner added successfully</h6>
        </div>
      </div>
    </div>

    <div id="edit-success-banner" class="modal fade">
      <div class="modal-dialog modal-sm" data-dismiss="modal">
        <div class="modal-body text-center">
          <i class="text-danger fs-34 d-inline-block icofont icofont-check-circled mb-4"></i>
          <h6>Banner updated successfully</h6>
        </div>
      </div>
    </div>    

    <div id="add-banner" class="modal fade" >
      <div class="modal-dialog modal-md">
        <div class="modal-content">
          <div class="modal-header bg-primary">
            <h6 class="modal-title">Add Banner</h6>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-12">
                <form action="#" id="upload_image_form" enctype="multipart/form-data" method="post" >
                  <div class="form-group">
                    <label>Associated Seller</label>
                    <select class="form-control" name="seller_Id" id="seller_Id">
                      <option value="">Select Store</option>
                      @foreach($VerifiedSeller as $rowseller)  
                        <option value="{{$rowseller->id}}">{{$rowseller->fullName}}</option>
                      @endforeach                     
                    </select>                    
                  </div>

                  <div class="form-group">
                    <label>Image Upper Content</label>
                    <input type="text" name="upper_content" id="upper_content" class="form-control">                      
                  </div>

                  <div class="form-group">
                    <label>Image Lower Content</label>
                    <input type="text" class="form-control" name="lower_content" id="lower_content">
                  </div>

                  <div class="form-group">
                    <label>Upload Image(Size will be 300*300) <span class="text-danger">* </span></label>
                     <div class="single-img">
                      <div class="img-box">
                      <img id="image_preview_container" src="{{url('public/admin/images/avatar-3.jpg')}}" alt="your image" class="img" />
                      <label class="file1"> Upload Image 
                      <input type='file' id="image" name="image" onchange="readURL(this);" /> 

                    </label>
                    <span class="text-danger" id="imageerror" style="display: none;">Please attach Image </span>
                    <!-- <div class="img-box">
                      <div class="img">
                      <img class="imagefile" src="assets/images/avatar-3.jpg"/>
                      </div>
                      <label class="fileInput">
                      Upload File
                      <input type="file" (change)="singleFiles($event)"/>
                      </label>
                    </div> -->
                  </div>
                </div>
                  </div>
                  <div class="form-group text-center">
                    <button type="submit" id="savebttn" class="btn btn-md btn-success">Save</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div id="edit-banner" class="modal fade" >
      <div class="modal-dialog modal-md">
        <div class="modal-content">
          <div class="modal-header bg-primary">
            <h6 class="modal-title">Edit Banner</h6>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-12">
                <form action="#" id="update_upload_image_form" enctype="multipart/form-data" method="post" >
                  <input type="hidden" id="EditId" name="EditId">
                  <div class="form-group">
                    <label>Associated Seller</label>
                     <select class="form-control" id="Editseller_Id" name="seller_Id">
                      <option value="">Select Store</option>
                      @foreach($VerifiedSeller as $rowseller)  
                        <option value="{{$rowseller->id}}">{{$rowseller->fullName}}</option>
                      @endforeach                     
                    </select>   
                  </div>
                  <div class="form-group">
                    <label>Image Upper Content</label>
                    <input type="text" name="upper_content" id="Editupper_content" class="form-control">                      
                  </div>
                  <div class="form-group">
                    <label>Image Lower Content</label>
                    <input type="text" class="form-control" name="lower_content" id="Editlower_content">
                  </div>

                  <div class="form-group">
                    <label>Upload Image(Size will be 300*300) <span class="text-danger">* </span></label>
                     <div class="single-img">
                      <div class="img-box">
                      <img id="EditOffer_image" src="{{url('public/admin/images/avatar-3.jpg')}}" alt="your image" class="img" />
                      <label class="file1"> Upload Image 
                      <input type='file' id="Editimage" name="image" onchange="readURL(this);" />                      
                       <input type="hidden" id="Editimage" name="image">
                    </label>
                    <span class="text-danger" id="Editimageerror" style="display: none;">Please attach Image </span>
                    <!-- <div class="img-box">
                      <div class="img">
                      <img class="imagefile" src="assets/images/avatar-3.jpg"/>
                      </div>
                      <label class="fileInput">
                      Upload File
                      <input type="file" (change)="singleFiles($event)"/>
                      </label>
                    </div> -->
                  </div>
                </div>
                  </div>
                  <div class="form-group text-center">
                    <button type="submit" id="updatebtn" class="btn btn-md btn-success">Save</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    @endsection

    @section('footerscript')
    <script type="text/javascript">   
        function BannerBlockUnblock($id){
             $.ajax({
             url:"offer-Banner-block/"+$id,
             success:function(response)
             {
               $('#unblock-sccess-modal').modal('show');
               $('#ajaxBannerList').html(response);
               // $('#blockUnblock')[0].reset();  
               location.reload(); 
             }
            })
        }

        function BannerUnBlockUnblock($id, $BannerCount){
          if($BannerCount >= '10'){
            $('#limitOver-modal').modal('show');
          }else{
               $.ajax({
               url:"offer-Banner-unblock/"+$id,
               success:function(response)
               {
                 $('#block-sccess-modal').modal('show');
                 $('#ajaxBannerList').html(response);
                 // $('#unblockUnblock')[0].reset();  
                 location.reload();           
               }              
              })
          }         
        }


        function limitOver(){
          $('#limitOver-modal').modal('show');
        }

        function seller_block(){
          $('#seller-block-modal').modal('show');
        }

        $(document).ready(function (e) {        
        var image = $('#image');
        image ='';
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        }); 
        $('#image').change(function(){          
            let reader = new FileReader();
            reader.onload = (e) => { 
              $('#image_preview_container').attr('src', e.target.result); 
            }
            reader.readAsDataURL(this.files[0]);  
        }); 

        //insert
        $('#upload_image_form').submit(function(e) {
             e.preventDefault();             
             // var seller_Id = $('#seller_Id').val();             
             var image = $('#image').val();
             var image_preview_container = $('#image_preview_container').val();
             if(image == ''){
               $('#imageerror').show();   
             }else{
                 var formData = new FormData(this);
                 // console.log(formData);
                 $.ajax({
                    type:'POST',
                    url: "{{ url('admin/add-offer-banner')}}",
                    data: formData,
                    cache:false,
                    contentType: false,
                    processData: false,
                    beforeSend:function(){             
                     $('#savebttn').text('Saving....');
                    },                   
                    success: function (response) {                        
                        $('#ajaxBannerList').html(response);
                        $('#add-banner').modal('hide')
                        $('#add-success-banner').modal('show');
                        location.reload();                
                    }
                });
             }            
        });

        //edit
        $(document).on('click', '.edit', function(){
          var id = $(this).attr('id');         
          var myurl="{{url('admin/edit-offer-Banner/')}}";
          $.ajax({
           url:myurl+'/'+id,
           type:"get",
           success:function(data)
           {
           // console.log(data[0]);
           var img="{{ url('/')}}";
           var default_img="{{ url('public/admin/images/cover-image.png')}}";
           $('#EditId').val(data[0].id);
           $('#Editseller_Id').val(data[0].seller_id);
           $('#Editupper_content').val(data[0].upper_content);
           $('#Editlower_content').val(data[0].lower_content);
           $('#Offer_image').val(data[0].Offer_image);
           if(data[0].Offer_image){
             $('#EditOffer_image').attr('src',img+'/'+data[0].Offer_image);
           } else {
             $('#EditOffer_image').attr('src',default_img);
           }
           }
         }); 
         $('#edit-banner').modal('show');
       });

       //update
       $('#Editimage').change(function(){          
            let reader = new FileReader();
            reader.onload = (e) => { 
              $('#EditOffer_image').attr('src', e.target.result); 
            }
            reader.readAsDataURL(this.files[0]);  
        }); 

       $('#update_upload_image_form').submit(function(e) {
           e.preventDefault();             
          // alert("dsfds");
           // var Editseller_Id = $('#Editseller_Id').val();             
           var EditOffer_image = $('#EditOffer_image').val();
           // if(Editseller_Id == ''){
           //   $('#EditselleridError').show();
           // }else{
               var editformData = new FormData(this);
               $.ajax({
                  type:'POST',
                  url: "{{ url('admin/update-offer-banner')}}",
                  data: editformData,
                  cache:false,
                  contentType: false,
                  processData: false,
                  beforeSend:function(){             
                     $('#updatebtn').text('Saving....');
                    },
                  success: function (response) {
                      $('#ajaxBannerList').html(response);
                      $('#edit-banner').modal('hide');
                      $('#edit-success-banner').modal('show');
                      location.reload();      
                  }
              });
                      
      });

        //Delete
        $(document).on('click', '.delete', function(){
          user_id = $(this).attr('id');  
          $('#delete-modal').modal('show');
        });

        $('#ok_button').click(function(){
          $.ajax({
           url:"offer-Banner-destroy/"+user_id,
           beforeSend:function(){
            $('#ok_button').text('Deleting...');
           },
           success:function(response)
           {
             // console.log(data);
             $('#delete-modal').modal('hide');
             $('#delete-sccess-modal').modal('show');
             $('#ajaxBannerList').html(response);
             location.reload();            
           }
          })
        });

    });     
    </script>
    @endsection