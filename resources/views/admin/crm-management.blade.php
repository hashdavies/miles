@extends('admin.header')
@section('title', 'CRM Management')
@section('content')
@include('admin/navbar')
<div class="responsiveTbPage ExplorerListPage">
  <div class="layout-content">
    <div class="layout-content-body">
      <div class="title-bar">
        <h1 class="title-bar-title">
          <span class="d-ib">CRM Management</span>
        </h1>
      </div>
      <div class="row">
        <div class="col-xs-12">
          <div class="card">
            
            <div class="card-body">
              <div class="table-responsive">
                <table class="table dataTable table-striped table-bordered">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>User Name</th>
                      <th>Email</th>
                      <th>Type</th>                                            
                      <th>City</th>
                      <th>Date & Time</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($contactData as $key=>$rowcontact)
                    <tr>
                      <td>#{{$key+1}}</td>
                      <td>{{$rowcontact->name}}</td>
                      <td>{{$rowcontact->email}}</td>
                      <td>{{$rowcontact->role}}</td>
                      <td>{{$rowcontact->city}}</td>
                      <td>{{$rowcontact->created_at}}</td>                       
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Block Modal -->
<div class="modal fade" id="block-modal">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-body text-center">
        <h2><i class="fa fa-toggle-off text-success"></i></h2>
        <h5>Are you sure you want to block this user?</h5>
        <div class="text-center">
          <button type="button" data-dismiss="modal" class="btn btn-success btn-sm">
            Yes
          </button>
          <button type="button" data-dismiss="modal" class="btn btn-danger btn-sm">
            No
          </button>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Unblock Modal -->
<div class="modal fade" id="unblock-modal">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-body text-center">
        <h2><i class="fa fa-toggle-on text-danger"></i></h2>
        <h5>Are you sure you want to unblock this user?</h5>
        <div class="text-center">
          <button type="button" data-dismiss="modal" class="btn btn-success btn-sm">
            Yes
          </button>
          <button type="button" data-dismiss="modal" class="btn btn-danger btn-sm">
            No
          </button>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="set-taxes">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header bg-primary">
        <h5 class="modal-title">Set Taxes</h5>
        <button type="button" data-dismiss="modal" class="close">&times;</button>
      </div>
      <div class="modal-body">
        <form>
          <div class="row">
              <div class="col-md-12">
                  <div class="form-group">
                       <label>Select Province</label>
                       <select class="form-control">
                          <option>choose</option>
                           <option>choose</option>
                            <option>choose</option>
                       </select>
                   </div> 
              </div>  
              <div class="col-md-12">
                  <div class="form-group">
                       <label>Set Tax % on offer amount</label>
                        <select class="form-control">
                          <option>choose</option>
                           <option>choose</option>
                            <option>choose</option>
                       </select>
                   </div> 
              </div>  
              <div class="col-md-12">
                  <div class="form-group">
                       <label>Set Manually</label>
                        <input type="text" class="form-control" name="">
                   </div> 
              </div>  
              <div class="col-md-12">
                  <div class="form-group">
                     <button type="button" data-dismiss="modal" class="btn btn-success btn-sm">
                      Confirm
                    </button>
                   </div> 
              </div>  
          </div>
        </form> 
      </div>
    </div>
  </div>
</div>

@endsection

@section('footerscript')
  


@endsection