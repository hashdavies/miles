@extends('admin.header')
  @section('title', 'Service Provider Management')
  @section('content')
  @include('admin/navbar')  

  <div class="responsiveTbPage ExplorerListPage">
  <div class="layout-content">
    <div class="layout-content-body">
      <div class="title-bar">
        <h1 class="title-bar-title">
          <span class="d-ib">Service Provider Management</span>
        </h1>
      </div>


      <div class="row">
        <div class="col-xs-12">
          <div class="card">
            <div class="card-header pb-1">
              <div class="row gutter-xs">                  
               <form action="{{url('admin/seller-mgmt-filter')}}" method="get">
                  <!-- <form action="#" method="get"> -->
                  @csrf  
                  <div class="col-xs-6 col-sm-3 col-md-3 mb-2">
                    <label class="wpx-50">From</label>
                    <input type="text" id="start_date" name="start_date" class="form-control" placeholder="mm/dd/yy" onkeypress="myfun()">
                  </div>
                  <div class="col-xs-6 col-sm-3 col-md-3 mb-2">
                    <label class="wpx-50">To</label>
                    <input type="text" placeholder="mm/dd/yy" id="end_date" name="end_date" class="form-control" onkeypress="myfun()">
                  </div>
                  <div class="col-xs-6 col-sm-2 col-md-2 mb-2">
                    <label>City</label>
                    <select class="form-control" name="address" onchange="myfun()">
                      <option value="">Choose City</option>
                      @foreach($City as $rowcity)
                        <option value="{{$rowcity->address}}">{{$rowcity->address}}</option>
                      @endforeach
                    </select>
                  </div>                
                  <div class="col-xs-6 col-sm-2 col-md-2 mb-2">
                    <label>Sort By</label>
                    <select class="form-control" name="status" onchange="myfun()">
                      <option value="">Choose Status</option>
                      <option value="1">Active</option>
                      <option value="2">Inactive</option>
                    </select>
                  </div>
                  <div class="col-xs-6 col-sm-2 col-md-2 mb-2">
                    <br>
                    <div id="showSubmitButton">
                    </div>
                  </div>                 
              </form> 
              
              </div>
            </div>
            <div class="card-body">
              <div class="table-responsive">

                <div class="clearfix"></div>
                  
                

                <div class="clearfix"></div>
                <div class="row">
                   <div class="col-md-offset-8 col-md-4">
                     <div class="text-right">
                           <a href="{{url('admin/export-service-provider-excel')}}" class="btn btn-success margin-right-5 mb-2">Export Excel</a>
                     <button class="btn btn-primary  mb-2" onclick="window.location.href='{{url('admin/add-service-provider')}}'">Add Services Provider</button>
                     </div>
                   </div>
                </div>
                <div class="clearfix"></div>

                <table class="table dataTable table-striped table-bordered">
                  <thead>
                    <tr>
                      <th class="wpx-80">Service Provider ID</th>
                      <th class="wpx-80">Service Provider Name</th>
                      <th>Email ID</th>
                      <th>Mobile No.</th>
                      <th>City</th>                      
                      <th>Service Type</th>
                      <th>Joining Date</th>
                      <th class="wpx-120">Status</th>
                      <th class="wpx-200 no-sort">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($storeData as $key=>$rowData)
                    <tr>
                      <td>#RGGDHRHTR{{$key+1}}</td>
                      <td>{{$rowData->firstName}} {{$rowData->lastName}}</td>
                      <td>{{$rowData->email}}</td>
                      <td>{{$rowData->countryCode}}-{{$rowData->mobileNo}}</td>
                      <td>{{$rowData->address}}</td>                      
                      <td>{{$rowData->subcategoryname}}</td>
                      <td>{{$rowData->registerDate}}</td>
                      <td>
                        <div class="d-flex">
                            @if($rowData->is_approved == 0)
                            <button type="button" onclick="verifyVendor({{$rowData['id']}}, 1)" class="btn btn-xs btn-success verifybtn"> Approve</button>

                            <button type="button" onclick="reject({{$rowData['id']}}, 1)" class="btn btn-xs btn-danger verifybtn"> Reject</button>

                          @elseif($rowData->is_approved == 1)
                            <span style="color: green">Active</span>
                          @elseif($rowData->is_approved == 2)
                            <span style="color: red">Deactivate</span>
                          @elseif($rowData->is_approved == 3)
                            <span style="color: red">Rejected</span>
                          @endif
                        </div> 
                      </td>     

                      <td>
                         <div class="d-flex">
                            <a href="{{url('admin/edit-service-provider/'.$rowData->id)}}" class="btn btn-sm margin-right-5 btn-success"><i class="fa fa-pencil"></i></a>

                            <button data-toggle="modal" class="btn margin-right-5 btn-sm btn-info" data-target="#view" title="view" onclick="ViewStoreDetails({{$rowData->id}})"><i class="fa fa-eye"></i></button>
                               
                            @if($rowData->is_approved == 1)
                               <button data-toggle="modal" data-target="#block-modal" class="btn btn-sm margin-right-5 btn-success" onclick="blockUnblock({{$rowData['id']}},2)"  title="Active"><i class="fa fa-toggle-off"></i></button>
                            @elseif($rowData->is_approved == 2)
                              <button data-toggle="modal" data-target="#block-modal" class="btn btn-sm btn-danger margin-right-5" onclick="blockUnblock({{$rowData['id']}},1)"  title="Inactive"><i class="fa fa-toggle-on"></i></button>
                            @endif                          

                            <button data-toggle="modal" data-target="#delete" class="btn btn-sm btn-danger margin-right-5" onclick="deleteSeller({{$rowData->id}})"><i class="fa fa-trash"></i></button>
                          </div>

                    </tr>
                    @endforeach()

                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Block Modal -->


<div class="modal fade" id="reject-modal">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-body">
        <h5>Rejected Reason</h5>
         <form id="rejectform" action="{{ url('admin/rejectedReason') }}" method="post">
            @csrf  
            <input type="hidden" id="seller_id" name="seller_id">
              <div class="col-md-12">
                  <div class="form-group" align="left">
                      <input type="checkbox" name="reject_reason[]" id="reason" value="Reason 1"> Reason 1
                      <br>
                      <input type="checkbox" name="reject_reason[]" id="reason1" value="Reason 2"> Reason 2
                      <br>
                      <input type="checkbox" name="reject_reason[]" id="reason2" value="Reason 3"> Reason 3
                      <br>
                      <input type="checkbox" name="reject_reason[]" id="reason3" value="Reason 4"> Reason 4
                      <span style="color: red; display: none;" id="reasonError">Choose atleast one reason</span>
                  </div>  
              </div> 
              <div class="col-md-12">
                  <div class="form-group" align="left">
                      <input type="submit" class="btn btn-sm btn-success" value="Save">
                  </div>  
              </div>           
           <br><br><br><br><br><br><br><br>
        </form>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="verify-modal">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-body text-center">
        <h2><i class="fa fa-toggle-off text-success"></i></h2>
        <h5 id="tilteblock1"></h5>
        <div class="text-center">
           <a class="btn btn-sm btn-success mr-1 countinueverify">yes</a>
          <button type="button" data-dismiss="modal" class="btn btn-danger btn-sm">
            No
          </button>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="rejected-reasons-modal">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-body text-center">
        <h2><i class="fa fa-toggle-off text-success"></i></h2>
        <h5>Are you sure you want to Reject this Service Provider?</h5>
        <div class="text-center">
           <button type="button" class="btn btn-sm btn-success mr-1" id="ok_button" data-dismiss="modal">Yes</button>

           <button type="button" data-dismiss="modal" class="btn btn-danger btn-sm">
            No
          </button>
        </div>
      </div>
    </div>
  </div>
</div>


  <!-- view modal -->
  <div class="modal fade" id="viewImg-modal">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-body text-center">
          <button type="button" data-dismiss="modal" class="close">&times;</button>

          <!-- <h2><i class="fa fa-toggle-off text-success"></i></h2> -->
          <!-- <h5>Are you sure you want to Reject this Service Provider?</h5> -->
          <img src="" class="w-100" id="storeLogo1" alt="profile" width="250px" height="250px">

          <!-- <div class="text-center">
             <button type="button" class="btn btn-sm btn-success mr-1" id="ok_button" data-dismiss="modal">Yes</button>

             <button type="button" data-dismiss="modal" class="btn btn-danger btn-sm">
              No
            </button>
          </div> -->
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="viewidproofImg-modal">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-body text-center">
          <button type="button" data-dismiss="modal" class="close">&times;</button>

          <!-- <h2><i class="fa fa-toggle-off text-success"></i></h2> -->
          <!-- <h5>Are you sure you want to Reject this Service Provider?</h5> -->
          <img src="" class="w-100" id="Idproof1" alt="profile" width="250px" height="250px">

          <!-- <div class="text-center">
             <button type="button" class="btn btn-sm btn-success mr-1" id="ok_button" data-dismiss="modal">Yes</button>

             <button type="button" data-dismiss="modal" class="btn btn-danger btn-sm">
              No
            </button>
          </div> -->
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="viewProfieImg-modal">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-body text-center">
          <button type="button" data-dismiss="modal" class="close">&times;</button>
          <!-- <h2><i class="fa fa-toggle-off text-success"></i></h2> -->
          <!-- <h5>Are you sure you want to Reject this Service Provider?</h5> -->
          <img src="" class="w-100" id="profileimage1" alt="profile" width="250px" height="250px">
                    
          <!-- <div class="text-center">
             <button type="button" class="btn btn-sm btn-success mr-1" id="ok_button" data-dismiss="modal">Yes</button>

             <button type="button" data-dismiss="modal" class="btn btn-danger btn-sm">
              No
            </button>
          </div> -->
        </div>
      </div>
    </div>
  </div>
  <!-- view modal -->

<div class="modal fade" id="block-modal">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-body text-center">
        <h2><i class="fa fa-toggle-off text-success"></i></h2>
        <h5 id="tilteblock"></h5>
        <div class="text-center">
           <a class="btn btn-sm btn-success mr-1 countinueblock">yes</a>
          <button type="button" data-dismiss="modal" class="btn btn-danger btn-sm">
            No
          </button>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Unblock Modal -->
<div class="modal fade" id="unblock-modal">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-body text-center">
        <h2><i class="fa fa-toggle-on text-danger"></i></h2>
        <h5>Are you sure you want to unblock this user?</h5>
        <div class="text-center">
          <button type="button" data-dismiss="modal" class="btn btn-success btn-sm">
            Yes
          </button>
          <button type="button" data-dismiss="modal" class="btn btn-danger btn-sm">
            No
          </button>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Delete Modal -->
<div class="modal fade" id="delete">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-body text-center">
        <h2><i class="fa fa-trash-o text-danger"></i></h2>
        <h5>Are you sure you want to delete this user?</h5>
        <div class="text-center">
          <a class="btn btn-sm btn-success continuedelete">Yes</a>                 

          <button type="button" data-dismiss="modal" class="btn btn-danger btn-sm">
            No
          </button>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="block-sccess-modal">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-body text-center">
        <div class="row">
        <div class="col-md-12">
          <i class="fa fa-trash margin-top-15 margin-bottom-15 text-danger fs-30"></i>
          <p>{{ session()->get('success_message')}}</p>
        </div>
      </div>
      </div>
    </div>
  </div>
</div>


<!-- View Modal -->
<div class="modal fade" id="view">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header bg-primary">
        <h5 class="modal-title">Basic Information</h5>
        <button type="button" data-dismiss="modal" class="close">&times;</button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center mb-3">
            <a href="" title="View Image"><img class="imagefile" src="" id="profileimage" width="90px" height="90px" data-target="#viewProfieImg-modal" data-toggle="modal" data-dismiss="modal"></a>
            <h5 id="fullName"></h5>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <table class="table">
              <tr>
                <th>Service Provider Id</th>
                <td id="editid"></td>
              </tr>
              <tr>                
                <th>Business Name</th>
                <td id="businessName"></td>
              </tr>

              <tr>                
                <th>Business Logo</th>
                <td>
                    <a href="" title="View Image"><img class="imagefile" src="" id="storeLogo" width="40px" height="40px" data-target="#viewImg-modal" data-toggle="modal" data-dismiss="modal"></a>
                </td>
              </tr>
             
              <tr>              
              <th>Email</th>
                 <td id="emailAddress"></td>
              </tr>              
              <tr>
                <th>Mobile Number</th>
                 <td id="mobileNo">data</td>
              </tr>
              <tr>
                <th>Joining Date</th>
                 <td id="registerDate">data</td>
              </tr>
              <tr>
                <th>City</th>
                 <td id="address"></td>
              </tr>
               <tr>
                <th>Service Type</th>
                 <td id="serviceType"></td>
              </tr>
               <tr>
                <th>Driving Licence</th>
                 <td id="drivingLicance"></td>
              </tr>

              <tr>
              <th>ID Proof</th>
                 <td>
                   <a href="" title="View Image"><img class="imagefile" src="" id="Idproof" width="40px" height="40px" data-target="#viewidproofImg-modal" data-toggle="modal" data-dismiss="modal"></a>                   
                 </td>
              </tr>

              <tr>
                <th>Service Provider Type</th>
                 <td id="serviceProvider"></td>
              </tr>
              <tr>
                <th>No. of Services Till Date</th>
                 <td id="totalservice"></td>
              </tr>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('footerscript')

   <script type="text/javascript">
     $('#subrej').on('click',function(){ 
           // e.preventDefault();   
    
           // $('#reasonError').hide();
           // var reason = $('#reason').val();
           // if($('#reason').prop("checked") == false){
           //     $('#reasonError').show(); 
           //     e.preventDefault();              
           //  }else{
              this.form.submit();
               // var formData = new FormData(this);
               //   // console.log(formData);
               //   $.ajaxSetup({
               //       headers: {
               //        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
               //      }
               //   });

               //   // var formData = new FormData(this);
               //   $.ajax({
               //      type:'POST',
               //      url: "{{ url('admin/rejectedReason') }}",
               //      // alert("dasdsa");
               //      data: formData,
               //      cache:false,
               //      contentType: false,
               //      processData: false,
               //      success: function (response) {                        
                        
               //          $('#add-banner').modal('hide')
               //          $('#add-success-banner').modal('show');
               //          location.reload();                
               //      }
               //  });
            // }       
     });

   </script>
   <script>
    $(document).ready(function(){
       $("input").click(function(){
        $("#showSubmitButton").html(`<a href="{{url('admin/services-provider-management')}}" class="btn btn-sm btn-primary pull-right" style="">Reset</a>
                <button type="submit" class="btn btn-sm btn-primary pull-right " style="margin-right: 12px;">Apply</button>`);

      });
    });
  </script>

  <script type="text/javascript">
      function myfun(){  
      $("#showSubmitButton").html(`<a href="{{url('admin/services-provider-management')}}" class="btn btn-sm btn-primary pull-right" style="">Reset</a>
                <button type="submit" class="btn btn-sm btn-primary pull-right " style="margin-right: 12px;">Apply</button>`);
      
    }   
  </script>

  <script type="text/javascript">
      function reject(id){ 
         $('#rejected-reasons-modal').modal('show');
         $('#ok_button').click(function(){
            $('#seller_id').val(id);

            $('#rejectform')[0].reset();
        
            $('#reject-modal').modal('show');
         });
      }

      function ViewStoreDetails(id){
        if(id>0){
            var globalarray = <?php echo json_encode($storeData); ?>;
            var theIndex = -1;
            for (var i = 0; i < globalarray.length; i++) {
              if (globalarray[i].id == id) {
                theIndex = i;
                break;
              }
            }
            $("#editid").html(`<td>#RGGDHRHTR`+globalarray[theIndex].id+`</td>`);

            $("#fullName").html(`<h5>`+globalarray[theIndex].firstName+` `+globalarray[theIndex].lastName+`</h5>`);
            $("#emailAddress").text(globalarray[theIndex].email);
            $("#mobileNo").html(`<td>`+globalarray[theIndex].countryCode+` `+globalarray[theIndex].mobileNo+`</td>`);
            $("#businessName").text(globalarray[theIndex].businessName);
            $("#address").text(globalarray[theIndex].address);
            
            // console.log(globalarray[theIndex].allservicecount);

            $("#serviceType").text(globalarray[theIndex].subcategoryname);
            $("#registerDate").text(globalarray[theIndex].registerDate);  

            $("#totalservice").text(globalarray[theIndex].allservicecount);  
                         
            if(globalarray[theIndex].dLNumber){
                $("#drivingLicance").text(globalarray[theIndex].dLNumber);
            } else {
               $("#drivingLicance").html(`<p>NA</p>`);
            }
            $("#serviceProvider").text(globalarray[theIndex].serviceProviderType);

            var img="{{ url('https://mylesimages.s3.ap-south-1.amazonaws.com/Serviceprovider')}}";
            var default_img="{{ url('public/admin/images/Group-user.jpg')}}";
            if(globalarray[theIndex].businesslogo){
               $('#storeLogo').attr('src',img+'/'+globalarray[theIndex].businesslogo);
               $('#storeLogo1').attr('src',img+'/'+globalarray[theIndex].businesslogo);
            } else {
               $('#storeLogo').attr('src',default_img);
               $('#storeLogo1').attr('src',default_img);
            }           
            var default_img="{{ url('public/admin/images/Group-user.jpg')}}";
            if(globalarray[theIndex].IDProof){
               $('#Idproof').attr('src',img+'/'+globalarray[theIndex].IDProof);
               $('#Idproof1').attr('src',img+'/'+globalarray[theIndex].IDProof);
            } else {
               $('#Idproof').attr('src',default_img);
               $('#Idproof1').attr('src',default_img);
            }  
            if(globalarray[theIndex].profileImage){
               $('#profileimage').attr('src',img+'/'+globalarray[theIndex].profileImage);
               $('#profileimage1').attr('src',img+'/'+globalarray[theIndex].profileImage);
            } else {
               $('#profileimage').attr('src',default_img);
               $('#profileimage1').attr('src',default_img);
            }              
            // console.log(globalarray[theIndex]);
      }       
    }
  </script>

  <script type="text/javascript">
    function verifyVendor(id,type){
       $('#verify-modal').modal('show');
       $("#tilteblock1").text("Are you sure you want to approve this Service Provider?");
       $(".countinueverify").attr("href","{{url('admin/venderVerify')}}"+"/"+id+"/"+type);
       // window.location = "{{ url('admin/venderVerify')}}"+"/"+id+"/"+type;
    }

    function deleteSeller(id){
      // alert($id);
       $(".continuedelete").attr("href","{{url('admin/delete_store')}}"+"/"+id);
    }

    function blockUnblock(id,type){
        if(type == 2){
          $("#tilteblock").text("Are you sure you want to Deactivate Service Provider?");
        }
        else if(type == 1){
          $("#tilteblock").text("Are you sure you want to Active Service Provider?");
        }
        $(".countinueblock").attr("href","{{url('admin/blockVendor')}}"+"/"+id+"/"+type);
    }
  </script>

  @if ($message = session()->has('success_message'))
      <script type="text/javascript">
         $(function() {
            $('#block-sccess-modal').modal('show');
            setTimeout(function() {$('#block-sccess-modal').modal('hide');}, 2000);
          });
      </script>
  @endif

  @if ($message = session()->has('deleteuser'))
      <script type="text/javascript">
         $(function() {
            $('#delete-sccess-modal').modal('show');
            setTimeout(function() {$('#delete-sccess-modal').modal('hide');}, 2000);
          });
      </script>
  @endif   
@endsection