  @extends('admin.header')
  @section('title', 'CMS Management')
  @section('content')
    @include('admin/navbar') 
      

    <div class="responsiveTbPage ExplorerListPage">
       <div class="layout-content">
          <div class="layout-content-body">
             <div class="title-bar">
                <h1 class="title-bar-title">
                   <span class="d-ib text-danger">CMS Management</span>
                   <button class="btn btn-success pull-right" data-target='#page-modal' data-toggle='modal'>Add More</button>
                </h1>
             
             </div>

             <div class="row">
                <div class="col-xs-12">
                   <div class="card">
                      <div class="card-header pb-1">
                         
                        @foreach($cmsPage as $key=>$rowPage)
                         <div class="row gutter-xs pt-5">
                            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                              <h4 class="m-0 pt-3 pull-left">{{$rowPage->title}}</h4>
                              
                              <button class="btn btn-danger pull-right" data-target='#CMS-modal' onclick="editCms({{$rowPage->id}})" data-toggle='modal'>
                                <i class="fa fa-edit" area-hidden="true"></i>
                              </button>

                            </div>
                         </div>
                         <div class="row pt-4">
                           <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                             <div class="card">
                                 <div class="card-body bg-silver border">
                                   <p>{!!$rowPage->content!!}</p>
                                 </div>
                             </div>
                           </div>
                         </div>
                         @endforeach


                         <div class="row gutter-xs pt-5">
                            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                              <h4 class="m-0 pt-3 pull-left">About Us</h4>
                              <button class="btn btn-danger pull-right" data-target='#help-modal' data-toggle='modal'>
                                <i class="fa fa-edit" area-hidden="true"></i>
                              </button>
                            </div>
                         </div>
                         <div class="row pt-4">
                           <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                             <div class="card">
                                 <div class="card-body bg-silver border">
                                   <p>{!!$about_us->help!!}</p>
                                 </div>
                             </div>
                           </div>
                         </div>


                         <div class="clearfix"></div>
                         <div class="row gutter-xs pt-5">
                            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                              <h4 class="m-0 pt-3 pull-left">Our Commitment</h4>
                              <button class="btn btn-danger pull-right" data-target='#terms-modal' data-toggle='modal'>
                                <i class="fa fa-edit" area-hidden="true"></i>
                              </button>
                            </div>
                         </div>
                         <div class="row pt-4">
                           <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                             <div class="card">
                                 <div class="card-body bg-silver border">
                                   <p>{!!$about_us->termscondition!!}</p>
                                 </div>
                             </div>
                           </div>
                         </div>


                         <div class="clearfix"></div>
                           <div class="row gutter-xs pt-5">
                              <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                <h4 class="m-0 pt-3 pull-left">COVID-19 Statement</h4>
                                <button class="btn btn-danger pull-right" data-target='#legal-modal' data-toggle='modal'>
                                  <i class="fa fa-edit" area-hidden="true"></i>
                                </button>
                              </div>
                           </div>
                           <div class="row pt-4">
                             <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                               <div class="card">
                                   <div class="card-body bg-silver border">
                                     <p>{!!$about_us->legal!!} </p>
                                   </div>
                               </div>
                             </div>
                         </div>



                         <div class="clearfix"></div>
                     <div class="row gutter-xs pt-5">
                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                          <h4 class="m-0 pt-3 pull-left">FAQ's</h4>
                          <button class="btn btn-success pull-right" data-target="#addFAQ-modal" data-toggle="modal">
                            <i class="fa fa-plus-circle" area-hidden="true"></i>
                          </button>
                        </div>
                     </div>
                     <div class="row pt-4">
                       <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                         <div class="card">
                             <div class="card-body border">
                               <div class="card border-0">
                                @foreach($faqs as $key=>$rowfaq)
                                 <div class="card-body bg-silver border mb-2">
                                    <p>
                                      <span class="pull-right cursor-pointer" id="removeFaq">
                                        <a href="{{url('admin/delete-faq/'.$rowfaq->id)}}"><i class="fa fa-trash text-danger"></i></a>
                                      </span>
                                      <span class="h4">Q.</span> 
                                      <span class="h5">{{$rowfaq->question}}</span>
                                    </p>
                                    <p class="mb-0"><span class="h4"></span> {!!$rowfaq->answer!!}</p>
                                 </div>
                                @endforeach

                               </div>
                             </div>
                         </div>
                       </div>
                     </div>
                        



                      </div>
                   </div>
                </div>
             </div>
          </div>
       </div>
    </div>

    <!-- Block Modal -->
    <div class="modal fade" id="block-modal">
       <div class="modal-dialog modal-sm">
          <div class="modal-content">
             <div class="modal-body text-center">
                <h2><i class="fa fa-toggle-off text-success"></i></h2>
                <h5>Are you sure you want to block this user?</h5>
                <div class="text-center">
                   <button type="button" data-dismiss="modal" class="btn btn-success btn-sm">
                   Yes
                   </button>
                   <button type="button" data-dismiss="modal" class="btn btn-danger btn-sm">
                   No
                   </button>
                </div>
             </div>
          </div>
       </div>
    </div>
    <!-- Unblock Modal -->
    <div class="modal fade" id="unblock-modal">
       <div class="modal-dialog modal-sm">
          <div class="modal-content">
             <div class="modal-body text-center">
                <h2><i class="fa fa-toggle-on text-danger"></i></h2>
                <h5>Are you sure you want to unblock this user?</h5>
                <div class="text-center">
                   <button type="button" data-dismiss="modal" class="btn btn-success btn-sm">
                   Yes
                   </button>
                   <button type="button" data-dismiss="modal" class="btn btn-danger btn-sm">
                   No
                   </button>
                </div>
             </div>
          </div>
       </div>
    </div>

    <!-- faq Modal -->
    <div class="modal fade" id="addFAQ-modal">
       <div class="modal-dialog modal-md">
          <div class="modal-content">
            <div class="modal-header bg-default">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="mb-0">Add FAQ</h4>
            </div>
             <div class="modal-body">
                <form action="{{url('admin/faq')}}" method="post" id="add_faq">
                @csrf                
                <div class="modal-body">
                    <div class="form-group">
                      <input type="text" class="form-control" id="que" name="question" placeholder="Enter Question">
                      <span class="text-danger" id="offerFeeError" style="display: none; text-align: left">Please enter question</span>
                    </div>
                    <div class="form-group">
                        <textarea rows="5" id="editor1" class="form-control" required="" name="answer" placeholder="Enter Answer"></textarea>

                        <span class="text-danger" id="offerDescriptionError" style="display: none;">Please enter answer</span>

                    </div>
                    <div class="w-100 text-center">
                        <button class="btn btn-success" type="submit">Save</button>
                    </div>
                </div>
              </form>
             </div>
          </div>
       </div>
    </div>

    <div class="modal fade" id="CMS-modal">
       <div class="modal-dialog modal-md">
          <div class="modal-content">
            <div class="modal-header bg-default">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="mb-0" id="editTitle"></h4>
            </div>
             <div class="modal-body">

                <form action="{{url('admin/add-cms-page')}}" method="post" id="addCmsPage">
                @csrf

                <input type="hidden" name="editid" id="editid">
                <div class="modal-body">
                  <div class="form-group">
                    <input type="text" name="title" class="form-control" required="" id="edittitle">
                    <span id="titleError" style="display: none; color: red;">Required</span>
                  </div>
                  <div class="form-group">
                    <div id="editcontent"></div>

                    <!-- <textarea id="editor7" class="form-control editcontent" required="" rows="5" name="description" placeholder="Help..."></textarea> -->

                  </div>
                  <div class="text-center">
                      <button type="submit" class="btn btn-success wpx-150 mt-3">Save</button>
                  </div>
                </div>
              </form>
             </div>
          </div>
       </div>
    </div>

    <!-- Delete Modal -->
    <!-- <div class="modal fade" id="delete-modal">
       <div class="modal-dialog modal-sm">
         <div class="modal-content">
           <div class="modal-body text-center">
             <h2><i class="fa fa-trash-o text-danger"></i></h2>
             <h5>Are you sure you want to delete this user?</h5>
             <div class="text-center">
               <button type="button" data-dismiss="modal" class="btn btn-success btn-sm">
                 Yes
               </button>
               <button type="button" data-dismiss="modal" class="btn btn-danger btn-sm">
                 No
               </button>
             </div>
           </div>
         </div>
       </div>
       </div> -->

       <!-- help Modal -->
       <div class="modal fade" id="help-modal">
         <div class="modal-dialog modal-md">
           <div class="modal-content">
            <div class="modal-header bg-default">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">About Us</h4>
            </div>
            <form action="{{url('admin/add-about-us')}}" method="post">
              @csrf
              <input type="hidden" name="editId" value="{{@$about_us->id}}">
              <div class="modal-body">
                  <div class="modal-body text-center">
                      <textarea id="editor2" class="form-control" required="" rows="5" name="about" placeholder="Help...">{{@$about_us->help}}</textarea>
                      <!-- <textarea rows="5" class="form-control" name="about" placeholder="Enter About Us">{{@$about_us->aboutus}}</textarea> -->
                  </div>
                  <div class="text-center">
                      <button type="submit" class="btn btn-success wpx-150 mt-3">Save</button>
                  </div>

              </div>
            </form>
           </div>
         </div>
       </div> 

       <div class="modal fade" id="page-modal">
         <div class="modal-dialog modal-md">
           <div class="modal-content">
            <div class="modal-header bg-default">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Add CMS</h4>
            </div>
            
            <form action="{{url('admin/add-cms-page')}}" method="post" id="addCmsPage">
              @csrf
              <div class="modal-body">
                <div class="form-group">
                  <input type="text" name="title" class="form-control" required="" id="title">
                  <span id="titleError" style="display: none; color: red;">Required</span>
                </div>
                <div class="form-group">
                  <textarea id="editor6" class="form-control" required="" rows="5" name="description" placeholder="Help..."></textarea>
                </div>
                <div class="text-center">
                    <button type="submit" class="btn btn-success wpx-150 mt-3">Save</button>
                </div>
              </div>
            </form>

           </div>
         </div>
       </div> 

       <!-- TERMS & CONDITIONS Modal -->
       <div class="modal fade" id="terms-modal">
         <div class="modal-dialog modal-md">
           <div class="modal-content">
            <div class="modal-header bg-default">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Our Commitment</h4>
            </div>
            <form action="{{url('admin/termsconditions')}}" method="post">
              @csrf 
              <input type="hidden"  name="edittermid" value="{{@$about_us->id}}">
              <div class="modal-body">
                  <div class="form-group">
                      <textarea rows="5" id="editor3" class="form-control" required="" name="termcondition" placeholder="Enter Terms & Conditions">{{@$about_us->termscondition}}</textarea>
                  </div>
                  <div class="text-center">
                      <button class="btn btn-success wpx-150 mt-3" type="submit">Save</button>
                  </div>
              </div>
            </form>
           </div>
         </div>
       </div>


       <!-- LEGAL Modal -->
       <div class="modal fade" id="legal-modal">
         <div class="modal-dialog modal-md">
           <div class="modal-content">
            <div class="modal-header bg-default">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">COVID-19 Statement</h4>
            </div>
            <form action="{{url('admin/privacypolicy')}}" method="post">
              @csrf
              <input type="hidden" name="editprivacyId" value="{{@$about_us->id}}">
              <div class="modal-body">
                  <div class="form-group">
                      <textarea rows="5" required="" id="editor4" class="form-control" name="privacypolicy" placeholder="Enter Privacy Policy">{{@$about_us->legal}}</textarea>
                  </div>
                  <div class="w-100 text-center">
                      <button type="submit" class="btn btn-success wpx-150 mt-3">Save</button>
                  </div>
              </div>
            </form>
           </div>
         </div>
       </div> 

    <div id="delete-sccess-modal" class="modal fade" role="dialog">
      <div class="modal-dialog modal-sm" data-dismiss="modal">
        <div class="text-center"></div> 
        <div class="modal-body text-center">
          <div class="row">
            <div class="col-md-12">
              <i class="text-success fs-34 d-inline-block icofont icofont-check-circled mb-4"></i>
              <h6>{{ session()->get('success_message') }}</h6>
            </div>
          </div>
        </div>
      </div>
    </div>     

<script>
CKEDITOR.replace( 'editor1' );                                          // (In case of angular):____   declare var CKEDITOR: any;      (If error occurs)
</script>
<script>
CKEDITOR.replace( 'editor2' );                                          // (In case of angular):____   declare var CKEDITOR: any;      (If error occurs)
</script>
<script>
CKEDITOR.replace( 'editor3' );                                          // (In case of angular):____   declare var CKEDITOR: any;      (If error occurs)
</script>
<script>
CKEDITOR.replace( 'editor4' );                                          // (In case of angular):____   declare var CKEDITOR: any;      (If error occurs)
</script>
<script>
CKEDITOR.replace( 'editor5' );                                          // (In case of angular):____   declare var CKEDITOR: any;      (If error occurs)
</script>
<script>
CKEDITOR.replace( 'editor6' );                                          // (In case of angular):____   declare var CKEDITOR: any;      (If error occurs)
</script>


  @endsection

  @section('footerscript')

    <script type="text/javascript">
      
      function editCms(id){
         if(id>0){
            

            var globalarray = <?php echo json_encode($cmsPage); ?>;
            var theIndex = -1;
            for (var i = 0; i < globalarray.length; i++) {
              if (globalarray[i].id == id) {
                theIndex = i;
                break;
              }
            }
            $("#editid").val(globalarray[theIndex].id);    

            $("#editTitle").text(globalarray[theIndex].title);
            $("#edittitle").val(globalarray[theIndex].title);

            $("#editcontent").html('<textarea id="editor12" class="form-control editcontent" required="" rows="5" name="description" placeholder="Help...">'+globalarray[theIndex].content+'</textarea>'); 

             CKEDITOR.replace( 'editor12' );            
         } 
      }

    </script>

      
      <script type="text/javascript">
           $('#addCmsPage').on('submit',function(e) {
           // e.preventDefault();
           alert(sadsa);
           $('#titleError').hide(); 
           $('#tempalte6Error').hide();
           var title = $('#title').val();  
           var tempalte6 = CKEDITOR.instances['editor6'].getData();
           
           if(title == ''){
               $('#titleError').show();   
               e.preventDefault();             
           }
           if(tempalte6 == ''){
               $('#tempalte6Error').show();  
               e.preventDefault();  
           }
        });
      </script>
      
      <script type="text/javascript">
           $('#add_faq').on('submit',function(e) {
           // e.preventDefault();

           $('#offerFeeError').hide(); 
           $('#offerDescriptionError').hide();
           var que = $('#que').val();  
           var tempalte = CKEDITOR.instances['editor1'].getData();
           
           if(que == ''){
               $('#offerFeeError').show();   
               e.preventDefault();             
           }
           if(tempalte == ''){
               $('#offerDescriptionError').show();  
               e.preventDefault();  
           }
        });


      </script>

      <script type="text/javascript">
        $(document).on('click','#removeFaq', function(){
          $(this).parent().parent().remove();
        })
      </script>

    <script>
      function deleteProduct(id){
        // alert(id);
        $(".countinueDelete").attr("href","{{url('admin/delete_faq')}}"+"/"+id);
      }     
    </script>

    <!-- @if ($message = session()->has('success_message'))
     <script type="text/javascript">
       $(function() {
          $('#delete-sccess-modal').modal('show');
          setTimeout(function() {$('#delete-sccess-modal').modal('hide');}, 2000);
        });
     </script>
    @endif  --> 
<script>
                                        // (In case of angular):____   declare var CKEDITOR: any;      (If error occurs)
</script>
  @endsection