@extends('admin.header')
  @section('title', 'Service Category Management')
  @section('content')
  @include('admin/navbar')  

  <div class="responsiveTbPage ExplorerListPage">
  <div class="layout-content">
    <div class="layout-content-body">
      <div class="title-bar">
        <h1 class="title-bar-title">
          <span class="d-ib">Service Category Management</span>
        </h1>
      </div>
      <div class="row">
        <div class="col-xs-12">
          <div class="card">
            <div class="card-header pb-1">
               <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                         <button type="button" onclick="window.location.href='{{url('admin/add-service-category')}}'" class="btn btn-success btn-sm">
                             Add Service
                        </button>
                     </div> 
                   </div>  
              </div>

              <form action="{{url('admin/service-category-filter')}}" method="get" id="categoryFilter">
              <div class="row gutter-xs">
                <div class="col-xs-6 col-sm-3 col-md-3 mb-2">
                  <label class="wpx-50">From</label>
                  <input type="text" id="start_date" placeholder="dd/mm/yy" name="from" onkeypress="myfun()" class="form-control">
                  <span style="color: red; display: none;" id="StartDateError">Please choose atleast one field</span>
                </div>
                
                <div class="col-xs-6 col-sm-3 col-md-3 mb-2">
                  <label class="wpx-50">To</label>
                  <input type="text" id="end_date" placeholder="dd/mm/yy" name="to" class="form-control" onkeypress="myfun()">                  
                  <span style="color: red; display: none;" id="EndDateError">End date is required</span>
                </div>

                <div class="col-xs-6 col-sm-2 col-md-2 mb-2">
                  <label>Sort By</label>
                  <select class="form-control" name="status" onchange="myfun()" id="status">
                    <option value="">A-Z</option>
                    <option value="1">Active</option>
                    <option value="0">Inactive</option>
                  </select>
                </div>
                <div class="col-xs-6 col-sm-2 col-md-2 mb-2">
                    <br>
                  <div id="showSubmitButton">
                  </div>
                </div>                   
              </div>

            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table dataTable table-striped table-bordered">
                  <thead>
                    <tr>
                      <th>ID</th>                      
                      <th>Service Category name</th>
                      <th>Sub category Name</th>
                      <th class="wpx-80 no-sort">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($data as $key=>$catdata)
                    <tr>
                      <td>#RGGDHRHTR{{$key+1}}</td>
                      <td>{{$catdata->category_name}}</td>
                      <td>{{$catdata->subcategoryname}}</td>
                      <td>
                         <div class="d-flex">                           
                           <a href="{{url('admin/edit-service-category/'.$catdata->id)}}" style="margin-right: 5px" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a> 

                           @if($catdata->status == '1')
                            <button type="button" style="margin-right: 5px" class="btn btn-success btn-sm" data-target="#block-modal" data-toggle="modal" title="Active" onclick="blockUnblock({{$catdata['id']}},0)"><i class="fa fa-toggle-off"></i></button>
                           @elseif($catdata->status == '0')
                            <button type="button" style="margin-right: 5px" class="btn btn-danger btn-sm" data-target="#unblock-modal" data-toggle="modal" title="Inactive" onclick="blockUnblock({{$catdata['id']}},1)"><i class="fa fa-toggle-on"></i></button>
                           @endif
                           
                           <button type="button" style="margin-right: 5px" class="btn btn-danger btn-sm" data-target="#delete-modal" onclick="deltcate({{$catdata->id}})" style="margin-right: 5px" data-toggle="modal"><i class="fa fa-trash-o"></i></button>

                         </div>
                      </td>
                    </tr>
                    @endforeach()
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Block Modal -->
<div class="modal fade" id="block-modal">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-body text-center">
        <h2><i class="fa fa-toggle-off text-success"></i></h2>
        <h5 id="tilteunblock"></h5>
        <div class="text-center">
          <a class="btn btn-success btn-sm countinueblock">Yes</a>
          <button type="button" data-dismiss="modal" class="btn btn-danger btn-sm">
            No
          </button>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Unblock Modal -->
<div class="modal fade" id="unblock-modal">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-body text-center">
        <h2><i class="fa fa-toggle-on text-danger"></i></h2>
        <h5 id="tilteblock"></h5>
        <div class="text-center">
          <a class="btn btn-success btn-sm countinueblock">Yes</a>
          <button type="button" data-dismiss="modal" class="btn btn-danger btn-sm">
            No
          </button>
        </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Delete Modal -->
<div class="modal fade" id="delete-modal">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-body text-center">
        <h2><i class="fa fa-trash-o text-danger"></i></h2>
        <h5>Are you sure you want to delete this Category?</h5>
        <div class="text-center">
          <a class="btn btn-success btn-sm continuedelete">Yes</a>
          <button type="button" data-dismiss="modal" class="btn btn-danger btn-sm">
            No
          </button>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="delete-success-modal">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-body text-center">
        <h2><i class="fa fa-trash-o text-danger"></i></h2>
        <h5>Category Deleted Successfully</h5>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="block-success-modal">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-body text-center">
        <h2><i class="fa fa-toggle-off text-success"></i></h2>
        <h5>{{session()->get('success_message')}}</h5>
      </div>
    </div>
  </div>
</div>

@endsection

@section('footerscript')
  <script type="text/javascript">
    
    function deltcate(id){
      $('.continuedelete').attr("href", "{{url('admin/delete-service-category')}}"+"/"+id);
    }

  </script>
    
    
 <!--  @if ($message = session()->has('success_message'))
    <script type="text/javascript">
       $(function() {
          $('#delete-success-modal').modal('show');
          setTimeout(function() {$('#delete-success-modal').modal('hide');}, 2000);
        });
    </script>
  @endif
 -->
   <script>
    $(document).ready(function(){
       $("input").click(function(){
        $("#showSubmitButton").html(`<a href="{{url('admin/service-category-filter')}}" class="btn btn-sm btn-primary pull-right" style="">Reset</a>
                <button type="submit" class="btn btn-sm btn-primary pull-right " style="margin-right: 12px;">Apply</button>`);

      });
    });
  </script>
  <script type="text/javascript">
      function myfun(){  
      $("#showSubmitButton").html(`<a href="{{url('admin/service-category-filter')}}" class="btn btn-sm btn-primary pull-right" style="">Reset</a>
                <button type="submit" class="btn btn-sm btn-primary pull-right " style="margin-right: 12px;">Apply</button>`);
      
    }   
  </script>
  <script type="text/javascript">
    
    $('#categoryFilter').on('submit', function(e){
      $('#StartDateError').hide();
      $('#EndDateError').hide();
      var start_date = $('#start_date').val();
      var end_date = $('#end_date').val();
      var status = $('#status').val();

      if((start_date == '') && (status == '')){
        $('#StartDateError').show();
        e.preventDefault();
      }
      if((start_date) && (end_date == '')){
        $('#EndDateError').show();
        e.preventDefault();
      }


    });

  </script>

   <script type="text/javascript">    
      function blockUnblock(id,type){
          if(type == 0){
            $("#tilteunblock").text("Are you sure you want to Inactive this Category?");
          }else if(type == 1){
            $("#tilteblock").text("Are you sure you want to Active this Category?");
          }
          $(".countinueblock").attr("href","{{url('admin/blockcategory')}}"+"/"+id+"/"+type);
      }      
    </script>
    
    @if ($message = session()->has('success_message'))
      <script type="text/javascript">
         $(function() {
            $('#block-success-modal').modal('show');
            setTimeout(function() {$('#block-success-modal').modal('hide');}, 2000);
          });
      </script>
    @endif

@endsection