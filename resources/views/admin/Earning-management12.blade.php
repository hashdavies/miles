@extends('admin.header')
@section('title', 'Earning Management')
@section('content')
@include('admin/navbar')
<div class="responsiveTbPage ExplorerListPage">
  <div class="layout-content">
    <div class="layout-content-body">
      <div class="title-bar">
        <h1 class="title-bar-title">
          <span class="d-ib">Earning Management</span>
        </h1>
      </div>
      <div class="row">
        <div class="col-xs-12">
          <div class="card">
            <div class="card-header pb-1">
              <div class="row gutter-xs">
                <div class="col-xs-12 col-sm-12 mb-1">
                  <div class="card-actions">
                    <!-- <button type="button" data-target="#set-taxes" data-toggle="modal" class="btn btn-success btn-sm mb-1">Set Taxes</button> -->
                    <a href="{{url('admin/tax-management')}}" class="btn btn-success btn-sm mb-1">Set Taxes</a>
                    <button type="button" onclick="getTax()" class="btn btn-primary btn-sm mb-1">Set Service Charge</button>
                  </div>
                </div>

                <form action="{{url('admin/earning-mgmt-filter')}}" method="get" id="add_filter">
                  <div class="row gutter-xs">
                    <div class="col-xs-6 col-sm-2 col-md-2 mb-2">
                      <label class="wpx-50">From</label>
                      <input type="text" id="start_date" name="from" class="form-control" placeholder="mm/dd/yyyy" onkeypress="myfun()">
                    </div>
                    <div class="col-xs-6 col-sm-2 col-md-2 mb-2">
                      <label class="wpx-50">To</label>
                      <input type="text" id="end_date" name="to" class="form-control" placeholder="mm/dd/yyyy" onkeypress="myfun()">
                    </div>

                    <div class="col-xs-6 col-sm-2 col-md-2 mb-2">
                      <label>Province</label>
                      <select class="form-control" name="province" onchange="myfun()" id="province">
                        <option value="">Choose Province</option>
                        @foreach($province as $rowprovince)
                          <option value="{{$rowprovince->province}}" data-id="{{$rowprovince->province}}">{{$rowprovince->province}}</option>
                        @endforeach
                      </select>
                    </div>
                     <div class="col-xs-6 col-sm-3 col-md-2 mb-2">
                      <label>City</label>
                      <select class="form-control" name="city" id="city" onchange="myfun()">
                        <option value="">Choose City</option>
                      </select>
                    </div> 
                    <div class="col-xs-6 col-sm-2 col-md-2 mb-2">
                      <label>Sort By</label>
                      <select class="form-control" name="status" onchange="myfun()">
                        <option value="">Choose status</option>
                        <option value="10">Active</option>
                        <option value="9">Inactive</option>
                      </select>
                    </div>                    
                    <div class="col-xs-6 col-sm-2 col-md-2 mb-2">
                      <br>
                      <div id="showSubmitButton">
                            
                        </div>
                    </div> 
                  </div>
                </form> 
     
                
                
              </div>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table dataTable table-striped table-bordered">
                  <thead>
                    <tr>
                      <th>Gig ID</th>
                      <th class="wpx-180">Transaction Id</th>
                      <th>User Name</th>
                      <th>Type</th>
                      <th>Date & Time</th>
                      <th>Assigned Service Provider</th>
                      <th>Payment Method</th>
                      <th>Offer Amount</th>
                      <th>Admin Service Charges %</th>
                      <th>Sub Total Amount</th>
                      <th class="wpx-80 no-sort">Status</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($CompleteGig as $key=>$rowcompletegig)
                    <tr>
                      <td>{{$rowcompletegig->gig_id }}</td>
                      <td>123</td>
                      <td>{{$rowcompletegig->UserName->firstName }} {{$rowcompletegig->UserName->lastName }}</td>                      
                      <td>{{@$rowcompletegig->serviceType->CategoryName->category_name }}</td>

                      <td>{{$rowcompletegig->updated_at }}</td>
                      <td>{{@$rowcompletegig->SellerName->firstName }} {{@$rowcompletegig->SellerName->lastName }}</td>
                      <td>Online</td>
                      <td>{{$rowcompletegig->offerAmount }} $</td>
                      <td>{{$rowcompletegig->serviceTax->serviceTax }} %</td>
                      <td>{{$rowcompletegig->subTotal}}</td>
                      <td>                        
                        <div class="d-flex">
                          @if($rowcompletegig->status == '9')
                            <button type="button" class="btn btn-success btn-sm">
                              Completed
                            </button>
                          @elseif($rowcompletegig->status == '10')
                            <button type="button" class="btn btn-danger btn-sm">
                              Incompleate
                            </button>
                          @endif
                        </div>  
                      </td>

                    </tr>
                    @endforeach
                       
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Block Modal -->
<div class="modal fade" id="block-modal">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-body text-center">
        <h2><i class="fa fa-toggle-off text-success"></i></h2>
        <h5>Are you sure you want to block this user?</h5>
        <div class="text-center">
          <button type="button" data-dismiss="modal" class="btn btn-success btn-sm">
            Yes
          </button>
          <button type="button" data-dismiss="modal" class="btn btn-danger btn-sm">
            No
          </button>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Unblock Modal -->
<div class="modal fade" id="unblock-modal">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-body text-center">
        <h2><i class="fa fa-toggle-on text-danger"></i></h2>
        <h5>Are you sure you want to unblock this user?</h5>
        <div class="text-center">
          <button type="button" data-dismiss="modal" class="btn btn-success btn-sm">
            Yes
          </button>
          <button type="button" data-dismiss="modal" class="btn btn-danger btn-sm">
            No
          </button>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="set-taxes">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header bg-primary">
        <h5 class="modal-title">Set Taxes</h5>
        <button type="button" data-dismiss="modal" class="close">&times;</button>
      </div>
      <div class="modal-body">
        <form>
          <div class="row">
              <div class="col-md-12">
                  <div class="form-group">
                       <label>Select Province</label>
                       <select class="form-control">
                         <option>choose</option>
                       </select>
                   </div> 
              </div>  
              <div class="col-md-12">
                  <div class="form-group">
                       <label>Set Tax % on offer amount</label>
                        <select class="form-control">
                          @foreach($servicetax as $rowtax)
                            <option>{{$rowtax->serviceTax}}</option>
                          @endforeach
                       </select>
                   </div> 
              </div>  
              <div class="col-md-12">
                  <div class="form-group">
                       <label>Set Manually</label>
                        <input type="text" class="form-control" name="">
                   </div> 
              </div>  
              <div class="col-md-12">
                  <div class="form-group">
                     <button type="button" data-dismiss="modal" class="btn btn-success btn-sm">
                      Confirm
                    </button>
                   </div> 
              </div>  
          </div>
        </form> 
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="set-service">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header bg-primary">
        <h5 class="modal-title">Set Service Charges</h5>
        <button type="button" data-dismiss="modal" class="close">&times;</button>
      </div>
      <div class="modal-body">
        <form action="{{url('admin/update-service-tax')}}" id="add_servicetax" method="post">
          @csrf
          <input type="hidden" id="editId" name="editId">
          <div class="row">
              <div class="col-md-12">
                  <div class="form-group">
                       <label>Set Service Charge %</label>
                        <input type="text" class="form-control" name="servicetax" id="servicetax">
                        <span style="display: none; color: red" id="Errorservicetax">Required </span>
                  </div> 
              </div>  
              <div class="col-md-12">
                  <div class="form-group">
                     <button type="submit" class="btn btn-success btn-sm">
                      Set
                    </button>
                   </div> 
              </div>  
          </div>
        </form> 
      </div>
    </div>
  </div>
</div>

 <div class="modal fade" id="success-modal">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-body text-center">
          <h2><i class="fa fa-link text-success"></i></h2>
          <h5>Service Charge set successfully!</h5>
        </div>
      </div>
    </div>
  </div>

@endsection

@section('footerscript')
  
  @if ($message = session()->has('success_message'))
    <script type="text/javascript">
      $(function() {
        $('#success-modal').modal('show');
         setTimeout(function() {$('#success-modal').modal('hide');}, 2000);
      });
    </script>
  @endif

  <script type="text/javascript">
    
      function getTax(){
        $.ajax({
           type:"GET",
           url:"{{url('admin/adminTax')}}",
           success:function(data){  
             // console.log(data[0].serviceTax); 
             $('#editId').val(data[0].id);
             $('#servicetax').val(data[0].serviceTax);
           }
        });
        $('#set-service').modal('show');
      }

  </script>
  

  <script type="text/javascript">
    $('#add_servicetax').on('submit', function(e){
      
      $('#Errorservicetax').hide();
      var servicetax = $('#servicetax').val();
      if(servicetax == ''){
        $('#Errorservicetax').show();
        e.preventDefault();
      }
    });
  </script>

  <script>
        $(document).ready(function(){
          // $("input").keydown(function(){
          //  alert("dcfdsfdas");
          // });
          $("input").click(function(){
            
            $("#showSubmitButton").html(`<a href="{{url('admin/earning-management')}}" class="btn btn-sm btn-primary pull-right" style="">Reset</a>
                    <button type="submit" class="btn btn-sm btn-primary pull-right " style="margin-right: 12px;">Apply</button>`);

          });
        });
      </script>

      <script type="text/javascript">
        
        function myfun(){  
          
          $("#showSubmitButton").html(`<a href="{{url('admin/earning-management')}}" class="btn btn-sm btn-primary pull-right" style="">Reset</a>
                    <button type="submit" class="btn btn-sm btn-primary pull-right " style="margin-right: 12px;">Apply</button>`);
          
        } 

      </script>
      <script type="text/javascript">
          $('#province').on('change',function(){
          // alert();
          var countryID = $("#province").find(":selected").data("id");;
          //var countryID = $(this).val(); 
          // alert(countryID);  
          if(countryID){
              $.ajax({
                 type:"GET",
                 url:"{{url('admin/UserCity')}}"+'/'+countryID,
                 success:function(res){  
                 //console.log(res[0]); 
                  if(res){

                      $("#city").empty();

                      $.each(res,function(key,value){
                          $("#city").append('<option value="'+value.city+'">'+value.city+'</option>');
                      });
                 
                  }else{
                     $("#city").empty();
                  }
                 }
              });
          }else{
              $("#city").empty();
         }
       });
      </script>


@endsection