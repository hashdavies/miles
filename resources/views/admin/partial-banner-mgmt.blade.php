                    <table class="table table-striped table-bordered complete-dataTable">                    
                      <thead>
                          <tr>
                            <th class="no-sort wpx-50">Sr. No</th>
                            <th class="wpx-200">Banner</th>
                            <th>Associated Seller</th>
                            <th>Upper Content</th>
                            <th>Lower Content</th>
                            <th class="wpx-200 text-center">Actions</th>
                          </tr>
                        </thead>
                      <tbody>
                        @foreach($bnrList as $key=>$rowList)
                        <tr>
                          <td>{{$key+1}}</td>  
                          <td><img src="{{url($rowList->Offer_image)}}" class="img-responsive" alt="User-Profile-Image" width="130px;" height="180px;"></td>
                          <td>@if(!empty($rowList->StoreName->fullName))
                            {{$rowList->StoreName->fullName}}
                            @else
                            N/A
                            @endif  
                          </td>  
                          <td>
                            @if(!empty($rowList->upper_content))
                            {{$rowList->upper_content}}
                            @else
                            N/A
                            @endif
                          </td>
                          <td>
                            @if(!empty($rowList->lower_content))
                            {{$rowList->lower_content}}
                            @else
                            N/A
                            @endif
                          </td>
                          <td class="text-center">
                            <button class="btn btn-sm btn-success edit" id="{{$rowList->id}}"><i class="ti-pencil fs-13"></i></button>
                            <button id="{{$rowList->id}}" class="btn btn-sm btn-danger delete"><i class="ti-trash fs-13"></i></button>
                            <label class="switch">
                              @if($rowList->status == 0 && $rowList->seller_status == 0)
                                <span class="slider round" onclick="seller_block()" style="top: 18px; bottom: -17px;"></span>
                              @elseif($rowList->status == 0)
                                @if($activeBanner >= 10)
                                  <span class="slider round" onclick="limitOver()" style="top: 18px; bottom: -17px;"></span>
                                @else
                                  <input type="checkbox" id="unblockUnblock" onclick="BannerUnBlockUnblock({{$rowList->id}}, {{$activeBanner}})">
                                  <span class="slider round"></span>
                                @endif
                              @elseif($rowList->status == 1)
                                <input type="checkbox" id="blockUnblock" onclick="BannerBlockUnblock({{$rowList->id}})" checked>
                                <span class="slider round"></span>
                              @endif                              
                            </label>

                          </td>               
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                    