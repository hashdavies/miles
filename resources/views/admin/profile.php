<?php include 'header.php';?>
<div class="profilePage"></div>
<div class="layout-content">
  <div class="row gutter-xs">
    <div class="profile-box">
      <div class="card text-center">
        <div class="card-image">
          <div class="overlay">
            <div class="overlay-gradient">
              <img class="card-img-top img-responsive" src="assets/img/bg2.jpg" alt="Instagram App">
            </div>
          </div>
        </div>
        <div class="card-avatar">
          <a class="card-thumbnail rounded sq-100" href="#">
            <img class="img-responsive" src="assets/img/user1.jpg" alt="Instagram">
          </a>
        </div>
        <div class="card-body">
          <h3 class="card-title">Name of Admin</h3>

          <p class="card-text">
            <span class="app-users">
             <span class="icon icon-envelope"></span>
             <strong>Email</strong>: admin@gmail.com
           </span>
          </p>
          <p class="card-text">
            <span class="app-users">
             <span class="icon icon-phone"></span>
             <strong>Phone</strong>: 9874467545
            </span>
          </p>
          <!-- <p class="card-text">
            <span class="app-users">
             <span class="icon icon-map-marker"></span>
            <strong>Address</strong>: Delhi, India

           </span>
          </p> -->
             
          <p class="card-text">
            <span class="app-users">
             <span class="icon icon-globe"></span>
             <strong>Region</strong>: Usa
            </span>
          </p>
          <div class="col-md-12 text-center">
            <a href="edit-profile.php" class="btn btn-primary btn-sm">Edit rofile</a>
             <!-- <a href="changePassword.php" class="btn btn-primary tn-sm">Change Password</a> -->
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

</div>
<?php include 'footer.php';?>
