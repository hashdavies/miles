@extends('admin.header')
@section('title', 'Trip Management')
@section('content')
  @include('admin/navbar')

  <div class="responsiveTbPage ExplorerListPage">
  <div class="layout-content">
    <div class="layout-content-body">
      <div class="title-bar">
        <h1 class="title-bar-title">
          <span class="d-ib">Trip Management</span>
        </h1>
      </div>
      <div class="row">
        <div class="col-xs-12">
          <div class="card">
            <div class="card-header pb-1">
              <div class="row gutter-xs">
                <div class="col-xs-12 col-sm-12 mb-1">
                  <div class="card-actions">
                    <button type="button" class="btn btn-primary btn-sm mb-1" data-target="#add-modal" data-toggle="modal">Add Trip Type</button>
                  </div>
                </div>
          
              </div>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table dataTable table-striped table-bordered">
                  <thead>
                    <tr>
                      <th class="wpx-180">Trip Type</th>
                      <th class="wpx-80 no-sort">Action</th>
                    </tr>
                  </thead>
                  <tbody>                   
                    @foreach($triptype as $rowtrip)
                     <tr>
                      <td>{{ $rowtrip->tripName }}</td>
                      <td>
                       <button type="button" style="margin-right: 5px" class="btn btn-primary btn-sm" onclick="editTrip({{$rowtrip->id}})"><i class="fa fa-pencil"></i></button>

                       @if($rowtrip->status == '0')
                          <button type="button" class="btn btn-success btn-sm" onclick="block({{ $rowtrip->id }}, 1)" data-target="#block-modal" data-toggle="modal"><i class="fa fa-toggle-off"></i></button>

                       @elseif($rowtrip->status == '1')
                          <button type="button" class="btn btn-danger btn-sm" data-target="#block-modal" onclick="block({{ $rowtrip->id }}, 0)" data-toggle="modal"><i class="fa fa-toggle-on"></i></button>
                       @endif                      
                        
                        <!--  -->
                        <!-- <button type="button" class="btn btn-danger btn-sm" data-target="#delete-modal" data-toggle="modal"><i class="fa fa-trash-o"></i></button> -->
                      </td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Block Modal -->
<div class="modal fade" id="block-modal">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-body text-center">
        <h2><i class="fa fa-toggle-off text-success"></i></h2>
        <h5 id="titleblock"></h5>
        <div class="text-center">
          <a class="btn btn-success btn-sm countinueblock">Yes</a>
          <button type="button" data-dismiss="modal" class="btn btn-danger btn-sm">
            No
          </button>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Delete Modal -->
<div class="modal fade" id="delete-modal">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-body text-center">
        <h2><i class="fa fa-trash-o text-danger"></i></h2>
        <h5>{{ session()->get('success_message') }}</h5>
      </div>
    </div>
  </div>
</div>

<!-- View Modal -->
<div class="modal fade" id="add-modal">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header bg-primary">
        <h5 class="modal-title">Add Trip Type</h5>
        <button type="button" data-dismiss="modal" class="close">&times;</button>
      </div>
      <div class="modal-body">
        <form action="{{url('admin/add_trip')}}" method="post" id="addtrip">
          @csrf
          <div class="row">
              <div class="col-md-12">
                  <div class="form-group">
                       <label>Enter Trip Name</label>
                       <input type="text" class="form-control" name="tripName" id="tripName">
                        <span style="color: red; display: none;" id="tripNameError">Enter Trip Name</span>
                   </div>                   
              </div>  
               <div class="col-md-12">
                  <div class="form-group">
                       <label>Enter Trip Details</label>
                       <textarea class="form-control" name="tripdetail" id="tripdetail" rows="4"></textarea>
                       <span style="color: red; display: none;" id="tripDetailsError">Enter Trip Details</span>
                   </div> 
              </div>  
              <div class="col-md-12">
                  <div class="form-group">
                     <button type="submit" class="btn btn-success btn-sm">
                      Save
                    </button>
                   </div> 
              </div>  
          </div>
        </form> 
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="edit-modal">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header bg-primary">
        <h5 class="modal-title">Edit Trip Type</h5>
        <button type="button" data-dismiss="modal" class="close">&times;</button>
      </div>
      <div class="modal-body">
        <form action="{{url('admin/update-trip')}}" method="post" id="updatetrip">
          @csrf
          <div id="EdittId"></div>
          <div class="row">
              <div class="col-md-12">
                  <div class="form-group">
                       <label>Enter Trip Name</label>
                       <div id="EdittripName"></div>
                       <span style="color: red; display: none;" id="edittripNameError">Enter Trip Name</span>
                   </div> 
              </div>  
               <div class="col-md-12">
                  <div class="form-group">
                       <label>Enter Trip Details</label>
                       <textarea class="form-control" name="edittripdetail" rows="4" id="EdittripDetails"></textarea>
                       <span style="color: red; display: none;" id="edittripDetailsError">Enter Trip Details</span>
                   </div> 
              </div>  
              <div class="col-md-12">
                  <div class="form-group">
                     <button type="submit" class="btn btn-success btn-sm">
                      Save
                    </button>
                   </div> 
              </div>  
          </div>
        </form> 
      </div>
    </div>
  </div>
</div>

@endsection

@section('footerscript')
  
  <script type="text/javascript">

      $('#addtrip').on('submit', function(e){
        $('#tripNameError').hide();
        $('#tripDetailsError').hide();
        var tripName = $('#tripName').val();
        var tripdetail = $('#tripdetail').val();
        if(tripName == ''){
          $('#tripNameError').show();
          e.preventDefault();
        }
        if(tripdetail == ''){
          $('#tripDetailsError').show();
          e.preventDefault();
        }
      });
  </script>

  @if ($message = session()->has('success_message'))
    <script type="text/javascript">
       $(function() {
          $('#delete-modal').modal('show');
          setTimeout(function() {$('#delete-modal').modal('hide');}, 2000);
        });
    </script>
  @endif

  <script type="text/javascript">
      
      function editTrip(id){

        if(id>0){
            var globalarray = <?php echo json_encode($triptype); ?>;
            var theIndex = -1;
            for (var i = 0; i < globalarray.length; i++) {
              if (globalarray[i].id == id) {
                theIndex = i;
                break;
              }
            }
            $("#EdittId").html(`<input type="hidden" class="form-control" name="editId" id="editId" value="`+globalarray[theIndex].id+`">`);
            $("#EdittripName").html(`<input type="text" class="form-control" name="edittriptype" id="edittriptype" value="`+globalarray[theIndex].tripName+`">`);
            $("#EdittripDetails").text(globalarray[theIndex].tripDetails);
            $('#edit-modal').modal('show');
        }
      }
  </script>
  
  <script type="text/javascript">
      $('#updatetrip').on('submit', function(e){
        $('#edittripNameError').hide();
        $('#edittripDetailsError').hide();
        var edittriptype = $('#edittriptype').val();
        var EdittripDetails = $('#EdittripDetails').val();
        if(edittriptype == ''){
          $('#edittripNameError').show();
          e.preventDefault();
        }
        if(EdittripDetails == ''){
          $('#edittripDetailsError').show();
          e.preventDefault();
        }
      });
  </script>

  <script type="text/javascript">
        
        function block(id,status){
          
          if(status == 1){
            $('#titleblock').text("Are you want to sure Block this Trip");
          }else if(status == 0){
            $('#titleblock').text("Are you want to sure Unblock this Trip");
          }
          $(".countinueblock").attr("href", "{{url('admin/blockTrip')}}"+"/"+id+"/"+status);
          
          }

  </script>

@endsection