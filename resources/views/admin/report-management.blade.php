@extends('admin.header')
@section('title', 'Earning Management')
@section('content')
@include('admin/navbar')
<div class="responsiveTbPage ExplorerListPage">
   <div class="layout-content">
      <div class="layout-content-body">
         <div class="title-bar">
            <h1 class="title-bar-title">
               <span class="d-ib text-danger">Earning Management</span>
            </h1>
         </div>
         <div class="row">
            <div class="col-xs-12">
               <div class="card">
                  <div class="card-header pb-1">
                     <div class="row gutter-xs pt-5">
                      <div class="clearfix"></div>
                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                          <h4>User Report </h4>
                        </div>
                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                          <a class="btn btn-success pull-right wpx-180" href="example.xlsx" download>Download Excel</a>
                        </div>
                     </div>
                     <div class="row pt-4">
                       <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                         <div class="">
                             <label class="d-flex cs_box">
                                <input type="checkbox" name="">
                                <p class="pl-2">Total no. of 
                                registerd user</p>
                             </label>
                         </div>
                       </div>
                       <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                          <div class="">
                             <label class="d-flex cs_box">
                                <input type="checkbox" name="">
                                <p class="pl-2">Total no. of 
                                registerd user</p>
                             </label>
                         </div>
                       </div>
                       <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                          <div class="">
                             <label class="d-flex cs_box">
                                <input type="checkbox" name="">
                                <p class="pl-2">Total no. of 
                                registerd user</p>
                             </label>
                         </div>
                       </div>
                     </div>

                     <div class="clearfix"></div>
                     <div class="row gutter-xs pt-5">
                      <div class="clearfix"></div>
                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                          <h4>Service Provider Report </h4>
                        </div>
                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                          <a class="btn btn-success pull-right wpx-180" href="example.xlsx" download>Download Excel</a>
                        </div>
                     </div>
                     <div class="row pt-4">
                       <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                         <div class="">
                             <label class="d-flex cs_box">
                                <input type="checkbox" name="">
                                <p class="pl-2">Total no. of 
                                registerd user</p>
                             </label>
                         </div>
                       </div>
                       <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                          <div class="">
                             <label class="d-flex cs_box">
                                <input type="checkbox" name="">
                                <p class="pl-2">Total no. of 
                                registerd user</p>
                             </label>
                         </div>
                       </div>
                       <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                          <div class="">
                             <label class="d-flex cs_box">
                                <input type="checkbox" name="">
                                <p class="pl-2">Total no. of 
                                registerd user</p>
                             </label>
                         </div>
                       </div>
                     </div>

                     <div class="clearfix"></div>
                     <div class="row gutter-xs pt-5">
                      <div class="clearfix"></div>
                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                          <h4>Service Report </h4>
                        </div>
                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                          <a class="btn btn-success pull-right wpx-180" href="example.xlsx" download>Download Excel</a>
                        </div>
                     </div>
                     <div class="row pt-4">
                       <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                         <div class="">
                             <label class="d-flex cs_box">
                                <input type="checkbox" name="">
                                <p class="pl-2">Total no. of 
                                registerd user</p>
                             </label>
                         </div>
                       </div>
                       <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                          <div class="">
                             <label class="d-flex cs_box">
                                <input type="checkbox" name="">
                                <p class="pl-2">Total no. of 
                                registerd user</p>
                             </label>
                         </div>
                       </div>
                       <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                          <div class="">
                             <label class="d-flex cs_box">
                                <input type="checkbox" name="">
                                <p class="pl-2">Total no. of 
                                registerd user</p>
                             </label>
                         </div>
                       </div>
                     </div>


                     <div class="clearfix"></div>
                     <div class="row gutter-xs pt-5">
                      <div class="clearfix"></div>
                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                          <h4>Payment Report</h4>
                        </div>
                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                          <a class="btn btn-success pull-right wpx-180" href="example.xlsx" download>Download Excel</a>
                        </div>
                     </div>
                     <div class="row pt-4">
                       <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                         <div class="">
                             <label class="d-flex cs_box">
                                <input type="checkbox" name="">
                                <p class="pl-2">Total no. of 
                                registerd user</p>
                             </label>
                         </div>
                       </div>
                       <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                          <div class="">
                             <label class="d-flex cs_box">
                                <input type="checkbox" name="">
                                <p class="pl-2">Total no. of 
                                registerd user</p>
                             </label>
                         </div>
                       </div>
                       <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                          <div class="">
                             <label class="d-flex cs_box">
                                <input type="checkbox" name="">
                                <p class="pl-2">Total no. of 
                                registerd user</p>
                             </label>
                         </div>
                       </div>
                     </div>

                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
 
<!-- Block Modal -->
<div class="modal fade" id="block-modal">
   <div class="modal-dialog modal-sm">
      <div class="modal-content">
         <div class="modal-body text-center">
            <h2><i class="fa fa-toggle-off text-success"></i></h2>
            <h5>Are you sure you want to block this user?</h5>
            <div class="text-center">
               <button type="button" data-dismiss="modal" class="btn btn-success btn-sm">
               Yes
               </button>
               <button type="button" data-dismiss="modal" class="btn btn-danger btn-sm">
               No
               </button>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- Unblock Modal -->
<div class="modal fade" id="unblock-modal">
   <div class="modal-dialog modal-sm">
      <div class="modal-content">
         <div class="modal-body text-center">
            <h2><i class="fa fa-toggle-on text-danger"></i></h2>
            <h5>Are you sure you want to unblock this user?</h5>
            <div class="text-center">
               <button type="button" data-dismiss="modal" class="btn btn-success btn-sm">
               Yes
               </button>
               <button type="button" data-dismiss="modal" class="btn btn-danger btn-sm">
               No
               </button>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- Delete Modal -->
<!-- <div class="modal fade" id="delete-modal">
   <div class="modal-dialog modal-sm">
     <div class="modal-content">
       <div class="modal-body text-center">
         <h2><i class="fa fa-trash-o text-danger"></i></h2>
         <h5>Are you sure you want to delete this user?</h5>
         <div class="text-center">
           <button type="button" data-dismiss="modal" class="btn btn-success btn-sm">
             Yes
           </button>
           <button type="button" data-dismiss="modal" class="btn btn-danger btn-sm">
             No
           </button>
         </div>
       </div>
     </div>
   </div>
   </div> -->
<div class="modal fade" id="set-taxes">
   <div class="modal-dialog modal-sm">
      <div class="modal-content">
         <div class="modal-header bg-primary">
            <h5 class="modal-title">Set Taxes</h5>
            <button type="button" data-dismiss="modal" class="close">&times;</button>
         </div>
         <div class="modal-body">
            <form>
               <div class="row">
                  <div class="col-md-12">
                     <div class="form-group">
                        <label>Select Province</label>
                        <select class="form-control">
                           <option>choose</option>
                           <option>choose</option>
                           <option>choose</option>
                        </select>
                     </div>
                  </div>
                  <div class="col-md-12">
                     <div class="form-group">
                        <label>Set Tax % on offer amount</label>
                        <select class="form-control">
                           <option>choose</option>
                           <option>choose</option>
                           <option>choose</option>
                        </select>
                     </div>
                  </div>
                  <div class="col-md-12">
                     <div class="form-group">
                        <label>Set Manually</label>
                        <input type="text" class="form-control" name="">
                     </div>
                  </div>
                  <div class="col-md-12">
                     <div class="form-group">
                        <button type="button" data-dismiss="modal" class="btn btn-success btn-sm">
                        Confirm
                        </button>
                     </div>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="set-service">
   <div class="modal-dialog modal-sm">
      <div class="modal-content">
         <div class="modal-header bg-primary">
            <h5 class="modal-title">Set Service Charges</h5>
            <button type="button" data-dismiss="modal" class="close">&times;</button>
         </div>
         <div class="modal-body">
            <form>
               <div class="row">
                  <div class="col-md-12">
                     <div class="form-group">
                        <label>Set Service Charge %</label>
                        <input type="text" class="form-control" name="">
                     </div>
                  </div>
                  <div class="col-md-12">
                     <div class="form-group">
                        <button type="button" data-dismiss="modal" class="btn btn-success btn-sm">
                        Set
                        </button>
                     </div>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
<!-- Rating Modal -->
<div class="modal fade" id="rating-modal">
   <div class="modal-dialog modal-md">
      <div class="modal-content">
         <div class="modal-body text-center bg-primary">
            <h4 class="text-left mb-0"><strong>RATINGS & REVIES</strong> given by user</h4>
         </div>
         <div class="modal-body text-center">
            <table class="table table-striped table-bordered no-footer" >
               <thead>
                  <tr>
                     <th class="h5">ORDER ID</th>
                     <td class="text-left h5">ASD324</td>
                  </tr>
                  <tr>
                     <th class="h5">RATINGS</th>
                     <td class="text-left h5">
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star-half-o" aria-hidden="true"></i>
                        <i class="fa fa-star-o" aria-hidden="true"></i>
                        <i class="fa fa-star-o" aria-hidden="true"></i>
                     </td>
                  </tr>
                  <tr>
                     <th class="h5">REVIEW</th>
                     <td></td>
                  </tr>
               </thead>
            </table>
            <p class="text-left">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
               tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
               quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
               consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
               cillum dolore eu fugiat nulla pariatur.
            </p>
         </div>
         <div class="modal-body text-center mt-0">
            <h4 class="text-left"><strong>RATING & REVIEW </strong> given by service provider</h4>
            <table class="table table-striped table-bordered no-footer" >
               <thead>
                  <tr>
                     <th class="h5">ORDER ID</th>
                     <td class="text-left h5">ASD324</td>
                  </tr>
                  <tr>
                     <th class="h5">RATINGS</th>
                     <td class="text-left h5">
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star-half-o" aria-hidden="true"></i>
                        <i class="fa fa-star-o" aria-hidden="true"></i>
                        <i class="fa fa-star-o" aria-hidden="true"></i>
                     </td>
                  </tr>
                  <tr>
                     <th class="h5">REVIEW</th>
                     <td></td>
                  </tr>
               </thead>
            </table>
            <p class="text-left">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
               tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
               quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
               consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
               cillum dolore eu fugiat nulla pariatur.
            </p>
         </div>
      </div>
   </div>
</div>
<!-- Rating Modal -->
@endsection