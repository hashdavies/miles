@extends('admin.header')
  @section('title', 'Service Provider Management')
  @section('content')
  @include('admin/navbar')  

<div class="responsiveTbPage ExplorerListPage">
  <div class="layout-content">
    <div class="layout-content-body">
      <div class="title-bar">
        <h1 class="title-bar-title">
          <span class="d-ib">Service Category Management > Service Subcategory</span>
        </h1>
      </div>
      <div class="row">
        <div class="col-md-8">
          <div class="card">
            
            <div class="card-body">
                <form action="{{url('admin/add-subcategory')}}" method="post" id="add_subcategory_services" enctype="multipart/form-data">
                   @csrf
                   <div class="row">
                      <div class="col-md-4">
                        <div class="form-group add-pic">
                            <div class="pic-box">
                                <div class="logoImg">
                                   <img src="{{url('public/admin/img/user.jpg')}}" id="result">
                                   <input id="logo-file" type="file" name="     sub_category_image" class="hide">
                                   <label for="logo-file" class="btn btn-large"></label>
                                </div>
                            </div>
                        </div>
                        <span style="color: red; display: none;" id="ErrorImage">Please attach image</span>
                      </div> 
                    </div>
                    <div class="row">
                      
                      <div class="col-md-12">
                          <div class="form-group">
                              <label class="black fw-600">Add Sub Service Category Name</label>
                              <input type="text" class="form-control" name="sub_category_name" id="subcatname">
                              <span style="color: red; display: none;" id="Errorsubcatename">Enter subcategory name</span>
                          </div>                          
                      </div>  
                       <div class="col-md-12">
                          <div class="form-group">
                              <label class="black fw-600">Add Sub Service Category Description</label>
                              <textarea class="form-control" name="sub_category_description" id="description" rows="4"></textarea>
                              <span style="color: red; display: none;" id="ErrorDescription">Enter subcategory description</span>
                          </div>
                      </div>  
                      <div class="col-md-12">
                          <div class="m-t-10 text-center">
                              <button type="submit" class="btn btn-md btn-primary">Save & Continue</button>
                          </div>
                      </div> 
                  </div>
                </form>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
 
<script type="text/javascript">
  $(".js-select2").multiSelect({});
</script>
<!-- Block Modal -->
<div class="modal fade" id="block-modal">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-body text-center">
        <h2><i class="fa fa-toggle-off text-success"></i></h2>
        <h5>Are you sure you want to block this user?</h5>
        <div class="text-center">
          <button type="button" data-dismiss="modal" class="btn btn-success btn-sm">
            Yes
          </button>
          <button type="button" data-dismiss="modal" class="btn btn-danger btn-sm">
            No
          </button>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Unblock Modal -->
<div class="modal fade" id="unblock-modal">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-body text-center">
        <h2><i class="fa fa-toggle-on text-danger"></i></h2>
        <h5>Are you sure you want to unblock this user?</h5>
        <div class="text-center">
          <button type="button" data-dismiss="modal" class="btn btn-success btn-sm">
            Yes
          </button>
          <button type="button" data-dismiss="modal" class="btn btn-danger btn-sm">
            No
          </button>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Delete Modal -->
<!-- <div class="modal fade" id="delete-modal">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-body text-center">
        <h2><i class="fa fa-trash-o text-danger"></i></h2>
        <h5>Are you sure you want to delete this user?</h5>
        <div class="text-center">
          <button type="button" data-dismiss="modal" class="btn btn-success btn-sm">
            Yes
          </button>
          <button type="button" data-dismiss="modal" class="btn btn-danger btn-sm">
            No
          </button>
        </div>
      </div>
    </div>
  </div>
</div> -->

<!-- View Modal -->

@endsection

@section('footerscript')
  <script type="text/javascript">
    $(".js-select2").multiSelect({});
  </script>

  <script type="text/javascript">
    $('#add_subcategory_services').on('submit', function(e){
      $('#ErrorImage').hide();
      $('#ErrorService').hide();
      $('#Errorsubcatename').hide();
      $('#ErrorDescription').hide();

      var sub_category_image = $('#logo-file').val();
      var serviceType = $('#serviceType').val();
      var subcatname = $('#subcatname').val();
      var description = $('#description').val();

      if(sub_category_image == ''){
        $('#ErrorImage').show();
        e.preventDefault();
      }
      if(serviceType == ''){
        $('#ErrorService').show();
        e.preventDefault();
      }
      if(subcatname == ''){
        $('#Errorsubcatename').show();
        e.preventDefault();
      }
      if(description == ''){
        $('#ErrorDescription').show();
        e.preventDefault();
      }
    });
  </script>
@endsection