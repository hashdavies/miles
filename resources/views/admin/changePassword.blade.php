@extends('admin.header')    
@section('title', 'Admin Dashboard')
@section('content')

<div class="profilePage"></div>

<div class="layout-content">
    <div class="layout-content-body">
        <div class="title-bar">
            <h1 class="title-bar-title">
              <span class="d-ib">Change Password /</span>
              <!-- <a href="profile.php">Back</a> -->
          </h1>
      </div>
      

      @if ($message = Session::get('message'))
      <div class="alert alert-success alert-block">
          <button type="button" class="close" data-dismiss="alert">×</button> 
          <strong>{{ $message }}</strong>
      </div>
      @endif

      @if ($error = Session::get('error'))
      <div class="alert alert-danger alert-block">
          <button type="button" class="close" data-dismiss="alert">×</button> 
          <strong>{{ $error }}</strong>
      </div>
      @endif

      <div class="row gutter-xs">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-5">
            <div class="card">
                <form class="form card-body" method="post" action="{{ route('change-password-admin') }}">
                    {{ csrf_field() }}

                    <div class="form-group">
                        <label for="" class=" control-label">Old Password</label>
                        <input id="" class="form-control" placeholder="Old Password" name="old_password" type="password" required="">
                    </div>
                    <div class="form-group">
                        <label for="" class=" control-label">New Password</label>
                        <input id="" class="form-control" placeholder="New Password" name="new_password" type="password" required="">
                    </div>

                    <div class="form-group">
                        <label for="" class=" control-label">Confirm Password</label>
                        <input id="" class="form-control" placeholder="Confirm Password" name="confirm_password" type="password" required="">
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary btn-block " type="submit">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
</div>
@endsection  