 @extends('admin.header')
  @section('title', 'Completed Management')
  @section('content')
      @include('admin/navbar')
<div class="responsiveTbPage ExplorerListPage">
  <div class="layout-content">
    <div class="layout-content-body">
      <div class="title-bar">
        <h1 class="title-bar-title">
          <span class="d-ib">Gig Management > Completed</span>
        </h1>
      </div>
      <div class="row">
        <div class="col-xs-12">
          <div class="card">
            <div class="card-header pb-1">
              <div class="row">
                  <div class="col-md-12 margin-bottom-40">
                      <ul class="nav nav-tabs nav-justified md-tabs " role="tablist">
                              <li class="nav-item">
                                  <a class="nav-link" href="{{url('admin/pending-gig')}}">
                                  Pending
                                  </a> 
                                  <div class="slide"></div>
                              </li>
                              <li class="nav-item ">
                                  <a class="nav-link active" href="{{url('admin/ongoing-gig')}}">
                                     On-Going
                                  </a>
                                  <div class="slide"></div>
                              </li>
                              
                              <li class="nav-item ">
                                  <a class="nav-link" href="{{url('admin/cancelled-gig')}}">
                                  Cancelled
                                  </a> 
                                  <div class="slide"></div>
                              </li>

                              <li class="nav-item active">
                                  <a class="nav-link" href="{{url('admin/completed-gig')}}">
                                      Completed
                                  </a> 
                                  <div class="slide"></div>
                              </li>
                          </ul>
                   </div> 
               </div> <br>
              <div class="row gutter-xs">
                <form action="{{url('admin/compleate-gig-filter')}}" method="get" class="">
                    <div class="col-xs-6 col-sm-3 col-md-3 mb-2">
                      <label class="wpx-50">From</label>
                      <input type="text" id="start_date" name="start_date" class="form-control" placeholder="mm/dd/yy" onkeypress="myfun()">
                    </div>
                    <div class="col-xs-6 col-sm-3 col-md-3 mb-2">
                      <label class="wpx-50">To</label>
                      <input type="text" placeholder="mm/dd/yy" id="end_date" name="end_date" class="form-control" onkeypress="myfun()">
                    </div>
                    <div class="col-xs-6 col-sm-2 col-md-2 mb-2">
                      <label>Province</label>
                      <select class="form-control" name="province" onchange="myfun()" id="province">
                        <option value="">Choose Province</option>
                          @foreach($province as $rowprovince)
                              <option data-id="{{$rowprovince->proviance}}" value="{{$rowprovince->proviance}}">{{$rowprovince->proviance}}</option>
                          @endforeach
                      </select>
                    </div>
                    <div class="col-xs-6 col-sm-2 col-md-2 mb-2">
                      <label>City</label>
                      <select class="form-control" name="city" onchange="myfun()" id="city">
                        <option value="">Choose City</option>
                         
                      </select>
                    </div>
                    <!-- <div class="col-xs-6 col-sm-2 col-md-2 mb-2">
                      <label>Sort By</label>
                      <select class="form-control" name="Status">
                        <option value="">Choose</option>
                        <option value="Active">Active</option>
                        <option value="Inctive">Inctive</option>
                      </select>
                    </div> -->
                    <div class="col-xs-6 col-sm-2 col-md-2 mb-2">
                      <br>
                      <div id="showSubmitButton">
                            
                        </div>
                    </div> 
                  <form> 

              </div>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table dataTable table-striped table-bordered">
                  <thead>
                    <tr>
                      <th>Gig ID</th>
                      <th class="wpx-180">Service Provider Name</th>
                      <th>User Name</th>
                      <th>Date & Time</th>
                      <th>Service Type</th>
                      <th>City</th>
                      <th>Status</th>
                      <th class="wpx-80 no-sort">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($completedGig as $key=>$rowcompleted)
                    <tr>
                        <td>#{{$key+1}}</td>
                        <td>{{@$rowcompleted->SellerName->firstName ?? 'N/A'}}</td>
                        <td>{{@$rowcompleted->UserName->firstName ?? 'N/A'}}</td>
                        <td>{{ $rowcompleted->GigDetails->pickupDate ?? 'N/A' }}
                            {{$rowcompleted->GigDetails->pickupTime ?? '' }}</td>
                        <td>{{@$rowcompleted->serviceName->category_name }} </td>
                        <td>{{$rowcompleted->subcategoryName->name }}</td>
                        <td>
                          @if($rowcompleted->status == '9')
                            <span class="text-success">Completed</span>
                          @endif                            
                        </td>
                        <td>
                           <div class="d-flex">
                                 <button type="button" onclick="ViewStoreDetails({{$rowcompleted->id}})" class="btn btn-info btn-sm" style="margin-right: 6px;"><i class="fa fa-eye"></i></button> 
                           </div>
                        </td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Block Modal -->
<div class="modal fade" id="block-modal">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-body text-center">
        <h2><i class="fa fa-toggle-off text-success"></i></h2>
        <h5>Are you sure you want to block this user?</h5>
        <div class="text-center">
          <button type="button" data-dismiss="modal" class="btn btn-success btn-sm">
            Yes
          </button>
          <button type="button" data-dismiss="modal" class="btn btn-danger btn-sm">
            No
          </button>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Unblock Modal -->
<div class="modal fade" id="unblock-modal">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-body text-center">
        <h2><i class="fa fa-toggle-on text-danger"></i></h2>
        <h5>Are you sure you want to unblock this user?</h5>
        <div class="text-center">
          <button type="button" data-dismiss="modal" class="btn btn-success btn-sm">
            Yes
          </button>
          <button type="button" data-dismiss="modal" class="btn btn-danger btn-sm">
            No
          </button>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Delete Modal -->
<!-- <div class="modal fade" id="delete-modal">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-body text-center">
        <h2><i class="fa fa-trash-o text-danger"></i></h2>
        <h5>Are you sure you want to delete this user?</h5>
        <div class="text-center">
          <button type="button" data-dismiss="modal" class="btn btn-success btn-sm">
            Yes
          </button>
          <button type="button" data-dismiss="modal" class="btn btn-danger btn-sm">
            No
          </button>
        </div>
      </div>
    </div>
  </div>
</div> -->

<!-- View Modal -->
    <div class="modal fade" id="view-modal">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header bg-primary">
            <h5 class="modal-title">View</h5>
            <button type="button" data-dismiss="modal" class="close">&times;</button>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right mb-3">
                 <button type="button" class="btn btn-success btn-sm"> Completed  </button>
              </div>
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <table class="table">
                  <tr>
                    <th>User Name</th>
                    <td id="userName"></td>
                    
                  </tr>
                  <tr>
                    
                    <th>Business/Shop Name</th>
                    <td id="sellerName">data</td>
                  </tr>
                  <tr>
                    <th>Pickup Location</th>
                     <td id="pickupLocation">data</td>
                  </tr>
                  <tr>
                    <th>Drop Location</th>
                     <td id="dropLocation">data</td>
                  </tr>
                  <tr>
                    <th>Gig Id</th>
                     <td id="gigId">data</td>
                  </tr>
                   <tr>
                    <th>Gig Type</th>
                     <td id="gigType">data</td>
                  </tr>
                   <tr>
                    <th>Date</th>
                     <td id="date">data</td>
                  </tr>
                    <tr>
                    <th>Time</th>
                     <td id="time">data</td>
                  </tr>
                   <tr>
                    <th>City</th>
                     <td id="city1">data</td>
                  </tr>
                  <tr>
                    <th>Items</th>
                     <td id="items">data</td>
                  </tr>
                   <tr>
                    <th>Ratings</th>
                     <td id="rating">
                      <!--  <i class="fa fa-star text-warning"></i>
                        <i class="fa fa-star text-warning"></i>
                         <i class="fa fa-star text-warning"></i> -->
                     </td>
                  </tr>
                  <tr>
                    <th>Amount</th>
                     <td id="Amount">data</td>
                  </tr>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  @endsection

  @section('footerscript')
        <script>
          $(document).ready(function(){
            // $("input").keydown(function(){
            //  alert("dcfdsfdas");
            // });
            $("input").click(function(){
              
              $("#showSubmitButton").html(`<a href="{{url('admin/completed-gig')}}" class="btn btn-sm btn-primary pull-right" style="">Reset</a>
                      <button type="submit" class="btn btn-sm btn-primary pull-right " style="margin-right: 12px;">Apply</button>`);

            });
          });
        </script>

        <script type="text/javascript">
          
          function myfun(){  
            
            $("#showSubmitButton").html(`<a href="{{url('admin/completed-gig')}}" class="btn btn-sm btn-primary pull-right" style="">Reset</a>
                      <button type="submit" class="btn btn-sm btn-primary pull-right " style="margin-right: 12px;">Apply</button>`);
            
          } 

        </script>
      <script type="text/javascript">
         function ViewStoreDetails(id){
        if(id>0){
            var globalarray = <?php echo json_encode($completedGig); ?>;
            var theIndex = -1;
            for (var i = 0; i < globalarray.length; i++) {
              if (globalarray[i].id == id) {
                theIndex = i;
                break;
              }
            }
              $("#userName").text(globalarray[theIndex].user_name['firstName']);
            if(globalarray[theIndex].seller_name){
              $("#sellerName").text(globalarray[theIndex].seller_name['firstName']);
            }else{
              $('#sellerName').text('N/A');
            }

            // $("#sellerName").text(globalarray[theIndex].seller_name['firstName']);

            $("#pickupLocation").text(globalarray[theIndex].gig_details['pickUpAddress']);
            $("#dropLocation").text(globalarray[theIndex].gig_details['dropAddress']);

            $("#gigId").text(globalarray[theIndex].id);
            if(globalarray[theIndex].gig_details.trip_type){
              $("#gigType").text(globalarray[theIndex].gig_details['trip_type']);
            }else{
              $('#gigType').text('N/A');
            } 
            if(globalarray[theIndex].gig_details.pickupDate){
              $("#date").text(globalarray[theIndex].gig_details['pickupDate']);
            }else{
              $('#date').text('N/A');
            }
            if(globalarray[theIndex].gig_details.pickupDate){
              $("#time").text(globalarray[theIndex].gig_details['pickupTime']);
            }else{
              $('#time').text('N/A');
            }
            if(globalarray[theIndex].subcategoryName){
              $("#city1").text(globalarray[theIndex].subcategoryName.name);
            }else{
              $('#city1').text('N/A');
            }            
            $("#items").text(globalarray[theIndex].gig_details['delivery_address']);

            $('#rating').empty();
            if(globalarray[theIndex].Ratings){

              for (var i = 0; i < globalarray[theIndex].Ratings['rating']; i++) {
                 $("#rating").append('<a href="{{url('admin/review-ratings-management')}}"><i class="fa fa-star text-warning"></i></a>');
              }           
              // $('#rating').text('N/A');
              for (var i = 0; i < (5 - globalarray[theIndex].Ratings['rating']); i++) {
                  $("#rating").append('<i class="fa fa-star-o text-warning"></i>');
              }              
            }else{
                 $('#rating').text('N/A');
            }

            // $("#rating").text(globalarray[theIndex].Ratings['rating']);
            // console.log(globalarray[theIndex]);

            $("#Amount").html(`$`+globalarray[theIndex].offerAmount+``); 
         $('#view-modal').modal('show');
      }      
    }
    </script>

    <script type="text/javascript">
        $('#province').on('change',function(){
        // alert();
        var countryID = $("#province").find(":selected").data("id");;
        //var countryID = $(this).val(); 
        // alert(countryID);  
        if(countryID){
            $.ajax({
               type:"GET",
               url:"{{url('admin/ongoingCity')}}"+'/'+countryID,
               success:function(res){  
               //console.log(res[0]); 
                if(res){

                    $("#city").empty();

                    $.each(res,function(key,value){
                        $("#city").append('<option value="'+value.cityName+'">'+value.city.name+'</option>');
                    });
               
                }else{
                   $("#city").empty();
                }
               }
            });
        }else{
            $("#city").empty();
       }
     });
    </script>
  @endsection