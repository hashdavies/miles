@extends('admin.header')
@section('title', 'Promo Code Management')
@section('content')
@include('admin/navbar')
<div class="responsiveTbPage ExplorerListPage">
   <div class="layout-content">
      <div class="layout-content-body">
         <div class="title-bar">
            <h1 class="title-bar-title">
               <span class="d-ib">Promocode Management</span>
            </h1>
         </div>
         <div class="row">
            <div class="col-xs-12">
               <div class="card">
                   <div class="card-header pb-1">
              <div class="row gutter-xs">
              <form action="{{url('admin/promo-code-management')}}" method="get" id="promoCodeFilter">
                    <div class="col-sm-6 col-md-6 col-lg-3">
                        <label class="wpx-50">From</label>
                        <input type="date" class="ml-2 form-control mr-3" id="startDate" value="{{@$_GET['startDate']}}" name="startDate" placeholder="mm/dd/yyyy">
                       <!--  <i class="fa fa-calendar mt-2 fs-18"></i> -->
                       
                       <span style="color: red; display: none; margin: 12px;" id="StartDateError">Please choose atleast one column</span>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-3">
                        <label class="wpx-50">To</label> 
                        <input type="date" class="ml-2 form-control mr-3" id="endDate" value="{{@$_GET['endDate']}}" placeholder="mm/dd/yyyy" name="endDate">
                        <!--<i class="fa fa-calendar mt-2 fs-18"></i> -->
                      <span style="color: red; display: none;" id="EndDateError">Please choose end date</span>  
                    </div>

                  <!-- <div class="col-xs-6 col-sm-2 col-md-2 mb-2">
                  <label>Province</label>
                  <select class="form-control" name="province" onchange="myfun()" id="province">
                    <option value="">Choose province</option>
                     
                  </select>
                </div>    
                <div class="col-xs-6 col-sm-2 col-md-2 mb-2">
                  <label>City</label>
                  <select class="form-control" name="city" id="city" onchange="myfun()">
                    <option value="">Choose City</option>
                     
                  </select>
                </div>   -->              
                
                <div class="col-xs-6 col-sm-4 col-md-2 mb-2">
                  <label>Sort By</label>
                  <select class="form-control" name="status" id="status">
                    <option value="">Choose Status</option>
                    <option value="1" @if(@$_GET['status'] == 1) selected @endif >Active</option>
                    <option value="2" @if(@$_GET['status'] == 2) selected @endif>Inactive</option>
                  </select>
                 <!--  <span style="color: red; display: none;" id="ErrorImage">Please attach image</span> -->
                </div>

                <div class="col-xs-6 col-sm-4 col-md-2 mb-2">
                  <br>
                  <a href="{{url('admin/promo-code-management')}}" class="btn btn-sm btn-primary pull-right" style="">Reset</a>
                     <button type="submit" class="btn btn-sm btn-primary pull-right " style="margin-right: 12px;">Apply</button>
                     
                </div> 
              </form>              

               <div class="col-xs-6 col-sm-4 col-md-2 "><br>
                      <button type="button" class="editButton btn btn-success btn-sm mr-1 pull-right mt-1" data-target="#promocode-modal" data-toggle="modal">Add Coupon</button>
               </div> 
              </div>
            </div>
                 <!--  <div class="card-header pb-1">
                     <div class="row gutter-xs">
                        <div class="col-xs-6 col-sm-2 col-md-2 mb-2">
                           <label>Province</label>
                           <select class="form-control">
                              <option value="">Choose</option>
                              <option value="Province 1">Province 1</option>
                              <option value="Province 1">Province 1</option>
                              <option value="Province 1">Province 1</option>
                           </select>
                        </div>
                        <div class="col-xs-6 col-sm-2 col-md-2 mb-2">
                           <label>City</label>
                           <select class="form-control">
                              <option value="">Choose</option>
                              <option value="City 1">City 1</option>
                              <option value="City 1">City 1</option>
                              <option value="City 1">City 1</option>
                           </select>
                        </div>
                        <div class="col-xs-6 col-sm-3 col-md-3 mb-2">
                           <label class="wpx-50">From</label>
                           <input type="date" id="start_date" name="from" class="form-control">
                        </div>
                        <div class="col-xs-6 col-sm-2 col-md-2 mb-2">
                           <label>Sort By</label>
                           <select class="form-control">
                              <option value="">Choose</option>
                              <option value="Active">Active</option>
                              <option value="Inctive">Inctive</option>
                           </select>
                        </div>
                        <div class="col-xs-6 col-sm-2 col-md-2 mt-2">
                           <div class="card-actions pull-left">
                              <button type="button" data-target="#promocode-modal" data-toggle="modal" class="btn btn-success btn-sm mt-3">Add Promocode</button>
                           </div>
                        </div>
                     </div>
                  </div> -->
                  <div class="card-body">
                     <div class="table-responsive">
                         <table class="table dataTable table-striped table-bordered">
                           <thead>
                              <tr>
                                 <th class="wpx-120">Promo Code</th>
                                 <th class="wpx-120">Promocode Image</th>
                                 <th>Start Date</th>
                                 <th>End Date</th>
                                 <th colspan="2">Limit Discount %</th>
                                 <th>Total Coupons</th>
                                 <th>Service</th>
                                 <th>Status</th>
                                 <th class="no-sort">Action</th>
                              </tr>
                           </thead>
                           <tbody>
                              @foreach($promoCodes as $promoCode)
                              <tr class="data-row">
                                 <input type="hidden" class="promoName"
                                    value="{{$promoCode->promo_name}}">
                                 <td>{{$promoCode->promo_name}}</td>
                                 <td>
                                    <img src="{{$promoCode->coupon_image}}" alt="plus-icon" height="100px" width="100px">
                                 </td>
                                
                                 <td class="startDate">{{$promoCode->start_date}}</td>
                                 <td class="endDate">{{$promoCode->end_date}}</td>
                                 
                                 <td class="promocodeDiscount">{{$promoCode->discount}} %</td>
                                 <td class="amountLimit">{{$promoCode->amount_limit}}</td>
                                 <td>{{$promoCode->coupon_per_user}}</td>
                                 <td>{{$promoCode->apply_promo_code}}</td>
                                 <td class="prdomocodeDiscount">
                                    @if($promoCode->status==1)
                                       <span class="text text-success">Active</span>
                                    @else
                                       <span class="text text-danger">Inactive</span>
                                    @endif
                                 </td>
                                 <td>
                                    <div class="d-flex">
                                       <!-- Delete -->
                                       <button type="button" class="btn btn-danger btn-sm mr-1" data-target="#delete-modal" data-toggle="modal" onClick="deleteCoupon({{$promoCode->id}})">
                                       <i class="fa fa-trash-o"></i>
                                       </button>

                                       <button type="button" class="editButton btn btn-primary btn-sm mr-1" onClick="viewData({{$promoCode->id}})" data-target="#edit-modal" data-toggle="modal">
                                       <i class="fa fa-pencil"></i>
                                       </button>
                                       @if($promoCode->status==1)
                                       <button type="button" class="btn btn-success btn-sm" onClick="blockCoupon({{$promoCode->id}})" data-target="#block-modal" data-toggle="modal">
                                       <i class="fa fa-toggle-off"></i>
                                       @else
                                       <button type="button" class="btn btn-danger btn-sm" onClick="unblockCoupon({{$promoCode->id}})" data-target="#unblock-modal" data-toggle="modal">
                                       <i class="fa fa-toggle-on"></i>
                                       @endif
                                       </button>
                                    </div>
                                 </td>
                              </tr>
                              @endforeach                              
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div> 

<!-- Block Modal -->
<div class="modal fade" id="block-modal">
   <div class="modal-dialog modal-sm">
      <div class="modal-content">
         <div class="modal-body text-center">
            <h2><i class="fa fa-toggle-off text-success"></i></h2>
            <h5>Are you sure you want to block this Promo-Code?</h5>
            <div class="text-center">
               <a data-dismiss="modal" class="btn btn-success btn-sm continueBlock">Yes</a>
               <!-- <a data-dismiss="modal" class="btn btn-danger btn-sm continueUnblock">no</a> -->
               <button type="button" data-dismiss="modal" class="btn btn-danger btn-sm">
               No
               </button>

            </div>
         </div>
      </div>
   </div>
</div>
<!-- Unblock Modal -->
<div class="modal fade" id="unblock-modal">
   <div class="modal-dialog modal-sm">
      <div class="modal-content">
         <div class="modal-body text-center">
            <h2><i class="fa fa-toggle-on text-danger"></i></h2>
            <h5>Are you sure you want to unblock this Promo-Code?</h5>
            <div class="text-center">
               <button type="button" data-dismiss="modal" class="btn btn-success btn-sm continueUnblock">
               Yes
               </button>
               <button type="button" data-dismiss="modal" class="btn btn-danger btn-sm">
               No
               </button>
            </div>
         </div>
      </div>
   </div>
</div>

<!-- _____________  Edit model _________________ -->
<div class="modal fade" id="edit-modal">
   <div class="modal-dialog modal-lg">
      <div class="modal-content p-10-all pt-5">
        <div class="row">
          <div class="col-12">
            <button type="button" data-dismiss="modal" class="close pr-3">×</button>
          </div>
        </div>
         <form class="form-group" id="promocode-edit">
            @csrf
            <input type="hidden" name="editId" id="editid" value="">
            <div class="row">
               <div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                  <div class="mb-3">
                     <label>Start Date</label>
                     <div class="d-flex">
                        <input type="date" name="start_date" id="editstart_date" class="form-control">
                        <span id="edit_error_start_date" class="error" style="color:red"></span>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                  <div class="mb-3">
                     <label>End Date</label>
                     <input type="date" name="end_date" id="editend_date" class="form-control">
                     <span id="edit_error_end_date" class="error" style="color:red;"></span>
                  </div>
               </div>
               <div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                  <div class="mb-3">
                     <label>Promo Code Name</label>
                     <input type="text" name="promo_name" id="editPromo_name" class="form-control">
                     <span id="edit_error_promo_name" class="error" style="color:red;"></span>
                  </div>
               </div>
               <div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                  <div class="mb-3">
                     <label>No. of Coupons per Users</label>
                     <div class="d-flex">
                       <input type="text" name="coupon_per_user" id="editcoupon_per_user" class="form-control">
                       <span id="edit_error_coupon_per_user" class="error" style="color:red;"></span>
                       <button class="fa fa-edit btn btn-danger" arai-hidden='true'></button>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                  <div class="mb-3">
                     <label>(Discount % Off)</label>
                     <input type="text" name="discount" id="editdiscount" class="form-control">
                     <span id="edit_error_discount" class="error" style="color:red;"></span>
                  </div>
               </div>
               <div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                  <div class="mb-3">
                     <label>Maximum Discount Amount (LIMIT)</label>
                     <input type="text" name="discount_limit" id="editdiscount_limit" class="form-control">
                     <span id="edit_error_discount_limit" class="error" style="color:red;"></span>
                  </div>
               </div>
               <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                  <div class="mb-3">
                     <label>Upload Banner Images</label>
                     <div class="d-flex">
                        <input type="file" name="coupon_image" id="editcoupon_image" class="form-control">
                        <button class="btn btn-primary"><i class="fa fa-upload" aria-hidden='true'></i> Upload Image</button>
                     </div>
                     <img id="couponImage" class="whpx-80"/>
                     <span id="edit_error_coupon_image" class="error" style="color:red;"></span>
                  </div>
               </div>
               <div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                  <div class="mb-3">
                     <label>Description</label>
                     <!-- <input type="" name="" id="editDescription"> -->
                     <span id="editDescription"></span>
                    <!--  <textarea class="form-control" id="" name="description" rows="4" id="editDescription"></textarea> -->
                     <span id="edit_error_description" class="error" style="color:red;"></span>
                  </div>
               </div>
               <div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                  <div class="mb-3">
                     <label>PROMOCODE VALID FOR OR</label>
                      <select class="form-control" name="apply_promo_code" id="editapply_promo_code">
                        <option value="all">All</option>
                        @foreach($serviceCategory as $rowService)
                        <option value="{{$rowService->sub_category_name}}">{{$rowService->sub_category_name}}</option>
                        @endforeach
                     </select>
                  </div>
               </div>
               <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                  <div class="text-center">
                <button class="btn btn-primary wpx-180"  type="submit">Submit</button>
                  </div>
               </div>
            </div>
         </form>
      </div>
   </div>
</div>

<!--___________ add Promocode  ___________ -->

<div class="modal fade" id="promocode-modal">
   <div class="modal-dialog modal-lg">
      <div class="modal-content p-10-all">
        <div class="row">
          <div class="col-12">
            <button type="button" data-dismiss="modal" class="close pr-3">×</button>
          </div>
        </div>
        <form class="form-group" enctype="multipart/form-data" id="promocode-add" method="POST" action="{{url('admin/promo-code-management-add')}}">
          @csrf          
            <div class="row">
               <div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                  <div class="mb-3">
                     <label>Start Date</label>
                     <input type="date" name="start_date" id="addStart_date" class="form-control">
                     <span id="Error_add_start_date" class="error" style="color:red"></span>
                  </div>
               </div>
               <div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                  <div class="mb-3">
                     <label>End Date</label>
                     <input type="date" name="end_date" id="add_end_date" class="form-control">
                     <span id="Error_add_end_date" class="error" style="color:red"></span>
                  </div>
               </div>
               <div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                  <div class="mb-3">
                     <label>Promo Code Name</label>
                     <input type="text" name="promo_name" id="add_promo_name" class="form-control">
                     <span id="Error_add_promo_name" class="error" style="color:red"></span>
                  </div>
               </div>
               <div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                  <div class="mb-3">
                     <label>No. of Coupons per Users</label>
                     <input type="number" name="coupon_per_user" id="add_coupon_per_user" class="form-control">
                     <span id="Error_add_end_coupon_per_user" class="error" style="color:red"></span>
                  </div>
               </div>
               <div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                  <div class="mb-3">
                     <label>(Discount % Off)</label>
                     <input type="number" name="discount" id="add_discount" class="form-control">
                     <span id="Error_add_discount" class="error" style="color:red"></span>
                  </div>
               </div>
               <div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                  <div class="mb-3">
                     <label>Maximum Discount Amount (LIMIT)</label>
                     <input type="number" name="discount_limit"  id="add_discount_limit" class="form-control">
                     <span id="Error_add_discount_limit" class="error" style="color:red"></span>
                  </div>
               </div>
               <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                  <div class="mb-3">
                     <label>Upload Banner Images</label>
                     <div class="d-flex">
                        <input type="file" name="coupon_image" id="add_coupon_image" class="form-control">
                        <button class="btn btn-primary submitBtn"><i class="fa fa-upload" aria-hidden='true'></i> Upload Image</button>                        
                     </div>

                     <p id="Error_add_coupon_image" class="error" style="color:red"></p>
                  </div>
               </div>
               <div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                  <div class="mb-3">
                     <label>Description</label>
                     <textarea class="form-control" id="add_description" name="description" rows="4"></textarea>                   
                        <span id="Error_add_description" class="error" style="color:red"></span>
                  </div>
               </div>
               <div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                  <div class="mb-3">
                     <label>PROMOCODE VALID FOR OR</label>
                     <select class="form-control" name="apply_promo_code">
                        <option value="all">All</option>

                        @foreach($serviceCategory as $rowService)
                        <option value="{{$rowService->sub_category_name}}">{{$rowService->sub_category_name}}</option>
                        @endforeach
                     </select>
                  </div>
               </div>
               <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                  <div class="text-center">
                     <button class="btn btn-primary wpx-180"  type="submit">Submit</button>
                  </div>
               </div>
            </div>
         </form>
      </div>
   </div>
</div>

<!-- Delete Modal -->
<div class="modal fade" id="deletemodal">
   <div class="modal-dialog modal-sm">
      <div class="modal-content">
         <div class="modal-body text-center">
            <h2><i class="fa fa-trash-o text-danger"></i></h2>
            <h5 id="modelmessage"></h5>
            <div class="text-center">
               <a id="deleteBtn" data-dismiss="modal" class="btn btn-success btn-sm continueDelete">Yes</a>
               <button type="button" data-dismiss="modal"  class="btn btn-danger btn-sm">
               No
               </button>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="set-taxes">
   <div class="modal-dialog modal-sm">
      <div class="modal-content">
         <div class="modal-header bg-primary">
            <h5 class="modal-title">Set Taxes</h5>
            <button type="button" data-dismiss="modal" class="close">&times;</button>
         </div>
         <div class="modal-body">
            <form>
               <div class="row">
                  <div class="col-md-12">
                     <div class="form-group">
                        <label>Select Province</label>
                        <select class="form-control">
                           <option>choose</option>
                           <option>choose</option>
                           <option>choose</option>
                        </select>
                     </div>
                  </div>
                  <div class="col-md-12">
                     <div class="form-group">
                        <label>Set Tax % on offer amount</label>
                        <select class="form-control">
                           <option>choose</option>
                           <option>choose</option>
                           <option>choose</option>
                        </select>
                     </div>
                  </div>
                  <div class="col-md-12">
                     <div class="form-group">
                        <label>Set Manually</label>
                        <input type="text" class="form-control" name="">
                     </div>
                  </div>
                  <div class="col-md-12">
                     <div class="form-group">
                        <button type="button" data-dismiss="modal" class="btn btn-success btn-sm">
                        Confirm
                        </button>
                     </div>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="set-service">
   <div class="modal-dialog modal-sm">
      <div class="modal-content">
         <div class="modal-header bg-primary">
            <h5 class="modal-title">Set Service Charges</h5>
            <button type="button" data-dismiss="modal" class="close">&times;</button>
         </div>
         <div class="modal-body">
            <form>
               <div class="row">
                  <div class="col-md-12">
                     <div class="form-group">
                        <label>Set Service Charge %</label>
                        <input type="text" class="form-control" name="">
                     </div>
                  </div>
                  <div class="col-md-12">
                     <div class="form-group">
                        <button type="button" data-dismiss="modal" class="btn btn-success btn-sm">
                        Set
                        </button>
                     </div>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="block-success-modal">
   <div class="modal-dialog modal-sm">
      <div class="modal-content">
         <div class="modal-body text-center">
            <h2><i class="fa fa-trash-o text-danger"></i></h2>
            <h5>{{$message = session()->get('success_message')}}</h5>
         </div>
      </div>
   </div>
</div>
@endsection

@section('footerscript')
  @if ($message = session()->has('success_message'))
    <script type="text/javascript">
       $(function() {
          $('#block-success-modal').modal('show');
          setTimeout(function() {$('#block-success-modal').modal('hide');}, 2000);
        });
    </script>
  @endif
  <script type="text/javascript">
    $("#promoCodeFilter").on('submit', function(e){
          $('#StartDateError').hide();
          $('#EndDateError').hide();
          var startDate = $('#startDate').val();
          var endDate = $('#endDate').val();
          var status = $('#status').val();
          if((startDate == '') && (status == '')){
            $('#StartDateError').show();
            e.preventDefault();
          }
          if((startDate) && (endDate == '')){
            $('#EndDateError').show();
            e.preventDefault();
          }
      });
  </script>
  <script type="text/javascript">
        function deleteCoupon(id) {
           $('.continueDelete').click(function(){
              $.ajax({
                 url:'promo-code-management-delete',
                 type:'post',
                 data : { '_token' : $('#token').attr('content'),
                    'id' : id
                 },
                 success:function(responce){
                       location.reload();               
                 }
              })
           });
        }
        // Block promocode
        function blockCoupon(id)
        {
           $(".continueBlock").click(function(){
              $.ajax({
                    url:'promo-code-management-block-user',
              type:'post',
              data : { '_token' : $('#token').attr('content'),
                 'id' : id,
                 'status' : 2
              },
              success:function(responce){ 
               location.reload();              
              }
              });  
           });
        }
        function unblockCoupon(id) {
           $(".continueUnblock").click(function(){
              $.ajax({
                    url:'promo-code-management-block-user',
              type:'post',
              data : { '_token' : $('#token').attr('content'),
                 'id' : id,
                 'status' : 1
              },
              success:function(responce){ 
               location.reload();              
              }
              });  
           });
        }
  </script>
  <script type="text/javascript">
     function viewData(id){               
               var globalarray = <?php echo json_encode($promoCodes); ?>;
               var theIndex = -1;
               for (var i = 0; i < globalarray.length; i++) {
                 if (globalarray[i].id == id) {
                   theIndex = i;
                   break;
                 }
               }               
               $("#editid").val(globalarray[theIndex].id);
               $("#editPromo_name").val(globalarray[theIndex].promo_name);
               $("#editstart_date").val(globalarray[theIndex].start_date);
               $("#editend_date").val(globalarray[theIndex].end_date);
               $("#editcoupon_per_user").val(globalarray[theIndex].coupon_per_user);
               $("#editdiscount").val(globalarray[theIndex].discount);
               $("#editdiscount_limit").val(globalarray[theIndex].amount_limit);
               $("#editapply_promo_code").val(globalarray[theIndex].apply_promo_code);
               $("#editDescription").html('<textarea class="form-control" id="" name="description" rows="4" id="editDescription">'+globalarray[theIndex].description+'</textarea>');
               var default_img="{{ url('public/admin/images/Group-user.jpg')}}";
               if(globalarray[theIndex].coupon_image){
                 console.log(globalarray[theIndex].coupon_image);
                 $('#couponImage').attr('src',globalarray[theIndex].coupon_image);
               } else {
                 $('#couponImage').attr('src',default_img);
               } 
          }
  </script>
  <script type="text/javascript">
        $("#promocode-add").on('submit', function(e){
             var addStart_date=$('#addStart_date').val();
             var add_end_date=$('#add_end_date').val();
             var add_promo_name=$('#add_promo_name').val();
             var add_coupon_per_user=$('#add_coupon_per_user').val();
             var add_discount=$('#add_discount').val();
             var add_discount_limit=$('#add_discount_limit').val()
             var add_coupon_image=$('#add_coupon_image').val()
             var add_description=$('#add_description').val();
             var solveError='';
            if(addStart_date=='')
            {
             $("#Error_add_start_date").html('Add-Date is Require');
                solveError="error";
                e.preventDefault();
            }
            else{
             $("#Error_add_start_date").html('');
            }
            if(add_end_date=='')
            {
             $("#Error_add_end_date").html('End-Date is Require');
                solveError="error";
                e.preventDefault();
            }
            else{
             $("#Error_add_end_date").html('');
            }
            if(add_promo_name=='')
            {
             $("#Error_add_promo_name").html('Promo-Name is Require');
                solveError="error";
                e.preventDefault();
            }
            else{
             $("#Error_add_promo_name").html('');
            }
            if(add_coupon_per_user=='')
            {
             $("#Error_add_end_coupon_per_user").html('Coupon-Per-User is Require');
                solveError="error";
                e.preventDefault();
            }
            else{
             $("#Error_add_end_coupon_per_user").html('');
            }
            if(add_discount=='')
            {
             $("#Error_add_discount").html('Discount is Require');
             solveError="error";
             e.preventDefault();
            }
            else{
             $("#Error_add_discount").html('');
            }
            if(add_discount_limit=='')
            {
             $("#Error_add_discount_limit").html('Discount-Limit is Require');
             solveError="error";
             e.preventDefault();
            }else{
             $("#Error_add_discount_limit").html('');
            }
            if(add_coupon_image=='')
            {
             $("#Error_add_coupon_image").html('Coupon-Image is Require');
             solveError="error";
             e.preventDefault();
            }else{
             $("#Error_add_coupon_image").html('');
            }
            if(add_description=='')
            {
             $("#Error_add_description").html('Description is Require');
             solveError="error";
             e.preventDefault();
            }else{
             $("#Error_add_description").html('');
            }
         });
       
    // edit form 
        $("#promocode-edit").on('submit', function(e){
            e.preventDefault();
            var editid=$('#editid').val();
            var editstart_date=$('#editstart_date').val();
             var editend_date=$('#editend_date').val();
             var editPromo_name=$('#editPromo_name').val();
             var editcoupon_per_user=$('#editcoupon_per_user').val();
             var editdiscount_limit=$('#editdiscount_limit').val();
             var editcoupon_image=$('#editcoupon_image').val()
             var edit_description=$('#edit_description').val();
             var solveError='';
            if(editstart_date=='')
            {
             $("#edit_error_start_date").html('End-Date is Require');
                solveError="error";
            }
            else{
             $("#edit_error_start_date").html('');
            }
            
            if(editend_date=='')
            {
             $("#edit_error_end_date").html('Promo-Name is Require');
                solveError="error";
            }
            else{
             $("#edit_error_end_date").html('');
            }
            
            if(editPromo_name=='')
            {
             $("#edit_error_promo_name").html('Coupon-Per-User is Require');
                solveError="error";
            }
            else{
             $("#edit_error_promo_name").html('');
            }
            
            if(editcoupon_per_user=='')
            {
             $("#edit_error_coupon_per_user").html('Discount is Require');
             solveError="error";
            }
            else{
             $("#edit_error_coupon_per_user").html('');
            }

            if(editdiscount_limit=='')
            {
             $("#edit_error_discount_limit").html('Discount-Limit is Require');
             solveError="error";
            }else{
             $("#edit_error_discount_limit").html('');
            }
           
            if(edit_description=='')
            {
             $("#edit_error_description").html('Description is Require');
             solveError="error";
            }else{
             $("#edit_error_description").html('');
            }

            if(solveError=='')
            {
              $.ajax({
                 url: 'promo-code-management-edit',
                 type: 'POST',
                 data: new FormData(this),
                 dataType: 'json',
                 contentType: false,
                 cache: false,
                 processData:false,
                 beforeSend: function(){
                     $('.submitBtn').attr("disabled","disabled");
                     $('#promocode-add').css("opacity",".5");
                 },
                 success: function(response){ 
                 // console.log(response);
                     //$('.statusMsg').html('');
                     if(response.status == 1){
                         $('#promocode-add')[0].reset();
                         $('#delete-modal').modal('show');
                         location.reload();  

                         // $('.statusMsg').html('<p class="alert alert-success">'+response.message+'</p>');
                     }else{
                         // $('.statusMsg').html('<p class="alert alert-danger">'+response.message+'</p>');
                     }
                     $('#promocode-add').css("opacity","");
                     $(".submitBtn").removeAttr("disabled");
                 }                   
             });
            }        
        });
   
    </script>
@endsection