  @if(ucwords($url) == 'Seller')                     
        <!doctype html>
        <html lang="en">
        <head>
          <meta charset="utf-8">
          <title>Myles | Sign In</title>
          <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
          <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui" />
          <link rel="icon" href="{{url('public/seller/images/favicon.png')}}" type="image/x-icon" />

          <link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600,700,800&display=swap" rel="stylesheet">
          <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css" rel="stylesheet"/>
          <link rel="stylesheet" type="text/css" href="{{url('public/seller/css/bootstrap.min.css/')}}" />
          <link rel="stylesheet" type="text/css" href="{{url('public/seller/css/font-awesome.min.css')}}" />
          <link rel="stylesheet" type="text/css" href="{{url('public/seller/css/iEdit.css')}}" />
          <link rel="stylesheet" type="text/css" href="{{url('public/seller/css/jquery.dataTables.min.css')}}" />
          <link rel="stylesheet" type="text/css" href="{{url('public/seller/icon/themify-icons/themify-icons.css')}}" />
          <link rel="stylesheet" type="text/css" href="{{url('public/seller/icon/icofont/css/icofont.css')}}" />
          <link rel="stylesheet" type="text/css" href="{{url('public/seller/css/intlTelInput.css')}}" />
          <link rel="stylesheet" type="text/css" href="{{url('public/seller/css/owl.carousel.css')}}" /> 
          <link rel="stylesheet" type="text/css" href="{{url('public/seller/css/component.css')}}" />
          <link rel="stylesheet" type="text/css" href="{{url('public/seller/css/style.css')}}" />  
          <link rel="stylesheet" type="text/css" href="{{url('public/seller/css/globle.css')}}" />
        </head>
        <body>
        <div class="theme-loader">
          <div class="ball-scale">
            <div class='contain'>
              <div class="ring"><div class="frame"></div></div>
              <div class="ring"><div class="frame"></div></div>
              <div class="ring"><div class="frame"></div></div>
              <div class="ring"><div class="frame"></div></div>
              <div class="ring"><div class="frame"></div></div>
              <div class="ring"><div class="frame"></div></div>
              <div class="ring"><div class="frame"></div></div>
              <div class="ring"><div class="frame"></div></div>
              <div class="ring"><div class="frame"></div></div>
              <div class="ring"><div class="frame"></div></div>
            </div>
          </div>
        </div>
        <div id="pcoded" class="pcoded iscollapsed expanded">
          <div class="pcoded-overlay-box"></div>
          <div class="pcoded-container navbar-wrapper">
            <section class="login">
          <div class="container">
            <div class="row">
              <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <a href="{{url('seller/dashboard')}}">
                  <img src="{{url('public/seller/images/logo.png')}}"/>
                </a>
              </div>
            </div>
            <div class="row">
              <div class="col-12 col-sm-12 col-md-6 col-lg-7 col-xl-7 full-hieght login-left">
                <div class="login-card">
                  <h1>Become a <span class="text-primary">Pania</span> partner</h1>
                  <h6>Sign in or create account to start selling</h6>
                  <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam,</p>
                </div>
              </div>
              <div class="col-12 col-sm-12 col-md-6 col-lg-5 col-xl-5 full-hieght login-right">
                <div class="login-card">
                  <div class="m-b-30">
                    <h4>Get started!</h4>
                    <p>Don't have an account?
                      @if(ucwords($url) == 'Seller')                               
                          <a class="text-primary" href="{{ url('seller/signup')}}"> Sign Up</a></h6>                  
                      @endif
                     </p>
                  </div>
                  @if (session('error'))
                      <span style="color: red"> {{ session('error') }}</span>
                  @endif
                  @isset($url)
                    <form class="w-100 form-control-bottom" method="POST" action='{{ url("login/$url") }}' aria-label="{{ __('Login') }}">
                  @else
                    <form class="w-100 form-control-bottom" method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
                  @endisset
                  @csrf
                    <div class="m-t-10">
                      <div class="form-group">
                        <label>Email Address</label>
                        <input id="email" type="text" class="form-control email {{ $errors->has('emailAddress') ? ' is-invalid' : '' }}" name="emailAddress" value="{{ old('emailAddress') }}" autofocus>
                        <span style="color: red">{{ $errors->first('emailAddress')}}</span>
                      </div>
                      <div class="form-group" style="margin-bottom: 5px">
                        <label>Enter Password</label>
                        <input id="password" type="password" class="form-control password {{ $errors->has('password') ? ' is-invalid' : '' }}" name="password">
                        <span class="showHidePass"></span>
                      </div>                       
                      <span style="color: red">{{ $errors->first('password')}}</span>
                    </div>
                    <div class="m-t-15">              
                      <div class="forgot-phone">
                        @if(ucwords($url) == 'Seller')                     
                            <a class="text-right f-w-600 text-inverse" href="{{ url('seller/forgot-password') }}">                              {{ __('Forgot Password?') }}
                            </a>  
                        @elseif(ucwords($url) == 'Admin')                  
                            <a class="text-right f-w-600 text-inverse" href="{{ url('admin/forgot-password') }}">                               {{ __('Forgot Your Password?') }}
                            </a> 
                        @endif                 
                      </div>
                    </div>
                    <div class="m-t-25">
                       <button type="submit" class="btn btn-primary btn-md btn-block">SIGN IN</button>             
                    </div>
                  </form> 

                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
      </div>
      <div class="modal fade" id="success-profile-modal">
        <div class="modal-dialog modal-sm">
          <div class="modal-content" routerLink="/buy-subscription" data-dismiss="modal">
            <div class="modal-body pt-4 text-center">
              <img src="{{url('public/seller/images/confirm.png')}}" class="mb-3"/>
              <h6>{{session()->get('success_profile')}}</h6>
              <h6 class="text-primary mt-3">Welcome To Pania!</h6>
            </div>
          </div>
        </div>
      </div>

      <script type="text/javascript">
          window.history.forward();
          function noBack()
          {
              window.history.forward();
          }
      </script>

      <script type="text/javascript" src="{{url('public/seller/js/jquery.min.js')}}"></script>
      <script type="text/javascript" src="{{url('public/seller/js/jquery-ui.min.js')}}"></script>
      <script type="text/javascript" src="{{url('public/seller/js/popper.min.js')}}"></script>
      <script type="text/javascript" src="{{url('public/seller/js/bootstrap.min.js')}}"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>

      <script type="text/javascript" src="{{url('public/seller/s/owl.carousel.min.js')}}"></script> 
      <script type="text/javascript" src="{{url('public/seller/js/iEdit.js')}}"></script> 
      <script type="text/javascript" src="{{url('public/seller/js/jquery.dataTables.min.js')}}"></script> 
      <script type="text/javascript" src="{{url('public/seller/js/Chart.min.js')}}"></script> 
      <script type="text/javascript" src="https://www.chartjs.org/samples/latest/utils.js"></script> 
      <script type="text/javascript" src="{{url('public/seller/js/common-pages.js')}}"></script> 

      @if ($message = session()->has('success_profile'))
         <script type="text/javascript">
           $(function() {
              $('#success-profile-modal').modal('show');
              setTimeout(function() {$('#success-profile-modal').modal('hide');}, 10000);
            });
         </script>
      @endif         
      </body>
      </html>

  @elseif(ucwords($url) == 'Admin')      

     <!doctype html>
     <html lang="en">
     <head>
        <meta charset="utf-8">
        <title>Myles | Admin</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    
        <link rel="mask-icon" href="safari-pinned-tab.svg" color="#6B52AB">
        <meta name="theme-color" content="#ffffff">
        <link rel="icon" href="{{url('public/admin/img/favicon.png')}}" type="image/png" sizes="16x16">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,400italic,500,700">
        <link rel="stylesheet" href="{{url('public/admin/css/vendor.min.css')}}">
        <link rel="stylesheet" href="{{url('public/admin/css/elephant.min.css')}}">
        <link rel="stylesheet" href="{{url('public/admin/css/application.min.css')}}">
        <link rel="stylesheet" href="{{url('public/admin/css/login-2.min.css')}}">
         
     </head>
     <body onLoad="noBack();" onpageshow="if (event.persisted) noBack();" onUnload="">
     <div class="login">
      <div class="login-body">
        <a class="login-brand" href="#">
          <img class="img-responsive" src="{{url('public/admin/img/logo.png')}}" alt="logo">
        </a>
        <div class="login-form">
          <h5>Sign In</h5>
            @if (Session::has('error'))            
               <span style="color: red">{{ Session::get('error') }}</span>
            @endif
            @isset($url)
              <form method="POST" data-toggle="validator" action='{{ url("login/$url") }}' aria-label="{{ __('Login') }}">
            @else
              <form method="POST" data-toggle="validator" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
            @endisset
            @csrf 
              <div class="form-group">
                <label for="email">Email</label>
                <input id="email" class="form-control email {{ $errors->has('emailAddress') ? ' is-invalid' : '' }}" type="email" name="emailAddress" spellcheck="false" autocomplete="off" value="{{ old('emailAddress') }}" data-msg-required="Please enter your email address." required>
              
            </div>
            @if($errors->first('emailAddress'))
                 <p style="color: red; text-align: left; margin-top: -7px;
margin-bottom: 9px;">{{ $errors->first('emailAddress') }}</p>
              @endif

            <div class="form-group">
                <label for="password">Password</label>
                <input id="password" type="password" class="form-control password {{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" minlength="6" data-msg-minlength="Password must be 6 characters or more." data-msg-required="Please enter your password." required>              
            </div>    
            @if($errors->first('password'))
               <p style="color: red; text-align: left; margin-top: -7px;
margin-bottom: 9px;">{{ $errors->first('password') }}</p>
            @endif
            <div class="form-group">
              <label class="custom-control custom-control-primary custom-checkbox">
                <input class="custom-control-input" type="checkbox" id="rememberMe" {{ old('remember') ? 'checked' : '' }}>
                <span class="custom-control-indicator"></span>
                <span class="custom-control-label">Remember Me</span>
              </label>
              <a href="{{ url('admin/forgot-password') }}" class="pull-right">Forgot password?</a>
            </div>
            <button class="btn btn-primary btn-block" type="submit">Sign in</button>              
          </form>
        </div>
      </div>
    </div>

    <script type="text/javascript">
        window.history.forward();
        function noBack()
        {
            window.history.forward();
        }
    </script>
    <script type="text/javascript" src="{{url('public/admin/js/jquery.min.js')}}"></script>
    <script src="{{url('public/admin/js/vendor.min.js')}}"></script>
    <script src="{{url('public/admin/js/elephant.min.js')}}"></script>
    <script src="{{url('public/admin/js/application.min.js')}}"></script>
    </body>
    </html>      
  @endif  
  <div class="modal fade" id="success-modal">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
          <div class="modal-body text-center">
            <h2><i class="fa fa-link text-danger"></i></h2>
            <h5>{{ session('success_message') }}</h5>
          </div>
        </div>
      </div>
    </div>
    
  @if ($message = session()->has('success_messages'))
    <script type="text/javascript">
       $(function() {
          $('#success-modalsds').modal('show');
          setTimeout(function() {$('#success-modalsds').modal('hide');}, 2000);
        });
    </script>
  @endif

  @if ($message = session()->has('success_message'))
    <script type="text/javascript">
       $(function() {
          $('#success-modal').modal('show');
          setTimeout(function() {$('#success-modal').modal('hide');}, 2000);
        });
    </script>
  @endif