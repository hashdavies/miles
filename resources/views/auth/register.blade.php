    @extends('layouts.registerheader')
    @section('content')
    <section class="section-self register " style="background-image:url({{ asset('public/frontend/assets/img/login/bg.png') }}); background-position:top center; background-size:cover;">
      <div class="testimonial_inner">
        <div class="container">
          <div class="row">
            <div class="col-lg-7 col-md-7"></div>
            <div class="col-lg-4 col-md-5">
              <div class="account_form">
                <form method="POST" action="{{ route('register') }}" aria-label="{{ __('Register') }}">
                @csrf
                  <h2 class="text-center">Create an Account</h2>
                  <div class="row mt-4">
                    <div class="col-lg-12 mb-20">
                      <label>{{ __('Username') }}<span>*</span></label>
                      <div class="input-container">
                        <i class="fa fa-user icon"></i>

                        <input id="name" type="text" class="{{ $errors->has('username') ? ' is-invalid' : '' }}" name="username" value="{{ old('username') }}" required autofocus placeholder="Enter your name">             
                        
                      </div>
                      <span style="color: red" role="alert"> {{ $errors->first('username') }} </span>
                    </div>

                    <div class="col-lg-12 mb-20">
                      <label>E-mail Address/Mobile Number <span>*</span></label>
                      <div class="input-container">
                        <i class="fa fa-envelope-o icon"></i>

                        <input id="email" type="text" class="{{ $errors->has('email_number') ? ' is-invalid' : '' }}" name="email_number" value="{{ old('email_number') }}" placeholder="Enter emaid id/mobile number" required>                                       
                      </div>
                      <span style="color: red" role="alert"> {{ $errors->first('email_number') }} </span>
                    </div>

                    <div class="col-12 mb-20">
                      <label>Password</label>
                      <div class="input-container">
                        <i class="fa fa-lock icon"></i>
                        <input id="password" type="password" class="{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Enter password" required> 
                        <button type="button" class="my-eye" onclick="myFunction()"><i class="fa fa-eye"></i></button>
                      </div>
                      <span style="color: red" role="alert"> {{ $errors->first('password') }} </span>
                    </div>

                    <div class="col-12 mb-20">
                      <label for="country">Country<span>*</span></label>
                      <div class="input-container">
                        <i class="fa fa-map-marker icon"></i>
                        <select class="niceselect_option" name="country" id="country" class="{{ $errors->has('country') ? ' is-invalid' : '' }}">
                          <option value="bangladesh">bangladesh</option>
                          <option value="Algeria">Algeria</option>
                          <option value="Afghanistan">Afghanistan</option>
                          <option value="Ghana">Ghana</option>
                          <option value="Albania">Albania</option>
                          <option value="Bahrain">Bahrain</option>
                          <option value="Colombia">Colombia</option>
                          <option value="Dominican_Republic">Dominican Republic</option>
                        </select> 
                      </div>
                      <span style="color: red" role="alert"> {{ $errors->first('country') }} </span>
                    </div>

                    <div class="col-md-12 login_submit mt-3">
                        <button type="submit d-block"> {{ __('Register') }} </button>                                 
                    </div>
                  </div>
                </form> 
              </div>
            </div>
            <div class="col-md-1"></div>
          </div>
        </div>
      </div>
    </section>
    @endsection

    @section('footer_script')
    <script src="{{ asset('frontend/assets/js/vendor/jquery-1.12.4.min.js') }}"></script>
    <script src="{{ asset('frontend/assets/js/popper.min.js') }}"></script>
    <script src="{{ asset('frontend/assets/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('frontend/assets/js/plugins.js') }}"></script>
    <script src="{{ asset('frontend/assets/js/ajax-mail.js') }}"></script>
    <script src="{{ asset('frontend/assets/js/main.js') }}"></script>
    @endsection