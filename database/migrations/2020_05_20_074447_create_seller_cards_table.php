<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSellerCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seller_cards', function (Blueprint $table) {
            $table->increments('id');
            $table->string('seller_Id');
            $table->string('bankName');
            $table->string('cardNumber');
            $table->string('cardName');
            $table->string('cardExpire');
            $table->string('cardCvv');            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seller_cards');
    }
}
