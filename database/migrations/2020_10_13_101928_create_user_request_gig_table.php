<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserRequestGigTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_request_gig', function (Blueprint $table) {
            $table->increments('id');
            $table->string('User_ID');
            $table->string('Seller_ID');
            $table->string('gigofferDate');
            $table->string('gigofferTime');
            $table->string('subTotal');
            $table->string('offerAmount');
            $table->string('registerDate');
            $table->string('bookingDate');
            $table->string('pickupStatus');
            $table->string('inTransitStatus');
            $table->string('gig_id');
            $table->string('gigCompleteDate');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_request_gig');
    }
}
