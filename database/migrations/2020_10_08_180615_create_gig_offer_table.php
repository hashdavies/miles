<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGigOfferTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gig_offer', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_id');
            $table->string('pickupLocation');
            $table->string('dropLocation');
            $table->string('items');
            $table->string('serviceType');
            $table->string('tripType');
            $table->string('estimatedWeight');
            $table->string('serviceDescription');
            $table->string('additionalNotes');
            $table->string('offerAmount');
            $table->string('offerDate');
            $table->string('status');
            $table->string('gigstatus');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gig_offer');
    }
}
