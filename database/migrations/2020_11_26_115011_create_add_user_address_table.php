<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddUserAddressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('add_user_address', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_id');
            $table->string('address');
            $table->string('user_latitude');
            $table->string('user_longitude');
            $table->string('province');
            $table->string('saveAs');
            $table->string('city');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('add_user_address');
    }
}
