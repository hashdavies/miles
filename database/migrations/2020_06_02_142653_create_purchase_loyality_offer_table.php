<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseLoyalityOfferTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_loyality_offer', function (Blueprint $table) {
            $table->increments('id');
            $table->string('Pur_Seller_ID');
            $table->string('Pur_User_ID');
            $table->string('Pur_loyaltyOfrName');
            $table->string('Pur_loyaltyStartDate');
            $table->string('Pur_LoyalityAddonDate');
            $table->string('Pur_loyaltyEndDate');
            $table->string('Pur_loyaltyMinimumAmount');
            $table->string('Pur_loyaltyRewardType');
            $table->text('Pur_loyaltyBenefits');
            $table->string('Pur_loyaltyVisit');
            $table->string('Pur_loyaltyPoint');
            $table->string('Pur_loyaltyPointReedem');
            $table->string('Pur_loyaltyDiscountPrice');
            $table->string('Pur_loyaltyBanner');
            $table->string('Pur_loyaltyVerify');            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_loyality_offer');
    }
}
