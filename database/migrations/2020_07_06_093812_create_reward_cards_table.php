<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRewardCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reward_cards', function (Blueprint $table) {
            $table->increments('id');
            $table->string('reward_name');
            $table->string('reward_image');
            $table->string('reward_card_number');
            $table->string('reward_card_icon');
            $table->string('reward_description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reward_cards');
    }
}
