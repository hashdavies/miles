<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOfferSubscriptionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offer_subscription', function (Blueprint $table) {
            $table->increments('id');           
            $table->string('ofr_name');
            $table->string('ofr_fee');
            $table->string('ofr_description');
            $table->string('ofr_image');
            $table->string('ofr_valid_time');
            $table->string('ofr_valid_month');
            $table->string('ofr_allowed');
            $table->string('ofr_upgrade_status');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offer_subscription');
    }
}
