<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoyalityPointTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loyality_point', function (Blueprint $table) {
            $table->increments('id');
            $table->string('Seller_ID');
            $table->string('Loyality_OfferID');
            $table->string('User_iD');
            $table->string('Loyality_Point');
            $table->string('Generate_Type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loyality_point');
    }
}
