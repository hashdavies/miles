<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');  
            $table->string('firstName')->nullable();
            $table->string('lastName')->nullable();
            $table->string('nickName')->nullable();
            $table->string('emailAddress')->nullable();
            $table->string('countryCode')->nullable();
            $table->string('mobileNo')->nullable();
            $table->string('password')->nullable();            
            $table->string('is_verified')->default(0)->nullable();
            $table->string('is_profile')->default(0)->nullable();
            $table->string('status')->default(0)->nullable();           
            $table->string('access_token')->nullable(); 
            $table->string('remember_token')->nullable();                
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
