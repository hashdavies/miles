<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoryServiceAddressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category_service_address', function (Blueprint $table) {
            
            $table->increments('id');
            $table->string('user_id');
            $table->string('cat_id');
            $table->string('cityName');
            $table->string('gig_name');
            $table->string('delivery_address');
            $table->string('estimated_weightx');
            $table->string('additional_services');
            $table->string('additionaldata');
            $table->string('pickUpLongtitude');
            $table->string('pickUpLatitude');
            $table->string('pickUpAddressType');
            $table->string('dropAddress');
            $table->string('dropLongtitude');
            $table->string('dropLatitude');
            $table->string('pickupAddressOtherName');
            $table->string('dropAddressOtherName');
            $table->string('dropAddressType');
            $table->string('proviance');
            $table->string('trip_type');
            $table->string('pickupDate');
            $table->string('pickupTime');
            $table->string('timezone');
            $table->string('budget');
            // $table->string('additionaldata');
            $table->string('returnTime');
            $table->string('returnDate');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('category_service_address');
    }
}
