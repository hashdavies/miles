<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admins', function (Blueprint $table) {
            $table->increments('id');             
            $table->string('email_number')->nullable();
            $table->string('password')->nullable();
            $table->string('country')->nullable();
            $table->string('city')->nullable();
            $table->string('profile_image')->nullable();            
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();            
            $table->string('store_name')->nullable();          
            $table->string('tax_reg_num')->nullable();
            $table->string('tax_reg_date')->nullable();
            $table->string('bank_acc_num')->nullable();
            $table->string('bank_code')->nullable();
            $table->string('acc_holder_name')->nullable();
            $table->string('access_token')->nullable();
            $table->string('is_approved')->default(0)->nullable();
            $table->string('is_verified')->default(0)->nullable();
            $table->string('is_profile')->default(0)->nullable();           
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admins');
    }
}
