<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGigOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gig_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_id');
            $table->string('seller_id');
            $table->string('gig_id');
            $table->string('payment_mode');
            $table->string('subtotal_price');
            $table->string('coupon_discount');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gig_order');
    }
}
