<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTripStopTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_trip_stop', function (Blueprint $table) {
            $table->increments('id');
            $table->string('service_category_address_id');
            $table->string('tripStopAddress');
            $table->string('tripstopLatitude');
            $table->string('tripstopLongtitude');
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_trip_stop');
    }
}
