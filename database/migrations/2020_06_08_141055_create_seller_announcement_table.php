<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSellerAnnouncementTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seller_announcement', function (Blueprint $table) {
            $table->increments('id');
            $table->string('announcement_1');
            $table->string('announcement_2');
            $table->string('announcement_3');
            $table->string('announcement_4');
            $table->string('announcement_5');
            $table->string('announcement_6');
            $table->string('announcement_7');
            $table->string('announcement_8');
            $table->string('announcement_9');
            $table->string('announcement_10');
            $table->string('DateTime');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seller_announcement');
    }
}
