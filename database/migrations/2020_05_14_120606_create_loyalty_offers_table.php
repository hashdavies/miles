<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoyaltyOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loyalty_offers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('loyaltyOfrName');
            $table->string('loyaltyStartDate');
            $table->string('loyaltyEndDate');
            $table->string('LoyalityAddonDate');
            $table->string('loyaltyMinimumAmount');
            $table->string('loyaltyRewardType');
            $table->string('loyaltyBenefits');
            $table->string('loyaltyVisit');
            $table->string('loyaltyPoint');
            $table->string('loyaltyPointReedem');
            $table->string('loyaltyDiscountPrice');
            $table->string('loyaltyBanner');
            $table->string('loyaltyVerify');
            $table->string('status');                        
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loyalty_offers');
    }
}
