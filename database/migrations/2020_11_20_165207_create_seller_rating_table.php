<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSellerRatingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seller_rating', function (Blueprint $table) {
            $table->increments('id');
            $table->string('seller_user_id');
            $table->string('seller_seller_id');
            $table->string('seller_rating');
            $table->string('seller_review');
            $table->string('seller_gigId');
            $table->string('seller_registerDate');
            $table->string('seller_registerTime');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seller_rating');
    }
}
