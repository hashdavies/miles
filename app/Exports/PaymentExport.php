<?php
namespace App\Exports;

use App\Models\UserPackage;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class PaymentExport implements FromCollection, WithHeadings,ShouldAutoSize,WithEvents
{
  public function collection()
  {
    return UserPackage::select('invoice_id','packageName','token','payerID','ack','paidamt','created_at','billingDate')->get();
  }

  public function headings(): array
    {
        return [
           'Invoice Id',
           'Package Name',
           'Token',
           'Payer ID',
           'ACK',
           'Paid Amt',
           'Date / Time',
           'Billing Date',
        ];
    }


    public function registerEvents(): array
    {
    

        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange = 'A1:W1'; // All headers
                $styleArray = [
				    'borders' => [
				        'outline' => [
				            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
				            'color' => ['argb' => '000'],
				        ],
				    ],
				    'font' => array(
                            'name'      =>  'Calibri',
                            'size'      =>  15,
                            'bold'      =>  true,
                            'color' => ['argb' => 'EB2B02'],
                        )

				];

                $event->sheet->getDelegate()->getStyle($cellRange)->applyFromArray($styleArray)->getFont()->setSize(12);
            

            },
        ];
    }


}