<?php
namespace App\Exports;

use App\Seller\Seller;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class ServiceProviderExport implements FromCollection, WithHeadings,ShouldAutoSize,WithEvents
{
  public function collection()
  {
    return Seller::select('businessName','firstName','lastName','nickName', 'email', 'countryCode', 'mobileNo', 'serviceType', 'dLNumber', 'rejectReason', 'registerDate', 'serviceProviderType', 'transitNumber', 'institutionNumber', 'accountNumber', 'city', 'province', 'address', 'address_latitude', 'address_longitude')->get();
  }

  public function headings(): array
    {
        return [
            'Business Name',
            'First Name',
            'Last Name',
            'Nick Name',
            'Email',
            'Country Code',            
            'Mobile Number',
            'Service Type',
            'DL Number',
            'Reject Reason',
            'Joining Date',
            'Service Provider Type',
            'Transit Number',
            'Institution Number',
            'Account Number',
            'City',
            'Province',
            'Address',            
            'Latitude',
            'Longitude',
        ];
    }


    public function registerEvents(): array
    {
    

        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange = 'A1:W1'; // All headers
                $styleArray = [
				    'borders' => [
				        'outline' => [
				            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
				            'color' => ['argb' => '000'],
				        ],
				    ],
				    'font' => array(
                            'name'      =>  'Calibri',
                            'size'      =>  15,
                            'bold'      =>  true,
                            'color' => ['argb' => 'EB2B02'],
                        )

				];

                $event->sheet->getDelegate()->getStyle($cellRange)->applyFromArray($styleArray)->getFont()->setSize(12);
            

            },
        ];
    }


}