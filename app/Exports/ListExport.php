<?php
namespace App\Exports;

use App\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class ListExport implements FromCollection, WithHeadings,ShouldAutoSize,WithEvents
{
  public function collection()
  {
    return User::select('firstName','lastName','nickName', 'emailAddress', 'countryCode', 'mobileNo', 'province', 'city', 'address', 'registerDate', 'address_latitude', 'address_longitude')->get();
  }

  public function headings(): array
    {
        return [
            'First Name',
            'Last Name',
            'Nick Name',
            'Email',
            'Country Code',
            'Mobile Number',
            'Province',
            'City',
            'Address',
            'Joining Date',
            'Latitude',
            'Longitude',
        ];
    }


    public function registerEvents(): array
    {
    

        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange = 'A1:W1'; // All headers
                $styleArray = [
				    'borders' => [
				        'outline' => [
				            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
				            'color' => ['argb' => '000'],
				        ],
				    ],
				    'font' => array(
                            'name'      =>  'Calibri',
                            'size'      =>  15,
                            'bold'      =>  true,
                            'color' => ['argb' => 'EB2B02'],
                        )

				];

                $event->sheet->getDelegate()->getStyle($cellRange)->applyFromArray($styleArray)->getFont()->setSize(12);
            

            },
        ];
    }


}