<?php

namespace App\Admin;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model; 
use Illuminate\Foundation\Auth\User as Authenticatable;
 

 
    class Admin extends Authenticatable
    {
        use Notifiable;

        protected $guard = 'admin';


        protected $fillable = [
            'first_name', 'emailAddress', 'password',
        ];


        protected $hidden = [
            'password', 'remember_token',
        ];


        protected $casts = [
        'email_verified_at' => 'datetime',
         ];
    }
 
