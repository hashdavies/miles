<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\DB;
use App\Model\PromoCode;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {
        $timp =date('Y-m-d');
        
        //dd($time_stamp);

            $data =PromoCode::where('end_date','<',$timp)->where('status','=','1')->get();
            if(count($data)>0){
                foreach ($data as $key => $value) {

                    if($value->to<$timp){
                        $offerData         =  PromoCode::find($value->id);
                        $offerData->status =  2;
                        $offerData->save();
                    }
                }
            }
          })->everyMinute();

        $schedule->command('demo:cron')
                 ->everyMinute();
    }


    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
