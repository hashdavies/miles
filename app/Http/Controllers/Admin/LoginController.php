<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Admin\Admin;
//use Auth;
use Illuminate\Support\Facades\Auth;
use DB;
use Validator;
use Mail;

class LoginController extends Controller
{
    public function forgetPassword()
    {
      return view('admin/forgot-password');
    }   

    //Send Forgot Password Link 
    public function sendforgetlink(Request $request)
    {
    	$this->validate($request, [
            'email'   => 'required|email'           
        ],[
            'email.required' => 'Email id is required',
            'email.email' => 'Invalid email id'
        ]);

        $admin = Admin::where('emailAddress',$request->email)->first();
        
        if(empty($admin)){
        return redirect()->back()
                         ->withInput($request->only('email'))
                         ->with(['email' => 'Incorrect Email Address']);
        }else{
            $token = str_random(32);          
            $url = url('/');
            $link = "<a href=".$url.'/admin/resetpassword?token='.$token.">Verify Link</a>";

            // $data1=array();         
            // $data1['email']= $request->email;
            // $data1['name']='test';
            // $data1['otp'] = $link;

            // $from = array();          
            // $from['email_to']= $request->email;
            // $from['name']='Verify Account'; 

            // Mail::send('emails.otp-mail', $data1, function ($message) use ($from) {
            // $message->to($from['email_to']);
            // $message->subject('Verify Account: OTP');
            // }); 

            $email = new \SendGrid\Mail\Mail(); 
            $email->setFrom("kuldeep.tomar@fluper.in", "Myles");
            $email->setSubject("Myles Admin Forgot Password");
            $email->addTo("$request->email", "Myles");
            $email->addContent(
                "text/html", "<strong>$link</strong>"
            );
            $sendgrid = new \SendGrid('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');
            try {
                $response = $sendgrid->send($email);
                $remove = DB::table('password_resets')->where('email',$request->email)->delete();
                $addToken = DB::table('password_resets')->insert(['email'=>$request->email,'token'=>$token]);
                return redirect()->back()->with(['success_message' => 'Password Link Send To Your Email Id']);
            } catch (Exception $e) {
                  return redirect()->back()->with(['error_message' => 'Password Link Not Send Try Again']);
            }
        }
    }

    
    //Show Reset Password Link
    public function showResetPassword(Request $request)
    {   
        $selectpwd = DB::table('password_resets')->where('token',$request->token)->first();
        if(empty($selectpwd)){
            return redirect('admin/forgot-password')->with(['error_message' => 'Invalid Token']);
        }else{
           return view('admin/reset-password')->with(['email'=>$selectpwd->email]);
        }

    }

    //Update Reset Password 
    public function updateResetPassword(Request $request)
    {
        $this->validate($request, [
            'email'=>'required',
            'password'=>'required|min:8|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/',
            'confirm_password'=>'required|same:password'
        ],[
            'password.required' => 'New Password is required',
            'password.regex' => 'Password should contain 8-15 characters (like : Admin@123)',
            'confirm_password.required' => 'New confirm password is required',
            'confirm_password.same' => 'Password does not match',
        ]);
     
        Admin::where('emailAddress',$request->email)->update(['password'=>bcrypt($request->password)]);
       
        $remove = DB::table('password_resets')->where('email',$request->email)->delete();
        return redirect('login/admin')->with(['success_message' => 'Password Reset Successfully']);
    }   


    public function __construct()
    {
        $this->middleware('guest:admin')->except('logout');
    }

    protected function guard()
    {
        return Auth::guard('admin');
    }

    public function logout(Request $request)
    {    	
        Auth::guard('admin')->logout();        
        $request->session()->invalidate();
        return redirect('login/admin');
    }

    

}
