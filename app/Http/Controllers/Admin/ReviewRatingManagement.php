<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\UserRating;
use App\Model\SellerRating;
use App\User;

class ReviewRatingManagement extends Controller
{
    
    public function ReviewRatingsManagement(){
    
    	$RatingReview = UserRating::orderBy('id', 'Desc')->get();
    	foreach($RatingReview as $key=>$rowrating){
    		// dd($rowrating->Seller_Id);
    		$RatingReview[$key]->sellerData = SellerRating::where('seller_seller_id', $rowrating->Seller_Id)->where('seller_user_id', $rowrating->User_Id)->first();
    	}        
        // dd($RatingReview);
    	$province = User::select('province')->groupBy('province')->get();
    	return view('admin.review-ratings-management')->with('RatingReview', $RatingReview)->with('province', $province);
    }


    public function RatingReview(Request $request){
		// dd($request->all());

    	$start_date = $request->from;
        $end_date = $request->to;
        $province = $request->province;
        $city = $request->city;
        $rating = $request->sort;
        
        $Startdate = strtr($start_date, '-', '/');
        $startEditdate =  date('Y-m-d', strtotime($Startdate));
               
        $Enddate = strtr($end_date, '-', '/');
        $endEditdate =  date('Y-m-d', strtotime($Enddate));
        
        // dd($startEditdate);
        
        $RatingReview = UserRating::select('user_rating.id', 'user_rating.User_Id', 'user_rating.rating', 'user_rating.ratingRegisterDate', 'users.province', 'users.city')->orderBy('id','Desc');        
        
        if($start_date != ''){
           $RatingReview->whereBetween('ratingRegisterDate', [$startEditdate, $endEditdate]);
        }

        if($province != ''){
           $RatingReview->where('province', $province);
        }

        if($city != ''){
            $RatingReview->where('city',$city);
        } 

        if($rating != ''){
            if($rating == 'desc'){
                $RatingReview->orderBy('id','Desc');
            }elseif($rating == '1'){               
                $RatingReview->where('rating','1')->orWhere('rating', '2');
            }elseif($rating == '3'){
                $RatingReview->where('rating','3')->orWhere('rating', '4');
            }elseif($rating == '5'){
                $RatingReview->where('rating',$rating);
            }            
        }
        // dd($RatingReview);
        // $userData = $userData->get();       
        // foreach($userData as $key=>$rowData){
        //     $userData[$key]->UserDetails = $rowData->RatingUserName->firstName;

        //      
        // }        
        $RatingReview = $RatingReview
        ->leftJoin('users', function($join) {
        $join->on('user_rating.id', '=', 'users.id');
        })
        ->get();
        // dd($RatingReview);
        $province = User::select('province')->groupBy('province')->get();
        return view('admin.review-ratings-management')->with('RatingReview', $RatingReview)->with('province', $province);
        }
    }
        
        // dd($request->all());
        // $start_date = $request->from;
        // $end_date = $request->to;
        // $province = $request->province;
        // $city = $request->city;
        // $status = $request->status;
        
        // $Startdate = strtr($start_date, '-', '/');
        // $startEditdate =  date('Y-m-d', strtotime($Startdate));
               
        // $Enddate = strtr($end_date, '-', '/');
        // $endEditdate =  date('Y-m-d', strtotime($Enddate));
        
        // // dd($startEditdate);

        // if($status){
        //     $CompleteGig = UsrRequestGig::select('user_request_gig.gig_id', 'user_request_gig.User_ID', 'user_request_gig.Seller_ID', 'user_request_gig.updated_at', 'user_request_gig.offerAmount', 'user_request_gig.status')->orderBy('user_request_gig.id', 'Desc');  
        // }else{
        //     $CompleteGig = UsrRequestGig::select('user_request_gig.gig_id', 'user_request_gig.User_ID', 'user_request_gig.Seller_ID', 'user_request_gig.updated_at', 'user_request_gig.offerAmount', 'user_request_gig.status')->whereIn('user_request_gig.status', array('9','10'));  
        // }
        
        // if($start_date != ''){
        //     $CompleteGig->whereBetween('user_request_gig.registerDate', [$startEditdate, $endEditdate]);
        // }
        // if($province != ''){            
        //     $CompleteGig->where('users.province',$province);            
        // } 
        // if($city != ''){
        //     $CompleteGig->where('users.city',$city);            
        // } 
        // if($status != ''){
        //     $CompleteGig->where('user_request_gig.status',$status);            
        // } 
        
        // $CompleteGig = $CompleteGig         
        // ->leftJoin('users', function($join) {
        // $join->on('user_request_gig.User_ID', '=', 'users.id');
        // })
        // ->get();
 
        // // dd($CompleteGig);

        // // foreach($OngoingGig as $key=>$rowongoingig){
        // //     $OngoingGigcategoryId = ServiceCategoryAddress::where('id', $rowongoingig->gig_id)->first();

        // //     $OngoingGig[$key]->subcategoryName = City::where('id', $OngoingGigcategoryId->cityName)->first();

        // //     $OngoingGig[$key]->serviceName = ServiceCategory::where('id', $OngoingGigcategoryId->cat_id)->first();

        // //     $OngoingGig[$key]->GigDetails = ServiceCategoryAddress::where('id', $rowongoingig->gig_id)->first();
        // // }

        // $servicetax = ServiceTax::orderBy('id', 'Desc')->get();
        // $province = User::select('province')->groupBy('province')->get();
        // // $CompleteGig = UsrRequestGig::whereIn('status', array('9','10'))->get();

        // foreach($CompleteGig as $key=>$rowcompleate){
        //    $CompleteGig[$key]->serviceTax  = ServiceTax::where('id', '1')->first();
        //    $CompleteGig[$key]->serviceType = ServiceCategoryAddress::where('id', $rowcompleate->gig_id)->first();
        // }

        // // dd($CompleteGig);

        // return view('admin.Earning-management')->with('servicetax', $servicetax)->with('province', $province)->with('CompleteGig', $CompleteGig);