<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Admin\admin;
use App\Model\WebUser;
use App\Model\UsrRequestGig;
use App\User;
use App\Seller;
use App\Model\UserRating;
use App\Model\SellerRating;
use Validator;
use Hash;

class HomeController extends Controller
{
    public function index(Request $request){ 

       if($request->day != '' || $request->end_date != '' || $request->start_date != '') {

        if($request->day == '1'){
            $todayDate = date('Y-m-d');
            $date = today()->subDays(1)->format('Y-m-d');
        }elseif($request->day == '7'){
         $todayDate = date('Y-m-d');
         $date = today()->subDays(7)->format('Y-m-d');     
     }elseif($request->day == '30'){
         $todayDate = date('Y-m-d');
         $date = today()->subDays(30)->format('Y-m-d');     
     }elseif($request->day == 'All'){
        $todayDate = date('Y-m-d');
        $date = today()->subDays(30)->format('Y-m-d'); 
    }else{
        $Startdate = strtr($request->start_date, '-', '/');
        $date =  date('Y-m-d', strtotime($Startdate));

        $Enddate = strtr($request->end_date, '-', '/');
        $todayDate =  date('Y-m-d', strtotime($Enddate));
    }

    $TotalSuccessGig  =   UsrRequestGig::whereBetween('gigCompleteDate', [$date, $todayDate])->where('status', '9')->count();             
    $TotalOngoingGig  =   UsrRequestGig::whereBetween('gigCompleteDate', [$date, $todayDate])->whereIn('status', array('5','7','8'))->count();
    $TotalCancelGig   =   UsrRequestGig::whereBetween('gigCompleteDate', [$date, $todayDate])->whereIn('status', array('3','6'))->count(); 
    $TotalrevenueGig  =   UsrRequestGig::whereBetween('gigCompleteDate', [$date, $todayDate])->where('status', '9')->sum('offeramount'); 

    $Totaluser  =   User::whereBetween('registerDate', [$date, $todayDate])->count();
    $Totaluserrecenty  =  User::whereBetween('registerDate', [$date, $todayDate])->count();
    $TotalServiceProvider  =   Seller::whereBetween('registerDate', [$date, $todayDate])->count(); 
    $TotalServiceProviderrecenty  =  Seller::whereBetween('registerDate', [$date, $todayDate])->count();
    $UserReview = UserRating::whereBetween('ratingRegisterDate', [$date, $todayDate])->count();
    $SellerReview = SellerRating::whereBetween('seller_registerDate', [$date, $todayDate])->count();
    $totalReview = $UserReview+$SellerReview;
    $mostrequest = User::count();

    return view('admin.dashboard', compact('TotalSuccessGig', 'TotalOngoingGig', 'TotalCancelGig', 'TotalrevenueGig', 'Totaluser', 'TotalServiceProvider', 'Totaluserrecenty', 'TotalServiceProviderrecenty', 'totalReview'));

}else{

    $TotalSuccessGig  =   UsrRequestGig::where('status', '9')->count(); 
    $TotalOngoingGig  =   UsrRequestGig::whereIn('status', array('5','7','8'))->count();
    $TotalCancelGig   =   UsrRequestGig::whereIn('status', array('3','6'))->count(); 
    $TotalrevenueGig  =   UsrRequestGig::where('status', '9')->sum('offeramount'); 
    $todayDate = date('Y-m-d');
    $date = today()->subDays(30)->format('Y-m-d');

    $Totaluser  =   User::count();
    $Totaluserrecenty  =  User::whereBetween('registerDate', [$date, $todayDate])->count();
    $TotalServiceProvider  =   Seller::count(); 
    $TotalServiceProviderrecenty  =  Seller::whereBetween('registerDate', [$date, $todayDate])->count();
    $UserReview = UserRating::count();
    $SellerReview = SellerRating::count();
    $totalReview = $UserReview+$SellerReview;
    $mostrequest = User::count();

    return view('admin.dashboard', compact('TotalSuccessGig', 'TotalOngoingGig', 'TotalCancelGig', 'TotalrevenueGig', 'Totaluser', 'TotalServiceProvider', 'Totaluserrecenty', 'TotalServiceProviderrecenty', 'totalReview'));

}




        // if($request->day == '7'){


        //     $todayDate = date('Y-m-d');
        //     $TotalSuccessGig  =   UsrRequestGig::where('gigCompleteDate', $todayDate)->where('status', '9')->count();             
        //     $TotalOngoingGig  =   UsrRequestGig::where('gigCompleteDate', $todayDate)->whereIn('status', array('5','7','8'))->count();
        //     $TotalCancelGig   =   UsrRequestGig::where('gigCompleteDate', $todayDate)->whereIn('status', array('3','6'))->count(); 
        //     $TotalrevenueGig  =   UsrRequestGig::where('gigCompleteDate', $todayDate)->where('status', '9')->sum('offeramount'); 

        //     $date = today()->subDays(1)->format('Y-m-d');
        //     $Totaluser  =   User::whereBetween('registerDate', [$date, $todayDate])->count();
        //     $Totaluserrecenty  =  User::whereBetween('registerDate', [$date, $todayDate])->count();
        //     $TotalServiceProvider  =   Seller::where('registerDate', $todayDate)->count(); 
        //     $TotalServiceProviderrecenty  =  Seller::whereBetween('registerDate', [$date, $todayDate])->count();
        //     $UserReview = UserRating::where('ratingRegisterDate', $todayDate)->count();
        //     $SellerReview = SellerRating::where('seller_registerDate', $todayDate)->count();
        //     $totalReview = $UserReview+$SellerReview;
        //     $mostrequest = User::count();

        //     return view('admin.dashboard', compact('TotalSuccessGig', 'TotalOngoingGig', 'TotalCancelGig', 'TotalrevenueGig', 'Totaluser', 'TotalServiceProvider', 'Totaluserrecenty', 'TotalServiceProviderrecenty', 'totalReview'));

        // }
        // if($request->day == '30'){


        //     $TotalSuccessGig  =   UsrRequestGig::where('status', '9')->count(); 
        //     $TotalOngoingGig  =   UsrRequestGig::whereIn('status', array('5','7','8'))->count();
        //     $TotalCancelGig   =   UsrRequestGig::whereIn('status', array('3','6'))->count(); 
        //     $TotalrevenueGig  =   UsrRequestGig::where('status', '9')->sum('offeramount'); 
        //     $todayDate = date('Y-m-d');
        //     $date = today()->subDays(30)->format('Y-m-d');

        //     $Totaluser  =   User::count();
        //     $Totaluserrecenty  =  User::whereBetween('registerDate', [$date, $todayDate])->count();
        //     $TotalServiceProvider  =   Seller::count(); 
        //     $TotalServiceProviderrecenty  =  Seller::whereBetween('registerDate', [$date, $todayDate])->count();
        //     $UserReview = UserRating::count();
        //     $SellerReview = SellerRating::count();
        //     $totalReview = $UserReview+$SellerReview;
        //     $mostrequest = User::count();
        //     return view('admin.dashboard', compact('TotalSuccessGig', 'TotalOngoingGig', 'TotalCancelGig', 'TotalrevenueGig', 'Totaluser', 'TotalServiceProvider', 'Totaluserrecenty', 'TotalServiceProviderrecenty', 'totalReview'));
        // }else{

        //     $TotalSuccessGig  =   UsrRequestGig::where('status', '9')->count(); 
        //     $TotalOngoingGig  =   UsrRequestGig::whereIn('status', array('5','7','8'))->count();
        //     $TotalCancelGig   =   UsrRequestGig::whereIn('status', array('3','6'))->count(); 
        //     $TotalrevenueGig  =   UsrRequestGig::where('status', '9')->sum('offeramount'); 
        //     $todayDate = date('Y-m-d');
        //     $date = today()->subDays(30)->format('Y-m-d');

        //     $Totaluser  =   User::count();
        //     $Totaluserrecenty  =  User::whereBetween('registerDate', [$date, $todayDate])->count();
        //     $TotalServiceProvider  =   Seller::count(); 
        //     $TotalServiceProviderrecenty  =  Seller::whereBetween('registerDate', [$date, $todayDate])->count();
        //     $UserReview = UserRating::count();
        //     $SellerReview = SellerRating::count();
        //     $totalReview = $UserReview+$SellerReview;
        //     $mostrequest = User::count();

        //     return view('admin.dashboard', compact('TotalSuccessGig', 'TotalOngoingGig', 'TotalCancelGig', 'TotalrevenueGig', 'Totaluser', 'TotalServiceProvider', 'Totaluserrecenty', 'TotalServiceProviderrecenty', 'totalReview'));
        // } 

}


public function DashboardFilter(){
   $start_date = $request->start_date;
   $end_date = $request->end_date;
   $address = $request->address;
   $status = $request->status;
        // dd($start_date);
   $Startdate = strtr($start_date, '-', '/');
   $startEditdate =  date('Y-m-d', strtotime($Startdate));

   $Enddate = strtr($end_date, '-', '/');
   $endEditdate =  date('Y-m-d', strtotime($Enddate));
        // dd($endEditdate);     
   $storeData = Seller::orderBy('id', 'desc');
   if($start_date != ''){
    $storeData->whereBetween('registerDate', [$startEditdate, $endEditdate]);
}
if($address != ''){
    $storeData->where('address',$address);
}       
if($status != ''){
    $storeData->where('is_approved',$status);
}  

$storeData = $storeData->get();
$City = Seller::select('address')->groupBy('address')->get();
return view('admin.services-provider-management')->with('storeData', $storeData)->with('City', $City);
}    

public function dashbord(){

  return view('admin.dashbord');
}

public function CrmManagement(){
  $contactData = WebUser::orderBy('id', 'Desc')->get();
  return view('admin.crm-management')->with('contactData', $contactData);
}

public function Profile(){
    $adminProfile = auth()->guard('admin')->user()->toArray();
        // dd($adminProfile->city);
    return view('admin.profile')->with('adminProfile', $adminProfile);
}

public function editProfile(){
    $adminProfile = auth()->guard('admin')->user()->toArray();
    // dd($adminProfile);
    return view('admin.edit-profile', compact('adminProfile'));   
}

public function updateProfile(Request $request){
   
    $id = auth('admin')->id();

    $this->validate($request, [
      'first_name'=>'required',
        'mobile'=>'required',
        'emailAddress'=>'required',
        'country'=>'required',   
    ]);             

    $updateProfile = Admin::where(['id'=>$id])->first(); 
    $updateProfile->first_name = $request->first_name;
    $updateProfile->mobile = $request->mobile;
    $updateProfile->emailAddress = $request->emailAddress;
    $updateProfile->country = $request->country;
   
    if($request->hasFile('profile_image')){
        $file = $request->file('profile_image');
        $filename = $file->getClientOriginalName();
        $file->move(public_path('/admin/img'), $file->getClientOriginalName());
        $updateProfile->profile_image = $filename;
    }

    if($updateProfile->save()){
       return back()->with('message', 'Profile Updated Successfully!');
   }else{
    return back()->with('error', 'Something went Wrong!');
}

}

public function ChangePassword(){
 return view('admin.ChangePassword');        
}

public function ChangePasswordAdmin(Request $request){
  $id = auth('admin')->id();
  $data = auth()->guard('admin')->user()->password;

  $validation = array(
      'old_password'=>'required',
      'new_password'=>'required|min:8',
      'confirm_password'=>'required|same:new_password',     
  );
  $validator = Validator::make($request->all(),$validation);
  if($validator->fails()){    
    return back()->with('error', $validator->errors($validator)->first());

}
$data = $request->all();

$user = auth()->guard('admin')->user()->password;
if(!Hash::check($data['old_password'], $user)){       
    return back()->with('error', 'Old Password Does not Match!');
}else{
   $save = Admin::where(['id'=>$id])->first();
   $save->password = Hash::make($request->new_password);
   if($save->save()){
       return back()->with('message', 'Password Updated Successfully!');

   }

}


}





}
