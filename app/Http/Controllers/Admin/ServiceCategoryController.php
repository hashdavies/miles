<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\SubcategoryService;
use App\Model\ServiceCategory;
use Storage;

class ServiceCategoryController extends Controller
{
    public function ServiceCategoryList(){
        $data = ServiceCategory::orderBy('id', 'desc')->get();

        foreach($data as $key => $row){
           $subcate_array = explode(',', $row->subcategoryID); 
           $sucatearrayname = SubcategoryService::whereIn('id', $subcate_array)->pluck('sub_category_name')->toArray(); 
           $sucatearrayname =  implode(',',$sucatearrayname);
            // dd($sucatearrayname);            
           $data[$key]->subcategoryname=$sucatearrayname;
        }   
    	return view('admin.services-category-management')->with('data', $data);
    }


    public function AddServiceCategory(){
        $subcategory = SubcategoryService::orderBy('id', 'desc')->get();
    	return view('admin.add-service-category')->with('subcategory', $subcategory);
    }

    public function AddServiceSubcategory(){
    	return view('admin.add-service-subcategory');
    }

    public function AddSubCategory(Request $request){
    	
        // dd($request->all());
        if ($request->hasFile('sub_category_image')) {

            $image = $request->file('sub_category_image');
            $s3 = \Storage::disk('s3');

            $subCateImg = time() .'.'. $image->getClientOriginalExtension();
            $filepath='Category/' . $subCateImg;
            $s3->put($filepath, file_get_contents($image), 'public');
            
           // $file1 = $request->file('sub_category_image');
           // $subCateImg = 'https://mylesimages.s3.ap-south-1.amazonaws.com/Category/'.time(). '.' .$file1->getClientOriginalExtension();
           // $filePath1 = 'Category/' . $subCateImg;
           // Storage::disk('s3')->put($filePath1, file_get_contents($file1));
        }
        SubcategoryService::create([
            'sub_category_image' => $subCateImg,
            'sub_category_name' => $request->sub_category_name,
            'sub_category_description' => $request->sub_category_description,
        ]);
    	return redirect('admin/add-service-category')->with('success_message', 'Subcategory added successfully');
    }

    public function AddService(Request $request){
        
        $rdate = date('Y-m-d'); 
        // dd($request->all());
        $subcateID = implode(',',$request->subcategory);
        // dd($request->subcategory);
      
        // if($request->hasFile('service_category_image'))
        //   {
        //     $image = $request->file('service_category_image');
        //     $s3 = \Storage::disk('s3');

        //     $IdProof = time() .'.'. $image->getClientOriginalExtension();
        //     $filepath='Category/' . $IdProof;
        //     $s3->put($filepath, file_get_contents($image), 'public');
        //   }
          
          ServiceCategory::create([
            'subcategoryID' => $subcateID, 
            'category_name' => $request->categoryName,
            // 'category_image' => $IdProof,
            'status' => '1',
            'registredDate' => $rdate,
          ]);
      
        // $subcatId = $request->subcategory;
        // $lastInsertID = ServiceCategory::orderBy('id','desc')->first();
        // if(!empty($lastInsertID)){
        //     $lastID = (($lastInsertID->id)+1);
        // }else{
        //     $lastID = 1;
        // }
        // foreach($subcatId as $key => $row)
        // {
        //     $input['subcategoryID'] = $row;
        //     $input['category_name'] = $request->categoryName;
        //     $input['category_image'] = $IdProof;
        //     $input['registredDate'] = $rdate;
        //     $input['status'] = '1';
        //     $input['categoryID'] = $lastID;
        //     ServiceCategory::create($input);
        // }
        return redirect('admin/services-category-management')->with('success_message', 'Subcategory added successfully');
    }

    public function ServiceCargoryFilter(Request $request){
    
        // dd($request->all());
        $start_date = $request->from;
        $end_date = $request->to;
        $status = $request->status;
        // dd($start_date);
        $Startdate = strtr($start_date, '-', '/');
        $startEditdate =  date('Y-m-d', strtotime($Startdate));
               
        $Enddate = strtr($end_date, '-', '/');
        $endEditdate =  date('Y-m-d', strtotime($Enddate));
        // dd($endEditdate);     
        $data = ServiceCategory::orderBy('id', 'desc');
        if($start_date != ''){
            $data->whereBetween('registredDate', [$startEditdate, $endEditdate]);
        }
        if($status != ''){
            $data->where('status',$status);
        }          
        $data = $data->get();
        foreach($data as $key => $row){
           $subcate_array = explode(',', $row->subcategoryID); 
           $sucatearrayname = SubcategoryService::whereIn('id', $subcate_array)->pluck('sub_category_name')->toArray(); 
           $sucatearrayname =  implode(',',$sucatearrayname);
            // dd($sucatearrayname);            
           $data[$key]->subcategoryname=$sucatearrayname;
        } 

        return view('admin.services-category-management')->with('data', $data);
    }

    public function BlockCategory($id, $type){
        // dd($id);
        if($type == 1){
           ServiceCategory::where('id', $id)->update(['status' => $type]);
           $msg = "Category active successfully";
        }elseif($type == 0){
           ServiceCategory::where('id', $id)->update(['status' =>$type]);
           $msg = "Category inactive successfully";
        }
        return redirect('admin/services-category-management')->with('success_message', $msg);
    }
    
    public function EditServiceCategory($id){
        
        $data = ServiceCategory::where('id', $id)->first();
        $subcategory = SubcategoryService::orderBy('id', 'desc')->get();
        
        $subcategoryselected = explode(',',$data->subcategoryID);
        
        // dd($all);

        // $subcategoryselected = ServiceCategory::where('categoryID', $data->categoryID)->pluck('subcategoryID')->toArray();

        return view('admin.edit-service-category')->with('data', $data)->with('subcategory', $subcategory)->with('subcategoryselected', $subcategoryselected);
    }

    public function DeleteServiceCategory($id){
        ServiceCategory::where('id', $id)->delete();
        return redirect()->back()->with('success_message', 'Deleted successfully');
    }

    public function UpdateCategory(Request $request){
        
        // dd($request->all());
        // ServiceCategory::where('categoryID',$request->CategoryID)->delete();
        if($request->file('service_category_image')){
            if($request->hasFile('service_category_image'))
            {
                $image = $request->file('service_category_image');
                $s3 = \Storage::disk('s3');

                $IdProof = time() .'.'. $image->getClientOriginalExtension();
                $filepath='Category/' . $IdProof;
                $s3->put($filepath, file_get_contents($image), 'public');
            }

            $subcateID = implode(', ',$request->subcategory);
            $rdate = date('Y-m-d');
            $subcatId = $request->subcategory;
            
            ServiceCategory::where('id', $request->editID)->update([
                'subcategoryID' => $subcateID, 
                'category_name' => $request->categoryName,
                'category_image' => $IdProof,               
            ]);
        }else{
            // dd($request->all());
            $subcateID = implode(', ',$request->subcategory);
            $rdate = date('Y-m-d');
            $subcatId = $request->subcategory;

            ServiceCategory::where('id', $request->editID)->update([
                'subcategoryID' => $subcateID, 
                'category_name' => $request->categoryName,
                // 'category_image' => $IdProof,               
            ]);
        }        
        return redirect('admin/services-category-management')->with('success_message', 'Subcategory added successfully');
    }




}