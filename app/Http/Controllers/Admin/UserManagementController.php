<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Admin\Admin;
use App\User;
use App\Seller;
use App\Model\Countries;
use App\Model\City;
use App\Model\State;
use App\Model\OfferBanner;
use Validator; 
use App\Model\ServiceCategory;
use App\Model\ServiceCategoryAddress;
use App\Model\AddUserAddress;
use Hash;
use App\Exports\ListExport;
use App\Exports\ServiceProviderExport; 
use Maatwebsite\Excel\Facades\Excel;
use App\Model\Notification;


class UserManagementController extends Controller
{
    public function userlist(){
        $province = User::select('province')->groupBy('province')->get();
    	$userData = User::OrderBy('id', 'desc')->get();
        foreach($userData as $key=>$rowUser){
           
            $gigAddress = ServiceCategoryAddress::groupBy('pickUpAddress')->orderBy('id', 'Desc')->where('user_id', $rowUser->id)->get();  
            $userAddremoreaddress = AddUserAddress::groupBy('address')->where('user_id', $rowUser->id)->get(); 

            $userData[$key]->test = 'kuldeeptomer';
        }
        return view('admin.user_management')->with('userData', $userData)->with('province', $province)->with('userAddremoreaddress', $userAddremoreaddress)->with('gigAddress', $gigAddress);
    }

    public function ExportExcel(){
        // dd("sads");
               // $Data = User::all();
               // $fields=array('User ID','User Name','Email ID','Mobile','Address');
               //    $export_data = implode("\t", array_values($fields)) . "\n";
               //    foreach($Data as $value){
               //      $tot_record_found=1;

               //          $rowData = array($value['id'],$value['first_name'].' '.$value['last_name'],$value['email'],$value['mobile'],$value['address']);

               //      array_walk($rowData, array('self', 'filterData'));
               //                          $export_data .= implode("\t", array_values($rowData)) . "\n";               
               //            }

               //            return response($export_data)
               //                    ->header('Content-Type','application/xls')               
               //                    ->header('Content-Disposition', 'attachment; filename="download.xls"')
               //                    ->header('Pragma','no-cache')
               //                    ->header('Expires','0');                     
               //    return view('download',['record_found' =>$tot_record_found]);


        // $data = User::get()->toArray();
            
        // return Excel::create('itsolutionstuff_example', function($excel) use ($data) {
        //     $excel->sheet('mySheet', function($sheet) use ($data)
        //     {
        //         $sheet->fromArray($data);
        //     });
        // })->download($type);
         return Excel::download(new ListExport, 'customerList.xlsx');
        // dd("Done");
    }

    public function ExportServiceProviderExcel(){
        return Excel::download(new ServiceProviderExport, 'serviceProviderList.xlsx');
    }

    public function AddServiceProvider(){
        $service = ServiceCategory::orderBy('id', 'Desc')->get();
        return view('admin.add-service-provider')->with('service', $service);
    }


    public function UserGigAddress($id){        
        $gigAddress = ServiceCategoryAddress::where('user_id', $id)->groupBy('pickUpAddress')->orderBy('id', 'Desc')->get(); 
        return response()->json($gigAddress);
    }

    public function UserAddress($id){
        $userAddremoreaddress = AddUserAddress::groupBy('address')->where('user_id', $id)->get(); 
        return response()->json($userAddremoreaddress);
    }

    public function usercity($province){
        $cities = User::groupBy('city')->where('province', $province)->get(); 
        return response()->json($cities);
    }

    public function UserMgmtFilter(Request $request){
        // dd($request->all());
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $province = $request->province;
        $city = $request->city;
        $status = $request->status;
        // dd($start_date);
        $Startdate = strtr($start_date, '-', '/');
        $startEditdate =  date('Y-m-d', strtotime($Startdate));
               
        $Enddate = strtr($end_date, '-', '/');
        $endEditdate =  date('Y-m-d', strtotime($Enddate));
        // dd($endEditdate);     
        $userData = User::orderBy('id', 'desc');
        if($start_date != ''){
            $userData->whereBetween('registerDate', [$startEditdate, $endEditdate]);
        }
        if($province != ''){
            $userData->where('province',$province);
        } 
        if($city != ''){
            $userData->where('city',$city);
        }       
        if($status != ''){
            $userData->where('status',$status);
        }          
        $userData = $userData->get();
        $province = User::select('province')->groupBy('province')->get();
        return view('admin.user_management')->with('userData', $userData)->with('province', $province);
    }

    public function storelist(){
     
        $storeData = Seller::OrderBy('id', 'desc')->get();
    	$City = Seller::select('address')->groupBy('address')->get();

        // $data = ServiceCategory::orderBy('id', 'desc')->get();
        foreach($storeData as $key => $row){
           $subcate_array = explode(',', $row->serviceType);
           $sucatearrayname = ServiceCategory::whereIn('id', $subcate_array)->pluck('category_name')->toArray(); 
           $sucatearraynamecount = ServiceCategory::whereIn('id', $subcate_array)->count(); 
           $sucatearrayname =  implode(', ',$sucatearrayname);
           $storeData[$key]->subcategoryname = $sucatearrayname;
           // dd($sucatearraynamecount);
           $storeData[$key]->allservicecount = $sucatearraynamecount;
        }
        return view('admin.services-provider-management')->with('storeData', $storeData)->with('City', $City);
    }

    public function SellerMgmtFilter(Request $request){
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $address = $request->address;
        $status = $request->status;
        // dd($start_date);
        $Startdate = strtr($start_date, '-', '/');
        $startEditdate =  date('Y-m-d', strtotime($Startdate));
               
        $Enddate = strtr($end_date, '-', '/');
        $endEditdate =  date('Y-m-d', strtotime($Enddate));
        // dd($endEditdate);     
        $storeData = Seller::orderBy('id', 'desc');
        if($start_date != ''){
            $storeData->whereBetween('registerDate', [$startEditdate, $endEditdate]);
        }
        if($address != ''){
            $storeData->where('address',$address);
        }       
        if($status != ''){
            $storeData->where('is_approved',$status);
        }  
                
        $storeData = $storeData->get();
        $City = Seller::select('address')->groupBy('address')->get();
        return view('admin.services-provider-management')->with('storeData', $storeData)->with('City', $City);
    }


    public function blockVendor($id, $type){

      Seller::where('id', $id)->update(['is_status'=>$type, 'is_approved'=>$type]);                 
      if($type == 1){
        Seller::where('id', $id)->update(['is_status'=>$type, 'is_approved'=>$type]); 
        $msg = "Service Provider Activated successfully";
      }elseif($type == 2){
        Seller::where('id', $id)->update(['is_status'=>$type, 'is_approved'=>$type]); 
        $msg = "Service Provider Deactivated successfully";
      }
      return redirect('admin/services-provider-management')->with('success_message', $msg);
    }

    public function venderVerify($id, $type){      
      Seller::where('id', $id)->update(['is_approved'=>$type, 'is_status' => $type]);

      if($type == 2){
        $msg = "Service Provider rejected successfully";
        $body_text="Oh No! you have rejected by admin plz contact";
      }
      elseif($type == 1){
        $msg = "Service Provider verified successfully";
        $body_text="Congratulations, your account has been approved";
      }
      $sellerData = Seller::where('id', $id)->first();
      $tokens = $sellerData->device_token;
      $result =[
        'is_applied'=>1,
      ];
      if($sellerData->device_type == '0'){
          $is_result=$this->androidPushNotification($body_text, $tokens, $result);
      }elseif($sellerData->device_type == '1'){
          $is_result=$this->iosPushNotification($body_text, $tokens,$result);
      } 
      // if($is_result == '1'){
      //   $data = new Notification;
      //   $data->seller_id = $sellerData->id;
      //   $data->message = $body_text;
      //   $data->save();               
      // }  
      return redirect('admin/services-provider-management')->with('success_message', $msg);
    }   

    public function RejectedReason(Request $request){
        
        // dd("dsfds");        
        $data = implode(', ', $request->reject_reason);
        Seller::where('id', $request->seller_id)->update([
            'rejectReason' => $data,
            'is_approved' => '3',
        ]);
        return redirect()->back()->with('success_message', 'Service Provider rejected successfully');
    } 
    

    public function vendorCountryList($id){
      $state = State::select('id')->where('country_id', $id)->get();
      $cities = City::orderBy('name', 'asc')->whereIn('state_id', $state)->get(); 
      return response()->json($cities);
    }

    public function blockUser($id, $type){
    	$data = User::find($id);    	
    	$data->status = $type;
    	$data->save();
    	if($type==0){
    		$msg = "Category unblock successfully.";
    	}
    	elseif($type==1){
    		$msg = "Category block successfully.";
    	}
    	return redirect('admin/user-management')->with('success_message',$msg);
    }

    public function DeleteUser($id){
    	User::where('id', $id)->delete();
    	return redirect('admin/user-management')->with('deleteuser', 'User Deleted Successfully');
    }

    public function DeleteStore($id){
      // dd($id);
      Seller::where('id', $id)->delete();
      return redirect()->back()->with('success_message', 'Service Provider deleted successfully');
    }   

    public function EditServiceProvider($id){
      $editData =  Seller::where('id', $id)->first();
      $service = ServiceCategory::orderBy('id', 'Desc')->get();
      return view('admin.edit-service-provider')->with('editData', $editData)->with('service',$service);
    }


// Additional functionality for admin profile

    public function SaveServiceProvider(Request $request){      
        // dd($request->all());

        $service = implode(',', $request->service);
        // dd($service);
        $registerDate =  date('Y-m-d');
        if($request->hasfile('businessLogo')){
            $image = $request->file('businessLogo');
            $s3 = \Storage::disk('s3');
            $businessLogoname = 'businesslogo_'.time() .'.'. $image->getClientOriginalExtension();
            $filepath='Serviceprovider/' . $businessLogoname;
            $s3->put($filepath, file_get_contents($image), 'public');
        } 
        if($request->hasfile('profileImage')){
            $imageprofile = $request->file('profileImage');
            $s3 = \Storage::disk('s3');
            $profileImagename =  'profileimage_'.time() .'.'. $imageprofile->getClientOriginalExtension();
            $filepathprofile='Serviceprovider/' . $profileImagename;
            $s3->put($filepathprofile, file_get_contents($imageprofile), 'public');
        }         
        if($request->hasfile('IdProof')){
            $imageIdProof = $request->file('IdProof');
            $s3 = \Storage::disk('s3');
            $IdProofimagename = 'idproof_'.time() .'.'. $imageIdProof->getClientOriginalExtension();
            $filepathIdProof='Serviceprovider/' . $IdProofimagename;
            $s3->put($filepathIdProof, file_get_contents($imageIdProof), 'public');
        } 

        $ServiceProvider = new Seller;
        $ServiceProvider->businessName  = $request->businessName;
        $ServiceProvider->firstName     = $request->firstName;
        $ServiceProvider->lastName      = $request->lastName;
        $ServiceProvider->nickName      = $request->firstName.$request->lastName;
        $ServiceProvider->email         = $request->email;
        $ServiceProvider->serviceType   = $service;
        $ServiceProvider->countryCode   = $request->countryCode;
        $ServiceProvider->mobileNo      = $request->phone;
        $ServiceProvider->dLNumber      = $request->dlNumber;
        $ServiceProvider->serviceProviderType = $request->serviceProviderType;
        $ServiceProvider->transitNumber       = $request->transitNumber;
        $ServiceProvider->institutionNumber   = $request->institutionNumber;
        $ServiceProvider->accountNumber       = $request->accountNumber;
        $ServiceProvider->address             = $request->address.', '.$request->postalcode;
        $ServiceProvider->province            = $request->province;
        $ServiceProvider->city                = $request->city;
        $ServiceProvider->address_latitude    = $request->address_latitude;
        $ServiceProvider->address_longitude   = $request->address_longitude;
        $ServiceProvider->registerDate        = $registerDate;
        $ServiceProvider->is_approved         = '1';
        $ServiceProvider->is_verified         = '1';
        $ServiceProvider->is_status           = '1';
        $ServiceProvider->is_profile          = '1';
        $ServiceProvider->profile_step        = '2';
        $ServiceProvider->password            = hash::make($request->password);
        
        if($request->hasFile('businessLogo')){
          $ServiceProvider->businesslogo        = $businessLogoname;
        }
        if($request->hasFile('profileImage')){
          $ServiceProvider->profileImage        = $profileImagename;
        }
        if($request->hasFile('IdProof')){
          $ServiceProvider->IDProof             = $IdProofimagename;
        }
        $ServiceProvider->save();

      return redirect('admin/services-provider-management')->with('success_message', 'Service Provider added successfully');
    }

     public function UpdateServiceceProvider(Request $request){      
        // dd($request->all());
        $id = $request->edit_id;
        // dd($id); 
                
        $registerDate =  date('Y-m-d');
        if($request->hasfile('businessLogo')){
            $image = $request->file('businessLogo');
            $s3 = \Storage::disk('s3');
            $businessLogo = 'businesslogo_'.time() .'.'. $image->getClientOriginalExtension();
            $filepath='Serviceprovider/' . $businessLogo;
            $s3->put($filepath, file_get_contents($image), 'public');
        } 
        if($request->hasfile('profileImage')){
            $imageprofile = $request->file('profileImage');
            $s3 = \Storage::disk('s3');
            $profileImage =  'profileimage_'.time() .'.'. $imageprofile->getClientOriginalExtension();
            $filepathprofile='Serviceprovider/' . $profileImage;
            $s3->put($filepathprofile, file_get_contents($imageprofile), 'public');
        }         
        if($request->hasfile('IdProof')){
            $imageIdProof = $request->file('IdProof');
            $s3 = \Storage::disk('s3');
            $IdProofimage = 'idproof_'.time() .'.'. $imageIdProof->getClientOriginalExtension();
            $filepathIdProof='Serviceprovider/' . $IdProofimage;
            $s3->put($filepathIdProof, file_get_contents($imageIdProof), 'public');
        }        
        if(!empty($request->service)){
          $servicedata = implode(',', $request->service);
        }
        // dd($servicedata);

        $ServiceProvider = Seller::find($id);
        $ServiceProvider->businessName  = $request->businessName;
        $ServiceProvider->firstName     = $request->firstName;
        $ServiceProvider->lastName      = $request->lastName;
        $ServiceProvider->nickName      = $request->firstName.$request->lastName;
        $ServiceProvider->email         = $request->email;        
        $ServiceProvider->countryCode   = $request->countryCode;
        // $ServiceProvider->mobileNo      = $request->phone;
        $ServiceProvider->dLNumber      = $request->dlNumber;
        $ServiceProvider->serviceProviderType = $request->serviceProviderType;
        $ServiceProvider->transitNumber       = $request->transitNumber;
        $ServiceProvider->institutionNumber   = $request->institutionNumber;
        $ServiceProvider->accountNumber       = $request->accountNumber;
        $ServiceProvider->address             = $request->address.', '.$request->postalcode;
        $ServiceProvider->province            = $request->province;
        $ServiceProvider->city                = $request->city;
        $ServiceProvider->address_latitude    = $request->address_latitude;
        $ServiceProvider->address_longitude   = $request->address_longitude;
        // $ServiceProvider->registerDate        = $registerDate;
        // $ServiceProvider->is_approved         = '1';
        // $ServiceProvider->is_verified         = '1';
        // $ServiceProvider->is_status           = '1';
        // $ServiceProvider->is_profile          = '1';
        // $ServiceProvider->profile_step        = '2';
        $ServiceProvider->password            = hash::make($request->password);
        
        if(!empty($request->service)){
          $ServiceProvider->serviceType   = $servicedata;
        }
        
        if($request->hasFile('businessLogo')){
          $ServiceProvider->businesslogo        = $businessLogo;
        }
        if($request->hasFile('profileImage')){
          $ServiceProvider->profileImage        = $profileImage;
        }
        if($request->hasFile('IdProof')){
          $ServiceProvider->IDProof             = $IdProofimage;
        }
        $ServiceProvider->save();

      return redirect('admin/services-provider-management')->with('success_message', 'Service Provider updated successfully');
    }

    // public function adminProfile(){
    //   $userId = Auth::guard('admin')->id();
    //   $adminProfile = Admin::where('id', $userId)->first();
    //   //dd($sellerProfile);
    //   return view('admin.profile')->with('adminProfile', $adminProfile);
    // }

    // public function changePassword(Request $request){
    //   //dd($request->all());
    //   $company_password = Auth::guard('admin')->user()->password; 
    //   $companyId = Auth::guard('admin')->user()->id; 
              
    //   // dd($companyId);  
       
    //   $this->validate($request, 
    //     [
    //     'old_password'     => 'required',
    //     'new_password'     => 'required|min:8|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/',
    //     'confirm_password' => 'required|same:new_password'
    //     ],
    //     [
    //       'old_password.required' => 'mandatory field required',
    //       'new_password.required' => 'please enter valid password',
    //       'new_password.min' => 'password must be at least 8 characters',
    //       'new_password.regex' => 'your password must be more than 8 characters long, should contain at-least one uppercase, one lowercase, one numeric and one special character.',
    //     ]
    //   );
      
    //   if($request->old_password == $request->new_password){

    //   return redirect()->back()->with('wrongpassword', 'Current password and old password doesn same'); 

    //   }else{

    //   if(!\Hash::check($request->old_password, $company_password)){      

    //        return back()->with('wrongpassword','Your old password is incorrect');
    //   }else{
    //       Seller::where('id', $companyId)->update([
    //         'password' => Hash::make($request->new_password)
    //       ]);


    //       // $data = array(            
    //       //   'email' => $companyemail,           
    //       // );

    //       // $from =array();
    //       // $from['name'] = 'WeSoar';
    //       // $from['address'] = 'technology.operations@wesoar.ai';
    //       // $from['email_to'] = $companyemail;

    //       // Mail::send('emails.changepassword',$data, function ($message) use ($from) {
    //       //    $message->from($from['address'],$from['name']);
    //       //    $message->to($from['email_to']);
    //       //    $message->subject('Wesoar: Update Password');
    //       // });


    //       return redirect()->back()->with('successpassword', 'Password Updated Successfully'); 
    //   }
    //  } 
    // }

    // public function DeleteVendor($id){
    //   dd($id);
    //   Seller::where('id', $id)->delete();
    //   return redirect()->back()->with('success_message', 'Vendor deleted successfully');
    // }   
    
}