<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\PromoCode;
use App\Model\ServiceCategory;
use App\Model\GigOrders;
use App\Seller\Seller;
use App\Model\Notification;
use App\Model\SubcategoryService;

class PaymentManagement extends Controller
{

    public function PromoCodeMannagement(Request $request){
      // dd("dsfsd");
      // PromoCode::all();
      $result1['serviceCategory'] = SubcategoryService::orderBy('id', 'Desc')->get();

      $start_date = $request->startDate;
      $end_date = $request->endDate;
      $status = $request->status;
      
      $startEditdate =  date('Y-m-d', strtotime($start_date));
      $EndEditdate =  date('Y-m-d', strtotime($end_date));

      // dd($startEditdate);
      $result = PromoCode::orderBy('id', 'Desc');
           
        if($start_date != ''){
            $result->where('start_date', $startEditdate);
        }
        if($end_date != ''){
            $result->where('end_date', $EndEditdate);
        }
        if($status != ''){
            $result->where('status',$status);            
        } 
        $result1['promoCodes'] = $result->get();

        // dd($result);
        return view('admin.promo-code-management')->with($result1); 
    }

    public function PaymentManagement(Request $request){

      $start_date = $request->start_date;
      $end_date = $request->end_date;
      $status = $request->status;
      $startEditdate =  date('Y-m-d', strtotime($start_date));
      $EndEditdate =  date('Y-m-d', strtotime($end_date));
      
      // dd($startEditdate);          

      $data = GigOrders::orderBy('id', 'Desc');           
        if($start_date != ''){
           $data->whereBetween('PaymentDate', [$startEditdate, $EndEditdate]);
        }
        if($status != ''){
            $data->where('paymentStatus',$status);            
        } 
        $data = $data->get();

      // $data = GigOrders::orderBy('id', 'Desc')->get();
      return view('admin.payment-management')->with('data', $data);
    }

    public function AddPromocode(Request $request){

       if ($request->hasFile('coupon_image')) {
            $image = $request->file('coupon_image');
            $s3 = \Storage::disk('s3');
            $subCateImg = time() .'.'. $image->getClientOriginalExtension();
            $filepath='Category/' . $subCateImg;
            $s3->put($filepath, file_get_contents($image), 'public');
       }

        $result =[
          'is_applied'=>1,
        ];
        $body_text = "Dear Customer New coupon added";
        $seller = Seller::get();
        foreach($seller as $key=>$rowseller){
          if($rowseller->device_type == '1'){

            $is_result=$this->iosPushNotification($body_text, $rowseller->device_token,$result);
            if($is_result == '1'){
              $data = new Notification;
              $data->seller_id = $rowseller->id;
              $data->message = $body_text;
              $data->save();
            }

          }elseif($rowseller->device_type == '0'){

            $is_result=$this->androidPushNotification($body_text, $rowseller->device_token, $result);
            
            if($is_result == '1'){
              $data = new Notification;
              $data->seller_id = $rowseller->id;
              $data->message = $body_text;
              $data->save();
            }

          } 
        }

        //   return response()->json(['message'=> 'Notification send successfully'],200);
        // }elseif($is_result == '0'){
        //   return response()->json(['message'=> 'Something went wrong!'],400);
        // }

        $saveCoupon = new PromoCode;
        $saveCoupon->start_date = $request->post('start_date');
        $saveCoupon->end_date = $request->post('end_date');
        $saveCoupon->promo_name = $request->post('promo_name');
        $saveCoupon->coupon_per_user = $request->post('coupon_per_user');
        $saveCoupon->discount = $request->post('discount');
        $saveCoupon->amount_limit = $request->post('discount_limit');
        $saveCoupon->description = $request->post('description');
        $saveCoupon->apply_promo_code = $request->post('apply_promo_code');
        $saveCoupon->coupon_image = $subCateImg; 
        $saveCoupon->status =1;
        if($saveCoupon->save()){
           return redirect()->back()->with('success_message','added successfully');
        }else{
           return redirect()->back()->with('success_message','Something went wrong!');
        }   
    }

    public function EditPromocode(Request $request){

        $saveCoupon = PromoCode::find($request->editId);
        if ($request->hasFile('coupon_image')) {
            $image = $request->file('coupon_image');
            $s3 = \Storage::disk('s3');
            $subCateImg = time() .'.'. $image->getClientOriginalExtension();
            $filepath='Category/' . $subCateImg;
            $s3->put($filepath, file_get_contents($image), 'public');
        $saveCoupon->coupon_image = $subCateImg;
        }
        // $saveCoupon->status =1;
        $saveCoupon->start_date = $request->post('start_date');
        $saveCoupon->end_date = $request->post('end_date');
        $saveCoupon->promo_name = $request->post('promo_name');
        $saveCoupon->coupon_per_user = $request->post('coupon_per_user');
        $saveCoupon->discount = $request->post('discount');
        $saveCoupon->amount_limit = $request->post('discount_limit');
        $saveCoupon->description = $request->post('description');
        $saveCoupon->apply_promo_code = $request->post('apply_promo_code');
        
       if($saveCoupon->save()){
          return json_encode(['status'=>1]);
       }else{
          return redirect()->back();
       }
      // return redirect()->back()->with('succcess', 'Coupon added successfully');
    }

    public function DeletePromocode(Request $req){
      
      $flight =PromoCode::find($req->id);
      if($flight->delete()){
        return json_encode(['status'=>1]);
      }
    }

    public function BlockPromoUser(Request $req){
      
      $flight =PromoCode::find($req->id);
      $flight->status=$req->status;
      if($flight->save()){
        return json_encode(['status'=>1]);
      }
    }

    public function FilterPromocode(Request $request)
    {
      dd($request->start_date); 
    }


}

