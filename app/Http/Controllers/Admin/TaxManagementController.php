<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\TripType;
use App\Model\UserTax;
use App\User;

class TaxManagementController extends Controller
{
    
    public function TaxManagement(){
    	$triptype = UserTax::orderBy('id', 'Desc')->get();
    	$servicetax = UserTax::select('province')->groupBy('province')->get();

    	$province = User::groupBy('province')->orderBy('id', 'Desc')->whereNotIn('province', $servicetax)->get();
        

    	$adminserviceTax = UserTax::select('tax_rate')->groupBy('tax_rate')->get();

    	return view('admin.tax-management')->with('triptype', $triptype)->with('servicetax', $servicetax)->with('province', $province)->with('adminserviceTax', $adminserviceTax);    	
    }

    public function AddCustomTax(Request $request){    	
    	// dd($request->all());
        if($request->tax_amount){
            UserTax::create([
                'province' => $request->tax_province,
                'tax_rate' => $request->tax_amount,            
            ]);
        }elseif($request->tax_amount_manual){
            UserTax::create([
                'province' => $request->tax_province,
                'tax_rate' => $request->tax_amount_manual,            
            ]);    
        }        
        return redirect()->back()->with('success_message', 'Tax added success');
    }

    public function UpdateTax(Request $request){
        // dd($request->all());
        
        if($request->tax_amount){
            UserTax::where('id', $request->editId)->update([
                'province' => $request->tax_province,
                'tax_rate' => $request->tax_amount,            
            ]);
        }elseif($request->tax_amount_manual){
            UserTax::where('id', $request->editId)->update([
                'province' => $request->tax_province,
                'tax_rate' => $request->tax_amount_manual,            
            ]);    
        }        
        return redirect()->back()->with('success_message', 'Tax updated succesfully');
    }

    
}