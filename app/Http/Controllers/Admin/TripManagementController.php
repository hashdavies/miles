<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\TripType;

class TripManagementController extends Controller
{
    public function TripManagement(){
    	$triptype = TripType::orderBy('id', 'Desc')->get();
    	return view('admin/trip-management')->with('triptype', $triptype);
    }

    public function AddTrip(Request $request){
    	// dd($request->all());
    	TripType::create([
    		'tripName' => $request->tripName,
    		'tripDetails' => $request->tripdetail,
    		'status' => '0',
    	]);
    	return redirect()->back()->with('success_message', 'Trip type added successfully');
    }

    public function UpdateTrip(Request $request){
    	
    	TripType::where('id', $request->editId)->update([
    		'tripName' => $request->edittriptype,
    		'tripDetails' => $request->edittripdetail,
    	]);
    	return redirect()->back()->with('success_message', 'Trip updated successfully');
    }

    public function BlockTrip($id, $status){
    	// dd($status);
    	TripType::where('id', $id)->update([
    		'status' => $status,
    	]);
    	if($status == '0'){
    		$msg = "UnBlock successfully";
    	}elseif($status == '1'){
    		$msg = "Block successfully";
    	}
    	return redirect()->back()->with('success_message', $msg);
    }

}