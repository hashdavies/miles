<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\About;
use App\Model\Faq;
use App\Model\CmsPage;

class CrmManagement extends Controller
{
    public function cmsMgmt(){
    	$about_us = About::where('id', '1')->first();
    	$faqs = Faq::orderBy('id', 'Desc')->get();
    	$cmsPage = CmsPage::orderBy('id', 'Desc')->get();
    	return view('admin.cms-management')->with('about_us', $about_us)->with('faqs', $faqs)->with('cmsPage', $cmsPage);
    }

    public function AddAbout(Request $request){
    	if(!empty($request->editId)){
    		About::where('id', $request->editId)->update([
	    		'help' => $request->about,
	    	]);
    	}else{
    		// dd($request->about);
    		About::create([
	    		'help' => $request->about,
	    	]);
    	}
    	return redirect()->back()->with('success_message', 'About us created successfully');
    }

    public function TermsConditions(Request $request){
	    if(!empty($request->edittermid)){
    		About::where('id', $request->edittermid)->update([
	    		'termscondition' => $request->termcondition,
	    	]);
    	}else{
    		// dd("sdsada");

    		About::create([
	    		'termscondition' => $request->termcondition,
	    	]);
    	}
    	return redirect()->back()->with('success_message', 'Terns & Conditions created successfully');
	}


	public function AddFaq(Request $request){
		// dd($request->all());
		Faq::create([
			'question'=> $request->question,
			'answer' => $request->answer,
		]);
		return redirect()->back()->with('success_message', 'Question created successfully');
	}

	public function DeleteFaq($id){
		// dd($id);
		Faq::where('id', $id)->delete();
		return redirect()->back()->with('success_message', 'Question deleted successfully');
	}

	public function PrivacyPolicy(Request $request){
		// dd($request->all());

		if(!empty($request->editprivacyId)){
    		About::where('id', $request->editprivacyId)->update([
	    		'legal' => $request->privacypolicy,
	    	]);
    	}else{
    		About::create([
	    		'legal' => $request->privacypolicy,
	    	]);
    	}
    	return redirect()->back()->with('success_message', 'Privacy Policy updated successfully');
	}

	public function AddCmsPage(Request $request){
		// dd($request->all());

		$slug = str_replace(' ','-', $request->title);
		// dd($slug);
		if($request->editid){
			$saveCms = CmsPage::find($request->editid);
			$saveCms->title =  $request->title;
			$saveCms->permalink =  strtolower($slug);
			$saveCms->content = $request->description;
			if($saveCms->save()){
				return redirect()->back()->with('success_message', 'Page updated successfully');
			}else{
				return redirect()->back()->with('success_message', 'Something went wrong!');
			}
		}else{
			$saveCms = new CmsPage;
			$saveCms->title =  $request->title;
			$saveCms->permalink =  strtolower($slug);
			$saveCms->content = $request->description;
			if($saveCms->save()){
				return redirect()->back()->with('success_message', 'Page created successfully');
			}else{
				return redirect()->back()->with('success_message', 'Something went wrong!');
			}
		
		}
	}

    
}
