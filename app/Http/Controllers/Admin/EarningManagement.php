<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\ServiceTax;
use App\User;
use App\Model\UsrRequestGig;
use App\Model\ServiceCategoryAddress;

class EarningManagement extends Controller
{
    public function EarningManagemnt(){
        // dd("test");

    	$servicetax = ServiceTax::orderBy('id', 'Desc')->get();
        $province = User::select('province')->groupBy('province')->get();
        $CompleteGig = UsrRequestGig::whereIn('status', array('9','10'))->get();
        
        foreach($CompleteGig as $key=>$rowcompleate){

           $CompleteGig[$key]->serviceTax  = ServiceTax::where('id', '1')->first();
           $CompleteGig[$key]->serviceType = ServiceCategoryAddress::where('id', $rowcompleate->gig_id)->first();
        
        }
    	return view('admin.test-management')->with('servicetax', $servicetax)->with('province', $province)->with('CompleteGig', $CompleteGig);

        // return view('admin.test-management');
    }

    public function AddServiceTax(Request $request){
    	// dd($request->all());
    	ServiceTax::create([
    		'servicetax' => $request->servicetax,
    	]);
    	return redirect()->back()->with('success_message','service tax added successfully');
    }

    public function admintax(){
        $data = ServiceTax::where('id', '1')->get()->toArray();
        return $data;    
    }

    public function UpdateServiceTax(Request $request){
        ServiceTax::where('id', $request->editId)->update([
            'servicetax' => $request->servicetax,
        ]);
        return redirect()->back()->with('success_message','service tax added successfully');
    }
    
    public function EariningFilter(Request $request){

        // dd($request->all());
        $start_date = $request->from;
        $end_date = $request->to;
        $province = $request->province;
        $city = $request->city;
        $status = $request->status;
        
        $Startdate = strtr($start_date, '-', '/');
        $startEditdate =  date('Y-m-d', strtotime($Startdate));
               
        $Enddate = strtr($end_date, '-', '/');
        $endEditdate =  date('Y-m-d', strtotime($Enddate));
        
        // dd($startEditdate);

        if($status){
            $CompleteGig = UsrRequestGig::select('user_request_gig.gig_id', 'user_request_gig.User_ID', 'user_request_gig.Seller_ID', 'user_request_gig.updated_at', 'user_request_gig.offerAmount', 'user_request_gig.status')->orderBy('user_request_gig.id', 'Desc');  
        }else{
            $CompleteGig = UsrRequestGig::select('user_request_gig.gig_id', 'user_request_gig.User_ID', 'user_request_gig.Seller_ID', 'user_request_gig.updated_at', 'user_request_gig.offerAmount', 'user_request_gig.status')->whereIn('user_request_gig.status', array('9','10'));  
        }
        
        if($start_date != ''){
            $CompleteGig->whereBetween('user_request_gig.registerDate', [$startEditdate, $endEditdate]);
        }
        if($province != ''){            
            $CompleteGig->where('users.province',$province);            
        } 
        if($city != ''){
            $CompleteGig->where('users.city',$city);            
        } 
        if($status != ''){
            $CompleteGig->where('user_request_gig.status',$status);            
        } 
        
        $CompleteGig = $CompleteGig         
        ->leftJoin('users', function($join) {
        $join->on('user_request_gig.User_ID', '=', 'users.id');
        })
        ->get();
 
        // dd($CompleteGig);

        // foreach($OngoingGig as $key=>$rowongoingig){
        //     $OngoingGigcategoryId = ServiceCategoryAddress::where('id', $rowongoingig->gig_id)->first();

        //     $OngoingGig[$key]->subcategoryName = City::where('id', $OngoingGigcategoryId->cityName)->first();

        //     $OngoingGig[$key]->serviceName = ServiceCategory::where('id', $OngoingGigcategoryId->cat_id)->first();

        //     $OngoingGig[$key]->GigDetails = ServiceCategoryAddress::where('id', $rowongoingig->gig_id)->first();
        // }

        $servicetax = ServiceTax::orderBy('id', 'Desc')->get();
        $province = User::select('province')->groupBy('province')->get();
        // $CompleteGig = UsrRequestGig::whereIn('status', array('9','10'))->get();

        foreach($CompleteGig as $key=>$rowcompleate){
           $CompleteGig[$key]->serviceTax  = ServiceTax::where('id', '1')->first();
           $CompleteGig[$key]->serviceType = ServiceCategoryAddress::where('id', $rowcompleate->gig_id)->first();
        }

        // dd($CompleteGig);

        return view('admin.Earning-management')->with('servicetax', $servicetax)->with('province', $province)->with('CompleteGig', $CompleteGig);
    }


}
