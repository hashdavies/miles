<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\UsrRequestGig;
use App\Model\ServiceCategoryAddress;
use App\Model\City;
use App\Model\ServiceCategory;
use App\Model\UserRating;

class GigManagementController extends Controller
{
    public function OngoingGig(){
    	$OngoingGig =  UsrRequestGig::with('GigDetails')->whereIn('status', array('5','7','8'))->orderBy('id', 'Desc')->get();

        foreach($OngoingGig as $key=>$rowongoingig){
            $OngoingGigcategoryId = ServiceCategoryAddress::where('id', $rowongoingig->gig_id)->first();

            $OngoingGig[$key]->subcategoryName = City::where('id', $OngoingGigcategoryId->cityName)->first();

            $OngoingGig[$key]->serviceName = ServiceCategory::where('id', $OngoingGigcategoryId->cat_id)->first();

            $OngoingGig[$key]->GigDetails = ServiceCategoryAddress::where('id', $rowongoingig->gig_id)->first();
        }
        // dd("dfasd");    	
        // $city = ServiceCategoryAddress::groupBy('cityName')->get();
        $province = ServiceCategoryAddress::groupBy('proviance')->get();

    	return view('admin.ongoing-gig')->with('OngoingGig', $OngoingGig)->with('province', $province);
    }

    public function CompleateGig(){
        $completedGig =  UsrRequestGig::with('GigDetails')->where('status', '9')->orderBy('id', 'Desc')->get();
        foreach($completedGig as $key=>$rowongoingig){

            $completedGig[$key]->Ratings = UserRating::where('gig_id', $rowongoingig->gig_id)->first();            

            $OngoingGigcategoryId = ServiceCategoryAddress::where('id', $rowongoingig->gig_id)->first();

            $completedGig[$key]->subcategoryName = City::where('id', $OngoingGigcategoryId->cityName)->first();

            $completedGig[$key]->serviceName = ServiceCategory::where('id', $OngoingGigcategoryId->cat_id)->first();

            $completedGig[$key]->GigDetails = ServiceCategoryAddress::where('id', $rowongoingig->gig_id)->first();
        }
        $city = ServiceCategoryAddress::groupBy('cityName')->get();
        $province = ServiceCategoryAddress::groupBy('proviance')->get();
    	return view('admin.completed-gig')->with('completedGig', $completedGig)->with('city', $city)->with('province', $province);
    }

    public function PendingGig(){
        $pendingGig =  UsrRequestGig::with('GigDetails')->whereIn('status', array('0','2','4'))->orderBy('id', 'Desc')->get();

        foreach($pendingGig as $key=>$rowongoingig){
            $OngoingGigcategoryId = ServiceCategoryAddress::where('id', $rowongoingig->gig_id)->first();

            $pendingGig[$key]->subcategoryName = City::where('id', $OngoingGigcategoryId->cityName)->first();

            $pendingGig[$key]->serviceName = ServiceCategory::where('id', $OngoingGigcategoryId->cat_id)->first();

            $pendingGig[$key]->GigDetails = ServiceCategoryAddress::where('id', $rowongoingig->gig_id)->first();

            $pendingGig[$key]->UserRating = ServiceCategoryAddress::where('id', $rowongoingig->gig_id)->first();
        }        
        $city = ServiceCategoryAddress::groupBy('cityName')->get();
        $province = ServiceCategoryAddress::groupBy('proviance')->get();
        return view('admin.pending-gig')->with('pendingGig', $pendingGig)->with('city', $city)->with('province', $province);
    }

    public function CancelledGig(){

    	$cancelGig =  UsrRequestGig::with('GigDetails')->whereIn('status', array('3','6'))->orderBy('id', 'Desc')->get();
        foreach($cancelGig as $key=>$rowongoingig){
            $OngoingGigcategoryId = ServiceCategoryAddress::where('id', $rowongoingig->gig_id)->first();

            $cancelGig[$key]->subcategoryName = City::where('id', $OngoingGigcategoryId->cityName)->first();

            $cancelGig[$key]->serviceName = ServiceCategory::where('id', $OngoingGigcategoryId->cat_id)->first();

            $cancelGig[$key]->GigDetails = ServiceCategoryAddress::where('id', $rowongoingig->gig_id)->first();
        }
        $city = ServiceCategoryAddress::groupBy('cityName')->get();
        $province = ServiceCategoryAddress::groupBy('proviance')->get();
    	return view('admin.cancelled-gig')->with('cancelGig', $cancelGig)->with('city', $city)->with('province', $province);
    }

    public function OngoingGigFilter(Request $request){
    	// dd($request->all());    	
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $city = $request->city;
        $status = $request->status;
        $province = $request->province;

        // dd($start_date);
        $Startdate = strtr($start_date, '-', '/');
        $startEditdate =  date('Y-m-d', strtotime($Startdate));
               
        $Enddate = strtr($end_date, '-', '/');
        $endEditdate =  date('Y-m-d', strtotime($Enddate));
        // dd($endEditdate);

        $OngoingGig = ServiceCategoryAddress::where('status', '4'); 
       
        if($start_date != ''){
            $OngoingGig->whereBetween('registerDate', [$startEditdate, $endEditdate]);
        }
        if($city != ''){
            $OngoingGig->where('cityName',$city);            
        } 
        if($province != ''){            
            $OngoingGig->where('proviance',$province);            
        }         
        // dd($OngoingGig);
        $OngoingGig = $OngoingGig
        ->leftJoin('user_request_gig', function($join) {
        $join->on('category_service_address.id', '=', 'user_request_gig.gig_id');
        })
        ->get();

        foreach($OngoingGig as $key=>$rowongoingig){

            $OngoingGigcategoryId = ServiceCategoryAddress::where('id', $rowongoingig->gig_id)->first();

            $OngoingGig[$key]->subcategoryName = City::where('id', $OngoingGigcategoryId->cityName)->first();

            $OngoingGig[$key]->serviceName = ServiceCategory::where('id', $OngoingGigcategoryId->cat_id)->first();

            $OngoingGig[$key]->GigDetails = ServiceCategoryAddress::where('id', $rowongoingig->gig_id)->first();
        }

        $city = ServiceCategoryAddress::groupBy('cityName')->get();
        $province = ServiceCategoryAddress::groupBy('proviance')->get();
        return view('admin.ongoing-gig')->with('OngoingGig', $OngoingGig)->with('city', $city)->with('province', $province);       
    }


    public function CompleateGigFilter(Request $request){
    	
    	$start_date = $request->start_date;
        $end_date = $request->end_date;
        $city = $request->city;
        $status = $request->status;
        $province = $request->province;

        $Startdate = strtr($start_date, '-', '/');
        $startEditdate =  date('Y-m-d', strtotime($Startdate));
               
        $Enddate = strtr($end_date, '-', '/');
        $endEditdate =  date('Y-m-d', strtotime($Enddate));
       
        $completedGig = ServiceCategoryAddress::where('status', '4'); 
             
        if($start_date != ''){
            $completedGig->whereBetween('registerDate', [$startEditdate, $endEditdate]);
        }
        if($city != ''){
            $completedGig->where('cityName',$city);            
        } 
        if($province != ''){
            $completedGig->where('proviance',$province);            
        }       
                         
        $completedGig = $completedGig
        ->leftJoin('user_request_gig', function($join) {
        $join->on('category_service_address.id', '=', 'user_request_gig.gig_id');
        })
        ->get();

        foreach($completedGig as $key=>$rowcompleate){
            $OngoingGigcategoryId = ServiceCategoryAddress::where('id', $rowcompleate->gig_id)->first();

            $completedGig[$key]->subcategoryName = City::where('id', $OngoingGigcategoryId->cityName)->first();

            $completedGig[$key]->serviceName = ServiceCategory::where('id', $OngoingGigcategoryId->cat_id)->first();

            $completedGig[$key]->GigDetails = ServiceCategoryAddress::where('id', $rowcompleate->gig_id)->first();
        }

        $city = ServiceCategoryAddress::groupBy('cityName')->get();
        $province = ServiceCategoryAddress::groupBy('proviance')->get();
    	return view('admin.completed-gig')->with('completedGig', $completedGig)->with('city', $city)->with('province', $province);
    }

    public function CancelGigFilter(Request $request){
    	$start_date = $request->start_date;
        $end_date = $request->end_date;
        $city = $request->city;
        $status = $request->status;
        $province = $request->province;

        $Startdate = strtr($start_date, '-', '/');
        $startEditdate =  date('Y-m-d', strtotime($Startdate));
        
        $Enddate = strtr($end_date, '-', '/');
        $endEditdate =  date('Y-m-d', strtotime($Enddate));
        $cancelGig = ServiceCategoryAddress::where('status', '6'); 
        
        if($start_date != ''){
            $cancelGig->whereBetween('registerDate', [$startEditdate, $endEditdate]);
        }
        if($city != ''){
            $cancelGig->where('cityName',$city);            
        }
        if($province != ''){
            $cancelGig->where('proviance',$province);            
        }

        if($status != ''){
            $cancelGig->where('is_approved',$status);
        }                  
        $cancelGig = $cancelGig
        ->leftJoin('user_request_gig', function($join) {
        $join->on('category_service_address.id', '=', 'user_request_gig.gig_id');
        })
        ->get();
        foreach($cancelGig as $key=>$rowcancel){
            $OngoingGigcategoryId = ServiceCategoryAddress::where('id', $rowcancel->gig_id)->first();
            $cancelGig[$key]->subcategoryName = City::where('id', $OngoingGigcategoryId->cityName)->first();
            $cancelGig[$key]->serviceName = ServiceCategory::where('id', $OngoingGigcategoryId->cat_id)->first();
            $cancelGig[$key]->GigDetails = ServiceCategoryAddress::where('id', $rowcancel->gig_id)->first();
        }
        $city = ServiceCategoryAddress::groupBy('cityName')->get();
        $province = ServiceCategoryAddress::groupBy('proviance')->get();
    	return view('admin.cancelled-gig')->with('cancelGig', $cancelGig)->with('city', $city)->with('province', $province);
    }

    public function OngoingCity($ProvinceName){

        // dd($ProvinceName);
        $cities = ServiceCategoryAddress::groupBy('cityName')->where('proviance', $ProvinceName)->get(); 
        
        foreach($cities as $key=>$rowcity){
            $cities[$key]->city = City::where('id', $rowcity->cityName)->first();
        }
        return response()->json($cities);
    }

   
}
