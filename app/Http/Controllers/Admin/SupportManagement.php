<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\UserSupport;
use App\Model\SellerSupport;

class SupportManagement extends Controller
{
    public function UserSupprt(){
    	
    	$userSupport = UserSupport::select('user_id')->get();
    	// fcm_token
    	return view('admin.users-support')->with('userSupport', $userSupport);
    }

    public function ServiceProvidersSupport(){
    	$sellerSupport = SellerSupport::select('seller_id')->distinct()->get();
    	return view('admin.service-providers-support')->with('sellerSupport', $sellerSupport);
    }



}
