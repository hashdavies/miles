<?php

namespace App\Http\Controllers\Seller;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Seller;

class HomeController extends Controller
{
    public function index()
    {
    	$user =  Auth::guard('seller')->id();
    	$sellername = Seller::where('id', $user)->first();
    	// dd($sellername->fullName); 
    	return view('seller.dashboard')->with('sellername', $sellername);
    }

    public function dashbord()
    {
    	return view('seller.dashbord');
    }
}
