<?php

namespace App\Http\Controllers\Seller;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Seller;
use DB;
use Validator;
use Exception;
use Twilio\Rest\Client;
use App\Model\SellerBanner;

class LoginController extends Controller
{
    public function signup(){
    	  return view('seller/signup');
    }

    public function BasicInformation(){
        $sellerId = Auth::guard('seller')->user()->id;

        $basicinfo = seller::where('id', $sellerId)->first();
        $sellerimages = SellerBanner::where('seller_id', $sellerId)->get();
        return view('seller.basic-information')->with('basicinfo', $basicinfo)->with('sellerimages', $sellerimages);
    }

    public function AddBasicInformation(Request $request){

        // dd($request->all());
        
        $sellerId = Auth::guard('seller')->user()->id; 
        Seller::where('id', $sellerId)->update([
          'aboutUs' => $request->aboutUs,
          'description' => $request->description,
          'facebook_link' =>$request->facebook,
          'instagram_link' =>$request->Instagram,
          'twitter' => $request->twitter,
          'email_address' =>$request->gmail,
          'phone_number' =>$request->telephone,
        ]);
        // dd($request->all());
        if(!empty($request->file('seller_images'))){
                       
            $images=array();            
            if($files=$request->file('seller_images')){

                foreach($files as $file){ 
                  $name='public/SellerBanner/'.$file->getClientOriginalName();
                  $file->move('public/SellerBanner',$name);
                  $images[]=$name;

                  DB::table('seller_banner')->insert([
                   'seller_id' => $sellerId,
                   'seller_banner' => $name,
                  ]);
                }
            }

        }else{
            SellerBanner::where('seller_id', $sellerId)->update([
                'seller_id' => $sellerId,
            ]);
        }
        return redirect('seller/dashboard')->with('success_message', 'Basic information updated successfully');
    }


    public function DeleteMultipleImage($id){
      //dd($id);
      $data = SellerBanner::where('id', $id)->delete();
      return $data;
    }


    public function logout(Request $request)
    {    	  
        Auth::guard('seller')->logout();        
        $request->session()->invalidate();
        return redirect('login/seller');
    } 

    public function ForgotPassword(){
        return view('seller/forgot-password');
    }

    public function SendForgetLink(Request $request){   
        $this->validate($request, [
            'mobileNumber' => 'required|numeric',
            'countryCode' => 'required'
        ],[
          'mobileNumber.required' => 'Mobile number is required'
        ]);  
        $seller = Seller::where('mobileNumber',$request->mobileNumber)->where('countryCode', $request->countryCode)->first();
        
        if(empty($seller)){
            return redirect()->back()
                     ->withInput($request->only('mobileNumber'))
                     ->with(['error_message' => 'Invalid mobile number']);        
        }else{ 
            $mobile_no = $request->countryCode.$request->mobileNumber; 
            $tokenvar = md5($request->mobileNumber); 
            // $otp = rand(9999,1000);
            $otp = '1234';
            session()->put('SessionOTP', '1234');
            session()->put('MobileNumber', $request->mobileNumber);
            session()->put('CountryCode', $request->countryCode);
            
            // $otpsave = session()->get('CountryCode');
            // $SessionDestroy = session()->flash('SessionOTP');
            // dd($otpsave);  
          
            try{
              // $sid = "AC37468cf1a94b4e76eeec9eb3d327869e";
              // $token = "ce51eb209281d4e59b1f5f96854c1551";

              // $client = new Client($sid, $token);
              // // dd("sadasd");
              // $client->messages->create(                   

              //       $mobile_no, array(
              //       'from' => '+15154542323',
              //       'body' => 'Thankyou for Register with A2A Express Your Otp Is' .''. $otp
              //     )
              //     );     
                                
              $removeToken = DB::table('password_resets')->where(['email'=>$request->mobileNumber])->delete();
              
              DB::table('password_resets')->insert(['email'=>$request->mobileNumber,'token'=>md5($request->mobileNumber)]);
              $token = md5($request->mobileNumber);
            } catch(Exception $e){
                $response = [
                  'message' => $e->getMessage()
                ];                        
                return $response;
            }            
            return redirect('seller/reset-password/' .$token)->with(['success_message' => 'OTP Send to your mobile Number']);
        }
    }
    
    public function ResetPassword($token){
        // $saveOtp = session()->get('SessionOTP');
        // $otpmobile = session()->get('MobileNumber');
        // $countrycoDe = session()->get('CountryCode');  
        // $SessionDestroy = session()->flash('SessionOTP');
        // dd($saveOtp); 
        $selectpwd = DB::table('password_resets')->where('token',$token)->first();
        if(empty($selectpwd)){
            return redirect('seller/forgot-password')->with(['error_message' => 'Invalid Token']);
        }else{
           return view('seller/reset-password')->with(['email'=>$selectpwd->email]);
        }       
    }
    

    public function resendOtp(){

      $SessionDestroy = session()->flash('SessionOTP');
      $otpmobile = session()->get('MobileNumber');
      $countrycode = session()->get('CountryCode');
      $otp = session()->get('SessionOTP');      
      session()->put('resendSessionOTP', '1234');
      $resendotp = session()->get('resendSessionOTP');
      return redirect()->back()->with('success_message', 'OTP resent successfully');
    }

    public function VerifyOtp(){
        return view('seller/verify-otp');
    }   

    public function UpdateResetPassword(Request $request){   

        $this->validate($request, [
            'otp'=>'required|integer',
            'password'=>'required|min:8|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/',
            'confirm_password'=>'required|same:password'
        ],[
          'otp.integer' => 'OTP must be 4 digit number',
          'otp.required' => 'OTP is required',
          'password.required' => 'New Password is required',
          'password.regex'  => 'Password should contains between 8 to 15 characters, and contains at least 1 lower case, 1 upper case, 1 numeric, 1 special character and no whitespace',
          'confirm_password.required' => 'New confirm password is required',
          'confirm_password.same' => 'New confirm password do not match',     
        ]);        
        $saveresendOtp = session()->get('resendSessionOTP');
        $saveOtp = session()->get('SessionOTP');
        $otpmobile = session()->get('MobileNumber');
        
        if($saveresendOtp == $request->otp){
          Seller::where('mobileNumber',$otpmobile)->update(['password'=>bcrypt($request->password)]);
          session()->flash('SessionOTP');
          session()->flash('MobileNumber');                 
          return redirect('login/seller')->with(['success_message' => 'Password Reset Successfully']); 
        }elseif($saveOtp == $request->otp){

          Seller::where('mobileNumber',$otpmobile)->update(['password'=>bcrypt($request->password)]);
          session()->flash('SessionOTP');
          session()->flash('MobileNumber');                 
          return redirect('login/seller')->with(['success_message' => 'Password Reset Successfully']); 
        }else{          
          return redirect()->back()->with('error_message', 'Invalid OTP');
        }               
    }
}