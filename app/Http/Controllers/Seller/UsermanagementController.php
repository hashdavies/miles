<?php

namespace App\Http\Controllers\Seller;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Seller;
use App\User;
use App\Model\PurchaseLoyalityOffer;
use App\Model\UserLoyalityPoint;


class UsermanagementController extends Controller
{
    public function userlist(){
    
    $companyId = Auth::guard('seller')->user()->id; 
    // dd($companyId);

    $userData = User::join('purchase_loyality_offer', 'users.id', '=', 'purchase_loyality_offer.Pur_User_ID')->select('users.id','registerDate','gender','dob', 'Pur_loyaltyOfrName', 'Pur_activationDate','Pur_Seller_ID')->orderBy('purchase_loyality_offer.id', 'desc')->where('Pur_Seller_ID', $companyId)->where('status', '1')->get();

    $userCity = User::select('city')->groupBy('city')->get();
      return view('seller.customer-management')->with('userData', $userData)->with('userCity', $userCity);
    }
    

    public function venderlist(){
    	return view('seller.vendor_management');
    }

    public function addVender(){
    	return view('seller.add_vender');
    }

    public function sellerProfile(){

    	$userId = Auth::guard('seller')->id();
    	$sellerProfile = Seller::where('id', $userId)->first();
    	//dd($sellerProfile);
    	return view('seller.profile')->with('sellerProfile', $sellerProfile);
    }

    public function changePassword(Request $request){
    	//dd($request->all());
      
      $company_password = Auth::guard('seller')->user()->password; 
      $companyId = Auth::guard('seller')->user()->id;               
      // dd($companyId);  
      
      $this->validate($request, 
        [
        'old_password'     => 'required',
        'new_password'     => 'required|min:8|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/',
        'confirm_password' => 'required|same:new_password'
        ],
        [
          'old_password.required' => 'mandatory field required',
          'new_password.required' => 'please enter valid password',
          'new_password.min' => 'password must be at least 8 characters',
          'new_password.regex' => 'your password must be more than 8 characters long, should contain at-least one uppercase, one lowercase, one numeric and one special character.',
        ]
      );
      
      if($request->old_password == $request->new_password){
      return redirect()->back()->with('wrongpassword', 'Current password and old password doesn same'); 
      }else{
        if(!\Hash::check($request->old_password, $company_password)){      
          return back()->with('wrongpassword','Your old password is incorrect');
        }else{
          Seller::where('id', $companyId)->update([
            'password' => Hash::make($request->new_password)
          ]);

          // $data = array(            
          //   'email' => $companyemail,           
          // );

          // $from =array();
          // $from['name'] = 'WeSoar';
          // $from['address'] = 'technology.operations@wesoar.ai';
          // $from['email_to'] = $companyemail;

          // Mail::send('emails.changepassword',$data, function ($message) use ($from) {
          //    $message->from($from['address'],$from['name']);
          //    $message->to($from['email_to']);
          //    $message->subject('Wesoar: Update Password');
          // });
          return redirect()->back()->with('successpassword', 'Password Updated Successfully'); 
        }
      } 
    }

    // public function blockUser($id, $type){
    //  	$data = User::find($id);    	
    // 	$data->status = $type;
    // 	$data->save();
    // 	if($type==0){
    // 		$msg = "Category unblock successfully.";
    // 	}
    // 	elseif($type==1){
    // 		$msg = "Category block successfully.";
    // 	}
    // 	return redirect('seller/customer-management')->with('success_message',$msg);
    // }

    // public function DeleteUser($id){
    // 	User::where('id', $id)->delete();
    // 	return redirect('seller/customer-management')->with('deleteuser', 'User Deleted Successfully');
    // }

    public function ViewCustomerDetails($id){
      
      // dd($id);
      
      $userInfo = User::where('id', $id)->first();
      return view('seller.customer-details')->with('userInfo', $userInfo);
    }

    
    public function ActivatedLoyaltyOffers($id){
      
      $ActivateOffer = PurchaseLoyalityOffer::where('Pur_User_ID', $id)->orderBy('id', 'Desc')->get();
      $UserData = User::where('id', $id)->first();
      return view('seller.activated-loyalty-offers')->with('ActivateOffer', $ActivateOffer)->with('UserData', $UserData);
    }


    public function ViewActivatedLoyaltyOffers($OfrID, $UserID){

      $OfferDetails = PurchaseLoyalityOffer::where('id', $OfrID)->first();
      $Userdetails = User::where('id', $UserID)->first();
      
      $SellerID = Auth::guard('seller')->id();

      $UserPoint = UserLoyalityPoint::where('Seller_ID', $SellerID)->where('Loyality_OfferID', $OfrID)->where('User_iD', $UserID)->get();
      
      $TotalPoints = UserLoyalityPoint::where('Seller_ID', $SellerID)->where('Loyality_OfferID', $OfrID)->where('User_iD', $UserID)->sum('Loyality_Point');

      return view('seller.view-activated-loyalty-offers')->with('OfferDetails', $OfferDetails)->with('Userdetails', $Userdetails)->with('UserPoint', $UserPoint)->with('TotalPoints', $TotalPoints);
    }


    public function CustomerMgmtFilter(Request $request){

        // dd("test");
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $gender = $request->gender;
        $age = $request->age;
        $city = $request->city;

        // dd($start_date);
        
        $Startdate = strtr($start_date, '/', '-');
        $startEditdate =  date('Y-m-d', strtotime($Startdate));

        $Enddate = strtr($end_date, '/', '-');
        $endEditdate =  date('Y-m-d', strtotime($Enddate));
       
        $userData = User::join('purchase_loyality_offer', 'users.id', '=', 'purchase_loyality_offer.Pur_User_ID')->select('users.id','registerDate','gender','dob', 'Pur_loyaltyOfrName', 'Pur_activationDate')->orderBy('purchase_loyality_offer.id', 'desc')->where('status', '1');


        if($start_date != ''){
            $userData->whereBetween('Pur_activationDate', [$startEditdate, $endEditdate]);
        }
        if($gender != ''){
            $userData->where('gender',$gender);
        }
        
        // if($age != ''){

        //     //$userData->where('age',$age);
        //     $Age = User::where('status', '1')->get();
        //     foreach($Age as $rowage){

        //       dd($rowage->dob);

        //     }            
        // }

        if($city != ''){
          // dd($city);
            $userData->where('city',$city);
        }       
        $userData = $userData->get();
        $userCity = User::select('city')->groupBy('city')->get();   
        return view('seller.customer-management')->with('userData', $userData)->with('userCity', $userCity);
    }

}