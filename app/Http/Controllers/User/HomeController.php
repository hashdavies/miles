<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\User;
use Redirect;
use Mail;
use Hash;
use App\Model\WebUser;
use App\Model\ContactUser;
use App\Model\About;
use App\Model\Faq;
use Config;
use App\Model\CmsPage;

class HomeController extends Controller
{

  // public $mailchimp;
  

  // public function __construct(\Mailchimp $mailchimp)
  // {
  //     $this->mailchimp = $mailchimp;
  // }

  public function CmsPage($id){
    $page = CmsPage::where('id', $id)->first();
    return view('user.page')->with('page', $page);
  }
   
  public function About(){
    // dd("sadasd");

    $about = About::get();
    return view('user.about')->with('about', $about);
  }

  public function Commitment(){
    
    // dd("ssda");

    $commitment = About::get();
    return view('user.commitment')->with('commitment', $commitment);
  }

  public function Faqs(){
    $faq = Faq::orderBy('id','asc')->get();
    return view('user.faqs')->with('faq', $faq);
  }

  public function Covid19(){
    $covid = About::get();
    return view('user.covid-19')->with('covid', $covid);
  }

  public function AddUser(Request $request){
    // dd($request->all());

      // try {
      //     $listId = 'eeecc6e917';
      //     $options = [
      //       'list_id'   => $listId,
      //       'subject' => 'Myles App Query',
      //       'from_name' => $request->input('name'),
      //       'from_email' => 'a5solutionscanada@gmail.com',
      //       'to_name' => $request->input('email')
      //     ];
          // $options = [
          //   'list_id'   => $listId,
          //   'subject' => 'mylesapp',
          //   'from_name' => 'testname',
          //   'from_email' => 'kuldeeptomer777@gmail.com',
          //   'to_name' => 'tomerkuldeep08@gmail.com',
          // ];

          // $content = [
          //   'html' => $request->input('role'),
          //   'text' => strip_tags($request->input('role'))
          // ];

          // $campaign = $this->mailchimp->campaigns->create('regular', $options, $content);
          // $this->mailchimp->campaigns->send($campaign['id']);

        //   WebUser::create([
        //     'name' => $request->name,
        //     'email' => $request->email,
        //     'city' => $request->city,
        //     'role' => $request->role,
        //   ]);
        //   return redirect()->back()->with('success_message','Thanks for register with us');
        // } catch (Exception $e) {          
        //   return redirect()->back()->with('success_message','Error from MailChimp');
        // }

        WebUser::create([
            'name' => $request->name,
            'email' => $request->email,
            'city' => $request->city,
            'role' => $request->role,
        ]);
        return redirect()->back()->with('success_message','Thanks for register with us');
  }


  public function addContactUser(Request $request){
     
      // try {
      //     $listId = 'eeecc6e917';
      //     $options = [
      //       'list_id'   => $listId,
      //       'subject' => 'Myles App Query',
      //       'from_name' => $request->input('conname'),
      //       'from_email' => 'a5solutionscanada@gmail.com',
      //       'to_name' => $request->input('conemail')
      //     ];
          // $options = [
          //   'list_id'   => $listId,
          //   'subject' => 'mylesapp',
          //   'from_name' => 'testname',
          //   'from_email' => 'kuldeeptomer777@gmail.com',
          //   'to_name' => 'tomerkuldeep08@gmail.com',
          // ];

        //   $content = [
        //     'html' => $request->input('conmessage'),
        //     'text' => strip_tags($request->input('conmessage'))
        //   ];

        //   $campaign = $this->mailchimp->campaigns->create('regular', $options, $content);
        //   $this->mailchimp->campaigns->send($campaign['id']);

        //   ContactUser::create([
        //     'conname' => $request->conname,
        //     'conemail' => $request->conemail,
        //     'conmobileno' => $request->conmobileno,
        //     'concity' => $request->concity,
        //     'conmessage' => $request->conmessage,
        //   ]);
        //   return redirect()->back()->with('success_message','Thanks for register with us');
        // } catch (Exception $e) {   
        //   return redirect()->back()->with('success_message','Error from MailChimp');
        // }

        ContactUser::create([
            'conname' => $request->conname,
            'conemail' => $request->conemail,
            'conmobileno' => $request->conmobileno,
            'concity' => $request->concity,
            'conmessage' => $request->conmessage,
        ]);
        return redirect()->back()->with('success_message','Thanks for register with us');
    
    }



    // ContactUser::create([
    //   'conname' => $request->conname,
    //   'conemail' => $request->conemail,
    //   'conmobileno' => $request->conmobileno,
    //   'concity' => $request->concity,
    //   'conmessage' => $request->conmessage,
    // ]);


       // $this->validate($request, [
       //  'subject' => 'required',
       //  'to_email' => 'required',
       //  'from_email' => 'required',
       //  'message' => 'required',
       //  ]);

        // try {

          // $options = [
          //   'list_id'   => $this->listId,
          //   'subject' => 'MylesContact',
          //   'from_name' => $request->conemail,
          //   'from_email' => 'kuldeeptomer777@gmail.com',
          //   'to_name' => $request->conemail
          // ];          

        //   $options = [
        //     'list_id'   => $this->listId,
        //     'subject' => "myles send mail",
        //     'from_name' => $request->input('conemail'),
        //     'from_email' => 'kuldeeptomer777@gmail.com',
        //     'to_name' => $request->input('conemail')
        //   ];

        //   $content = [
        //     'html' => $request->conmessage,
        //     'text' => strip_tags($request->conmessage)
        //   ];

        //   // dd('regular', $options, $content);

        //   $campaign = $this->mailchimp->campaigns->create('regular', $options, $content);
          
        //   // dd(campaigns->create('regular', $options, $content));

        //   $this->mailchimp->campaigns->send($campaign['id']);

        //   $mailchimp->campaigns->send($campaign['id']);

        //   return redirect()->back()->with('success','send campaign successfully');
          
        // } catch (Exception $e) {
        //   return redirect()->back()->with('error','Error from MailChimp');
        // }


    // return redirect()->back()->with('success_message', 'Thanks for register with us');


  


  //   public function sendotp() {
  //   	return view('user.otp');
  //   }

  //   public function verify(Request $request)
  //   {
  //   	$this->validate($request, ['otp1' => 'required|numeric',
  //   		'otp2' =>'required|numeric',
  //   		'otp3' =>'required|numeric',
  //   		'otp4' =>'required|numeric',
  //   	]);

  //   	$collection = collect([
		//     ['otp' => $request->otp1],
		//     ['otp' => $request->otp2],
		//     ['otp' => $request->otp3],
		//     ['otp' => $request->otp4],
		// ]);
		
		// $data = $collection->implode('otp', '');		
		// $user_id = Auth::user()->id;		
		// $chk = User::where('id', $user_id)->where('otp', $data)->first();    	
  //   	//dd($chk->id);

  //   	if($chk){
  //   		User::where('id', $chk->id)->update([
  //   			'is_verified' => '1'
  //   		]);
  //   		return redirect('user/create-profile')->with('msg', 'OTP Successfully Match');
  //   	}else{
  //   		return redirect::back()->with('msg', 'Invalid OTP Please try again');
  //   	}
  //   }

  //   public function createprofile(){

  //   	$user_id = Auth::user()->id;
  //   	$profiledata = User::find($user_id);
  //   	return view('user/create-profile')->with('profiledata', $profiledata);
  //   }

  //   public function saveprofile(Request $request){
    	
  //   	$this->validate($request, [
  //   		'username' => 'required|string',
  //   		'email_number' => 'required|string',
  //  		 	'country' => 'required|string',
  //  		 	'city' => 'required|string',   		 	 
  //  		 	'shipping_address' => 'required|string',
  //  		 	'street' => 'required|string',
  //  		 	'zip_code' => 'required|numeric',
  //  		 	//'profile_image' => 'required|image|mimes:jpg, png, gif, svg, jpeg|max:2048',   		 
  //      	]);
        
  //   	//dd($request->user_id);
  //   	if(!empty($request->profile_image)){

  //   		$imageName = time(). '.' .$request->file('profile_image')->getClientOriginalExtension();
	 //    	$request->file('profile_image')->move(base_path(). '/public/profilepic/', $imageName);

	 //       	User::where('id', $request->user_id)->update([
	 //       		'username' => $request->username,
	 //       		'email_number' => $request->email_number,
	 //       		'country' => $request->country,
	 //       		'city' => $request->city,
	 //       		'mobile_number' => $request->mobile_number,
	 //       		'alt_mobile_number' => $request->alt_mobile_number,
	 //       		'shipping_address' => $request->shipping_address,
	 //       		'street' => $request->street,
	 //       		'zip_code' => $request->zip_code,
	 //       		'profile_image' =>$imageName
	 //       	]);

  //   	}else{

		// 	User::where('id', $request->user_id)->update([
	 //       		'username' => $request->username,
	 //       		'email_number' => $request->email_number,
	 //       		'country' => $request->country,
	 //       		'city' => $request->city,
	 //       		'mobile_number' => $request->mobile_number,
	 //       		'alt_mobile_number' => $request->alt_mobile_number,
	 //       		'shipping_address' => $request->shipping_address,
	 //       		'street' => $request->street,
	 //       		'zip_code' => $request->zip_code,	       		
	 //       	]);			
  //   	}
  //      	return redirect('/home');  	
  //   }


  //   public function forgotpassword(){
  //   	return view('user/forgot_password');
  //   }


  //   public function userreset(Request $request){

  //       // dd("dgfdgfd");
  //   	$chk = User::where('email_number', $request->email_number)->first();

  //   	if(!empty($chk)){

  //   		if(is_numeric($request->email_number)){
	 //    		$this->validate($request, ['email_number' => 'required|string|min:9|max:10'],[
	 //    			'email_number.required' => 'Enter Valid Mobile Number'
	 //    		]);
	 //            $otp = "1234";
	 //            // dd($otp);

	 //            //return view('user/otp');

	 //        }else{

	 //            $this->validate($request, ['email_number' => 'required|email'],[
	 //            	'email_number' => 'Enter Valid Email Address'
	 //            ]);
	 //            $otp =  "1235"; 
  //               // dd();
	 //            $data1=array();         
	 //            $data1['email']=$request->email_number;
	 //            $data1['name']=$request->username;
	 //            $data1['otp'] =$otp;

	 //            $from = array();          
	 //            $from['email_to']=$request->email_number;
	 //            $from['name']='Verify Account'; 

	 //            Mail::send('emails.otp-mail', $data1, function ($message) use ($from) {
	 //            $message->to($from['email_to']);
	 //            $message->subject('Resset Password : OTP');
	 //            });   
	 //           // return view('user/otp');
	 //        }
            
	 //        //dd($chk->id);
	 //        User::where('id', $chk->id)->update(['otp' => $otp]);
	 //        //return view('user/otp');
	 //        $id = encrypt($chk->id);
	 //        // dd($id);
	 //        return redirect('user/otpverify/'.$id);
	      
  //   	}else{
  //   		return redirect::back()->with('msg' , 'Invalid Email Address');
  //   	} 
  //   }
     
  //   public function otpverification($id){
		    	
  //   	$ids = decrypt($id);
  //   	//dd($ids);
  //   	$otp = '12';

  //   	User::where('id', $ids)->where('otp', $otp)->update([
  //   	]);
  //   	return view('user/otp')->with('id', $ids);    
  //   }
     
  //   public function otpmatch(Request $request){
  //   	//dd($request->all());

  //   	$this->validate($request, ['otp1' => 'required|numeric',
  //   		'otp2' =>'required|numeric',
  //   		'otp3' =>'required|numeric',
  //   		'otp4' =>'required|numeric',
  //   	]);

  //   	$collection = collect([
		//     ['otp' => $request->otp1],
		//     ['otp' => $request->otp2],
		//     ['otp' => $request->otp3],
		//     ['otp' => $request->otp4],
		// ]);
		
		// $data = $collection->implode('otp', '');		
		// //dd($request->id);

		// $chk = User::where('id', $request->id)->where('otp', $data)->first();    	
  //   	//dd($chk->id);

  //   	if($chk){
  //   		User::where('id', $chk->id)->update([
  //   			'is_verified' => '1'
  //   		]);

  //   		$id = encrypt($request->id); 
  //   		//dd($id);
  //   		return redirect('user/change-password/'.$id)->with('msg', 'OTP Successfully Match');
  //   	}else{
  //   		return redirect::back()->with('msg', 'Invalid OTP Please try again');
  //   	}
  //   }


  //   public function changepassword($id){
  //   	//dd($id);    	 
  //   	return view('user/change-password')->with('id', $id);
  //   }


  //   public function passwordreset(Request $request){
  //   	//$id = $request->id;    	
  //   	$ids = decrypt($request->id);
    	
  //   	$this->validate($request, [
  //   		'password' => 'required|string|min:6',
  //   		'c_password' => 'required|same:password',
  //   		],[
  //   			'password.required' => 'Please Enter Valid Password',
  //   			'c_password.required' => 'Please Enter Confirm Password'
  //   		]);  	

  //   	//dd($ids);

  //   	User::where('id', '2')->update([
  //   		'password'=>Hash::make($request->password)
  //   	]);
  //   	return redirect('/');

  //   	//dd($request->all());
  //   }


}
