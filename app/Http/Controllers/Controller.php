<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Exceptions;
use App\Model\CouponUses;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public function androidPushNotification($body_text, $tokens,$result){
        //$body_text, $tokens, $notification_type,$sender_id
        // $body_text ="chala gaya";
        // $notification_type="1";
        // $sender_id= '1';
        // dd($tokens);
        // dd($tokens);
        $url = 'https://fcm.googleapis.com/fcm/send';       
        $headers = array (
            'Authorization: key='."AAAAX1fyjug:APA91bG7J6KxoK1bZbPerf3yG4gwcKuJuHGe8ZNOWPdSR4ifnGAO9lNQC33MV4hBwlEKId4IX-_9Cvci0mwVrpBt7zGPX7YCVrj773g2aVelO47GJ5mtGuPbeRPb0toR77O8NX1M_WRU",
            'Content-Type: application/json'
        );
        $tokens=array($tokens);
        $ch = curl_init();
        curl_setopt ( $ch, CURLOPT_URL, $url );
        curl_setopt ( $ch, CURLOPT_POST, true );
        curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
        curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
       
        if(isset($result))
        {
        $fields = array (
        'registration_ids' => $tokens, 
        'data' => array (
        	"title" => $body_text,
            "message" => $body_text,
            )
        );
       }
       else
       {
        $fields = array (
        'registration_ids' => $tokens, 
        'data' => array (
        	"title" => $body_text,
            "message" => $body_text,
            )
        );
       }
        // dd($fields);
        $fields = json_encode ( $fields );
        curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );
        $result = curl_exec ( $ch );
       // dd($result);
        if(curl_error($ch))
        {
            echo 'error:' . curl_error($ch);exit();
        }
        $json = json_decode($result, true);
        if($json['success']){
            $status=1;
        }
        curl_close ( $ch );
        if($json['success']){
            //dd("vxzncvjkbhxo");
            return 1;
        } else {
            return 0;
        }
    }
     
    public static function iosPushNotification($body_text,$tokens,$result){
        $sender_id = 1;
        $notification_type = $body_text;
        $title = "Myles Registered";
 
        // dd($tokens);
        $deviceToken = $tokens;
        $url = "https://fcm.googleapis.com/fcm/send";      
        $notification = array('title' => $notification_type , 'text' => $body_text, 'sound' => 'default', 'badge' => '1');
        $headers = array();
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Authorization: key=AAAAX1fyjug:APA91bG7J6KxoK1bZbPerf3yG4gwcKuJuHGe8ZNOWPdSR4ifnGAO9lNQC33MV4hBwlEKId4IX-_9Cvci0mwVrpBt7zGPX7YCVrj773g2aVelO47GJ5mtGuPbeRPb0toR77O8NX1M_WRU';

        $arrayToSend = array(
            'to' => $deviceToken, 
            'notification' => $notification,
            'priority'=>'high',
            'data' => [
            'sender_id'  => $sender_id,
            'title'   => $title,
            'notification_type'=>$notification_type
            
            ]
        );

        $json = json_encode($arrayToSend);
        // dd($json);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt ( $ch, CURLOPT_POST, true );
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST,"POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);
        curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
        $response = curl_exec($ch);
      //dd($response);
        curl_close($ch);
        if ($response === false) {
            return 0;
            // die('FCM Send Error: ' . curl_error($ch));
        }else{
            return 1;
        }
    }

    // public function iosPushNotification($title = 'test',$body_text, $tokens, $result, $notification_type= 5, $sender_id =1){ 
    //     // dd($tokens);
    //     $deviceToken = $tokens;
    //     $url = "https://fcm.googleapis.com/fcm/send";
    //     //Log::info('--------------------Tokens-----------'.print_r($deviceToken,true));
        
    //     // $notification = array('title' =>"Order status" , 'text' => $body_text, 'sound' => 'default', 'badge' => '1');
    //     $notification = array('title' => $notification_type , 'text' => $body_text, 'sound' => 'default', 'badge' => '1');

    //     $headers = array();
    //     $headers[] = 'Content-Type: application/json';
    //     $headers[] = 'Authorization: key=AAAAX1fyjug:APA91bG7J6KxoK1bZbPerf3yG4gwcKuJuHGe8ZNOWPdSR4ifnGAO9lNQC33MV4hBwlEKId4IX-_9Cvci0mwVrpBt7zGPX7YCVrj773g2aVelO47GJ5mtGuPbeRPb0toR77O8NX1M_WRU';

    //     $arrayToSend = array(
    //         'to' => $deviceToken, 
    //         'notification' => $notification,
    //         'priority'=>'high',
    //         'data' => [
    //         'sender_id'  => $sender_id,
    //         'title'   => $title,
    //         'notification_type'=>$notification_type
            
    //         ]
    //     );

    //     // "notification": {
    //     //   "title": "Check this Mobile (title)",
    //     //   "body": "Rich Notification testing (body)",
    //     //   "mutable_content": true,
    //     //   "sound": "Tri-tone"
    //     // },

    //     // "data": {
    //     //  "url": "<url of media image>",
    //     //  "dl": "<deeplink action on tap of notification>"
    //     // }

    //     // $arrayToSend = [
    //     //     'aps' => [                    
    //     //          'content-available'=>1,
    //     //          'alert'=>$body_text,
    //     //       ],
    //     //     'type'=>'1',          
    //     // ];      

    //     //Log::info('--------------------Tokens-----------'.print_r($arrayToSend,true));
    //     $json = json_encode($arrayToSend);
    //     // dd($json);
    //     // $ch = curl_init();
    //     $ch= curl_init();
    //     curl_setopt($ch, CURLOPT_URL, $url);
    //     curl_setopt ( $ch, CURLOPT_POST, true );
    //     curl_setopt($ch, CURLOPT_CUSTOMREQUEST,"POST");
    //     curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
    //     curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);
    //     curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
    //     $response = curl_exec($ch);
    //     // dd($response);
    //     curl_close($ch);
    //     if ($response === false) {
    //         return 0;
    //         // die('FCM Send Error: ' . curl_error($ch));
    //     }else{
    //         return 1;
    //     }
    // }
    

    public function couponUses($user_id,$coupon_id)
    {
        if($coupon_id !=null)
        {
            if(CouponUses::where('user_id',$user_id)->where('coupon_id',$coupon_id)->exists())
            {
                $count=CouponUses::where('user_id',$user_id)->where('coupon_id',$coupon_id)->value('count');
                
                CouponUses::where('user_id',$user_id)->where('coupon_id',$coupon_id)->update(['count'=>$count+1]);
            }
            else
            {
                $coupon=new CouponUses();
                $coupon->user_id=$user_id;
                $coupon->coupon_id=$coupon_id;
                $coupon->count=1;
                $coupon->save();
            }
        }        
        return true;        
    } 


}