<?php

namespace App\Http\Controllers\Api\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\UserSupport;
use App\User;

class UserChatController extends Controller
{
    function useradminchat(Request $request){    
    	// dd("sadsad");
    	$data = UserSupport::create([
    		'user_id' => $request->user_id,
    		'user_message' => $request->usermessage,
    	]);
    	$alldata = UserSupport::where('user_id', $request->user_id)->get();
    	$response['data'] = $alldata; 
    	return response()->json($response, 200);
    }

    public function SaveToken(Request $request){
        // dd("sdsadas");
        $input = $request->all();

        $fcm_token = $input['fcm_token'];
        $user_id = $input['user_id'];

        $user    =   User::findorfail($user_id);

        $user->fcm_token = $fcm_token;
        $user->save();

        return response()->json([
            'success' => true,
            'message' => 'User token updated successfully'
        ]);

    }

}