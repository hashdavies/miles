<?php

namespace App\Http\Controllers\Api\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use PDF;
use App\Model\ServiceTax;
use App\Model\ServiceCategoryAddress;
use App\Model\UserTax;
use Dompdf\Dompdf;
use Dompdf\Options;
 
use App\Model\UsrRequestGig;
use Storage;


class PdfController extends Controller
{
    function userOngoingGigPdf(Request $request){

      // dd("sdsa");
        	
      // $dompdf->load_html_file('https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css');

      // $servicescharge = ServiceTax::where('id', '1')->first(); 
      // $data = ServiceCategoryAddress::where('id', $request->gig_id)->first();          

      //     $Tax = UserTax::where('province', $data->proviance)->first();
        
      //     if($Tax){  
           
      //       $ServicePrice = ((@$rowData->offerAmount)*(@$servicescharge->serviceTax)/100)+($rowData->offerAmount);

      //       $data->servicecharge = (@$rowData->offerAmount)*(@$servicescharge->serviceTax)/100;

      //       $data->tax = (string)round(((@$ServicePrice)*(@$Tax->tax_rate)/100), 2);

      //       $subtotalprice = ((@$ServicePrice)*(@$Tax->tax_rate)/100)+(@$ServicePrice);
      //      $data->subTotal = (string)round(@$subtotalprice, 2);
      //     }else{
      //       $data->tax = "0";
      //       $ServicePrice = ((@$rowData->offerAmount)*(@$servicescharge->serviceTax)/100)+($rowData->offerAmount);
      //       $data->servicecharge = (@$rowData->offerAmount)*(@$servicescharge->serviceTax)/100;
            
      //       $data->subTotal = ((@$rowData->offerAmount)*(@$servicescharge->serviceTax)/100)+(@$rowData->offerAmount);
      //       $subtotalprice = @$ServicePrice;
      //       $data->subTotal = (string)round(@$subtotalprice, 2);
      //     }
 
      //     dd($data);

      // $data = User::get();
      // $pdf = PDF::loadView('pdf_view',compact('data'));
      // $pdf->setOptions(['isPhpEnabled' => true,'isRemoteEnabled' => true]);

      // $filename = "MylesGigDetails.pdf";
      // // Save file to the directory
      // $pdf->save(public_path().'/Usergigpdf/'.$filename);
      // return response()->json(['url'=>url('/public/Usergigpdf').'/'.$filename]);

    	// $dompdf->load_html_file('https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css');

      // $dompdf = new Dompdf();
      // $dompdf->set_option('isHtml5ParserEnabled', true);
      // $dompdf->set_option('isRemoteEnabled', true);   
      // $html = view('invoice',['data'=>$data])->render();
      // $dompdf->loadHtml($html);
      // $dompdf->setPaper('A4', 'portrait');
      // $dompdf->render();
      // $fileName = 'invoice/'.$orderId.'.pdf';
      // // Storage::put('public/'.$fileName, $dompdf->output());

      // $pdf->save(public_path().'/Usergigpdf/'.$filename);
      // return response()->json(['url'=>url('/public/Usergigpdf').'/'.$filename]);

      $gigData = ServiceCategoryAddress::with('UserName')->where('id', $request->gig_id)->first();
      $gigOfferData = UsrRequestGig::where('Seller_ID', $request->seller_id)->where('gig_id', $request->gig_id)->first();

      // dd($gigOfferData);                
 
      $servicescharge = ServiceTax::where('id', '1')->first(); 
      $province = ServiceCategoryAddress::where('id', $request->gig_id)->first();     
      
      // dd($province->proviance);

      if($province->proviance){
          $Tax = UserTax::where('province', $province->proviance)->first();

          if($Tax){
              $ServicePrice = ((@$gigOfferData->offerAmount)*(@$servicescharge->serviceTax)/100)+($gigOfferData->offerAmount);
              $servicechargePrice = (@$gigOfferData->offerAmount)*(@$servicescharge->serviceTax)/100;
              // dd($servicechargePrice);
              $tax = (string)round(((@$ServicePrice)*(@$Tax->tax_rate)/100), 2);

              $subtotalprice = ((@$ServicePrice)*(@$Tax->tax_rate)/100)+(@$ServicePrice);
              $subTotal = (string)round(@$subtotalprice, 2);
          }else{

              $tax = "0";
              $ServicePrice = ((@$rowdata->offerAmount)*(@$servicescharge->serviceTax)/100)+($rowdata->offerAmount);

              $servicechargePrice = (@$rowdata->offerAmount)*(@$servicescharge->serviceTax)/100;
              
              // dd($servicechargePrice);

              $subtotalprice = @$ServicePrice;
              $subTotal = (string)round(@$subtotalprice, 2);
          }
      }
      
      // dd($servicecharge);      
      

      $dompdf = new Dompdf();
      $dompdf->set_option('isHtml5ParserEnabled', true);
      $dompdf->set_option('isRemoteEnabled', true);   
      $html = view('pdf_view',['data'=>$gigData, 'offerAmount'=>$gigOfferData->offerAmount, 'bookDate'=>$gigOfferData->bookingDate, 'servicecharge'=>$servicechargePrice, 'tax'=>$tax, 'subTotal' => $subTotal])->render();
      $dompdf->loadHtml($html);
      $dompdf->setPaper('A4', 'portrait');
      $dompdf->render();
      $fileName = 'MylesGigDetails.pdf';
      Storage::put('public/'.$fileName, $dompdf->output());
      return response()->json(['url'=>url('/storage/app/public').'/'.$fileName]);

      // $pdf = PDF::loadView('pdf_view',compact('data'));
      // $pdf = new PDF;
      // $pdf->setOptions(['isPhpEnabled' => true,'isRemoteEnabled' => true, 'isHtml5ParserEnabled' => true]);
      // $html = view('pdf_view',['data'=>$data])->render();
      // $pdf->loadHtml($html);

      // $filename = "MylesGigDetails.pdf";
      // $dompdf->save(public_path().'/Usergigpdf/'.$filename);
      
    }

}
