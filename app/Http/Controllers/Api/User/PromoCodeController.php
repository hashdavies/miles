<?php

namespace App\Http\Controllers\Api\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\PromoCode;
use Exceptions;
use App\Model\Notification;
use App\Model\GigOrders;
use App\Model\CouponUses;

class PromoCodeController extends Controller
{
    function UserGetPromoCode(Request $request){
    	// $data = PromoCode::where('c', $request->service_id)->get();

    	$user_id = $request->user_id;
        $sub_total_amount = $request->sub_total_amount;
        $service = $request->service;        
        $todayDate = date('Y/m/d');

        // dd($todayDate);
        // $data = PromoCode::where('status', '1')->where('end_date', '>=', $todayDate)->Where('amount_limit','<=',99)->get();
        
        $data = PromoCode::whereIn('apply_promo_code', ['all',$request->service])->where('status', '1')->get();        
        // dd(count($data));        
        //return response()->json($data);

        if(count($data)>0){

            //$data = PromoCode::where('apply_promo_code', 'all')->where('status', '1')->get();
                foreach($data as $key=>$rowData){

                    $subtotal = ($request->sub_total_amount*$rowData->discount)/100;
                    $data[$key]->Subtotal_Price = $request->sub_total_amount-$subtotal;
                }
        }else{
            $data = PromoCode::where('status', '1')->where('end_date', '>=', $todayDate);
            // dd("fdss");
            if($sub_total_amount != ''){
                $data->Where('amount_limit','<=', (int)$sub_total_amount);
            }
            if($service != ''){
                $data->Where('apply_promo_code', $service);
            }        
            $data = $data->get();
            foreach ($data as $key => $value) {
                if(CouponUses::where('user_id',$user_id)->where('coupon_id',$value->id)->exists())
                {
                    $count=CouponUses::where('user_id',$user_id)->where('coupon_id',$value->id)->value('count');

                    if($count>$value->coupon_per_user)
                    {
                      $data[$key]->coupon_redemption= 0;
                    }
                    else
                    {
                      $data[$key]->coupon_redemption=1;
                    }
                }
                else
                {
                    $data[$key]->coupon_redemption=null;
                }
                foreach($data as $key=>$rowData){
                    $subtotal = ($request->sub_total_amount*$rowData->discount)/100;
                    $data[$key]->Subtotal_Price = $request->sub_total_amount-$subtotal;
                }
                
                $data[$key]->coupon_valid_to = date('d-M-Y', strtotime($value->end_date));
           } 
        }        
        // dd($usercount);
        // $data  = CartSeller::join('products', 'cart_sellers.product_id', '=', 'products.id')->join('product_images', 'cart_sellers.product_id', '=', 'product_images.product_id')->where('cart_sellers.seller_id',$request->sellerDetail->id)->select('cart_sellers.*','products.name','products.price','products.discount','products.selling_price','product_images.image')
            // ->get()->unique();
        // $data = PromoCode::get();
        
        // dd($data);

        if(count($data)>0){
            return response()->json(['message'=> 'success', 'data'=>$data],200);
        }else{
            return response()->json(['message'=> 'success', 'data'=>"you can not apply this counpon because your amount and service not match"],200);
        }        
    }

    function sendNoti(Request $request){
    	// dd("sdfsd");
    	// dd($request->UserData->device_token);
        $title = "Myles Registered";
    	$body_text="Congratulations! You have successfully registered.";
    	$tokens = $request->UserData->device_token;
    	$result =[
    		'is_applied'=>1,
    	];//"Test notification from kuldeep tomer";
        if(!empty($request->UserData->device_token)){
          // dd($request->UserData->device_type);

          if($request->UserData->device_type != ''){
            if($request->UserData->device_type == '0'){
                $is_result=$this->androidPushNotification($body_text, $tokens, $result);
            }elseif($request->UserData->device_type == '1'){
                $is_result=$this->iosPushNotification($body_text, $tokens,$result);
            }
          }else{
            return response()->json(['message'=> 'Invalid device type!'],400);
          }
        }else{
            return response()->json(['message'=> 'Invalid device token!'],400);
        }
    	// dd($is_result);
    	if($is_result == '1'){
    		$data = new Notification;
    		$data->user_id = $request->UserData->id;
            $data->message = $body_text;
            $data->save();
    		return response()->json(['message'=> 'Notification send successfully'],200);
    	}elseif($is_result == '0'){
    		return response()->json(['message'=> 'Something went wrong!'],400);
    	}
    }
    

    function NotificationList(Request $request){
        $data = Notification::where('user_id', $request->UserData->id)->orderBy('id', 'Desc')->get();
        return response()->json(['message'=> 'success', 'data'=>$data],200);
    }

    function UserUnReadNotification(Request $request){
        $data = Notification::where('user_id', $request->user_id)->where('read_unread_notification', '1')->count();
        return response()->json(['message'=> 'success', 'data'=>$data],200);
    }

    function UserReadNotification(Request $request){
        $data = Notification::where('user_id', $request->user_id)->where('read_unread_notification', '1')->update([
            'read_unread_notification' => '0',
        ]);
        return response()->json(['message'=> 'success', 'data'=>0],200);
    }

}