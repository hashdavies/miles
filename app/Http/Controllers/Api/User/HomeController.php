<?php

namespace App\Http\Controllers\Api\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\ServiceCategory;
use App\Model\SubcategoryService;
use App\Model\ServiceCategoryAddress;
use App\Model\Countries;
use App\Model\State;
use App\Model\City;
use App\User;
use DB;
use Storage;
use App\Seller;
use App\Model\UsrRequestGig;
use Validator;
use App\Model\About;
use App\Model\UserTripStop;
use App\Model\ServiceCategoryImage;
use App\Model\Notification;
use App\Model\CmsPage;

class HomeController extends Controller
{
    function subcategorylist(){
        // dd("sadasd");
        $response['data'] = ServiceCategory::groupBy('category_name')->get();
        return response()->json($response, 200);
    }

    function citylist(Request $request){

        $countryCode = User::where('id', $request->UserId)->first();
        $countryId = Countries::where('phonecode', $countryCode->countryCode)->first();
        $state = State::select('id')->where('country_id', $countryId->id)->get(); 
        $response['data'] = City::orderBy('name', 'asc')->whereIn('state_id', $state)->get(); 
        return response()->json($response,200);
    }

    function usersubcategory(Request $request){
        
        // dd($request->categoryID);
        $catid = ServiceCategory::where('id', $request->categoryID)->get();
        // $data = ServiceCategory::orderBy('id', 'desc')->get();

        foreach($catid as $key => $row){
           $subcate_array = explode(',', $row->subcategoryID); 
           $sucatearrayname = SubcategoryService::whereIn('id', $subcate_array)->get(); 
           // $sucatearrayname =  explode(',',$sucatearrayname);
            // dd($sucatearrayname);            
           // $data[$key]->subcategoryname=$sucatearrayname;
        }   
        $response['data'] = $sucatearrayname;
        return response()->json($response,200);
    }    

    function addcitycategorysubcategory(Request $request){   
      //dd($request->UserData->id);
      $validations =[
        'cat_id'=>'required',
        'city_id'=>'required',
        'gig_name' => 'required',
        'delivery_address'=>'required',
        'estimated_weightx' => 'required',
        'additional_services'=>'required',
        // 'categoryimages'=>'required|mimes:jpg,png,svg,gif|max:2048',
        // 'additionaldata' => 'required',       
        'user_id' => 'required',
     ];   
     $validator=Validator::make($request->all(),$validations);
     if($validator->fails())
     {
        $response['message']=$validator->errors($validator)->first();
        return response()->json($response,400);
     } 

     DB::beginTransaction();
     try { 

     $data = ServiceCategoryAddress::create([
        'user_id' => $request->UserData->id,
        'cat_id' => $request->cat_id,
        'cityName' => $request->city_id,
        'gig_name' => $request->gig_name,
        'delivery_address' => $request->delivery_address,
        'estimated_weightx' => $request->estimated_weightx,
        'additional_services' => $request->additional_services,
        'additionaldata' => $request->additionaldata,
     ]);            
     // dd($data->id);
     $sellerdata = Seller::where('is_approved', '1')->get();
     $RegDate = date('Y-m-d');     
     // dd($sellerdata);
     foreach($sellerdata as $key=>$rowseller){
        // dd($rowseller->serviceType);
        $subcate_array = explode(',', $rowseller->serviceType);
    
        if(in_array($request->cat_id, $subcate_array)){
            // dd("match");
            UsrRequestGig::create([
             'User_ID'=>$request->UserData->id,
             'Seller_ID' => $rowseller->id,
             'status' => '0',
             'registerDate' => $RegDate,
             'gig_id' => $data->id,
            ]);  
        }
      } 
      $images=array();
        if($files=$request->file('categoryimages')){
            foreach($files as $key=>$file){

                $s3 = \Storage::disk('s3');
                $subCateImg = rand(10,100).time() .'.'. $file->getClientOriginalExtension();
                $filepath = 'Serviceprovider/' . $subCateImg;
                $s3->put($filepath, file_get_contents($file), 'public');
                $images[]=$subCateImg;
                /*Insert your data*/
                DB::table('category_service_image')->insert([
                 'cat_id' => $request->cat_id,
                 'user_id' => $request->UserData->id,
                 'serviceimage' => $subCateImg, 
                 'gig_id' => $data->id,  
                ]);
                /*Insert your data*/
            }
        }   

       DB::commit();
      } catch (\Exception $e) {
          return $e;
          DB::rollback();
      } 

      $response['data'] = $data;
      $response['message'] = "Data successfully save";
      return response()->json($response,200);                
    }

    function Pickuplocation(Request $request){
        // dd($request->all());
        if($request->tripType == '1'){
           // dd("sadsa");
           $CreateGig = ServiceCategoryAddress::find($request->gig_id);
           $CreateGig->pickUpAddress = $request->pickUpAddress;
           $CreateGig->pickUpLongtitude = $request->pickUpLongtitude;
           $CreateGig->pickUpLatitude = $request->pickUpLatitude;
           $CreateGig->dropAddress = $request->dropAddress;
           $CreateGig->dropLongtitude = $request->dropLongtitude;
           $CreateGig->dropLatitude = $request->dropLatitude;
           $CreateGig->pickupDate = $request->pickupDate;
           $CreateGig->pickupTime = $request->pickupTime;
           $CreateGig->budget = $request->budget;
           $CreateGig->timezone = $request->timezone;
           $CreateGig->trip_type = $request->tripType;

           $CreateGig->pickUpAddressType = $request->pickUpAddressType;
           $CreateGig->dropAddressType = $request->dropAddressType;
           $CreateGig->proviance = $request->proviance;

           if($CreateGig->save()){

                $data1 = ServiceCategoryAddress::where('id', $request->gig_id)->first();

                $data = ServiceCategoryAddress::with(['CaregoryImages' => function($q) use ($data1){
                    $q->where('gig_id',$data1->id);
                }])->
                with('CategoryName')->with('CityName')->with('TripType')->where('id', $request->gig_id)->first();
                
                $response['message'] = "Data successfully save";
                $response['data'] = $data;
                return response()->json($response,200); 

           }else{
                $response['message'] = "Something went wrong";
                return response()->json($response,200); 
           }

        }elseif($request->tripType == '2'){

               $CreateGig = ServiceCategoryAddress::find($request->gig_id);
               $CreateGig->pickUpAddress = $request->pickUpAddress;
               $CreateGig->pickUpLongtitude = $request->pickUpLongtitude;
               $CreateGig->pickUpLatitude = $request->pickUpLatitude;
               $CreateGig->dropAddress = $request->dropAddress;
               $CreateGig->dropLongtitude = $request->dropLongtitude;
               $CreateGig->dropLatitude = $request->dropLatitude;
               $CreateGig->pickupDate = $request->pickupDate;
               $CreateGig->pickupTime = $request->pickupTime;

               $CreateGig->returnTime = $request->returnTime;
               $CreateGig->returnDate = $request->returnDate;

               $CreateGig->proviance = $request->proviance;

               $CreateGig->pickUpAddressType = $request->pickUpAddressType;
               $CreateGig->dropAddressType = $request->dropAddressType;

               $CreateGig->budget = $request->budget;
               $CreateGig->timezone = $request->timezone;
               $CreateGig->trip_type = $request->tripType;
               if($CreateGig->save()){
                   
                   $data1 = ServiceCategoryAddress::where('id', $request->gig_id)->first();

                   $data = ServiceCategoryAddress::with(['CaregoryImages' => function($q) use ($data1){
                        $q->where('gig_id',$data1->id);
                    }])->
                    with('CategoryName')->with('CityName')->with('TripType')->where('id', $request->gig_id)->first();
                   
                    $response['data'] = $data;
                    $response['message'] = "Data successfully save";
                    return response()->json($response,200); 
               }else{
                    $response['message'] = "Something went wrong";
                    return response()->json($response,200); 
               }
        
        }elseif($request->tripType == '3'){

            // DB::beginTransaction();
            try{

               $CreateGig = ServiceCategoryAddress::find($request->gig_id);
               $CreateGig->pickUpAddress = $request->pickUpAddress;
               $CreateGig->pickUpLongtitude = $request->pickUpLongtitude;
               $CreateGig->pickUpLatitude = $request->pickUpLatitude;
               $CreateGig->dropAddress = $request->dropAddress;
               $CreateGig->dropLongtitude = $request->dropLongtitude;
               $CreateGig->dropLatitude = $request->dropLatitude;
               $CreateGig->pickupDate = $request->pickupDate;
               $CreateGig->pickupTime = $request->pickupTime;
               $CreateGig->returnTime = $request->returnTime;
               $CreateGig->returnDate = $request->returnDate;
               $CreateGig->budget = $request->budget;
               $CreateGig->timezone = $request->timezone;
               $CreateGig->trip_type = $request->tripType;

               $CreateGig->proviance = $request->proviance;
               
               $CreateGig->pickUpAddressType = $request->pickUpAddressType;
               $CreateGig->dropAddressType = $request->dropAddressType;
               $CreateGig->stopType = $request->stopType;
               $CreateGig->save();
               // dd($CreateGig->id);

                // $tripStopAddress = ["1, 2nd cross bhavani nagar, Nellore, Andra Pradesh, 534567", "2nd Stop Address"];
                // $tripstopLatitude = ["45.1234", "19.345"];
                // $tripstopLongtitude = ["24.4567", "89.2345"];
                // $tripStopAddressType = ["home", "office"];

                $tripStopAddress = $request->tripStopAddress;
                $tripstopLatitude = $request->tripStopLatitude;
                $tripstopLongtitude = $request->tripStopLongtitude;
                $tripStopAddressType = $request->tripStopAddressType;
                // dd($tripStopAddress);
                
                
                // dd($tripStopAddress);
                if(!empty($tripStopAddress)){                  
                  // dd("dsd");
                  
                  foreach(@$tripStopAddress as $key => $row)
                  {
                      // dd($row);$CreateGig->id
                      $input['service_category_address_id'] = @$CreateGig->id;
                      $input['tripStopAddress'] = @$row;
                      $input['tripstopLatitude'] = @$tripstopLatitude[$key];
                      $input['tripstopLongtitude'] = @$tripstopLongtitude[$key];
                      $input['stopType'] = @$request->stopType;
                      $input['tripStopAddressType'] = @$tripStopAddressType[$key];
                      UserTripStop::create($input);
                      // dd($input);
                  }
                }                

                $data1 = ServiceCategoryAddress::where('id', $request->gig_id)->first();

                $data = ServiceCategoryAddress::with(['CaregoryImages' => function($q) use ($data1){
                   $q->where('gig_id',$data1->id);
                    }])->
                  with('CategoryName')->with('CityName')->with('TripType')->where('id', $request->gig_id)->first();
                
                $response['message'] = "Data successfully save";
                $response['data'] = $data;   

                return response()->json($response,200); 
               
             // dd("Sdsa");
                // DB::commit();

            } catch (\Exception $e) {

                // return $e;
                // dd("Sdsa"); 
                dd($e);
                // DB::rollback();
            }         
        }
    }    

    function ConfirmationGig(Request $request){
        if($request->confirmStatus == 1){
          // dd("sdfsf");
          $gigConfirm = ServiceCategoryAddress::find($request->gigID);
          // dd($gigConfirm->user_id);
          if($gigConfirm){
            $gigConfirm->gigStatus = '1';
            if($gigConfirm->save()){

                // $userData = User::where('id', $gigConfirm->user_id)->first();
                // $body_text="Thankyou for created new gig.";
                // $tokens = $userData->device_token;
                // $result =[
                //   'is_applied'=>1,
                // ];
                // $is_result = '';
                // if($userData->device_type == '0'){
                //       $is_result=$this->androidPushNotification($body_text, $tokens, $result);
                // }elseif($userData->device_type == '1'){
                //       $is_result=$this->iosPushNotification($body_text, $tokens,$result);
                // } 
                // if($is_result == '1'){
                //   $data = new Notification;
                //   $data->user_id = $gigConfirm->user_id;
                //   $data->message = $body_text;
                //   $data->save(); 
                // }

              $sellerdata = Seller::where('is_approved', '1')->get();
              foreach($sellerdata as $key=>$rowseller){
                // dd($rowseller->device_token);
                $body_text="A new gig has been arrived";
                $tokens = $rowseller->device_token;
                $result =[
                  'is_applied'=>1,
                ];
                $is_result = '';
                if($rowseller->device_type == '0'){
                      $is_result=$this->androidPushNotification($body_text, $tokens, $result);
                }elseif($rowseller->device_type == '1'){
                      $is_result=$this->iosPushNotification($body_text, $tokens,$result);
                } 
                if($is_result == '1'){
                    $data = new Notification;
                    $data->seller_id = $rowseller->id;
                    $data->message = $body_text;
                    $data->save();               
                } 
              } 
              return response()->json(['message'=>'Gig confirm successfully'], 200);
            }else{
              return response()->json(['message'=>'Something went wrong!'], 400);
            }
          }else{
            return response()->json(['message'=>'Invalid Gig Id'], 400);
          }          

        }elseif($request->confirmStatus == 0){
          //dd("sadas");
          try{            
            ServiceCategoryAddress::where('id', $request->gigID)->delete();
            UsrRequestGig::where('gig_id', $request->gigID)->delete();
            ServiceCategoryImage::where('gig_id', $request->gigID)->delete();
            return response()->json(['message'=>'Gig cancel successfully'], 200);

          }catch(\Exception $e){
            dd($e);
          }
       }
    }


    function servicetype(Request $request){
        // dd($request->all());
        ServiceCategoryAddress::where('id', $request->gig_id)->update([
            'trip_type' => $request->trip_type,
            'pickupDate' => $request->pickupDate,
            'pickupTime' => $request->pickupTime,
            'budget' => $request->budget,
            'timezone' => $request->timezone,
        ]);
        $data = ServiceCategoryAddress::where('id', $request->gig_id)->first();
        
        $response['data'] = $data;
        $response['message'] = "Data saved successfully";
        return response()->json($response,200); 
    }

    function GetSubCategory(Request $request){

        $all = Category::where('parent_id', $request->cat_id)->get();
            $category = [];
            foreach ($all as $key => $a) {
                $subCategory = Category::where('parent_id',$a->id)->select('id as parent_id','product_image','category_name')->get();

                $category[] = ['parent_id'=>$a->id,'category_name'=>$a->category_name,'Sub_sub_categories'=>$subCategory];                        
            }
            if(!empty($subCategory)){
                $finalResponse = ['SubCategories'=>$category];

                $response['data'] = $finalResponse;
                return response()->json($response, 200);
            }
            else
            {
                $response['message'] = "Subcategory not found";
                return response()->json($response, 200); 
            }    
        }


        function getProductCategoryList(Request $request){
            // dd($request->all());

            if($request->subsubcat_id){                            
               $data = ProductTable::with('getcategory', 'getSubcategory', 'getSubsubcategory', 'getBrand')->orderBy('id', 'Desc')->where('cat_id', $request->cat_id)->where('subcat_id', $request->subcat_id)->where('subsubcat_id', $request->subsubcat_id)->get();

               foreach($data as $key=>$row){
                    $data[$key]->variant = ProductInventory::with('getVariantImageData', 'gerVariantOffer', 'getVariantRating')->where('product_id', $row->id)->get();        
               } 

               $response['data'] = $data;
               return response()->json($response, 200);
           
            }elseif(!empty($request->subcat_id)){

                // dd("subcategory");
                $data = ProductTable::with('getcategory', 'getSubcategory', 'getSubsubcategory', 'getBrand')->orderBy('id', 'Desc')->where('cat_id', $request->cat_id)->Where('subcat_id', $request->subcat_id)->where('subsubcat_id', '=', null)->get();

                foreach($data as $key=>$row){
                    $data[$key]->variant = ProductInventory::with('getVariantImage', 'gerVariantOffer', 'getVariantRating')->where('product_id', $row->id)->get();        
                }

                $response['data'] = $data;
                return response()->json($response, 200);

            }elseif($request->cat_id){
                 
                $data = ProductTable::with('getcategory', 'getSubcategory', 'getSubsubcategory', 'getBrand')->orderBy('id', 'Desc')->where('cat_id', $request->cat_id)->where('subcat_id','=',null)->where('subsubcat_id', '=', null)->get();
                                   
                foreach($data as $key=>$row){
                    $data[$key]->variant = ProductInventory::with('getVariantImage', 'gerVariantOffer', 'getVariantRating')->where('product_id', $row->id)->get();        
                }

                $finalResponse = ['data'=>$data];
                return response()->json($finalResponse, 200);
                // $response['data'] = $data;
                // return response()->json($response, 200);
            }
        }


        function SellerOffer(Request $request){
            // dd($request->all());
            $sellerOffer = ProductOffer::with('getcategory', 'getVariantcolorName')->where('vendor_Id', $request->vendor_id)->get();
            // $product  = ProductOffer::where('vendor_Id', $request->vendor_id)->get();
            // $data1 = explode(',', $product->product_Id);
            // dd($sellerOffer->valid_To);
            $response['data'] =  $sellerOffer;
            return response()->json($response, 200);
        }


        function viewSellerDetails(Request $request){
            // dd($request->vendor_Id);        
            $data = Seller::select('id', 'email_number', 'first_name', 'last_name', 'profile_image')->where('id', $request->vendor_Id)->first();
            $productname = ProductTable::with('getReview')->select('id', 'product_name', 'product_image')->where('vendor', $request->vendor_Id)->get();  
            $offer = ProductOffer::select('id', 'offerName', 'offer_Image')->where('vendor_Id', $request->vendor_Id)->get();     

            $response['data'] = ['seller'=>$data, 'Product'=>$productname, 'offer'=>$offer]; 
            return response()->json($response, 200);
        }
     

        function AddWishlist(Request $request){
                        
            $data  = User::where('access_token', $request->header('accessToken'))->first();
            // dd($data->id);
            $variantData = ProductWishlist::where('variantID', $request->variantID)->first();
            if(!empty($variantData)){
                ProductWishlist::where('variantID', $request->variantID)->update([
                    'wishList' => $request->wishList,
                ]);
                $response['message'] = 'updated successfully';
                return response()->json($response, 200);
            }else{
                ProductWishlist::create([
                    'userID' => $data->id,
                    'variantID' => $request->variantID,
                    'wishList' => $request->wishList,
                ]);
                $response['message'] = 'added successfully';
                return response()->json($response, 200);            
            }           
       } 

       function _sign($params)
        {
            $strToSign = '';
            $params['key'] = '_MERCHANT_KEY_';
            $params['ts'] = time();
            foreach ($params as $k => $v) {
                if ($v !== NULL) {
                    $strToSign .= "$k:$v:";
                }
            }
            $strToSign .= '_MERCHANT_SECRET_';
            $params['sign'] = md5($strToSign);

            return $params;
        }

        function TermsCondition(){
            // dd("sadas");
            $data = CmsPage::where('id', '2')->first();
            $response['message'] = 'Terms of Service';
            $response['data'] = $data->content;
            return response()->json($response, 200); 
        }

}