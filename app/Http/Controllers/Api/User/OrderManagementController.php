<?php

namespace App\Http\Controllers\Api\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\TripType;
use App\Model\TermsConditions;
use App\User;
use Validator;
use Hash;

class OrderManagementController extends Controller
{

    function userorderpending(Request $request){
    	
    	dd($request->user_id);
    
    }


    function userorderongoing(Request $request){
    	dd($request->user_id);    
    }

    function userorderpast(Request $request){
    	dd($request->user_id);
    }


    function usertriptype(){
        $data = TripType::where('status', '0')->orderBy('id', 'Desc')->get();
        $response['data'] = $data; 
        return response()->json($response,200);
    }

    function TermsConditions(){
        $data = TermsConditions::get();
        $response['data'] = $data; 
        return response()->json($response,200);
    }

    function userupdateprofile(Request $request){
      $validations =[        
          'firstName' => 'required',
          'lastName' => 'required',
          'nickName' => 'required',
          'email' => 'required',
          'countryCode' => 'required|numeric',
          'mobileNo' => 'required|numeric',
          // 'password' => 'required',
          // 'cpassword' => 'required|same:password', 
          'address' => 'required',
      ]; 
      $validator=Validator::make($request->all(),$validations);
      if($validator->fails())
      {
          $response['message']=$validator->errors($validator)->first();
          return response()->json($response,400);
      } 

      if ($request->hasFile('profileImage')) {
           $imageprofile = $request->file('profileImage');
           $s3 = \Storage::disk('s3');
           $profileImage = 'profileimage_'.time() .'.'. $imageprofile->getClientOriginalExtension();
           $filepathprofile='Serviceprovider/' . $profileImage;
           $s3->put($filepathprofile, file_get_contents($imageprofile), 'public');        
      }      

      $userupdate = User::find($request->editId);
      $userupdate->firstName           = $request->firstName;
      $userupdate->lastName            = $request->lastName;
      $userupdate->nickName            = $request->nickName;
      $userupdate->emailAddress        = $request->email;
      $userupdate->countryCode         = $request->countryCode;
      $userupdate->mobileNo            = $request->mobileNo;
      $userupdate->address             = $request->address;
      $userupdate->address_latitude    = $request->address_latitude;
      $userupdate->address_longitude   = $request->address_longitude;
      $userupdate->province            = $request->province;
      $userupdate->city                = $request->city;

      if($request->password){
        $userupdate->password          = hash::make($request->password);
      }      
      if($request->hasFile('profileImage')){
        $userupdate->profileImage      = $profileImage;
      }
      $userupdate->save();

      $data = User::where('id', $request->editId)->first();
      $response['message']='profile updated successfully';
      $response['data'] = $data;        
      return response()->json($response,200);
    }

}