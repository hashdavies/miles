<?php

namespace App\Http\Controllers\Api\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\UsrRequestGig;
use App\Model\ServiceCategoryAddress;
use App\Seller;
use App\Model\SellerRating;
use App\Model\UserRating;
use App\Model\UserTax;
use App\Model\ServiceTax;
use App\Model\ServiceCategory;
use DB;
use App\Model\UserTripStop;
use App\Model\GigOrders;

class GigOfferController extends Controller
{
    function userpendinggig(Request $request){
        // dd("sdsa");
        // $data = ServiceCategoryAddress::with('UserName')->where('User_ID', $request->user_id)->where('gigStatus', '1')->orderBy('id','desc')->get();
          
        // foreach($data as $key=>$rowData){
        // $data = UsrRequestGig::with('UserName')->with('SellerName')->with('GigDetails')->where('User_ID', $request->user_id)->where('status', '2')->orderBy('id', 'Desc')->get();

        // $data  = UsrRequestGig::with('UserName')->with('SellerName')->with('GigDetails')->with(['UserName','SellerName', 'GigDetails' => function($q){
        //        $q->where('gigStatus', '1');
        // }])->where('User_ID', $request->user_id)->whereIn('status', array('0','2'))->orderBy('id', 'Desc')->get();

          // $data[$key]->Offers = $Offers;

          // foreach($data as $key=>$rowoffer){

          //   $sellername  = Seller::where('id',$rowoffer->Seller_ID)->first();

          //    $Offers[$key]->sellerName = $sellername->firstName;
          //    $Offers[$key]->lastName = $sellername->lastName;
          //    $Offers[$key]->nickName = $sellername->nickName;
          //    $Offers[$key]->email = $sellername->email;
          //    $Offers[$key]->countryCode = $sellername->countryCode;
          //    $Offers[$key]->serviceType = $sellername->serviceType;
          //    $Offers[$key]->businessName = $sellername->mobileNo;
          //    $Offers[$key]->mobileNo = $sellername->businessName;
          //    $Offers[$key]->dLNumber = $sellername->dLNumber;
          //    $Offers[$key]->businesslogo = $sellername->businesslogo;
          //    $Offers[$key]->rejectReason = $sellername->rejectReason;
          //    $Offers[$key]->IDProof = $sellername->IDProof;
          //    $Offers[$key]->registerDate = $sellername->registerDate;
          //    $Offers[$key]->serviceProviderType = $sellername->serviceProviderType;
          //    $Offers[$key]->transitNumber = $sellername->transitNumber;
          //    $Offers[$key]->institutionNumber = $sellername->institutionNumber;
          //    $Offers[$key]->accountNumber = $sellername->accountNumber;
          //    $Offers[$key]->city = $sellername->city;
          //    $Offers[$key]->province = $sellername->province;
          //    $Offers[$key]->address = $sellername->address;
          //    $Offers[$key]->profileImage = $sellername->profileImage;             
          //    $Offers[$key]->address_latitude = $sellername->address_latitude;
          //    $Offers[$key]->address_longitude = $sellername->address_longitude;
             
          //    $rating = UserRating::select('rating')->where('user_id', $request->user_id)->where('seller_id', $rowoffer->Seller_ID)->first();

          //   $Offers[$key]->SellerRating  = @$rating->rating;
          // }           
          

        //  $data  = UsrRequestGig::with('UserName')->with('SellerName')->with('GigDetails')->with(['UserName','SellerName', 'GigDetails' => function($q){
        //        $q->where('gigStatus', '1');
        // }])->where('User_ID', $request->user_id)->whereIn('status', array('0','2'))->orderBy('id', 'Desc')->get();

        $data = ServiceCategoryAddress::with('UserName')->with('StopAddress')->where('User_ID', $request->user_id)->whereIn('gigStatus', array('1','2'))->orderBy('id','desc')->get();        

        foreach($data as $key=>$rowData){
          $Offers  = UsrRequestGig::select('Seller_ID', 'offerAmount', 'status','gig_id')->where('user_id', $request->user_id)->where('gig_id', $rowData->id)->whereIn('status', array('2'))->orderBy('id', 'Desc')->get();

          $data[$key]->Offers = $Offers;

          foreach($Offers as $key=>$rowoffer){
             $sellername  = Seller::where('id',$rowoffer->Seller_ID)->first();
             $subcate_array = explode(',', $sellername->serviceType); 
             $sucatearrayname = ServiceCategory::whereIn('id', $subcate_array)->pluck('category_name')->toArray(); 
             $sucatearrayname =  implode(',',$sucatearrayname);            
             $Offers[$key]->sellerName = $sellername->firstName;
             $Offers[$key]->lastName = $sellername->lastName;
             $Offers[$key]->nickName = $sellername->nickName;
             $Offers[$key]->email = $sellername->email;
             $Offers[$key]->countryCode = $sellername->countryCode;
             $Offers[$key]->serviceType = $sucatearrayname;
             $Offers[$key]->businessName = $sellername->businessName;
             $Offers[$key]->mobileNo = $sellername->mobileNo;
             $Offers[$key]->dLNumber = $sellername->dLNumber;
             $Offers[$key]->businesslogo = $sellername->businesslogo;
             $Offers[$key]->rejectReason = $sellername->rejectReason;
             $Offers[$key]->IDProof = $sellername->IDProof;
             $Offers[$key]->registerDate = $sellername->registerDate;
             $Offers[$key]->serviceProviderType = $sellername->serviceProviderType;
             $Offers[$key]->transitNumber = $sellername->transitNumber;
             $Offers[$key]->institutionNumber = $sellername->institutionNumber;
             $Offers[$key]->accountNumber = $sellername->accountNumber;
             $Offers[$key]->city = $sellername->city;
             $Offers[$key]->province = $sellername->province;
             $Offers[$key]->address = $sellername->address;
             $Offers[$key]->profileImage = $sellername->profileImage;             
             $Offers[$key]->address_latitude = $sellername->address_latitude;
             $Offers[$key]->address_longitude = $sellername->address_longitude;
             
             $rating = UserRating::select('rating')->where('user_id', $request->user_id)->where('seller_id', $rowoffer->Seller_ID)->first();
             $Offers[$key]->SellerRating  = @$rating->rating;
          }           
          
        }

          // $UserRating = UserRating::where('user_id', $request->user_id)->where('seller_id', $rowData->Seller_ID)->orderBy('id', 'Desc')->first();
          //    // dd($UserRating->rating);
          //    if($UserRating){
          //       $data[$key]->SellerRating  =  $UserRating->rating;
          //    }else{
          //       $data[$key]->SellerRating  =  "null";
          //    }   
          // $servicescharge = ServiceTax::where('id', '1')->first(); 
          // // $servicesTax = ServiceTax::where('id', '1')->first(); 
          // $province = ServiceCategoryAddress::where('id', $rowData->gig_id)->first();          
          // // dd($province);
          // $data[$key]->servicecharge = $servicescharge->serviceTax;
          // if(UserTax::where('province', $province->proviance)->exists()){
          //   $Tax = UserTax::where('province', $province->proviance)->first();
          //   $data[$key]->tax = $Tax->tax_rate;
          //   $subtotalprice = (($province->budget)*($Tax->tax_rate)/100)+($province->budget)+($servicescharge->serviceTax);
          //   $data[$key]->subTotal = $subtotalprice;
          //   // dd($subtotalprice);
          // }else{
          //   $data[$key]->tax = "0";
          //   // dd($rowData->offerAmount);
          //   $subtotal = $province->budget+$servicescharge->serviceTax;
          //   $data[$key]->subTotal = $subtotal;
            // dd($subtotal);          
          // }
      //}
      $response['data'] = $data; 
      return response()->json($response,200);
    }

    function usergigofferdetails(Request $request){
    	// dd("dfdsf");      
      $data = UsrRequestGig::with('UserName')->with('SellerName')->with('GigDetails')->where('User_ID', $request->user_id)->where('status', '2')->orderBy('id', 'Desc')->get();
      foreach($data as $key=>$rowData){
          // $data[$key]->SellerRating  = UserRating::select('rating')->where('user_id', $request->user_id)->where('seller_id', $rowData->Seller_ID)->orderBy('id', 'Desc')->first();           
          // dd($rowData);
          // dd($rowData->offerAmount);
          $UserRating = UserRating::where('user_id', $request->user_id)->where('seller_id', $rowData->Seller_ID)->orderBy('id', 'Desc')->first();
             // dd($UserRating->rating);
             if($UserRating){
                $data[$key]->SellerRating  =  $UserRating->rating;
             }else{
                $data[$key]->SellerRating  =  "null";
             } 
          $servicescharge = ServiceTax::where('id', '1')->first(); 
          $province = ServiceCategoryAddress::where('id', $rowData->gig_id)->first();       
          $data[$key]->servicecharge = $servicescharge->serviceTax;
          $Tax = UserTax::where('province', $province->proviance)->first();  
            $data[$key]->servicecharge = @$servicescharge->serviceTax;
            if($Tax){
              $data[$key]->tax = @$Tax->tax_rate;
              $subtotalprice = ((@$rowData->offerAmount)*(@$servicescharge->serviceTax)/100)+($rowData->offerAmount)+(@$Tax->tax_rate);
              $data[$key]->subTotal = @$subtotalprice;
            }else{
              $data[$key]->tax = "0";
              $data[$key]->subTotal = ((@$rowData->offerAmount)*(@$servicescharge->serviceTax)/100)+@$rowData->offerAmount;
            }

          // if(UserTax::where('province', $province->proviance)->exists()){

          //   $Tax = UserTax::where('province', $province->proviance)->first();

          //   $data[$key]->tax = $Tax->tax_rate;
          //   $subtotalprice = (($rowData->offerAmount)*($Tax->tax_rate)/100)+($rowData->offerAmount)+($servicescharge->serviceTax);


          //   $data[$key]->subTotal = $subtotalprice;
            
          //   // dd($subtotalprice);

          // }else{
          //   $data[$key]->tax = "0";

          //   // dd($rowData->offerAmount);
            
          //   $subtotal = $rowData->offerAmount+$servicescharge->serviceTax;
          //   $data[$key]->subTotal = $subtotal;
          //   // dd($subtotal);          
          // }
      }    	
      $response['data'] = $data; 
    	return response()->json($response,200);
    }

    function UserGigOfferDecline(Request $request){    	
      $currentDate = date('Y-m-d');
      DB::beginTransaction();
      try{
          $data = UsrRequestGig::where('id', $request->gig_Id)->update([
            'status' => '3',
            'gigCompleteDate' =>  $currentDate, 
          ]);
        // ServiceCategoryAddress::where('id', $rowdata->gig_id)->update(['gigStatus'=>3]);
        DB::commit();

        $sellerData = UsrRequestGig::where('id', $request->gig_Id)->first();
        $sellerDetails = Seller::where('id', $sellerData->Seller_ID)->first();
        // dd($sellerDetails->device_token);
         $body_text = "your offer has been declined";
         $tokens = $sellerDetails->device_token;
         $result =[
           'is_applied'=>1,
         ];
         $is_result = '';
         if($sellerDetails->device_type == '0'){
              $is_result=$this->androidPushNotification($body_text, $tokens, $result);
         }elseif($sellerDetails->device_type == '1'){
              $is_result=$this->iosPushNotification($body_text, $tokens,$result);
         } 
         if($is_result == '1'){
            $data = new Notification;
            $data->seller_id = $sellerDetails->id;
            $data->message = $body_text;
            $data->save();               
         }

        $response['message'] = 'Decline successfully'; 
        return response()->json($response,200);
      
      } catch (\Exception $e) {
          return $e;
          DB::rollback();
      }     	
    }

    function usergigaccept(Request $request){     

        if(UsrRequestGig::where('User_ID', $request->user_id)->where('Seller_ID', $request->seller_id)->where('gig_id', $request->gig_Id)->exists()){
    
          $all = UsrRequestGig::where('User_ID', $request->user_id)->where('gig_id', $request->gig_Id)->get();
           
            $currentDate = date('Y-m-d'); 
            foreach($all as $key=>$rowdata){
                if(($rowdata->gig_id == $request->gig_Id) && ($rowdata->Seller_ID == $request->seller_id)){
                    UsrRequestGig::where('id', $rowdata->id)->update([
                        //'status' => '4',
                        'status' => '2',
                        'bookingDate' => date('Y-m-d'),
                        'gigCompleteDate' =>  $currentDate, 
                    ]);
                    ServiceCategoryAddress::where('id', $rowdata->gig_id)->update(['gigStatus'=>2]);  

                    $data = UsrRequestGig::with(['SellerName','SellerName.SellerRating','GigDetail'])->where('id', $rowdata->id)->first(); 

                    $data->StopAddress = UserTripStop::where('service_category_address_id', $data->gig_id)->get();


                    $servicescharge = ServiceTax::where('id', '1')->first(); 
                    $province = ServiceCategoryAddress::where('id', $rowdata->gig_id)->first();       
                    
                    if($province->proviance){
                      // dd($province->proviance);
                        $Tax = UserTax::where('province', $province->proviance)->first();
                        
                          if($Tax){
                            $ServicePrice = ((@$rowdata->offerAmount)*(@$servicescharge->serviceTax)/100)+($rowdata->offerAmount);
                            $data->servicecharge = (@$rowdata->offerAmount)*(@$servicescharge->serviceTax)/100;

                            $data->tax = (string)round(((@$ServicePrice)*(@$Tax->tax_rate)/100), 2);

                            $subtotalprice = ((@$ServicePrice)*(@$Tax->tax_rate)/100)+(@$ServicePrice);
                            $data->subTotal = (string)round(@$subtotalprice, 2);
                          }else{
                            $data->tax = "0";
                            $ServicePrice = ((@$rowdata->offerAmount)*(@$servicescharge->serviceTax)/100)+($rowdata->offerAmount);
                            $data->servicecharge = (@$rowdata->offerAmount)*(@$servicescharge->serviceTax)/100;
                            $subtotalprice = @$ServicePrice;
                            $data->subTotal = (string)round(@$subtotalprice, 2);
                        }
                    }
                    $response['data'] = $data;
                }else{
                    UsrRequestGig::where('id', $rowdata->id)->update([
                        'status' => '3',
                        'bookingDate' => date('Y-m-d'),
                        'gigCompleteDate' =>  $currentDate,
                    ]);  
                    // $data = '';               
                }
            }
            $data = ServiceCategoryAddress::with('UserName')->with('StopAddress')->where('User_ID', $request->user_id)->whereIn('gigStatus', array('1','2'))->orderBy('id','desc')->get();      

            $response['message'] = 'User Accepted Successfully';           
            return response()->json($response,200);
         
        }else{
           $response['message'] = 'Gig Not found'; 
           return response()->json($response,400);
        }        
    }
    
    
    function usergigorder(Request $request){
       $data = UsrRequestGig::with('UserName')->with('SellerName')->with('GigDetails')->where('User_ID', $request->user_id)->where('status', '4')->get();
       foreach($data as $key=>$rowData){

             $UserRating = UserRating::where('user_id', $request->user_id)->where('seller_id', $rowData->Seller_ID)->orderBy('id', 'Desc')->first();

             // dd($UserRating->rating);
             if($UserRating){
                $data[$key]->SellerRating  =  $UserRating->rating;
             }else{
                $data[$key]->SellerRating  =  "null";
             }             
             $servicescharge = ServiceTax::where('id', '1')->first(); 
             $servicesTax = ServiceTax::where('id', '1')->first(); 
             $province = ServiceCategoryAddress::where('id', $rowData->gig_id)->first();          
             // dd($province);
             $Tax = UserTax::where('province', $province->proviance)->first();          
             $data[$key]->servicecharge = $servicescharge->serviceTax;
             if($Tax){

              $data[$key]->tax = $Tax->tax_rate;
              $subtotalprice = ((@$rowData->offerAmount)*(@$servicescharge->serviceTax)/100)+(@$rowData->offerAmount)+(@$Tax->tax_rate);
              $data[$key]->subTotal = @$subtotalprice;
           
             }else{

              // dd($rowData->offerAmount);
              $data[$key]->tax = "0";
              // dd($province->budget);
              $data[$key]->subTotal = ((@$rowData->offerAmount)*(@$servicescharge->serviceTax)/100)+@$rowData->offerAmount;
            
            }                   
       }
       $response['data'] = $data; 
       return response()->json($response,200);
    }

    function usergigongoing(Request $request){

       // $data = UsrRequestGig::with('UserName')->with('SellerName')->with('GigDetails')->where('User_ID', $request->user_id)->whereIn('status', array('4','5','7','8'))->get();        
       // $data  = UsrRequestGig::select('Seller_ID', 'offerAmount', 'status','gig_id')->where('user_id', $request->user_id)->whereIn('status', array('2'))->orderBy('id', 'Desc')->get();

      $data = UsrRequestGig::with('UserName')->with('SellerName')->with('GigDetails')->where('User_ID', $request->user_id)->whereIn('status', array('4','5','7','8'))->get();
        // dd($data);
        foreach($data as $key=>$rowData){

          // dd($rowData->GigDetails->id);

          $data[$key]->StopAddress = UserTripStop::where('service_category_address_id', $rowData->GigDetails->id)->get(); 

            // $data[$key]->SellerRating  = SellerRating::select('sellerRating')->where('seller_user_id', $request->user_id)->where('seller_seller_id', $rowData->Seller_ID)->orderBy('id', 'Desc')->first(); 
            $UserRating = UserRating::where('user_id', $request->user_id)->where('seller_id', $rowData->Seller_ID)->orderBy('id', 'Desc')->first();

             // dd($UserRating->rating);
             if($UserRating){
                $data[$key]->SellerRating  =  $UserRating->rating;
             }else{
                $data[$key]->SellerRating  =  "null";
             }   

            $servicescharge = ServiceTax::where('id', '1')->first(); 
            $province = ServiceCategoryAddress::where('id', $rowData->gig_id)->first();  
              // if($province->proviance){
              // dd($province->proviance);
              // $data[$key]->tax = "sdsada";
              // dd("fdgdgd");

                $Tax = UserTax::where('province', $province->proviance)->first();
                if($Tax){  
                  // $data[$key]->tax = @$Tax->tax_rate;
                  $ServicePrice = ((@$rowData->offerAmount)*(@$servicescharge->serviceTax)/100)+($rowData->offerAmount);

                  $data[$key]->servicecharge = (@$rowData->offerAmount)*(@$servicescharge->serviceTax)/100;

                  $data[$key]->tax = (string)round(((@$ServicePrice)*(@$Tax->tax_rate)/100), 2);

                  $subtotalprice = ((@$ServicePrice)*(@$Tax->tax_rate)/100)+(@$ServicePrice);
                 $data[$key]->subTotal = (string)round(@$subtotalprice, 2);
                }else{
                  $data[$key]->tax = "0";
                  $ServicePrice = ((@$rowData->offerAmount)*(@$servicescharge->serviceTax)/100)+($rowData->offerAmount);
                  $data[$key]->servicecharge = (@$rowData->offerAmount)*(@$servicescharge->serviceTax)/100;
                  
                  $data[$key]->subTotal = ((@$rowData->offerAmount)*(@$servicescharge->serviceTax)/100)+(@$rowData->offerAmount);
                  $subtotalprice = @$ServicePrice;
                  $data->subTotal = (string)round(@$subtotalprice, 2);
                }


            // }

            // dd($province->proviance);
            // $Tax = UserTax::where('province', $province->proviance)->first();          
            // // dd($Tax->tax_rate); 
            // $data[$key]->servicecharge = $servicescharge->serviceTax;

            // if($Tax){

            //   $data[$key]->tax = $Tax->tax_rate;
            //   $subtotalprice = ((@$rowData->offerAmount)*(@$servicescharge->serviceTax)/100)+(@$rowData->offerAmount)+(@$Tax->tax_rate);
            //   $data[$key]->subTotal = @$subtotalprice;
           
            // }else{ 

            //   $data[$key]->tax = "0";
            //   // dd($province->budget);
            //   $data[$key]->subTotal = ((@$rowData->offerAmount)*(@$servicescharge->serviceTax)/100)+@$rowData->offerAmount;
              
            //   // dd($rowData->offerAmount);
            // }

            // if($Tax){
            //   $data[$key]->tax = $Tax->tax_rate;
            //   // dd($servicescharge->serviceTax);
              
            //   $subtotalprice = (($rowData->offerAmount)*($Tax->tax_rate)/100)+($rowData->offerAmount)+($servicescharge->serviceTax);
            //   $data[$key]->subTotal = $subtotalprice;            
            //   // dd($subtotalprice);
            // }else{
            //   // dd($rowData->offerAmount);
            //   $data[$key]->tax = "0";
            //   // dd($province->budget);
            //   $data[$key]->subTotal = ($rowData->offerAmount)+($servicescharge->serviceTax);           
            // } 
       }
       $response['data'] = $data; 
       return response()->json($response,200);
    }

    function usergigcancel(Request $request){ 
        $currentDate = date('Y-m-d');        
        UsrRequestGig::where('id', $request->gig_Id)->update([
            'status' => '6', 
            'gigCompleteDate' =>  $currentDate,         
        ]);
        $response['message'] = 'Gig cancel successfully'; 
        return response()->json($response,200);
    }

    function userpastgig(Request $request){
       // $data = UsrRequestGig::with('UserName')->with('SellerName')->with('GigDetails')->where('User_ID', $request->user_id)->whereIn('status', array('3','6','9'))->orderBy('id', 'Desc')->groupBy('gig_id')->get();
     
       // $data = UsrRequestGig::with('UserName')->with('SellerName')->with('GigDetails')->where('User_ID', $request->user_id)->whereIn('status', array('3','6','9'))->orderBy('id', 'Desc')->groupBy('status')->get();
       $data = UsrRequestGig::with('UserName')->with('SellerName')->with('GigDetails')->where('User_ID', $request->user_id)->whereIn('status', array('9'))->orderBy('id', 'Desc')->get();


       foreach(@$data as $key=>$rowData){
        $paymentstatus =  GigOrders::where('gig_id', $rowData->gig_id)->first();
          $data[$key]->PaymentStatus =  @$paymentstatus->paymentStatus;

          $data[$key]->StopAddress = UserTripStop::where('service_category_address_id', $rowData->GigDetails->id)->get(); 
          
            // $data[$key]->UserRating  = UserRating::select('rating')->where('User_Id', $request->user_id)->where('Seller_Id', $rowData->Seller_ID)->orderBy('id', 'Desc')->first(); ;

            $SellerRationg = SellerRating::where('seller_user_id', $request->user_id)->where('seller_seller_id', $rowData->Seller_ID)->orderBy('id', 'Desc')->first();  

            // dd($SellerRationg);

            if($SellerRationg){
                $data[$key]->UserRating  = @$SellerRationg->sellerRating;
            }else{
                $data[$key]->UserRating  = "null";
            }

            $UserRating = UserRating::where('user_id', $request->user_id)->where('seller_id', $rowData->Seller_ID)->orderBy('id', 'Desc')->first();

             if($UserRating){
                $data[$key]->SellerRating  =  @$UserRating->rating;
             }else{
                $data[$key]->SellerRating  =  "null";
             }  

             $servicescharge = ServiceTax::where('id', '1')->first(); 
             $province = ServiceCategoryAddress::where('id', $rowData->gig_id)->first();  
               // if($province->proviance){
               // dd($province->proviance);
               // $data[$key]->tax = "sdsada";
                $Tax = UserTax::where('province', $province->proviance)->first();
                if($Tax){  
                  
                  // $data[$key]->tax = @$Tax->tax_rate;

                  $ServicePrice = ((@$rowData->offerAmount)*(@$servicescharge->serviceTax)/100)+($rowData->offerAmount);

                  $data[$key]->servicecharge = (@$rowData->offerAmount)*(@$servicescharge->serviceTax)/100;

                  $data[$key]->tax = (string)round(((@$ServicePrice)*(@$Tax->tax_rate)/100), 2);

                  $subtotalprice = ((@$ServicePrice)*(@$Tax->tax_rate)/100)+(@$ServicePrice);
                 $data[$key]->subTotal = (string)round(@$subtotalprice, 2);
                }else{
                  $data[$key]->tax = "0";
                  $ServicePrice = ((@$rowData->offerAmount)*(@$servicescharge->serviceTax)/100)+($rowData->offerAmount);
                  $data[$key]->servicecharge = (@$rowData->offerAmount)*(@$servicescharge->serviceTax)/100;
                  $data[$key]->subTotal = ((@$rowData->offerAmount)*(@$servicescharge->serviceTax)/100)+(@$rowData->offerAmount);
                  $subtotalprice = @$ServicePrice;
                  $data->subTotal = (string)round(@$subtotalprice, 2);
                }

            // $servicescharge = ServiceTax::where('id', '1')->first(); 
            // // $servicesTax = ServiceTax::where('id', '1')->first(); 
            // $province = ServiceCategoryAddress::where('id', $rowData->gig_id)->first();          
            // // dd($province->proviance);
            // $Tax = UserTax::where('province', $province->proviance)->first();          
            // $data[$key]->servicecharge = $servicescharge->serviceTax;
            // if($Tax){
            //   $data[$key]->tax = $Tax->tax_rate;
            //   $subtotalprice = ((@$rowData->offerAmount)*(@$servicescharge->serviceTax)/100)+(@$rowData->offerAmount)+(@$Tax->tax_rate);
            //   $data[$key]->subTotal = @$subtotalprice;
            // }else{
            //   $data[$key]->tax = "0";
            //   // dd($province->budget);
            //   $data[$key]->subTotal = ((@$rowData->offerAmount)*(@$servicescharge->serviceTax)/100)+@$rowData->offerAmount;
            // }

            // if($Tax){
            //   $data[$key]->tax = $Tax->tax_rate;
            //   $subtotalprice = (($rowData->offerAmount)*($Tax->tax_rate)/100)+($rowData->offerAmount)+($servicescharge->serviceTax);
            //   $data[$key]->subTotal = $subtotalprice;
            // }else{
            //   // dd($rowData->offerAmount);
            //   $data[$key]->tax = "0";
            //   // dd($province->budget);
            //   $data[$key]->subTotal = ($rowData->offerAmount)+($servicescharge->serviceTax);
            
            // }  
       }

       $response['data'] = $data; 
       return response()->json($response,200);
    }   

    
    function userviewlocation(Request $request){   
        $data = ServiceCategoryAddress::where('id', $request->gig_Id)->get();
        
        foreach($data as $key=>$rowData){
            // $data[$key]->SellerRating  = SellerRating::select('sellerRating')->where('seller_user_id', $rowData->user_id)->where('seller_seller_id', $request->seller_id)->orderBy('id', 'Desc')->first(); 
          $UserRating = UserRating::where('user_id', $request->user_id)->where('seller_id', $rowData->Seller_ID)->orderBy('id', 'Desc')->first();

             // dd($UserRating->rating);
          if($UserRating){
             $data[$key]->SellerRating  =  $UserRating->rating;
          }else{
             $data[$key]->SellerRating  =  "null";
          }   
          $data[$key]->Seller_Details = Seller::where('id', $request->seller_id)->first();
        }        

        $all = UsrRequestGig::where('id', $request->request_id)->first();
        foreach($data as $key=>$rowall){
            $data[$key]->GigStatus = $all->status;
        }
        $response['data'] = $data; 
       return response()->json($response,200);
    }
}