<?php

namespace App\Http\Controllers\Api\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\UserRating;
use Validator;

class RatingReviewController extends Controller
{
    
    function userratingreview(Request $request){
    	
    	$validations =[
	        'user_id'=>'required',
	        'seller_id'=>'required',
	        'rating' => 'required',
	        'review'=>'required',
	    ];   
	    $validator=Validator::make($request->all(),$validations);
	    if($validator->fails())
	    {
	        $response['message']=$validator->errors($validator)->first();
	        return response()->json($response,400);
	    } 
	    if(UserRating::where('User_Id', $request->user_id)->where('Seller_Id', $request->seller_id)->exists()){	    	
	    	//$existid = UserRating::where('User_Id', $request->user_id)->where('Seller_Id', $request->seller_id)->get();
	    	$regDate = date("Y-m-d");
		    $regTime = date("h:i:sa");
		    
	    	UserRating::where('User_Id', $request->user_id)->where('Seller_Id', $request->seller_id)->update([
	    		'rating' => $request->rating,
	    		'review' => $request->review,
	    		'gig_id' => $request->gig_id,
	    		'ratingRegisterDate' => $regDate,
	    		'registerTime' => $regTime,
	    	]);
	    	$response['message'] = "Comment updated successfully"; 
	    	$response['data'] = UserRating::where('User_Id', $request->user_id)->orderBy('id', 'Desc')->get(); 
	        return response()->json($response,200);

	    }else{

	    	// dd("not match");
		    $regDate = date("Y-m-d");
		    $regTime = date("h:i:sa");
		    UserRating::create([
	    		'User_Id' => $request->user_id,
	    		'Seller_Id' => $request->seller_id,
	    		'rating' => $request->rating,
	    		'review' => $request->review,
	    		'gig_id' => $request->gig_id,
	    		'ratingRegisterDate' => $regDate,
	    		'registerTime' => $regTime,
	    	]);
	    	$response['message'] = "Comment added successfully"; 
	    	$response['data'] = UserRating::where('User_Id', $request->user_id)->orderBy('id', 'Desc')->get(); 
	        return response()->json($response,200);
	    }    	
    }
}