<?php

namespace App\Http\Controllers\Api\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Model\AddUserAddress;
use App\Model\ServiceCategoryAddress;
use App\Model\UserTripStop;

class AddressManagementController extends Controller
{    
    public function useraddresslist(Request $request){
    	$data1 = User::select('id','address', 'address_latitude', 'address_longitude')->where('id', $request->user_id)->get();
        $data2 = AddUserAddress::where('user_id', $request->user_id)->get();
    	$result = $data1->merge($data2);
        $response['data'] = $result;        
        return response()->json($response,200);  
    }

    function UserGigAddress(Request $request){        
        // dd($request->all());
        
        $datapickup = ServiceCategoryAddress::select('id','pickUpAddress as savedaddress', 'pickUpLongtitude as savedlongitude', 'pickUpLatitude as savedlatitude', 'pickUpAddressType as savedAddressType', 'stopType')->where('user_id', $request->user_id)->where('is_delete_address', 0)->where('gigStatus', '!=','0')->get();
        
        foreach ($datapickup as $key => $value) {
            $datapickup[$key]->type = 'gigpickupaddress';             
        }

        $datadrop = ServiceCategoryAddress::select('id', 'dropAddress as savedaddress', 'dropLongtitude as savedlongitude', 'dropLatitude as savedlatitude', 'dropAddressType as savedAddressType', 'stopType')->where('user_id', $request->user_id)->where('is_delete_dropaddress', 0)->where('gigStatus', '!=','0')->get();

        foreach ($datadrop as $key23 => $valuerow) {
            $datadrop[$key23]->type = 'gigdropaddress';             
        }


        $data20 = ServiceCategoryAddress::select('id','pickUpAddress', 'pickUpLongtitude', 'pickUpLatitude', 'pickUpAddressType', 'dropAddress', 'dropLongtitude', 'dropLatitude', 'dropAddressType', 'stopType')->where('user_id', $request->user_id)->where('gigStatus', '!=','0')->get();

        // dd($data);
        // $stop_address[] = '';
        
         
        $stop_address = array();
        foreach ($data20 as $key => $value20) {
            $stop = UserTripStop::where('is_delete_address', 0)->where('service_category_address_id', $value20->id)->select('id', 'service_category_address_id', 'tripStopAddress as savedaddress', 'tripstopLatitude as savedlatitude', 'tripstopLongtitude as savedlongitude', 'tripStopAddressType as savedAddressType','stopType')->get();

            // $stop= UserTripStop::where('service_category_address_id', $value20->id)->get();
            if(count($stop)>0){
                foreach($stop as $key65=>$rowStop){
                    $stop[$key65]->type = "stopaddress";
                    $stop_address[] =$rowStop;
 
                }                
            }
        }

        // return response()->json($stop_address);


        // $datastopAddress = ServiceCategoryAddress::select('id','pickUpAddress', 'pickUpLongtitude', 'pickUpLatitude', 'pickUpAddressType', 'dropAddress', 'dropLongtitude', 'dropLatitude', 'dropAddressType', 'stopType')->where('user_id', $request->user_id)->where('gigStatus', '!=','0')->get();

        // foreach(@$datastopAddress as $key=>$rowData){
        //     $name = UserTripStop::select('id', 'service_category_address_id', 'tripStopAddress as pickUpAddress','tripstopLatitude as pickUpLongtitude', 'tripstopLongtitude as pickUpLatitude', 'stopType as pickUpAddressType')->where('service_category_address_id', @$rowData->id)->where('is_delete_address', 0)->get();
 
        //     foreach($name as $key1=>$value){
        //         $name[$key1]->type = 'stopaddress';
        //     }
        // }

        $data2 = AddUserAddress::where('user_id', $request->user_id)->select('id','address as savedaddress', 'address_latitude as savedlongitude', 'address_longitude as savedlatitude', 'saveAs as savedAddressType', 'city', 'province')->get();

        foreach ($data2 as $key3 => $data2value) {
            $data2[$key3]->type = 'secoundryaddress';
        }
        // $result = $data->merge($data2);
        // $result = $datapickup->merge($name);
        // $result1 = $datapickup->merge($datadrop);
        $result = array_merge($datapickup->toArray(), $datadrop->toArray());
        $result1 = array_merge($result, $data2->toArray());

        $response['data'] =  array_merge($result1, $stop_address);  
       
        // $response['stop_address'] = $stop_address;
       
        return response()->json($response,200); 
    }


    public function userupdateaddress(Request $request){ 
        // dd("fsdfs");

        if($request->user_id){
            // dd($request->all());
            AddUserAddress::where('id', $request->edit_id)->update([
                'address' =>$request->address,
                'address_latitude' =>$request->address_latitude,
                'address_longitude' =>$request->address_longitude,
                'province' => $request->province,
                'city' => $request->city,
                'saveAs' => $request->saveAs,
            ]);
            $data = AddUserAddress::where('id', $request->edit_id)->first();
            $response['data'] = $data;
            $response['message'] = "Address updated successfully";
            return response()->json($response,200); 
        }else{
            // dd("fdsfs");
            User::where('id', $request->edit_id)->update([
                'address' =>$request->address,
                'address_latitude' =>$request->address_latitude,
                'address_longitude' =>$request->address_longitude,
                'province' => $request->province,
                'city' => $request->city,
                // 'saveAs' => $request->saveAs,
            ]);
            $data = User::where('id', $request->edit_id)->first();
            $response['data'] = $data;
            $response['message'] = "Address updated successfully";
            return response()->json($response,200); 
        }
          
    }

    public function addmoreaddress(Request $request){
        // dd($request->all());
        AddUserAddress::create([
            'user_id' => $request->user_id,
            'address' => $request->address,
            'address_latitude' => $request->address_latitude,
            'address_longitude' => $request->address_longitude,
            'province' => $request->province,
            'saveAs' => $request->saveAs,             
        ]);
        // $response['data'] = $data;
        $response['message'] = "Address updated successfully";
        return response()->json($response,200);           
    }

    public function deleteuseraddress(Request $request){
        // dd();
        AddUserAddress::where('id', $request->edit_id)->delete();
        $response['message'] = "Address deleted successfully";
        return response()->json($response,200);  
    }

    public function DeleteUserGigAddress(Request $request){
        // dd($request->all());
        if($request->type == 'secoundryaddress'){
            // dd($request->id);
            AddUserAddress::where('id', $request->id)->delete();
            $response['message'] = 'Address deleted successfully';
            return response()->json($response,200); 
        
        }elseif($request->type == 'gigpickupaddress'){
            
            // UserTripStop::where('service_category_address_id', $request->id)->update([
            //     'is_delete_address' => '1',
            // ]);
            ServiceCategoryAddress::where('id', $request->id)->update([
                'is_delete_address' => '1',
            ]);
            $updateAddressStatus = ServiceCategoryAddress::find($request->id);
            $updateAddressStatus->is_delete_address = 1;
            if($updateAddressStatus->save()){
                $response['message'] = 'Address deleted successfully';
                return response()->json($response,200); 
            }else{
                $response['message'] = 'something went wrong!';
                return response()->json($response,400);
            }

        }elseif($request->type == 'gigdropaddress'){
            
            // UserTripStop::where('service_category_address_id', $request->id)->update([
            //     'is_delete_address' => '1',
            // ]);
            // ServiceCategoryAddress::where('id', $request->id)->update([
            //     'is_delete_dropaddress' => '1',
            // ]);
            $updateAddressStatus = ServiceCategoryAddress::find($request->id);
            $updateAddressStatus->is_delete_dropaddress = 1;
            if($updateAddressStatus->save()){
                $response['message'] = 'Address deleted successfully';
                return response()->json($response,200); 
            }else{
                $response['message'] = 'something went wrong!';
                return response()->json($response,400);
            }

        }elseif($request->type == 'stopaddress'){

            UserTripStop::where('id', $request->id)->update([
                'is_delete_address' => '1',
            ]);
            $updateAddressStatus = UserTripStop::find($request->id);
            $updateAddressStatus->is_delete_address = 1;
            if($updateAddressStatus->save()){
                $response['message'] = 'Address deleted successfully';
                return response()->json($response,200); 
            }else{
                $response['message'] = 'something went wrong!';
                return response()->json($response,400);
            }

        }else{
            $response['message'] = 'Sorry you can not deleted Address';
            return response()->json($response,200);             
        }
    }

}