<?php

namespace App\Http\Controllers\Api\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\CardDetails;
use App\Model\GigOrders;
use Validator;
use Carbon\Carbon;
use App\Stripe;
use Session;
use App\Model\UsrRequestGig;
use App\Model\Notification;
use App\User;
use App\Seller;
use App\Model\ServiceCategoryAddress;

class PaymentController extends Controller
{
    function AddVirtualCard(Request $request){
    	// dd($request->id);
    	if($request->type == '1'){
    		if(CardDetails::where('card_number', $request->card_number)->where('user_id', $request->user_id)->exists()){
    			return response()->json(['message'=>'This card already added']);
	    	}else{
	    		$saveCard = new CardDetails;
		    	$saveCard->card_number = $request->card_number;
		    	$saveCard->card_holder_name = $request->card_holder_name;
		    	$saveCard->card_expire_date = $request->card_expire_date;
		    	$saveCard->user_id = $request->user_id;
		    	if($saveCard->save()){
		    		return response()->json(['message'=>'Card added successfully', 'data'=>$saveCard],200);
		    	}else{
		    		return response()->json(['message'=>'Something went wrong!'],400);
		    	}
			}

    	}elseif($request->type == '2'){
    		CardDetails::where('id', $request->card_id)->delete();
    		return response()->json(['message'=>'Deleted successfully'],200);
    	}elseif($request->type == '3') {
    		
    		// dd($request->all());
    		$data = CardDetails::where('id', $request->card_id)->update([
    			'card_number' => $request->card_number,
    			'card_holder_name' => $request->card_holder_name,
    			'card_expire_date' => $request->card_expire_date,
    		]);    		
	    	return response()->json(['message'=>'Card updated successfully'],200);
	    }   	    	
    }

    function ViewCard(Request $request){
    	$data  =  CardDetails::where('user_id', $request->user_id)->get();
    	return response()->json(['message'=>'success', 'data' => $data],200);
    }

    function GigOrderPayment(Request $request){
        // dd($request->UserData->emailAddress);
        if(!empty($request->customerId)){
           $validations =[        
              'user_id' => 'required',
              'seller_id' => 'required',
              'gig_id' => 'required',
              'subtotal_price' => 'required', 
              'customerId' => 'required'        
            ]; 
        }else{
            $validations =[        
              'user_id' => 'required',
              'seller_id' => 'required',
              'gig_id' => 'required',
              'subtotal_price' => 'required', 
              'stripeToken' => 'required'        
            ]; 
        }         
        $validator=Validator::make($request->all(),$validations);
        if($validator->fails())
        {
           $response['message']=$validator->errors($validator)->first();
           return response()->json($response,400);
        }
        $this->couponUses($request->user_id, $request->coupon_discount);
        \Stripe\Stripe::setApiKey ('sk_test_51Hdr0XIzF8CI1MBL6tjnTGqudQ0FV0lJdcCBehI05JStAkyu2zGD4VyPGUp0odqaGfwVayeB6HFlGugLaSjshplA001IMg8rah' );
        try {
            if($request->currencyType == '$'){
                $currency = 'usd';
            }elseif($request->currencyType == '₹'){
                $currency = 'inr';
            }else{
                $currency = 'inr';
            }
            if(!empty($request->customerId)){
                // dd("sada");
                $charge = \Stripe\Charge::create ( array (                    
                    "amount"    => $request->subtotal_price * 100,
                    "currency"  => $currency,
                    'customer'  => $request->customerId,
                    "description" => "Test payment." 
                ) );
                Session::flash ( 'success-message', 'Payment receve successfully !' );

                $saveCard = new CardDetails;
                $saveCard->card_number = $charge->source->last4;
                $saveCard->card_holder_name = $charge->source->name; 
                $saveCard->user_id = $request->user_id;
                $saveCard->cardbrand = $charge->source->brand;
                $saveCard->customerId = $request->customerId;
                $saveCard->save();

                UsrRequestGig::where('gig_id', $request->gig_id)->where('status', 2)->update([
                    'status' => '4',
                    // 'status' => '2',
                    'bookingDate' => date('Y-m-d'),
                ]);      
                
                $updateGigStatus = ServiceCategoryAddress::find($request->gig_id);
                $updateGigStatus->gigStatus = '4';
                $updateGigStatus->save();

                // $paymentStatus = new GigOrders;
                // $paymentStatus->user_id = $request->user_id;
                // $paymentStatus->seller_id = $request->seller_id;
                // $paymentStatus->gig_id = $request->gig_id;
                // $paymentStatus->subtotal_price = $request->subtotal_price;
                // $paymentStatus->coupon_discount = $request->coupon_discount;
                // $paymentStatus->paymentStatus = '1';
                // $paymentStatus->PaymentDate = date('Y-m-d');
                // $paymentStatus->payment_mode = 'online';            
                // $paymentStatus->tranzectionId = $charge->balance_transaction;
                
                // $paymentStatus->customerId = $request->customerId;
                // $paymentStatus->fingerprint = $charge->source->fingerprint;
                 
                // if($paymentStatus->save()){
                // 9105413412

                $paymentStatus =  GigOrders::where(['user_id'=>$request->user_id, 'seller_id'=>$request->seller_id, 'gig_id'=>$request->gig_id])->update([
                  'subtotal_price' =>$request->subtotal_price,
                  'coupon_discount' =>$request->coupon_discount,
                  'paymentStatus' =>'1',
                  'PaymentDate' =>date('Y-m-d'),
                  'payment_mode' =>'online',
                  'tranzectionId' => $charge->balance_transaction,
                  'customerId' => $request->customerId,
                  'fingerprint' => $charge->source->fingerprint,

                ]);

                    $userDetails = User::where('id', $request->user_id)->first();
                    if($userDetails){
                       $body_text = "Your payment has been successfully receved";
                       $tokens = $userDetails->device_token;
                       $result =[
                         'is_applied'=>1,
                       ];
                       $is_result = '';
                       if($userDetails->device_type == '0'){
                            $is_result=$this->androidPushNotification($body_text, $tokens, $result);
                       }elseif($userDetails->device_type == '1'){
                            $is_result=$this->iosPushNotification($body_text, $tokens,$result);
                       } 
                       if($is_result == '1'){
                          $data = new Notification;
                          $data->user_id = $userDetails->id;
                          $data->message = $body_text;
                          $data->save();               
                       }
                    } 
                    $sellerDetails = Seller::where('id', $request->seller_id)->first();
                    // dd($sellerDetails);
                    $body_text = "Payment has been successfully received";
                    $tokens = $sellerDetails->device_token;
                    $result =[
                       'is_applied'=>1,
                    ];
                    $is_result = '';
                    if($sellerDetails->device_type == '0'){
                          $is_result=$this->androidPushNotification($body_text, $tokens, $result);
                    }elseif($sellerDetails->device_type == '1'){
                          $is_result=$this->iosPushNotification($body_text, $tokens,$result);
                    } 
                    if($is_result == '1'){
                        $data = new Notification;
                        $data->seller_id = $sellerDetails->id;
                        $data->message = $body_text;
                        $data->save();               
                     }                        

                    return response()->json(['message'=>'success', 'data' => @$paymentStatus], 200);
                // }else{
                //     return response()->json(['message'=>'something went wrong!'], 400);
                // }
            }else{
                $customer = \Stripe\Customer::create([
                    'email'     => $request->UserData->emailAddress,
                    'source'    => $request->stripeToken,  
                ]);             
                $charge = \Stripe\Charge::create ( array (                    
                    "amount"    => $request->subtotal_price * 100,
                    "currency"  => $currency,
                    'customer'  => $customer->id,
                    "description" => "Test payment." 
                ) );
                Session::flash ( 'success-message', 'Payment receved successfully!' );

                $saveCard = new CardDetails;
                $saveCard->card_number = $charge->source->last4;
                $saveCard->card_holder_name = $charge->source->name; 
                $saveCard->user_id = $request->user_id;
                $saveCard->cardbrand = $charge->source->brand;
                $saveCard->customerId = $customer->id;
                $saveCard->save();

                UsrRequestGig::where('gig_id', $request->gig_id)->where('status', 2)->update([
                    'status' => '4',
                    // 'status' => '2',
                    'bookingDate' => date('Y-m-d'),
                ]); 

                $updateGigStatus = ServiceCategoryAddress::find($request->gig_id);
                $updateGigStatus->gigStatus = '4';
                $updateGigStatus->save();
                    
                // $paymentStatus = new GigOrders;
                // $paymentStatus->user_id = $request->user_id;
                // $paymentStatus->seller_id = $request->seller_id;
                // $paymentStatus->gig_id = $request->gig_id;
                // $paymentStatus->subtotal_price = $request->subtotal_price;
                // $paymentStatus->coupon_discount = $request->coupon_discount;
                // $paymentStatus->paymentStatus = '1';
                // $paymentStatus->PaymentDate = date('Y-m-d');
                // $paymentStatus->payment_mode = 'online';            
                // $paymentStatus->tranzectionId = $charge->balance_transaction;

                // $paymentStatus->customerId = $customer->id;
                // $paymentStatus->fingerprint = $charge->source->fingerprint;
                 
                // if($paymentStatus->save()){

                 $paymentStatus =  GigOrders::where(['user_id'=>$request->user_id, 'seller_id'=>$request->seller_id, 'gig_id'=>$request->gig_id])->update([
                  'subtotal_price' =>$request->subtotal_price,
                  'coupon_discount' =>$request->coupon_discount,
                  'paymentStatus' =>'1',
                  'PaymentDate' =>date('Y-m-d'),
                  'payment_mode' =>'online',
                  'tranzectionId' => $charge->balance_transaction,
                  'customerId' => $request->customerId,
                  'fingerprint' => $charge->source->fingerprint,

                ]);


                    $userDetails = User::where('id', $request->user_id)->first();
                    if($userDetails){
                       $body_text = "Your payment has been successfully received";
                       $tokens = $userDetails->device_token;
                       $result =[
                         'is_applied'=>1,
                       ];
                       $is_result = '';
                       if($userDetails->device_type == '0'){
                            $is_result=$this->androidPushNotification($body_text, $tokens, $result);
                       }elseif($userDetails->device_type == '1'){
                            $is_result=$this->iosPushNotification($body_text, $tokens,$result);
                       } 
                       if($is_result == '1'){
                          $data = new Notification;
                          $data->user_id = $userDetails->id;
                          $data->message = $body_text;
                          $data->save();               
                       }
                    }

                    $sellerDetails = Seller::where('id', $request->seller_id)->first();
                    // dd($sellerDetails);
                    $body_text = "Payment has been successfully received";
                    $tokens = $sellerDetails->device_token;
                    $result =[
                       'is_applied'=>1,
                    ];
                    $is_result = '';
                    if($sellerDetails->device_type == '0'){
                          $is_result=$this->androidPushNotification($body_text, $tokens, $result);
                    }elseif($sellerDetails->device_type == '1'){
                          $is_result=$this->iosPushNotification($body_text, $tokens,$result);
                    } 
                    if($is_result == '1'){
                        $data = new Notification;
                        $data->seller_id = $sellerDetails->id;
                        $data->message = $body_text;
                        $data->save();               
                     }

                    return response()->json(['message'=>'success', 'data' => $paymentStatus], 200);
                // }else{
                //     return response()->json(['message'=>'something went wrong!'], 400);
                // }

            }

        }catch(\Stripe\Exception\CardException $e) {
            // Since it's a decline, \Stripe\Exception\CardException will be caught
            echo 'Status is:' . $e->getHttpStatus() . '\n';
            echo 'Type is:' . $e->getError()->type . '\n';
            echo 'Code is:' . $e->getError()->code . '\n';
            // param is '' in this case
            echo 'Param is:' . $e->getError()->param . '\n';
            echo 'Message is:' . $e->getError()->message . '\n';

        }catch ( \Exception $e ) {
            Session::flash ( 'fail-message', "Error! Please Try again.".json_encode($e->getMessage(), true) );
            //return ["result"    =>"payment Failed","token"     =>$req->token,"amount"    =>$req->price];
            return ["info"=>$e->getMessage()];
            //return Redirect::back ();
        }
    }


    
    function userGigconfirmation(Request $request){

       $validations =[        
          'user_id' => 'required',
          'seller_id' => 'required',
          'gig_id' => 'required',
          'subtotal_price' => 'required', 
        
        ]; 
        $validator=Validator::make($request->all(),$validations);
        if($validator->fails())
        {
           $response['message']=$validator->errors($validator)->first();
           return response()->json($response,400);
        }

        UsrRequestGig::where('gig_id', $request->gig_id)->where('status', 2)->update([
            'status' => '4',
            // 'status' => '2',
            'bookingDate' => date('Y-m-d'),
        ]);      
        
        $updateGigStatus = ServiceCategoryAddress::find($request->gig_id);
        $updateGigStatus->gigStatus = '4';
        $updateGigStatus->save();

        $paymentStatus = new GigOrders;
        $paymentStatus->user_id = $request->user_id;
        $paymentStatus->seller_id = $request->seller_id;
        $paymentStatus->gig_id = $request->gig_id;
        $paymentStatus->subtotal_price = $request->subtotal_price;
        $paymentStatus->coupon_discount = $request->coupon_discount;
        $paymentStatus->paymentStatus = '0';
        $paymentStatus->payment_mode = 'pending';            
        $paymentStatus->save();
        return response()->json(['message'=>'success', 'data' => $paymentStatus], 200);
    }


}