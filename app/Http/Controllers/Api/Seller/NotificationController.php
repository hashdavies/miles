<?php

namespace App\Http\Controllers\Api\Seller;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Notification;

class NotificationController extends Controller
{
    public function SellerNotificationList(Request $request){

    	$data = Notification::where('seller_id', $request->UserData->id)->orderBy('id', 'Desc')->get();
        return response()->json(['message'=> 'success', 'data'=>$data],200);
    }

    function SellerUnReadNotification(Request $request){
        $data = Notification::where('seller_id', $request->seller_id)->where('read_unread_notification', '1')->count();
        return response()->json(['message'=> 'success', 'data'=>$data],200);
    }

    function SellerReadNotification(Request $request){
        $data = Notification::where('seller_id', $request->seller_id)->where('read_unread_notification', '1')->update([
            'read_unread_notification' => '0',
        ]);
        return response()->json(['message'=> 'success', 'data'=>0],200);
    }

}
