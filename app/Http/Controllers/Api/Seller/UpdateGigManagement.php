 <?php

namespace App\Http\Controllers\Api\Seller;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\UsrRequestGig;
use App\Model\UserRating;
use App\Model\SellerRating;
use Validator;

class UpdateGigManagement extends Controller
{
    
    function sellerupdatepickupaddress(Request $request){
        // dd("sadas");
    	UsrRequestGig::where('id', $request->gig_id)->update([
    		'status' => '7', 
    	]);
    	$response['message'] = "Pickup address updated successfully";
    	return response()->json($response, 200);
    }

    function sellerupdatetranslt(Request $request){
    	UsrRequestGig::where('id', $request->gig_id)->update([
    		'status' => '8', 
    	]);
    	$response['message'] = "Pickup in translt updated successfully";
    	return response()->json($response, 200);
    }

    function sellerupdatecompleted(Request $request){
    	UsrRequestGig::where('id', $request->gig_id)->update([
    	    'status' => '9', 
    	]);
    	$response['message'] = "Pickup completed successfully";
    	return response()->json($response, 200);
    }


    function sellergiveratingreviewed(Request $request){
        $validations =[
            'seller_user_id'=>'required',
            'seller_seller_id'=>'required',
            'seller_rating' => 'required',
            'seller_review'=>'required',
            'seller_gigId' =>'required',
        ];   
        $validator=Validator::make($request->all(),$validations);
        if($validator->fails())
        {
            $response['message']=$validator->errors($validator)->first();
            return response()->json($response,400);
        } 
        if(SellerRating::where('seller_user_id', $request->seller_user_id)->where('seller_seller_id', $request->seller_seller_id)->exists()){
            //$existid = UserRating::where('User_Id', $request->user_id)->where('Seller_Id', $request->seller_id)->get();
            $regDate = date("Y-m-d");
            $regTime = date("h:i:sa");
            SellerRating::where('seller_user_id', $request->seller_user_id)->where('seller_seller_id', $request->seller_seller_id)->update([
                'sellerRating' => $request->seller_rating,
                'seller_review' => $request->seller_review,
                'seller_gigId' => $request->seller_gigId,
                'seller_registerDate' => $regDate,
                'seller_registerTime' => $regTime,
            ]);
            $response['message'] = "Comment updated successfully"; 
            $response['data'] = SellerRating::where('seller_seller_id', $request->seller_seller_id)->orderBy('id', 'Desc')->get();  
            return response()->json($response,200);

        }else{
            // dd($request->seller_rating);
            $regDate = date("Y-m-d");
            $regTime = date("h:i:sa");
            SellerRating::create([
                'seller_user_id' => $request->seller_user_id,
                'seller_seller_id' => $request->seller_seller_id,
                'seller_review' => $request->seller_review,
                'sellerRating' => $request->seller_rating,
                'seller_gigId' => $request->seller_gigId,
                'seller_registerDate' => $regDate,
                'seller_registerTime' => $regTime,
            ]);

            $response['message'] = "Comment added successfully"; 
            $response['data'] = SellerRating::where('seller_seller_id', $request->seller_seller_id)->orderBy('id', 'Desc')->get(); 
            return response()->json($response,200); 
        }
        // $response['data'] = UserRating::where('User_Id', $request->user_id)->where('Seller_Id', $request->seller_id)->orderBy('id', 'Desc')->get(); 
        // return response()->json($response,200);
    }

}