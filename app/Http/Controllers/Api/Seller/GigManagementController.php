<?php

namespace App\Http\Controllers\Api\Seller;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\ServiceCategoryAddress;
use App\Model\GigOffer;
use App\Model\UsrRequestGig;
use App\Model\ServiceCategory;
use App\User;
use App\Model\SellerRating;
use App\Model\Notification;
use App\Seller;
use App\Model\UserTripStop;
use App\Model\GigOrders;


class GigManagementController extends Controller
{
    function sellerpendinggig(Request $request){
      $UserGigDetail = UsrRequestGig::where('Seller_ID', $request->seller_id)->where('status', '0')->get();
      foreach(@$UserGigDetail as $key=>$rowData){

         $data = ServiceCategoryAddress::with('StopAddress')->select('id', 'pickUpAddress', 'pickUpLongtitude', 'pickUpLatitude','additional_services','dropAddress', 'dropLongtitude', 'dropLatitude','budget','pickupDate','pickupTime','created_at as posted_date')->where('id', $rowData->gig_id)->first();
      }
      $response['data'] = @$data; 
      return response()->json($response,200);
    }

    function sellergigoffer(Request $request){      
        // dd("asdas");
        // $data = ServiceCategoryAddress::with('UserName')->with('CategoryName')->orderBy('id','desc')->get();
        $Rdate = date('Y-m-d');
        $data = UsrRequestGig::where('Seller_ID', $request->seller_id)->whereIn('status', array('0', '2', '4'))->orderBy('id','desc')->get();
        foreach($data as $key => $rowimage){  


          $data[$key]->StopAddress = UserTripStop::where('service_category_address_id', $rowimage->gig_id)->get(); 

          // $data[$key]->UserRating  = SellerRating::select('rating')->where('User_Id', $rowimage->User_ID)->where('Seller_Id', $request->seller_id)->orderBy('id', 'Desc')->first(); 
          
          $SellerRating = SellerRating::where('seller_user_id', $rowimage->User_ID)->where('seller_seller_id', $request->seller_id)->orderBy('id', 'Desc')->first();
         
          if($SellerRating){
             $data[$key]->UserRating  =  $SellerRating->sellerRating;
          }else{
             $data[$key]->UserRating  =  "null";
          } 
          $data[$key]->UserGigDetail = ServiceCategoryAddress::with('CategoryName')->with('UserName')->where('id', $rowimage->gig_id)->first();
        }
    	$response['data'] = $data; 
    	return response()->json($response,200);
    }
    
    function sellergigignore(Request $request){
        $currentDate = date('Y-m-d'); 
        UsrRequestGig::where('id', $request->gig_Id)->update([
            'status' => '1',
            'gigCompleteDate' =>  $currentDate, 
        ]);
        $allData = UsrRequestGig::where('id', $request->gig_Id)->first();
        // dd($allData->User_ID);


        $userDetails = User::where('id', $allData->User_ID)->first();
         // dd($userDetails->device_token);

         $body_text = "Your gig has been ingored";
         $tokens = $userDetails->device_token;
         $result =[
           'is_applied'=>1,
         ];
         $is_result = '';
         if($userDetails->device_type == '0'){
              $is_result=$this->androidPushNotification($body_text, $tokens, $result);
         }elseif($userDetails->device_type == '1'){
              $is_result=$this->iosPushNotification($title,$body_text, $tokens,$result);
         } 
         
         if($is_result == '1'){
            $data = new Notification;
            $data->user_id = $allData->User_ID;
            $data->message = $body_text;
            $data->save();               
         }

         // $sellerDetails = Seller::where('id', $request->seller_id)->first();
         // // dd($sellerDetails->device_token);
         // $body_text = "Thankyou for give offer for New Gig";
         // $tokens = $sellerDetails->device_token;
         // $result =[
         //   'is_applied'=>1,
         // ];
         // $is_result = '';
         // if($sellerDetails->device_type == '0'){
         //      $is_result=$this->androidPushNotification($body_text, $tokens, $result);
         // }elseif($sellerDetails->device_type == '1'){
         //      $is_result=$this->iosPushNotification($body_text, $tokens,$result);
         // } 
         // if($is_result == '1'){
         //    $data = new Notification;
         //    $data->seller_id = $sellerDetails->id;
         //    $data->message = $body_text;
         //    $data->save();               
         // }
         
        $response['message'] = "Your Gig has been ignore successfully"; 
        return response()->json($response,200);
    }

    function sellermakeoffer(Request $request){    	
        $rDate = date('Y-m-d');
        $rTime = date("h:i:sa");
        $currentDate = date('Y-m-d');        
        UsrRequestGig::where('User_ID', $request->user_id)->where('Seller_ID', $request->seller_id)->where('gig_id', $request->gig_id)->update([
            'gigofferDate' => $rDate,
            'gigofferTime' => $rTime,
            'offerAmount' => $request->offerAmount,
            'status' => '2',
            'gigCompleteDate' =>  $currentDate,
        ]);
         $userDetails = User::where('id', $request->user_id)->first();
         $body_text = "Dear Customer you have offer for your gig";
         $tokens = $userDetails->device_token;
         $result =[
           'is_applied'=>1,
         ];
         $is_result = '';
         if($userDetails->device_type == '0'){
              $is_result=$this->androidPushNotification($body_text, $tokens, $result);
         }elseif($userDetails->device_type == '1'){
              $is_result=$this->iosPushNotification($body_text, $tokens,$result);
         } 
         if($is_result == '1'){
            $data = new Notification;
            $data->user_id = $request->user_id;
            $data->message = $body_text;
            $data->save();               
         }

         // $sellerDetails = Seller::where('id', $request->seller_id)->first();
         // $body_text = "Thankyou for giving offer for new gig";
         // $tokens = $sellerDetails->device_token;
         // $result =[
         //   'is_applied'=>1,
         // ];
         // $is_result = '';
         // if($sellerDetails->device_type == '0'){
         //      $is_result=$this->androidPushNotification($body_text, $tokens, $result);
         // }elseif($sellerDetails->device_type == '1'){
         //      $is_result=$this->iosPushNotification($body_text, $tokens,$result);
         // } 
         // if($is_result == '1'){
         //    $data = new Notification;
         //    $data->seller_id = $sellerDetails->id;
         //    $data->message = $body_text;
         //    $data->save();               
         // }
            
    	$response['message'] = "Gig offer save successfully";
    	return response()->json($response, 200);
    }

    function sellergigorder(Request $request){      

        $data = UsrRequestGig::with('UserName')->with('SellerName')->where('Seller_ID', $request->seller_id)->where('status', '4')->orderBy('id', 'Desc')->get();
        foreach($data as $key => $rowdata){  
            // $data[$key]->UserRating  = UserRating::select('rating')->where('User_Id', $rowdata->User_ID)->where('Seller_Id', $request->seller_id)->orderBy('id', 'Desc')->first(); 

          $SellerRating = SellerRating::where('seller_user_id', $rowdata->User_ID)->where('seller_seller_id', $request->seller_id)->orderBy('id', 'Desc')->first();
         
              if($SellerRating){
                 $data[$key]->UserRating  =  $SellerRating->sellerRating;
              }else{
                 $data[$key]->UserRating  =  "null";
              }
            $data[$key]->gig_details = ServiceCategoryAddress::with('CategoryName')->where('id', $rowdata->gig_id)->first();
        }
        $response['data'] = $data;
        return response()->json($response, 200);
    }


    function sellerstartgig(Request $request){
        $currentDate = date('Y-m-d');
        UsrRequestGig::where('User_ID', $request->user_id)->where('Seller_ID', $request->seller_id)->where('gig_id', $request->gig_id)->update([
             'status' => '5',
             'gigCompleteDate' =>  $currentDate, 
        ]);

         $userDetails = User::where('id', $request->user_id)->first();
         if($userDetails){
           $body_text = "Dear customer your gig has been started and can be tracked now";
           $tokens = $userDetails->device_token;
           $result =[
             'is_applied'=>1,
           ];
           $is_result = '';
           if($userDetails->device_type == '0'){
                $is_result=$this->androidPushNotification($body_text, $tokens, $result);
           }elseif($userDetails->device_type == '1'){
                $is_result=$this->iosPushNotification($body_text, $tokens,$result);
           } 
           if($is_result == '1'){
              $data = new Notification;
              $data->user_id = $userDetails->id;
              $data->message = $body_text;
              $data->save();               
           }
         }      

        $response['message'] = 'Gig start successfully';
        return response()->json($response, 200);
    }

    function sellerongoinggig(Request $request){
    
        $gigdata = UsrRequestGig::with('UserName')->with('SellerName')->where('Seller_ID', $request->seller_id)->whereIn('status', array('5','7','8'))->orderBy('id', 'Desc')->get();
        foreach($gigdata as $key => $rowdata){   

            $gigdata[$key]->StopAddress = UserTripStop::where('service_category_address_id', $rowdata->gig_id)->get();
            
            // $gigdata[$key]->UserRating  = UserRating::select('rating')->where('User_Id', $rowdata->User_ID)->where('Seller_Id', $request->seller_id)->first();
             // $data[$key]->UserRating = "dds";
                         
             $SellerRating = SellerRating::where('seller_user_id', $rowdata->User_ID)->where('seller_seller_id', $request->seller_id)->orderBy('id', 'Desc')->first();
         
              if($SellerRating){
                 $gigdata[$key]->UserRating  =  $SellerRating->sellerRating;
              }else{
                 $gigdata[$key]->UserRating  =  "null";
              }

            $gigdata[$key]->gig_details = ServiceCategoryAddress::with('CategoryName')->where('id', $rowdata->gig_id)->first();
        }

        $response['data'] = $gigdata;
        return response()->json($response, 200);
    }

    function sellercompleatedgig(Request $request){
        $currentDate = date('Y-m-d');
        $data = UsrRequestGig::where('id', $request->gig_id)->update([
            'status' => '9',
            'gigCompleteDate' =>  $currentDate,                      
        ]);
        //dd($request->UserData->id);

        $sellerDetails = Seller::where('id', $request->UserData->id)->first();
        // dd($sellerDetails->device_token);
         $body_text = "Gig has been successfully completed";
         $tokens = $sellerDetails->device_token;
         $result =[
           'is_applied'=>1,
         ];
         $is_result = '';
         if($sellerDetails->device_type == '0'){
              $is_result=$this->androidPushNotification($body_text, $tokens, $result);
         }elseif($sellerDetails->device_type == '1'){
              $is_result=$this->iosPushNotification($body_text, $tokens,$result);
         } 
         if($is_result == '1'){
            $data = new Notification;
            $data->seller_id = $sellerDetails->id;
            $data->message = $body_text;
            $data->save();               
         }

        $userDetails = User::where('id', $request->user_id)->first();
        if($userDetails){
           $body_text = "Dear customer your gig has been completed";
           $tokens = $userDetails->device_token;
           $result =[
             'is_applied'=>1,
           ];
           $is_result = '';
           if($userDetails->device_type == '0'){
                $is_result=$this->androidPushNotification($body_text, $tokens, $result);
           }elseif($userDetails->device_type == '1'){
                $is_result=$this->iosPushNotification($body_text, $tokens,$result);
           } 
           if($is_result == '1'){
              $data = new Notification;
              $data->user_id = $userDetails->id;
              $data->message = $body_text;
              $data->save();               
           }
        }         
        $response['message'] = 'Gig compleated successfully';
        return response()->json($response, 200);
    }

    function sellerpastgig(Request $request){
        $data = UsrRequestGig::with('UserName')->with('SellerName')->where('Seller_ID', $request->seller_id)->whereIn('status', array('1','3','6','9'))->orderBy('id', 'Desc')->get();
        
        foreach($data as $key => $rowdata){   
           
           $paymentstatus =  GigOrders::where('gig_id', $rowdata->gig_id)->first();
           $data[$key]->PaymentStatus =  @$paymentstatus->paymentStatus;


           $data[$key]->StopAddress = UserTripStop::where('service_category_address_id', $rowdata->gig_id)->get();

            // $data[$key]->UserRating  = UserRating::select('rating')->where('User_Id', $rowdata->User_ID)->where('Seller_Id', $request->seller_id)->orderBy('id', 'Desc')->first();
             $SellerRating = SellerRating::where('seller_user_id', $rowdata->User_ID)->where('seller_seller_id', $request->seller_id)->orderBy('id', 'Desc')->first();
         
              if($SellerRating){
                 $data[$key]->UserRating  =  $SellerRating->sellerRating;
              }else{
                 $data[$key]->UserRating  =  "null";
              }

            $data[$key]->gig_details = ServiceCategoryAddress::with('CategoryName')->where('id', $rowdata->gig_id)->first();
        }
        $response['data'] = $data;
        return response()->json($response, 200);       
    }

    function sellerviewlocation(Request $request){      
        // dd($request->all());
        $data = ServiceCategoryAddress::with('UserName')->where('id', $request->gig_Id)->get();

        $all = UsrRequestGig::where('id', $request->request_id)->first();
        // dd($all);
        foreach($data as $key=>$rowall){
            // $data[$key]->UserRating  = UserRating::select('rating')->where('User_Id', $rowall->User_ID)->where('Seller_Id', $request->seller_id)->orderBy('id', 'Desc')->first();            
            $SellerRating = SellerRating::where('seller_user_id', $rowall->user_id)->where('seller_seller_id', $request->seller_id)->first();         
              //dd($rowall->user_id);
              if($SellerRating){
                 $data[$key]->UserRating  =  $SellerRating->sellerRating;
              }else{
                 $data[$key]->UserRating  =  "null";
              }
            $data[$key]->GigStatus = $all->status;
        }
        $response['data'] = $data; 
        return response()->json($response,200);
    }
}