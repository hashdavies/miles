<?php

namespace App\Http\Controllers\Api\Seller;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\SellerSupport;

class SellerChatController extends Controller
{
    function selleradminchat(Request $request){

    	$data = new SellerSupport;
    	$data->seller_id = $request->seller_id;
    	$data->seller_message = $request->seller_message;
    	$data->save();

    	$alldata = SellerSupport::where('seller_id', $request->seller_id)->get();
   		$response['data'] = $alldata;
   		return response()->json($response, 200);

    }

}
