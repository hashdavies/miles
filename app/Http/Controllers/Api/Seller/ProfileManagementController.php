<?php

namespace App\Http\Controllers\Api\Seller;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Seller;
use Validator;
use Hash;
use Storage;

class ProfileManagementController extends Controller
{
    function sellerupdateprofile(Request $request){
      if($request->serviceProviderType == 'business')
	    {   
    	$validations =[        
          'businessName' => 'required|string',
          'firstName' => 'required',
          'lastName' => 'required',
          'nickName' => 'required',
          'email' => 'required|email',
          'countryCode' => 'required|numeric',
          'mobileNo' => 'required|numeric',
          // 'businessLogo' => 'required|mimes:jpg,jpeg,png,svg|max:8192',
          // 'password' => 'required',
          // 'cpassword' => 'required|same:password', 
          'address' => 'required',
      ]; 
      $validator=Validator::make($request->all(),$validations);
      if($validator->fails())
      {
          $response['message']=$validator->errors($validator)->first();
          return response()->json($response,400);
      }

      if ($request->hasFile('businessLogo')) {
           $image = $request->file('businessLogo');
           $s3 = \Storage::disk('s3');
           $businessLogo = 'businesslogo_'.time() .'.'. $image->getClientOriginalExtension();
           $filepath='Serviceprovider/' . $businessLogo;
           $s3->put($filepath, file_get_contents($image), 'public');
      }
      if ($request->hasFile('profileImage')) {
           $imageprofile = $request->file('profileImage');
           $s3 = \Storage::disk('s3');
           $profileImage = 'profileimage_'.time() .'.'. $imageprofile->getClientOriginalExtension();
           $filepathprofile='Serviceprovider/' . $profileImage;
           $s3->put($filepathprofile, file_get_contents($imageprofile), 'public');
      }      
      
      // dd($request->all());
      $sellerupdate = Seller::find($request->edit_id);
      $sellerupdate->serviceProviderType = $request->serviceProviderType;
      $sellerupdate->businessName        = $request->businessName;
      $sellerupdate->firstName           = $request->firstName;
      $sellerupdate->lastName            = $request->lastName;
      $sellerupdate->nickName            = $request->nickName;
      $sellerupdate->email               = $request->email;
      // $sellerupdate->password            = hash::make($request->password);
      $sellerupdate->countryCode         = $request->countryCode;
      $sellerupdate->mobileNo            = $request->mobileNo;
      $sellerupdate->address             = $request->address;

      if($request->password){
          $sellerupdate->password            = hash::make($request->password);
      }
      if($request->hasFile('businessLogo')){
        $sellerupdate->businesslogo        = $businessLogo;
      }
      if($request->hasFile('profileImage')){
        $sellerupdate->profileImage        = $profileImage;
      }

      $sellerupdate->save();

      $data = Seller::where('id', $request->edit_id)->first();
      $response['message']='Profile updated successfully';
      $response['data'] = $data;        
      return response()->json($response,200);
    
    }elseif ($request->serviceProviderType == 'freelancer'){

      $validations =[        
          'profileImage' => 'required|mimes:jpg,png,svg,jpeg|max:8192',
          'firstName' => 'required',
          'lastName' => 'required',
          'nickName' => 'required',
          'email' => 'required|email',
          'countryCode' => 'required|numeric',
          'mobileNo' => 'required|numeric',
          // 'businessLogo' => 'required|mimes:jpg,jpeg,png,svg|max:8192',
          // 'password' => 'required',
          // 'cpassword' => 'required|same:password',
          'address' => 'required',
      ]; 
      $validator=Validator::make($request->all(),$validations);
      if($validator->fails())
      {
          $response['message']=$validator->errors($validator)->first();
          return response()->json($response,400);
      }
      if ($request->hasFile('businessLogo')) {
           $image = $request->file('businessLogo');
           $s3 = \Storage::disk('s3');
           $businessLogo = 'businesslogo_'.time() .'.'. $image->getClientOriginalExtension();
           $filepath='Serviceprovider/' . $businessLogo;
           $s3->put($filepath, file_get_contents($image), 'public');
     
      }
      if ($request->hasFile('profileImage')) {
           $imageprofile = $request->file('profileImage');
           $s3 = \Storage::disk('s3');
           $profileImage = 'profileimage_'.time() .'.'. $imageprofile->getClientOriginalExtension();
           $filepathprofile='Serviceprovider/' . $profileImage;
           $s3->put($filepathprofile, file_get_contents($imageprofile), 'public');      	
      }         
        $sellerupdate = Seller::find($request->edit_id);
        $sellerupdate->serviceProviderType = $request->serviceProviderType;
        $sellerupdate->businessName        = $request->businessName;
        $sellerupdate->firstName           = $request->firstName;
        $sellerupdate->lastName            = $request->lastName;
        $sellerupdate->nickName            = $request->nickName;
        $sellerupdate->email               = $request->email;
        // $sellerupdate->password            = hash::make($request->password);
        $sellerupdate->countryCode         = $request->countryCode;
        $sellerupdate->mobileNo            = $request->mobileNo;
        $sellerupdate->address             = $request->address;
        
        if($request->password){
          $sellerupdate->password            = hash::make($request->password);
        }
        if($request->hasFile('businessLogo')){
          $sellerupdate->businesslogo        = $businessLogo;
        }
        if($request->hasFile('profileImage')){
          $sellerupdate->profileImage        = $profileImage;
        }
        $sellerupdate->save();

      $data = Seller::where('id', $request->edit_id)->first();

      $response['message']='Profile updated successfully';
      $response['data'] = $data;        
      return response()->json($response,200);
    }
  }
}