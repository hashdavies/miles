<?php

namespace App\Http\Controllers\Api\Seller;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\UsrRequestGig;
use App\Model\UserRating;
use App\Model\SellerRating;
use Validator;
use App\User;
use App\Seller;
use App\Model\Notification;

class UpdateGigAddressManagement extends Controller
{
    function sellerupdatepickupaddress(Request $request){
        // dd("sadas");
    	UsrRequestGig::where('id', $request->gig_id)->update([
    		'status' => '7', 
    	]);
    	$response['message'] = "Pickup address updated successfully";
    	return response()->json($response, 200);
    }

    function sellerupdatetranslt(Request $request){
    	UsrRequestGig::where('id', $request->gig_id)->update([
    		'status' => '8', 
    	]);
    	$response['message'] = "Pickup in translt updated successfully";
    	return response()->json($response, 200);
    }

    function sellerupdatecompleted(Request $request){
    	UsrRequestGig::where('id', $request->gig_id)->update([
    	    'status' => '9', 
    	]);

        $alldata = UsrRequestGig::where('id', $request->gig_id)->first();
         
        $sellerDetails = Seller::where('id', $alldata->Seller_ID)->first();
        // dd($sellerDetails->device_token);
         $body_text = "Gig has been successfully completed";
         $tokens = $sellerDetails->device_token;
         $result =[
           'is_applied'=>1,
         ];
         $is_result = '';
         if($sellerDetails->device_type == '0'){
              $is_result=$this->androidPushNotification($body_text, $tokens, $result);
         }elseif($sellerDetails->device_type == '1'){
              $is_result=$this->iosPushNotification($body_text, $tokens,$result);
         } 
         if($is_result == '1'){
            $data = new Notification;
            $data->seller_id = $alldata->Seller_ID;
            $data->message = $body_text;
            $data->save();               
         }
         // dd($alldata->User_ID);
        
        $userDetails = User::where('id', $alldata->User_ID)->first();
        
        if($userDetails){
           $body_text = "Dear customer your gig has been completed";
           $tokens = $userDetails->device_token;
           $result =[
             'is_applied'=>1,
           ];
           $is_result = '';
           
           if($userDetails->device_type == '0'){
                $is_result=$this->androidPushNotification($body_text, $tokens, $result);
           }elseif($userDetails->device_type == '1'){
                $is_result=$this->iosPushNotification($body_text, $tokens,$result);
           } 
           if($is_result == '1'){
              $data = new Notification;
              $data->user_id = $alldata->User_ID;
              $data->message = $body_text;
              $data->save();               
           }
        }

    	$response['message'] = "Pickup completed successfully";
    	return response()->json($response, 200);
    }

    function sellergiveratingreview(Request $request){
        $validations =[
            'seller_user_id'=>'required',
            'seller_seller_id'=>'required',
            'seller_rating' => 'required',
            'seller_review'=>'required',
            'seller_gigId' =>'required',
        ];   
        $validator=Validator::make($request->all(),$validations);
        if($validator->fails())
        {
            $response['message']=$validator->errors($validator)->first();
            return response()->json($response,400);
        } 
        if(SellerRating::where('seller_user_id', $request->seller_user_id)->where('seller_seller_id', $request->seller_seller_id)->exists()){
            
            //$existid = UserRating::where('User_Id', $request->user_id)->where('Seller_Id', $request->seller_id)->get();
            $regDate = date("Y-m-d");
            $regTime = date("h:i:sa");
            
            SellerRating::where('seller_user_id', $request->seller_user_id)->where('seller_seller_id', $request->seller_seller_id)->update([
                'sellerRating' => $request->seller_rating,
                'seller_review' => $request->seller_review,
                'seller_gigId' => $request->seller_gigId,
                'seller_registerDate' => $regDate,
                'seller_registerTime' => $regTime,
            ]);
            $response['message'] = "Comment updated successfully"; 
            $response['data'] = SellerRating::where('seller_seller_id', $request->seller_seller_id)->orderBy('id', 'Desc')->get();  
            return response()->json($response,200);
        }else{
            // dd($request->seller_rating);
            $regDate = date("Y-m-d");
            $regTime = date("h:i:sa");
            SellerRating::create([
                'seller_user_id' => $request->seller_user_id,
                'seller_seller_id' => $request->seller_seller_id,
                'seller_review' => $request->seller_review,
                'sellerRating' => $request->seller_rating,
                'seller_gigId' => $request->seller_gigId,
                'seller_registerDate' => $regDate,
                'seller_registerTime' => $regTime,
            ]);
            $response['message'] = "Comment added successfully"; 
            $response['data'] = SellerRating::where('seller_seller_id', $request->seller_seller_id)->orderBy('id', 'Desc')->get(); 
            return response()->json($response,200); 
        }
        // $response['data'] = UserRating::where('User_Id', $request->user_id)->where('Seller_Id', $request->seller_id)->orderBy('id', 'Desc')->get(); 
        // return response()->json($response,200);
    }
}