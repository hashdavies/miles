<?php

namespace App\Http\Controllers\Api\Seller;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\AddSellerAddress;
use App\Seller\Seller;
 
use App\Model\SellerRating;
use Validator;

class AddressManagement extends Controller
{
    public function selleraddaddress(Request $request){
        
        // dd($request->all());
        $data = AddSellerAddress::create([
            'seller_id' => $request->seller_id,
            'address' => $request->address,
            'address_latitude' => $request->address_latitude,
            'address_longitude' => $request->address_longitude,
            'province' => $request->province,
            'saveAs' => $request->saveAs,
            'city' => $request->city,             
        ]);
        // $response['data'] = $data;
        $response['message'] = "Address updated successfully";
        return response()->json($response,200);           
    }

    public function sellerupdateaddress(Request $request){

    	if($request->seller_id){
            AddSellerAddress::where('id', $request->edit_id)->update([
                'address' =>$request->address,
                'address_latitude' =>$request->address_latitude,
                'address_longitude' =>$request->address_longitude,
                'province' => $request->province,
                'city' => $request->city,
                'saveAs' => $request->saveAs,
            ]);
            $data = AddSellerAddress::where('id', $request->edit_id)->first();
            $response['data'] = $data;
            $response['message'] = "Address updated successfully";
            return response()->json($response,200); 
        }else{
            // dd("fdsfs");
            Seller::where('id', $request->edit_id)->update([
                'address' =>$request->address,
                'address_latitude' =>$request->address_latitude,
                'address_longitude' =>$request->address_longitude,
                'city' => $request->city,
                'province' => $request->province,
            ]);
            $data = Seller::where('id', $request->edit_id)->first();
            $response['data'] = $data;
            $response['message'] = "Address updated successfully";
            return response()->json($response,200); 
        }
    }

    public function selleraddresslist(Request $request){

    	$data1 = Seller::select('id','address', 'address_latitude', 'address_longitude')->where('id', $request->seller_id)->get();
        $data2 = AddSellerAddress::where('seller_id', $request->seller_id)->get();    	
        $result = $data1->merge($data2);
        $response['data'] = $result;
        return response()->json($response,200);  
    }

    public function deleteselleraddress(Request $request){
        AddSellerAddress::where('id', $request->edit_id)->delete();
        $response['message'] = "Address deleted successfully";
        return response()->json($response,200);  
    }

    function sellergiveratingreviewed(Request $request){
       
        // dd("fgdgd");
        $validations =[
            'seller_user_id'=>'required',
            'seller_seller_id'=>'required',
            'seller_rating' => 'required',
            'seller_review'=>'required',
            'seller_gigId' =>'required',
        ];   
        $validator=Validator::make($request->all(),$validations);
        if($validator->fails())
        {
            $response['message']=$validator->errors($validator)->first();
            return response()->json($response,400);
        } 
        
        if(SellerRating::where('seller_user_id', $request->seller_user_id)->where('seller_seller_id', $request->seller_seller_id)->exists()){
            
            //$existid = UserRating::where('User_Id', $request->user_id)->where('Seller_Id', $request->seller_id)->get();
            $regDate = date("Y-m-d");
            $regTime = date("h:i:sa");
            
            SellerRating::where('seller_user_id', $request->seller_user_id)->where('seller_seller_id', $request->seller_seller_id)->update([
                'sellerRating' => $request->seller_rating,
                'seller_review' => $request->seller_review,
                'seller_gigId' => $request->seller_gigId,
                'seller_registerDate' => $regDate,
                'seller_registerTime' => $regTime,
            ]);
            $response['message'] = "Comment updated successfully"; 
            $response['data'] = SellerRating::where('seller_seller_id', $request->seller_seller_id)->orderBy('id', 'Desc')->get();  
            return response()->json($response,200);

        }else{
            // dd($request->seller_rating);
            $regDate = date("Y-m-d");
            $regTime = date("h:i:sa");
            SellerRating::create([
                'seller_user_id' => $request->seller_user_id,
                'seller_seller_id' => $request->seller_seller_id,
                'seller_review' => $request->seller_review,
                'sellerRating' => $request->seller_rating,
                'seller_gigId' => $request->seller_gigId,
                'seller_registerDate' => $regDate,
                'seller_registerTime' => $regTime,
            ]);

            $response['message'] = "Comment added successfully"; 
            $response['data'] = SellerRating::where('seller_seller_id', $request->seller_seller_id)->orderBy('id', 'Desc')->get(); 
            return response()->json($response,200); 
        }
        // $response['data'] = UserRating::where('User_Id', $request->user_id)->where('Seller_Id', $request->seller_id)->orderBy('id', 'Desc')->get(); 
        // return response()->json($response,200);
    }


}