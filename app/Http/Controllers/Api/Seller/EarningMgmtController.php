<?php

namespace App\Http\Controllers\Api\Seller;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\GigOrders;
use App\Model\UsrRequestGig;
use Validator;
use App\Seller\Seller;

class EarningMgmtController extends Controller
{
    function SellerEarning(Request $request){

        $validations =[        
	       'seller_id' => 'required',
	    ]; 
	    $validator=Validator::make($request->all(),$validations);
	    if($validator->fails())
	    {
	       $response['message']=$validator->errors($validator)->first();
	       return response()->json($response,400);
	    } 
    	if($request->from_date){
    		
			  $validations =[        
		          'from_date' => 'required',
		          'to_date' => 'required',
		      ]; 
		      $validator=Validator::make($request->all(),$validations);
		      if($validator->fails())
		      {
		         $response['message']=$validator->errors($validator)->first();
		         return response()->json($response,400);
		      } 
		      $from_date = $request->from_date;
		      $to_date = $request->to_date;
		      
		      // dd($from_date);

		      $Earning = GigOrders::where('seller_id', $request->seller_id)->where('paymentStatus', '1')->whereBetween('PaymentDate', [$from_date, $to_date])->sum('subtotal_price');
		     
		      $PendingEarning = GigOrders::where('seller_id', $request->seller_id)->where('paymentStatus', '0')->whereBetween('PaymentDate', [$from_date, $to_date])->sum('subtotal_price');

		      $MylesCompleted = UsrRequestGig::where(['Seller_ID'=>$request->seller_id, 'status'=>'9'])->whereBetween('gigCompleteDate', [$from_date, $to_date])->count();

		      $GigDetails = GigOrders::where('seller_id', $request->seller_id)->where('paymentStatus', '0')->whereBetween('PaymentDate', [$from_date, $to_date])->sum('subtotal_price');

    	}else{

    		$Earning = GigOrders::where('seller_id', $request->seller_id)->where('paymentStatus', '1')->sum('subtotal_price');

	    	$PendingEarning = GigOrders::where('seller_id', $request->seller_id)->where('paymentStatus', '0')->sum('subtotal_price');

	    	$MylesCompleted = UsrRequestGig::where(['Seller_ID'=>$request->seller_id, 'status'=>'9'])->count();

	    	$GigDetails = GigOrders::where('seller_id', $request->seller_id)->where('paymentStatus', '0')->sum('subtotal_price');    	
    	}
    	$data = ['TotalEarning'=>(int)round($Earning,2), 'PendingEarning'=> $PendingEarning, 'MylesCompleted' => $MylesCompleted, 'GigDetails'=>$GigDetails];    	
    	return response()->json(['message'=>'success', 'data' => $data], 200);
    }


    public function SellerEarningHistory(Request $request){    	
    	$validations =[        
	       'seller_id' => 'required',
	    ]; 
	    $validator=Validator::make($request->all(),$validations);
	    if($validator->fails())
	    {
	       $response['message']=$validator->errors($validator)->first();
	       return response()->json($response,400);
	    } 
	    if($request->from_date){
    		  $validations =[        
		          'from_date' => 'required',
		          'to_date' => 'required',
		      ]; 
		      $validator=Validator::make($request->all(),$validations);
		      if($validator->fails())
		      {
		         $response['message']=$validator->errors($validator)->first();
		         return response()->json($response,400);
		      }
		      $from_date = $request->from_date;
		      $to_date = $request->to_date;
		      $data = GigOrders::with(['UserGigAddressDetails', 'UserName'])->where('seller_id', $request->seller_id);
		      if($from_date != ''){
		        $data->whereBetween('PaymentDate', [$from_date, $to_date]);
		      }
		      $data = $data->get();		      
		}else{
			$data = GigOrders::with(['UserGigAddressDetails', 'UserName'])->where('seller_id', $request->seller_id)->where('paymentStatus', '1')->get();
		}
    	return response()->json(['message'=>'success', 'data' => $data], 200);
    }

    public function SellerOnDuty(Request $request){
    	// dd($request->UserData->id);
    	$pendingGig =  UsrRequestGig::where('Seller_ID', $request->UserData->id)->whereIn('status', array('0','2','4'))->count();
    									
    	$OngoingGig = UsrRequestGig::where('Seller_ID', $request->UserData->id)->whereIn('status', array('5','7','8'))->orderBy('id', 'Desc')->count();

    	$CompletedGig = UsrRequestGig::where('Seller_ID', $request->UserData->id)->whereIn('status', array('9'))->orderBy('id', 'Desc')->count();

    	$CancelGig = UsrRequestGig::where('Seller_ID', $request->UserData->id)->whereIn('status', array('6'))->orderBy('id', 'Desc')->count();

    	$TotalEarning = GigOrders::where('seller_id', $request->UserData->id)->where('paymentStatus', '1')->sum('subtotal_price'); 

    	$data = ['PendingGig' => $pendingGig, 'OngoingGig'=>$OngoingGig, 'CompletedGig' => $CompletedGig, 'CancelGig' => $CancelGig, 'TotalEarning'=>(int)round($TotalEarning,2)];    	
    	return response()->json(['message'=>'success', 'data' => $data], 200);
    }

    public function sellerOffDuty(Request $request){
    	// dd($request->sellerMode);
    	// 1  = On Duty
    	// 0 = Off Duty
    	if($request->sellerMode == '1'){
    		$SellerUpdate = Seller::find($request->seller_id);
	    	$SellerUpdate->is_duty = '1';
	    	if($SellerUpdate->save()){
	    		return response()->json(['message'=>'Switch to On duty successfully'], 200);
	    	}else{
	    		return response()->json(['message'=>'Something went wrong!'], 400);
	    	}
    	}elseif($request->sellerMode == '0'){
    		$SellerUpdate = Seller::find($request->seller_id);
	    	$SellerUpdate->is_duty = '0';
	    	if($SellerUpdate->save()){
	    		return response()->json(['message'=>'Switch to off duty successfully'], 200);
	    	}else{
	    		return response()->json(['message'=>'Something went wrong!'], 400);
	    	}
    	}    	
    }

}