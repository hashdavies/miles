<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Admin\Admin;
use App\Seller;
use Illuminate\Http\Request;
use Redirect;
use Mail;
use App\SellerOtp;
use App\Otp;
use App\Model\Countries;
use App\Model\Category;
use Twilio\Rest\Client; 
use App\Model\City;
use App\Model\State;
use App\Model\BusinessHour;
use Auth;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */
    use RegistersUsers;
    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/user/otp';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('guest');
        $this->middleware('guest');
        $this->middleware('guest:admin');
        $this->middleware('guest:seller');
    }
    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */

    protected function validator(array $data)
    {
        if(is_numeric($data['email_number'])){
            return Validator::make($data, [
                'username' => ['required', 'string', 'max:255'],
                'email_number' => ['required', 'string', 'min:9', 'max:11', 'unique:users'],
                'password' => ['required', 'string', 'min:8'],
                'country' => ['required', 'string'],
             ],[
                'email_number.required' => 'Please Enter Valid Mobile Number'
            ]);
        }else{
            return Validator::make($data, [
                'username' => ['required', 'string', 'max:255'],
                'email_number' => ['required', 'email', 'max:255', 'unique:users'],
                'password' => ['required', 'string', 'min:8'],
                'country' => ['required', 'string'],
            ],[
                'email_number.required' => 'Please Enter Valid Email Address'
            ]);
        }        
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    
    protected function create(array $data)
    {
        if(is_numeric($data['email_number'])){
            $mobile = $data['email_number'];          
            $collection = collect([
                ['mobile' => '+91'],
                ['mobile' => $data['email_number']],                            
            ]); 
            $mobile_no = $collection->implode('mobile', '');
            $otp = rand(1000, 9999);       
            try{
               $sid = "AC37468cf1a94b4e76eeec9eb3d327869e";
               $token = "ce51eb209281d4e59b1f5f96854c1551";               
               $client = new Client($sid, $token);
               $client->messages->create(
                  $mobile_no, array(
                    'from' => '+15154542323',
                    'body' => 'Thankyou for Register with A2A Express Your Otp Is' .''. $otp
                  )
                );              
               } catch(Exception $e){
                  $response = [
                    'message' => $e->getMessage()
                  ];                  
                  return $response;
               }
        }else{           
            $otp = rand(1000, 9999);
            $data1=array();         
            $data1['email']=$data['email_number'];
            $data1['name']=$data['username'];
            $data1['otp'] =$otp;
            $from = array();          
            $from['email_to']=$data['email_number'];
            $from['name']='Verify Account'; 
            Mail::send('emails.otp-mail', $data1, function ($message) use ($from) {
            $message->to($from['email_to']);
            $message->subject('Verify Account: OTP');
            });            
        }
        $accessToken=md5(uniqid(rand()));
        return $userdata = User::create([
            'username' => $data['username'],
            'email_number' => $data['email_number'],
            'password' => Hash::make($data['password']),
            'country' => $data['country'],
            'access_token' => $accessToken,
            'otp' =>$otp,
        ]);
    }
      
    public function showAdminRegisterForm()
    {
        return view('auth.admin_register', ['url' => 'admin']);
    }

    public function showSellerRegisterForm()
    { 
        // dd(url()->previous());

        // $SignupId = session()->get('signUpId');
        // $SessionDestroy = session()->flash('signUpId');
        // if($SignupId){
        //     $sellerData = Seller::where('id', $SignupId)->first();
        //     return view('seller.signup', ['url' => 'seller'])->with('sellerData', $sellerData);
        // }else{
            return view('seller.signup', ['url' => 'seller']);
        // }        
    }

    public function showSellerRegister($id){
        $id = decrypt($id);
        $preData = Seller::where('id', $id)->first();
        return view('seller.signup', ['url' => 'seller'])->with('preData', $preData);
    }

    protected function createAdmin(Request $request){
        $admin = Admin::create([
            'first_name' => $request['name'],
            'email_number' => $request['email_number'],
            'password' => Hash::make($request['password']),
        ]);
        return redirect()->intended('login/admin');
    }

    protected function createSeller(Request $request){          
        $this->validate($request, [
           'fullName' => 'required',
           'emailAddress' => 'required',
           'password'=>'required|min:8|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/',          
           'password_confirmation' => 'required|same:password',                    
           'mobileNumber' => 'required', 
           'terms' => 'required',
        ],[  
         'fullName.required' => 'Full name field is required',
         'mobileNumber.required' => 'Please enter valid mobile number',      
         'emailAddress.required' =>'Enter Valid Email Address', 
         'emailAddress.email' => 'Please enter valid email address',
         'password.required' => 'Password field is required',
         'password.min' => 'Password should be at least 8 character long',
         'password.regex'  => 'Password should contains between 8 to 15 characters, and contains at least 1 
lower case, 1 upper case, 1 numeric, 1 special character and no whitespace',        
         'password_confirmation.same' => 'New password and confirm password does not match', 
         'password_confirmation.required' => 'Confirm password field is required',
         'terms.required' => 'Please select terms and conditions',
        ]); 
        $otp = '1234';
        // $data1=array();         
        // $data1['email']=$request->emailAddress;
        // $data1['name']=$request->fullName;
        // $data1['otp'] =$otp;

        // $from = array();          
        // $from['email_to']=$request->emailAddress;
        // $from['name']='Verify Account'; 

        // Mail::send('emails.otp-mail', $data1, function ($message) use ($from) {
        // $message->to($from['email_to']);
        // $message->subject('Verify Account: OTP');
        // });   

        // $accessToken=md5(uniqid(rand()));          
        // $r_date = date('d/m/yy');        
        // $query = Seller::create([   
        //     'fullName' => $request->fullName,         
        //     'emailAddress'=>$request->emailAddress,
        //     'countryCode' => $request->countryCode,
        //     'mobileNumber' => $request->mobileNumber,
        //     'password'=> hash::make($request->password),           
        //     'registered_date' => $r_date,
        //     'access_token'=> $accessToken,
        //     'is_verified' => '0',
        //     'is_approved' => '0',
        //     'is_status' => '0',
        //     'is_profile' => '0',
        //     'profile_step' => '1'
        // ]);  
        // $id = encrypt($query->id);       
        // SellerOtp::create([
        //     'seller_id'=>$query->id,
        //     'otp'=>$otp
        // ]);               
        // return redirect()->intended('seller/otp_verify/' .$id);

         $aleradyemail = Seller::where('emailAddress', $request->emailAddress)->where('is_verified', '1')->first();
         $aleradymobile = Seller::where('mobileNumber', $request->mobileNumber)->where('is_verified', '1')->first();
         if(!empty($aleradyemail)){
            return redirect()->back()->with('success_message', 'you have already register with us');
         }elseif(!empty($aleradymobile)){
            return redirect()->back()->with('success_message', 'you have already register with us');
         }else{
             $selectEmail_mobile = Seller::where('mobileNumber', $request->mobileNumber)->where('emailAddress', $request->emailAddress)->where('is_verified', '0')->first();
             $selectmobile = Seller::where('mobileNumber', $request->mobileNumber)->where('is_verified', '0')->first();
             $selectEmail = Seller::where('emailAddress', $request->emailAddress)->where('is_verified', '0')->first();
             if(!empty($selectEmail_mobile)){                
                $accessToken=md5(uniqid(rand()));          
                $r_date = date('d/m/yy');        
                $query = Seller::where('id', $selectEmail_mobile->id)->update([   
                    'fullName' => $request->fullName,         
                    'emailAddress'=>$request->emailAddress,
                    'countryCode' => $request->countryCode,
                    'mobileNumber' => $request->mobileNumber,
                    'password'=> hash::make($request->password),           
                    'registered_date' => $r_date,
                    'access_token'=> $accessToken,
                    'is_verified' => '0',
                    'is_approved' => '0',
                    'is_status' => '0',
                    'is_profile' => '0',
                    'profile_step' => '1'
                ]); 
                $id = encrypt($selectEmail_mobile->id);
                SellerOtp::where('seller_id', $selectEmail_mobile->id)->delete();    
                SellerOtp::create([
                    'seller_id'=>$selectEmail_mobile->id,
                    'otp'=>$otp
                ]);               
                return redirect()->intended('seller/otp_verify/' .$id);
             }elseif(!empty($selectmobile)){
                $accessToken=md5(uniqid(rand()));          
                $r_date = date('d/m/yy');        
                $query = Seller::where('id', $selectmobile->id)->update([   
                    'fullName' => $request->fullName,         
                    'emailAddress'=>$request->emailAddress,
                    'countryCode' => $request->countryCode,
                    'mobileNumber' => $request->mobileNumber,
                    'password'=> hash::make($request->password),           
                    'registered_date' => $r_date,
                    'access_token'=> $accessToken,
                    'is_verified' => '0',
                    'is_approved' => '0',
                    'is_status' => '0',
                    'is_profile' => '0',
                    'profile_step' => '1'
                ]);  
                $id = encrypt($selectmobile->id); 
                SellerOtp::where('seller_id', $selectmobile->id)->delete();
                SellerOtp::create([
                    'seller_id'=>$selectmobile->id,
                    'otp'=>$otp
                ]);               
                return redirect()->intended('seller/otp_verify/' .$id);
             }elseif(!empty($selectEmail)){  
                $accessToken=md5(uniqid(rand()));          
                $r_date = date('d/m/yy');        
                $query = Seller::where('id', $selectEmail->id)->update([   
                    'fullName' => $request->fullName,         
                    'emailAddress'=>$request->emailAddress,
                    'countryCode' => $request->countryCode,
                    'mobileNumber' => $request->mobileNumber,
                    'password'=> hash::make($request->password),           
                    'registered_date' => $r_date,
                    'access_token'=> $accessToken,
                    'is_verified' => '0',
                    'is_approved' => '0',
                    'is_status' => '0',
                    'is_profile' => '0',
                    'profile_step' => '1'
                ]);  
                $id = encrypt($selectEmail->id);       
                SellerOtp::where('seller_id', $selectEmail->id)->delete();
                SellerOtp::create([
                    'seller_id'=>$selectEmail->id,
                    'otp'=>$otp
                ]);               
                return redirect()->intended('seller/otp_verify/' .$id);
             }else{               
                $accessToken=md5(uniqid(rand()));          
                $r_date = date('d/m/yy');        
                $query = Seller::create([   
                    'fullName' => $request->fullName,         
                    'emailAddress'=>$request->emailAddress,
                    'countryCode' => $request->countryCode,
                    'mobileNumber' => $request->mobileNumber,
                    'password'=> hash::make($request->password),           
                    'registered_date' => $r_date,
                    'access_token'=> $accessToken,
                    'is_verified' => '0',
                    'is_approved' => '0',
                    'is_status' => '0',
                    'is_profile' => '0',
                    'profile_step' => '1'
                ]);  
                $id = encrypt($query->id);  
                SellerOtp::create([
                    'seller_id'=>$query->id,
                    'otp'=>$otp
                ]);               
                return redirect()->intended('seller/otp_verify/' .$id);
             }
         }
    }

    protected function ResendOtp($id){  
        $otp = '1234';
        SellerOtp::where('seller_id', $id)->update([           
            'otp'=>$otp
        ]);
        return redirect()->back()->with('success_message','OTP resent successfully');
    }

    protected function VerifyOtp($ids){
        $decryptId = $ids;
        $id = decrypt($ids); 
        $data = Seller::where('id', $id)->first();       
        return view('seller/verify-otp')->with('id', $id)->with('data', $data)->with('decryptId', $decryptId);
    }

    protected function MatchOtp(Request $request){
        $data = $request->otp;
        $otp =implode("",$data);
        $sellerverify = SellerOtp::where('seller_id', $request->id)->first(); 
        if(($sellerverify->otp) == $otp){
            $id = encrypt($request->id);
            Seller::where('id', $request->id)->update([
                'profile_step' => '2',
                'is_verified' => '1',
            ]);
            return redirect()->intended('seller/business-location/' .$id);  
        }else{            
            return redirect()->back()->with('error_message', 'Incorrect OTP');
        }
    }   

    protected function BusinessLocation($id){        
        $id_data = decrypt($id);
        $countryName = Seller::select('countryCode')->where('id', $id_data)->first();
        $data = Seller::where('id', $id_data)->first();
        $country = Countries::where('phonecode', $countryName->countryCode)->first();
        $state = State::where('country_id', $country->id)->get(); 
        $getCity = City::get();
        $category = Category::orderBy('category_name', 'asc')->get();
        return view('seller/business-location')->with('data',$data)->with('state', $state)->with('getCity', $getCity)->with('category', $category);
    }

    protected function GetOldAddress($id){
      $SellerEdit = Seller::where('id',$id)->first();                         
      return response()->json([$SellerEdit]);
    }

    protected function PrimaryBusinessLocation(Request $request){        
        $this->validate($request, [
            'id'=> 'required',
            'primaryPostalCode' => 'required|string',
            'primaryAddress' => 'required|string',
            'primaryState' => 'required|string',
            'primaryCity' => 'required|string',
            'Category' => 'required|string',            
        ],[
            'primaryPostalCode.required' => 'Postal code is required',
            'primaryAddress.required' => 'Address is required',
            'primaryState.required' => 'State is required',
            'primaryCity.required' => 'City is required',
            'Category.required' => 'Category is required',
        ]);     

        Seller::where('id', $request->id)->update([
            'primaryPostalCode' => $request->primaryPostalCode,
            'primaryAddress' => $request->primaryAddress,
            'primaryState' => $request->primaryState,
            'primaryCity' => $request->primaryCity,  
            'Category' => $request->Category,         
            'is_verified' => '1',
            'profile_step' => '3',
            'address_latitude' => $request->lat,
            'address_longitude' => $request->long,
        ]);        
        $sellerId = encrypt($request->id);
        $state = State::get();
        return redirect('seller/business-details/' .$sellerId);
    }

    protected function Sellerbusiness($id){  

        $ID = decrypt($id);
        $countryName = Seller::select('countryCode')->where('id', $ID)->first();
        $country = Countries::where('phonecode', $countryName->countryCode)->first();
        $state = State::where('country_id', $country->id)->get(); 
        $getCity = City::get();
        $monday = BusinessHour::where('sellerID', $ID)->where('business_days', 'Monday')->first();
        $tuesday = BusinessHour::where('sellerID', $ID)->where('business_days', 'Tuesday')->first();
        $wednesday = BusinessHour::where('sellerID', $ID)->where('business_days', 'Wednesday')->first();
        $thursday = BusinessHour::where('sellerID', $ID)->where('business_days', 'Thursday')->first();
        $friday = BusinessHour::where('sellerID', $ID)->where('business_days', 'Friday')->first();
        $saturday = BusinessHour::where('sellerID', $ID)->where('business_days', 'Saturday')->first();
        $sunday = BusinessHour::where('sellerID', $ID)->where('business_days', 'Sunday')->first();
        $updatedData = Seller::where('id', $ID)->first();
        $stateupdated = State::where('name', $updatedData->secoundryState)->value('id');
        $city = City::where('state_id', $stateupdated)->get();
        $daysCount = BusinessHour::count('sellerID');
        if($daysCount == 7){
            $business_hour=BusinessHour::where('sellerID', $ID)->first();
        }else{
            $business_hour=null;
        }
        return view('seller/business-details')->with('id', $ID)->with('state', $state)->with('getCity', $getCity)->with('monday', $monday)->with('tuesday', $tuesday)->with('wednesday', $wednesday)->with('thursday', $thursday)->with('friday', $friday)->with('saturday', $saturday)->with('sunday', $sunday)->with('business_hour',$business_hour)->with('updatedData', $updatedData)->with('stateupdated', $stateupdated)->with('city',$city);
    }

    protected function BusinessDetailsSubmit(Request $request){

        $sellerStoreId = rand(9999,1000);       
        $all = "PANI$sellerStoreId";
        // dd($all);

        $seller = Seller::find($request->id);
        $seller->businessName = $request->businessName;
        $seller->panNo = $request->panNo;
        $seller->secoundryAddress = $request->secoundryAddress;
        $seller->secoundryState = $request->secoundryState;
        $seller->secoundryCity = $request->secoundryCity;
        $seller->secoundryPostalCode = $request->secoundryPostalCode;        
        $seller->profile_step = '4';
        $seller->seller_sore_id = $all;

       
        if($request->storeLogo){
           $imageName = 'storeLogo_'.time(). '.' .$request->file('storeLogo')->getClientOriginalExtension();
           $request->file('storeLogo')->move(base_path(). '/public/businessDocument/', $imageName);
           $seller->storeLogo = $imageName;
        }
        
        if($request->businessProof){
            $BusinessProof = 'businessProof_'.time(). '.' .$request->file('businessProof')->getClientOriginalExtension();
            $request->file('businessProof')->move(base_path(). '/public/businessDocument/', $BusinessProof);
            $seller->businessProof = $BusinessProof;
        }

        if($request->identityProof){
            $businessidentity = 'identityProof_'.time(). '.' .$request->file('identityProof')->getClientOriginalExtension();
            $request->file('identityProof')->move(base_path(). '/public/businessDocument/', $businessidentity);
            $seller->identityProof = $businessidentity;
        }
        $seller->save();  
        
         if($request->checkAll){
             $business_day = $request->business_days;
                foreach($business_day as $key => $no)
                {
                   $datall =  BusinessHour::select('id')->where('business_days', $no)->where('SellerID', $request->id)->get();
                   BusinessHour::whereIn('id', $datall)->delete(); 
                   BusinessHour::create([
                    'business_days' => $no,
                    'HourFrom' => $request->HourFrom,
                    'MinFrom' => $request->MinFrom,
                    'HourTo' => $request->HourTo,
                    'MinTo' => $request->MinTo,
                    'SellerID' => $request->id,                    
                    'status' => '1',
                   ]);
                }                
         }else{
             $business_day = $request->business_days;
             $data = array();
            foreach($business_day as $day){
                    $data['business_days'] = $day;
                $day_time = BusinessHour::where('SellerID', $request->id)->where('business_days',$day)->select('HourFrom','MinFrom', 'HourTo', 'MinTo')->first();
                $data['HourFrom'] = $day_time->HourFrom;
                $data['MinFrom'] = $day_time->MinFrom;
                $data['HourTo'] = $day_time->HourTo;
                $data['MinTo'] = $day_time->MinTo;
                $datanew[] = $data;
            }             
            BusinessHour::where('SellerID', $request->id)->delete();
            foreach($datanew as $rowday){            
                $businesshour = new BusinessHour();
                $businesshour->SellerID = $request->id;
                $businesshour->business_days = $rowday['business_days'];
                $businesshour->HourFrom = $rowday['HourFrom'];
                $businesshour->MinFrom = $rowday['MinFrom'];
                $businesshour->HourTo = $rowday['HourTo'];
                $businesshour->MinTo = $rowday['MinTo'];
                $businesshour->status = '1';                    
                $businesshour->save();
            }                                
         }
        return redirect('login/seller')->with('success_profile', 'Your request has been sent for approval.');
    }


    public function AddBusinnessDays(Request $request){
            $SellerID =$request->SellerID;
            $customdays = $request->customdays;
            $HourFrom = $request->HourFrom;
            $MinFrom = $request->MinFrom;
            $HourTo = $request->HourTo;
            $MinTo = $request->MinTo;
        BusinessHour::create([
            'business_days' => $customdays,
            'HourFrom' => $HourFrom,
            'MinFrom' => $MinFrom,
            'HourTo' => $HourTo,
            'MinTo' => $MinTo,
            'SellerID' => $SellerID,
            'status' => '1',
        ]);       
        $monday = BusinessHour::where('sellerID', $SellerID)->where('business_days', 'Monday')->first();
        $tuesday = BusinessHour::where('sellerID', $SellerID)->where('business_days', 'Tuesday')->first();
        $wednesday = BusinessHour::where('sellerID', $SellerID)->where('business_days', 'Wednesday')->first();
        $thursday = BusinessHour::where('sellerID', $SellerID)->where('business_days', 'Thursday')->first();
        $friday = BusinessHour::where('sellerID', $SellerID)->where('business_days', 'Friday')->first();
        $saturday = BusinessHour::where('sellerID', $SellerID)->where('business_days', 'Saturday')->first();
        $sunday = BusinessHour::where('sellerID', $SellerID)->where('business_days', 'Sunday')->first();
        return view('seller.partial_days')->with('monday', $monday)->with('tuesday', $tuesday)->with('wednesday', $wednesday)->with('thursday', $thursday)->with('friday', $friday)->with('saturday', $saturday)->with('sunday', $sunday);
    }


    public function EditBusinnessDays(Request $request){
        $SellerID =$request->SellerID;
        $EditId = $request->EditId;
        $customdays = $request->customdays;
        $HourFrom = $request->HourFrom;
        $MinFrom = $request->MinFrom;
        $HourTo = $request->HourTo;
        $MinTo = $request->MinTo;        
        BusinessHour::where('id', $EditId)->update([
            'business_days' => $customdays,
            'HourFrom' => $HourFrom,
            'MinFrom' => $MinFrom,
            'HourTo' => $HourTo,
            'MinTo' => $MinTo,
            'SellerID' => $SellerID,
            'status' => '1',
        ]);        
        $monday = BusinessHour::where('sellerID', $SellerID)->where('business_days', 'Monday')->first();
        $tuesday = BusinessHour::where('sellerID', $SellerID)->where('business_days', 'Tuesday')->first();
        $wednesday = BusinessHour::where('sellerID', $SellerID)->where('business_days', 'Wednesday')->first();
        $thursday = BusinessHour::where('sellerID', $SellerID)->where('business_days', 'Thursday')->first();
        $friday = BusinessHour::where('sellerID', $SellerID)->where('business_days', 'Friday')->first();
        $saturday = BusinessHour::where('sellerID', $SellerID)->where('business_days', 'Saturday')->first();
        $sunday = BusinessHour::where('sellerID', $SellerID)->where('business_days', 'Sunday')->first();

        return view('seller.partial_days')->with('monday', $monday)->with('tuesday', $tuesday)->with('wednesday', $wednesday)->with('thursday', $thursday)->with('friday', $friday)->with('saturday', $saturday)->with('sunday', $sunday);
    }

    protected function checkOneDayExist(Request $request){
        foreach($request->chkArray as $row)
        {
            if(BusinessHour::where('SellerID',$request->Seler_ID)->where('business_days', $row)->exists())
            {
                $arr=[];
            }else{
                $arr[]=$row;
            }
        }
        return response()->json(['data'=>$arr]);
    }

    protected function getBusinessHour($id){
        $data = BusinessHour::where('id', $id)->first();
        return response()->json($data);
    }

    protected function AddBusinessHour(Request $request){ 
        $Business_data = json_decode($request->businessFormdata);        
        $this->validate($request, [
            'HourFrom'=> 'required',
            'HourTo' => 'required',
        ]);
        if($request->HourFrom == $request->HourTo){
            return back()->with('donotmatchtime', 'Start time and end time can not be same');
        }else{
            $data = BusinessHour::where('business_days', $request->business_days)->where('SellerID', $request->SellerID)->first();           
            if(!empty($data)){
                  BusinessHour::where('id', $data->id)->update([
                    'HourFrom' => $request->HourFrom,
                    'MinFrom' => $request->MinFrom,
                    'HourTo' => $request->HourTo,
                    'MinTo' => $request->MinTo,
                    'status' => '1',
                  ]);  
            }else{
                BusinessHour::create([
                    'business_days' => $request->business_days,
                    'SellerID' => $request->SellerID,
                    'HourFrom' => $request->HourFrom,
                    'MinFrom' => $request->MinFrom,
                    'HourTo' => $request->HourTo,
                    'MinTo' => $request->MinTo,
                    'status' => '1',
                ]);
            }
            Seller::where('id', $request->SellerID)->update([
                'businessName' => $Business_data->businessName,
                'panNo' => $Business_data->panNo,
                'secoundryAddress' => $Business_data->secoundryAddress,
                'secoundryState' => $Business_data->secoundryState,
                'secoundryCity' => $Business_data->secoundryCity,
                'secoundryPostalCode' =>$Business_data->secoundryPostalCode
            ]);
            return redirect()->back()->with('delete_message', 'Business hour added successfully');
        }
    }

    protected function UpdateBusinessHour(Request $request){
        $Business_data = json_decode($request->businessFormeditdata);        
        if($request->editHourFrom == $request->editHourTo){
            return back()->with('donotmatchtime', 'Start time and end time can not be same');
        }else{
        BusinessHour::where('id', $request->editHourId)->update([
            'HourFrom' => $request->editHourFrom,
            'MinFrom' => $request->editMinFrom,
            'HourTo' => $request->editHourTo,
            'MinTo' => $request->editMinTo,
            'status' => '1',
        ]); 

        $seller = Seller::find($request->editSellerID);
        $seller->businessName = $Business_data->businessName;
        $seller->panNo = $Business_data->panNo;
        $seller->secoundryAddress = $Business_data->secoundryAddress;
        $seller->secoundryState = $Business_data->secoundryState;
        $seller->secoundryCity = $Business_data->secoundryCity;
        $seller->secoundryPostalCode = $Business_data->secoundryPostalCode;        
        $seller->profile_step = '4';       

        if($request->hasFile('storeLogo')){
           $imageName = 'storeLogo_'.time(). '.' .$request->file('storeLogo')->getClientOriginalExtension();
           $request->file('storeLogo')->move(base_path(). '/public/businessDocument/', $imageName);
           $seller->storeLogo = $imageName;
        }else{
            $seller->storeLogo = $request->storelogoEdit;
        }
        
        if($request->hasFile('businessProof')){
            $BusinessProof = 'businessProof_'.time(). '.' .$request->file('businessProof')->getClientOriginalExtension();
            $request->file('businessProof')->move(base_path(). '/public/businessDocument/', $BusinessProof);
            $seller->businessProof = $BusinessProof;
        }else{
            $seller->businessProof = $request->businessProofEdit;
        }

        if($request->hasFile('identityProof')){
            $businessidentity = 'identityProof_'.time(). '.' .$request->file('identityProof')->getClientOriginalExtension();
            $request->file('identityProof')->move(base_path(). '/public/businessDocument/', $businessidentity);
            $seller->identityProof = $businessidentity;
        }else{
            $seller->identityProof = $request->identityProofEdit;
        }
        $seller->save();        
        return redirect()->back()->with('delete_message', 'Business hour updaed successfully');
        }
    }
    protected function CountryList($id){
        $cities = City::orderBy('name', 'asc')->where('state_id', $id)->get(); 
        return response()->json($cities);
    }
}