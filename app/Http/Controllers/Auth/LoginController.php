<?php
namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers; 
use Illuminate\Http\Request;
use Auth;
use Hash;
use App\Seller;
  
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
    use AuthenticatesUsers;
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/user/create-profile';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       // $this->middleware('guest')->except('logout');
        $this->middleware('guest')->except('logout');
        $this->middleware('guest:admin')->except('logout');
        $this->middleware('guest:seller')->except('logout');
    }

    public function showAdminLoginForm()
    {
        return view('auth.login', ['url' => 'admin']);
    }

    // protected function credentials(Request $request)
    // {
        
    //     $admin = Admin::where('email_number',$request->email)->first();
    //     if (Auth::guard('admin')->attempt(['email_number' => $request->email_number,
    //       'password' => $request->password
    //       ], $request->get('remember'))) {
            
    //             return ['email'=>$request->email,'password'=>$request->password];
    //         }
    //     }
    //     return $request->only($this->username(), 'password');
    // }

    public function adminLogin(Request $request)
    {
        $this->validate($request, [
            'emailAddress'   => 'required|email',
            'password' => 'required|min:8'
        ],[
            'emailAddress.required' => 'Email id is required',
            'emailAddress.email' => 'Invalid email id',
            'password.required' => 'Password is required',
            'password.min' => 'Password must be 8 character long'
        ]);

        if (Auth::guard('admin')->attempt(['emailAddress' => $request->emailAddress,'password' => $request->password], $request->get('remember')))
        {
           return redirect()->intended('/admin/dashboard');
        }
        return back()->with('error','Invalid Credentials');
    }

    public function showSellerLoginForm()
    {
        return view('auth.login', ['url' => 'seller']);
    }

    public function sellerLogin(Request $request)
    {
        $this->validate($request, [
            'emailAddress'   => 'required|email',
            'password' => 'required|min:8'
        ],[
            'emailAddress.required' => 'Email id is required',
            'emailAddress.email' => 'Invalid email id',
            'password.required' => 'Password is required',
            'password.min' => 'Password should be at least 8 character long'
        ]);
        $seller = Seller::where('emailAddress',$request->emailAddress)->first();
        if($seller){
            if(Hash::check($request->password,$seller->password)){
                if($seller->is_verified == 1){
                    if($seller->profile_step == 2){                
                        $id = encrypt($seller->id);
                        return redirect()->intended('/seller/business-location/' .$id);                                                                                                                                                                                       
                    }elseif($seller->profile_step == 3){                        
                        $id = encrypt($seller->id);
                        return redirect()->intended('/seller/business-details/' .$id);
                    }elseif($seller->profile_step == 4){
                        if($seller->is_status == 1 || $seller->is_approved == 1) {
                         if (Auth::guard('seller')->attempt(['emailAddress' => $request->emailAddress, 'password' => $request->password], $request->get('remember'))) 
                            return redirect()->intended('/seller/dashboard');

                        }elseif($seller->is_status == 2 || $seller->is_approved == 2){
                            return back()->withInput($request->only('emailAddress', 'remember'))->with('error','Blocked by admin please contact');
                        }elseif($seller->is_status == 0 || $seller->is_approved == 0){                                      
                           return back()->withInput($request->only('email', 'remember'))->with('error','You are not verified by the admin'); 
                        }
                    }                    
                }else{
                    return back()->withInput($request->only('email', 'remember'))->with('error','You are not regisered with us');
                }                
            }else{
                return back()->withInput($request->only('email', 'remember'))->with('error','Invalid Credentials'); 
            } 
        }
    
        return back()->withInput($request->only('email', 'remember'))->with('error','Invalid Credentials');        
    }     
}
