<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use DB;
use Storage;
use Twilio\Rest\Client; 
use Token;
use File;
use Hash;
use Auth;
use Mail;
use App\User;
use App\Otp;
use Illuminate\Support\Str;
use App\Seller;
use App\SellerOtp;
use App\Model\Countries;
use App\Model\State;
use App\Model\City;
use App\Model\UsrRequestGig;
use Exception;
use App\Model\CmsPage;
use App\Model\About;
use App\Model\Faq;
use App\Model\ContactUser;
use App\Model\Notification;
use Redirect;
use Config;

class RegisterController extends Controller
{


function AddData(Request $request){
  // dd($request->firstName);
  $data = new User;
  $data->firstName = $request->firstName;
  $data->lastName = $request->lastName;
  if($data->save()){
    return response()->json(['message'=>'Data save succesfully', 'data'=>$data],200);
  }else{
    return response()->json(['message'=>'something went wrong'],400);
  }
}

function addDataUpdate(Request $request){
  // dd("sadsa");
  $data = User::find($request->edit_id);
  $data->firstName = $request->firstName;
  $data->lastName = $request->lastName;
  if($data->save()){
    return response()->json(['message'=>'Data update succesfully', 'data'=>$data],200);
  }else{
    return response()->json(['message'=>'something went wrong'],400);
  }  
}


function addDataDelete(Request $request){
  // dd($request->delete_id);
  User::where('id', $request->delete_id)->delete();
  return response()->json(['message'=>'Data deleted succesfully'],200);
}



  function UserSignUp(Request $request)
  {  
    // dd($request->all());
    $validations =[
        'firstName'=>'required',
        'lastName'=>'required',
        'nickName' => 'required',
        'countryCode'=>'required',
        'mobileNo' => 'required|min:8|max:14|unique:users',
        'emailAddress'=>'required|email|unique:users',
        'password'=>'required',
        'cPassword' => 'required',       
        'address' => 'required',
        'address_latitude' => 'required',
        'address_longitude' => 'required',
        'device_token' => 'required',
        'device_type' => 'required|integer|between:0,1',
    ];   
    $validator=Validator::make($request->all(),$validations);
    if($validator->fails())
    {
        $response['message']=$validator->errors($validator)->first();
        return response()->json($response,400);
    }

    if($request->password == $request->cPassword){
      //$otp = rand(1000,9999);
    $otp = '1234';
    $email = $request->get('emailAddress');
    // $messageData = ['email'=>$request->get('emailAddress'),'name'=>$request->fullName,'otp'=>$otp];
    // Mail::send('emails.otp-mail',$messageData,function($message) use($email){
    //     $message->to($email)->subject('Paniya');
    // });    

    $todayDate = date('Y-m-d');
    $accessToken=md5(uniqid(rand()));

    $aleradyemail = User::where('emailAddress', $request->emailAddress)->where('is_verified', '1')->first();
    $aleradymobile = User::where('mobileNo', $request->mobileNo)->where('is_verified', '1')->first();
    if(!empty($aleradyemail)){
        $response['message']='you have already register with us';            
        return response()->json($response,400);
     }elseif(!empty($aleradymobile)){
        $response['message']='you have already register with us';            
        return response()->json($response,400);
     }else{
        $data = [
           'firstName' => $request->firstName, 
           'lastName' => $request->lastName,
           'nickName' => $request->nickName,
           'countryCode' => $request->countryCode,
           'mobileNo' => $request->mobileNo, 
           'emailAddress' => $request->emailAddress,
           'address' => $request->address,
           'address_latitude' => $request->address_latitude,
           'address_longitude' => $request->address_longitude,
           'province' => $request->province,
           'city' => $request->city,
        ];
        $selectEmail_mobile = User::where('mobileNo', $request->mobileNo)->where('emailAddress', $request->emailAddress)->where('is_verified', '0')->first();

        $selectmobile = User::where('mobileNo', $request->mobileNo)->where('is_verified', '0')->first();
        $selectEmail = User::where('emailAddress', $request->emailAddress)->where('is_verified', '0')->first();
        if(!empty($selectEmail_mobile)){
            User::where('id', $selectEmail_mobile->id)->update([
                'firstName'=> $request->firstName,
                'lastName' => $request->lastName,
                'nickName' => $request->nickName,
                'countryCode' => $request->countryCode, 
                'mobileNo'=>$request->mobileNo,
                'emailAddress'=>$request->emailAddress,
                'password'=>hash::make($request->password),
                'address' => $request->address,
                'address_longitude' => $request->address_longitude,
                'address_latitude' => $request->address_latitude,
                'province' => $request->province,
                'city' => $request->city,
                'registerDate' => $todayDate, 
                'status'=>'1',    
                'is_verified' => '0',      
                'access_token' => $accessToken,
                'device_token' => $request->device_token,
                'device_type' => $request->device_type,
             ]);
             Otp::where('user_id',$selectEmail_mobile->id)->update(['otp'=>$otp]);

             $response['message']='OTP sent successfully on your registered Email id';
             $response['data'] = $data;
             return response()->json($response,200);
         }elseif(!empty($selectmobile)){
             User::where('id', $selectmobile->id)->update([
                'firstName'=> $request->firstName,
                'lastName' => $request->lastName,
                'nickName' => $request->nickName,
                'countryCode' => $request->countryCode, 
                'mobileNo'=>$request->mobileNo,
                'emailAddress'=>$request->emailAddress,
                'address' => $request->address,
                'address_longitude' => $request->address_longitude,
                'address_latitude' => $request->address_latitude,
                'password'=>hash::make($request->password),
                'province' => $request->province,
                'city' => $request->city,
                'registerDate' => $todayDate, 
                'status'=>'1',    
                'is_verified' => '0',      
                'access_token'=>$accessToken,
                'device_token' => $request->device_token,
                'device_type' => $request->device_type,
             ]);
             Otp::where('user_id', $selectmobile->id)->update(['otp'=>$otp]);
             $response['message']='OTP sent successfully on your registered Email id';
             $response['data'] = $data;
             return response()->json($response,200);
         }elseif(!empty($selectEmail)){          
             User::where('id', $selectEmail->id)->update([
                'firstName'=> $request->firstName,
                'lastName' => $request->lastName,
                'nickName' => $request->nickName,
                'countryCode' => $request->countryCode, 
                'mobileNo'=>$request->mobileNo,
                'emailAddress'=>$request->emailAddress,
                'address' => $request->address,
                'address_longitude' => $request->address_longitude,
                'address_latitude' => $request->address_latitude,
                'password'=>hash::make($request->password), 
                'province' => $request->province,
                'city' => $request->city,
                'registerDate' => $todayDate, 
                'status'=>'1',    
                'is_verified' => '0',      
                'access_token'=>$accessToken,
                'device_token' => $request->device_token,
                'device_type' => $request->device_type,
             ]);
             Otp::where('user_id', $selectEmail->id)->update(['otp'=>$otp]);          
             $response['message']='OTP sent successfully on your registered Email id';
             $response['data'] = $data;
             return response()->json($response,200);
         }else{               
           $query = User::create([
              'firstName'=> $request->firstName,
              'lastName' => $request->lastName,
              'nickName' => $request->nickName,
              'countryCode' => $request->countryCode, 
              'mobileNo'=>$request->mobileNo,
              'emailAddress'=>$request->emailAddress,
              'address' => $request->address,
              'address_longitude' => $request->address_longitude,
              'address_latitude' => $request->address_latitude,
              'password'=>hash::make($request->password), 
              'province' => $request->province,
              'city' => $request->city,
              'registerDate' => $todayDate, 
              'status'=>'1',    
              'is_verified' => '0', 
              'device_token' => $request->device_token,
              'device_type' => $request->device_type,
              // 'device_token' => $request->device_token,
              // 'device_type' => $request->device_type,                
              // 'access_token'=>$accessToken,              
           ]);
           Otp::create(['user_id'=>$query->id,'otp'=>$otp]);
           // $sellerdata = Seller::get();
           $RegDate = date('Y-m-d');
                      
           // foreach($sellerdata as $key=>$rowseller){
           //    UsrRequestGig::create([
           //    'User_ID'=>$query->id,
           //    'Seller_ID' => $rowseller->id,
           //    'status' => '0',
           //    'registerDate' => $RegDate,
           //   ]);
           // }

           // dd($query->id);

            $body_text="Congratulations! You have successfully registered.";
            $tokens = $request->device_token;
            $result =[
              'is_applied'=>1,
            ];
             if($request->device_type == '0'){
                  $is_result=$this->androidPushNotification($body_text, $tokens, $result);
             }elseif($request->device_type == '1'){
                  $is_result=$this->iosPushNotification($body_text, $tokens,$result);
             } 
              if($is_result == '1'){
                $data = new Notification;
                $data->user_id = $query->id;
                $data->message = $body_text;
                $data->save();               
              }
              // elseif($is_result == '0'){
              //   return response()->json(['message'=> 'Something went wrong!'],400);
              // }
           $response['message']='OTP sent to your mobile number';
           $response['data'] = $query;
           return response()->json($response,200);
         }
     }      
    }else{
       $response['message']='Password and password confirmation do not match';
       return response()->json($response,400);
    }
  }

  function Dispute(){
    $data = CmsPage::where('id', '3')->first();
    $response['message']='success';
    $response['disputeResolution']=$data->content;
    return response()->json($response,200);
  }

  function UserverifyOtp(Request $request)
  {
    $validations =[       
      'emailAddress'=>'required|email',
      'otp'=>'required',       
    ];  
    $validator=Validator::make($request->all(),$validations);
    if($validator->fails())
    {
      $response['message']=$validator->errors($validator)->first();
      return response()->json($response,400);
    }     

    $user_id = User::select('id')->where('emailAddress',$request->emailAddress)->first();
    if($user_id){

      $chk = Otp::where('user_id',$user_id->id)->where('otp',$request->otp)->first();
      if($chk)
      {
         $accessToken = md5(uniqid(rand()));
         $chk1 = User::where('id',$user_id->id)->update(['is_verified'=>1, 'access_token'=>$accessToken]);
         $data = User::where('id',$user_id->id)->first();

         $response['message']='OTP verified successfully';
         $response['data'] = $data;
         return response()->json($response,200);
      }
      else
      {
        $response['message']='Incorrect OTP';
        return response()->json($response,400);
      }

    }else{
       $response['message']='Incorrect Email Address';
       return response()->json($response,400);
    }  
  }


  function UserResendOtp(Request $request)
  {
     $validations =[
       'emailAddress'=>'required',
     ];
     $validator=Validator::make($request->all(),$validations);
     if($validator->fails())
     {
     $response['message']=$validator->errors($validator)->first();
     return response()->json($response,400);
     }
     $query = User::where('emailAddress',$request->emailAddress)->first();
     if($query){           
          $otp = '1234';
          Otp::where('user_id', $query->id)->update(['otp'=>$otp]);
          $email = $request->get('emailAddress');
          $messageData = ['email'=>$request->get('email'),'otp'=>$otp];
          Mail::send('seller.otp-mail',$messageData,function($message) use($email){
                  $message->to($email)->subject('Myles');
               });
          $response['message']='OTP sent to your mobile number'; 
          return response()->json($response,200);         
     }else{
          $response['message'] = 'You are not registered with us';
          return response()->json($response, 400);
     }       
  }

  function UserForgotResendOtp(Request $request){
    $validations =[
      'mobileNo'=>'required',
     ];  
     $validator=Validator::make($request->all(),$validations);
     if($validator->fails())
     {
     $response['message']=$validator->errors($validator)->first();
     return response()->json($response,400);
     }
     $query = User::where('mobileNo',$request->mobileNo)->where('countryCode', $request->countryCode)->first();
     if($query){           
        if($query->is_verified == 0){
           $response['message'] = 'You are not Verfied OTP with us';
           return response()->json($response, 400);
        }else{
            $otp = '1234';
            Otp::where('user_id', $query->id)->delete();
            Otp::create([
              'otp'=>$otp,
              'user_id' =>$query->id 
            ]);            
            // $email = $request->get('mobileNo');
            // $messageData = ['email'=>$request->get('email'),'otp'=>$otp];

            // Mail::send('seller.otp-mail',$messageData,function($message) use($email){
            //         $message->to($email)->subject('Paniya');
            //      });
            $response['message']='OTP sent to your mobile number'; 
            return response()->json($response,200);
        }          
     }else{
        $response['message'] = 'You are not registered with us';
        return response()->json($response, 400);
     }
  }


  function UserLogin(Request $request)
  {
    if($request->loginType == '1'){
        // dd("google");        
        $validator = Validator::make($request->all(), [   
          'device_token' => 'required',
          'device_type' => 'required|integer',
          'social_id'=>'required',   
          'social_type'=>'required|integer',            
        ]);
        if ($validator->fails()) {
            $response['message']=$validator->errors($validator)->first();
            return response()->json($response,400);   
        }        

        if(User::where('social_id',$request->social_id)->where('social_type',$request->social_type)->exists())
        {
          // dd("dsfds");

            if(User::where('social_id','=',$request->social_id)->where('social_type','=',$request->social_type)->where('password','!=',null)->exists())
            {
                User::where('social_id','=',$request->social_id)->where('social_type','=',$request->social_type)->update(['access_token'=>Str::random(60),'device_token'=>$request->device_token,'device_type'=>$request->device_type]);

            }else{
            
                User::where('social_id','=',$request->social_id)->where('social_type','=',$request->social_type)->update(['firstName'=>$request->name,'access_token'=>Str::random(60),'device_token'=>$request->device_token,'device_type'=>$request->device_type]);

            }                
            $user_data=User::where('social_id','=',$request->social_id)->where('social_type','=',$request->social_type)->first();

            return response()->json(['status'=>200,'message'=>'Login Successfully','data'=>$user_data],200);
        }
        else
        {
           
            if(!(isset($request->email)) && $request->social_type==2)
            {
               $userData =[
                    'is_register'=>0,
                ];
                $response['message'] = "User not found.";
                $response['data']    =  $userData;
                return response()->json($response,200);     
            }
            

                // $validator = Validator::make($request->all(), [
                // 'email' => 'required|email',               
                // ]);
                // if ($validator->fails()) {
                //     $response['message']=$validator->errors($validator)->first();
                //     return response()->json($response,400);   
                // }

                if(User::where('social_id',$request->social_id)->exists())
                {
                    $save = User::where('social_id',$request->social_id)->first();

                    $save->social_id=$request->social_id;
                    $save->social_type=$request->social_type;
                    $save->access_token=Str::random(60);
                    $save->is_profile=0;
                    $save->is_verified=1;
                    $save->status = 1;
                    $save->device_token=$request->device_token;
                    $save->device_type=$request->device_type;
                    $save->save();
                }
                else
                {
                    $save = new User;
                    $save->firstName=@$request->name;
                    $save->emailAddress=@$request->email;
                    $save->social_id=$request->social_id;
                    $save->is_profile=0;
                    $save->is_verified=1;
                    $save->status = 1;
                    // $save->social_type=$request->social_type;
                    $save->social_type=$request->social_type;
                    $save->access_token=Str::random(60);
                    $save->device_token=$request->device_token;
                    $save->device_type=$request->device_type;
                    $save->save();
                }                               
                return response()->json(['status'=>200,'message'=>'Login Successfully','data'=>$save],200);
        }
        
    }elseif($request->loginType == '0'){

      if(is_numeric($request->email_number))
        {
          // dd("dfdsf");

          $validations = [        
            'email_number'=>'required|numeric',
            'password'=>'required',
            'countryCode'=>'required',
          ];
          $validator=Validator::make($request->all(),$validations);
          if($validator->fails())
          {
              $response['message']=$validator->errors($validator)->first();
              return response()->json($response,400);
          }

          $query = User::where('mobileNo',$request->email_number)->where('countryCode', $request->countryCode)->first();
          // dd($query);

          if($query)
          {
            // dd($request->password);
              if(Hash::check($request->password, $query->password))
              {
                // dd(Hash::check($request->password, $query->password));
                $accessToken=md5(uniqid(rand()));
                User::where('mobileNo',$request->email_number)->update(['access_token'=>$accessToken,'device_token'=>$request->device_token,'device_type'=>$request->device_type]);

                 //  $response['message']='Loginned Successfully';
                 //  $response['data'] = User::where('email_number',$request->email_number)->first();
                 // $response['token'] = $token;
                $admin = User::where('mobileNo',$request->email_number)->first();
                if(!empty($admin)){
                    if ($admin->status == 1) {
                        $response['message']='Login Successfully';
                        $response['data'] = $admin;
                        return response()->json($response,200);
                    }else{
                         $response['message']='You are blocked by Admin please contact'; 
                         return response()->json($response,400);
                    }
                }
              }
              else
              {
                  $response['message']='Invalid Credential';                
                  return response()->json($response,400);
              }
          }
          else
          {
              $response['message']='Invalid Credential';            
              return response()->json($response,400);
          }
        }else{
          $validations = [        
            'email_number'=>'required|email',
            'password'=>'required',       
          ];
          $validator=Validator::make($request->all(),$validations);
          if($validator->fails())
          {
              $response['message']=$validator->errors($validator)->first();
              return response()->json($response,400);
          }
          $query = User::where('emailAddress',$request->email_number)->first();
          if($query)
          {
              if(Hash::check($request->password,$query->password))
              {
                $accessToken=md5(uniqid(rand()));
                User::where('emailAddress',$request->email_number)->update(['access_token'=>$accessToken,'device_token'=>$request->device_token,'device_type'=>$request->device_type]);
                  //$response['message']='Loginned Successfully';
                  //$response['data'] = User::where('email_number',$request->email_number)->first();
                 // $response['token'] = $token;
                $admin = User::where('emailAddress',$request->email_number)->first();
                if(!empty($admin)){
                    if ($admin->status == 1) {
                        $response['message']='Login Successfully';
                        $response['data'] = $admin;
                        return response()->json($response,200);
                    }else{
                         $response['message']='You are blocked by Admin please contact'; 
                         return response()->json($response,400);
                    }
                }
              }
              else
              {
                  $response['message']='Invalid Credential';
                  return response()->json($response,400);
              }
          }
          else
          {
              $response['message']='Invalid Credential';            
              return response()->json($response,400);
          }      
        } 
     }             
  }

  function UserForgotPassword(Request $request)
  {
     $validations =[          
        'mobileNo'=>'required',  
     ];
     $validator=Validator::make($request->all(),$validations);
     if($validator->fails())
     {
        $response['message']=$validator->errors($validator)->first();
        return response()->json($response,400);
     }
      $query = User::where('mobileNo',$request->mobileNo)->where('countryCode', $request->countryCode)->first();
      if($query){      
        if($query->is_verified == 1){
           //$otp = rand(1000,9999);              
         $otp = '1234';
         //  $body = 'Thaks for register with A2A Express your forgot OTP is ' .$otp;
         //  $mobile_no = $request->country_code.''.$request->email_number;

         //  //dd($mobile_no);

         // try{
         //  $sid = "AC37468cf1a94b4e76eeec9eb3d327869e";
         //  $token = "ce51eb209281d4e59b1f5f96854c1551";           

         //  $client = new Client($sid, $token);
         //  $client->messages->create(
         //         $mobile_no, array(
         //           'from' => '+15154542323',
         //           'body' => $body
         //         )
         //       );          
         //   } catch(Exception $e){
         //     $response = [
         //       'message' => $e->getMessage()
         //     ];              
         //     //return $response;
         //     return response()->json($response,400);
         //   }           
          //$response['data'] = $query;          
          // dd($query->id);

          Otp::where('user_id', $query->id)->delete();
          Otp::create([
            'otp'=>$otp,
            'user_id' =>$query->id 
          ]);

          $response['message'] = 'OTP sent to your mobile number';
          $data = ['mobileNo'=>$request->mobileNo,
            'countryCode'=>$request->countryCode,                           
          ];
          $response['data'] = $data;             
          return response()->json($response,200); 
        }else{
           $response['message']='This number is not registered with us';
           return response()->json($response,400);
        }
       }
       else
       {
           $response['message']='Incorrect mobile number';
           return response()->json($response,400);
       }  
  }

  function userForgotOtpMatch(Request $request){
      $validations =[  
        'mobileNo'=>'required',
        'countryCode' => 'required', 
        'otp'=>'required',              
      ];
      $validator=Validator::make($request->all(),$validations);
      if($validator->fails())
      {
          $response['message']=$validator->errors($validator)->first();
          return response()->json($response,400);
      }
      $user_id = User::where('mobileNo',$request->mobileNo)->first();
      if(!empty($user_id)){
        $countryCode = User::where('id',$user_id->id)->where('countryCode',$request->countryCode)->first();
        if($countryCode){ 
         $chk = Otp::where('user_id',$user_id->id)->where('otp',$request->otp)->first();        
             if($chk)
             {                
                 $response['message']='OTP verified successfully';
                 $response['mobileNo'] = $user_id->mobileNo;
                 return response()->json($response,200);
             }
             else
             {
                $response['message']='Incorrect OTP';
                return response()->json($response,400);
             }      
        } else {
          $response['message']='Country code is Incorrect';
          return response()->json($response,400);   
        } 
      }else{        
        $response['message']='Incorrect mobile number';
        return response()->json($response,400);      
      }
  }

  function UserResetPassword(Request $request)
  {
    $validations =[          
        'mobileNo'=>'required',
        'password' => 'required'        
    ];
    $validator=Validator::make($request->all(),$validations);
     if($validator->fails())
     {
        $response['message']=$validator->errors($validator)->first();
        return response()->json($response,400);
     }
     $query = User::where('mobileNo',$request->mobileNo)->update(['password'=>Hash::make($request->password)]);
     if($query)
     {
      $response['message']='Password reset successful';        
      return response()->json($response,200);
     }else{
      $response['message']='Incorrect mobile number';        
      return response()->json($response,400);
     }
  }

  function SellerSignUp(Request $request){
    // dd($request->all());
    if($request->serviceProviderType == 'business')
    {   
      $validations =[        
          'businessName' => 'required|string',
          'firstName' => 'required',
          'lastName' => 'required',
          'nickName' => 'required',
          'email' => 'required|email|unique:sellers',
          'countryCode' => 'required|numeric',
          'mobileNo' => 'required|numeric|unique:sellers',
          // 'dLNumber' => 'required',
          // 'businessLogo' => 'required|mimes:jpg,jpeg,png,svg|max:8192',
          'IdProof' => 'required|mimes:jpg,jpeg,png,svg|max:8192',
          'password' => 'required',
          'cpassword' => 'required|same:password', 
          'serviceType' => 'required', 
          'address' => 'required',
          'address_longitude' =>'required',
          'address_latitude' => 'required', 
          'device_token' => 'required',
          'device_type' => 'required',  
      ]; 
      $validator=Validator::make($request->all(),$validations);
      if($validator->fails())
      {
          $response['message']=$validator->errors($validator)->first();
          return response()->json($response,400);
      }
      if ($request->hasFile('businessLogo')) {
           // $image = $request->file('businessLogo');
           // $s3 = \Storage::disk('s3');
           // $businessLogo = time() .'.'. $image->getClientOriginalExtension();
           // $filepath='Serviceprovider/' . $businessLogo;
           // $s3->put($filepath, file_get_contents($image), 'public');
           $image = $request->file('businessLogo');
           $s3 = \Storage::disk('s3');
           $businessLogo = 'businesslogo_'.time() .'.'. $image->getClientOriginalExtension();
           $filepath='Serviceprovider/' . $businessLogo;
           $s3->put($filepath, file_get_contents($image), 'public');
      }
      if ($request->hasFile('IdProof')) {

           $imageIdProof = $request->file('IdProof');
           $s3 = \Storage::disk('s3');
           $IdProofimage = 'idproof_'.time() .'.'. $imageIdProof->getClientOriginalExtension();
           $filepathIdProof='Serviceprovider/' . $IdProofimage;
           $s3->put($filepathIdProof, file_get_contents($imageIdProof), 'public');
           
           // $imageIdProof = $request->file('IdProof');
           // $s3 = \Storage::disk('s3');
           // $IdProofimage = time() .'.'. $imageIdProof->getClientOriginalExtension();
           // $filepathidprofile='Serviceprovider/' . $IdProofimage;
           // $s3->put($filepathidprofile, file_get_contents($imageIdProof), 'public');
      }
    // dd("sdas");
      $r_date = date('Y-m-d');
      $accessToken=md5(uniqid(rand()));
        $sellerupdate = New Seller;
        $sellerupdate->serviceProviderType = $request->serviceProviderType;
        $sellerupdate->businessName        = $request->businessName;
        $sellerupdate->firstName           = $request->firstName;
        $sellerupdate->lastName            = $request->lastName;
        $sellerupdate->nickName            = $request->nickName;
        $sellerupdate->email               = $request->email;
        $sellerupdate->password            = hash::make($request->password);
        $sellerupdate->countryCode         = $request->countryCode;
        $sellerupdate->mobileNo            = $request->mobileNo;
        $sellerupdate->address             = $request->address;
        $sellerupdate->serviceType         = $request->serviceType; 
        $sellerupdate->address_longitude   = $request->address_longitude;
        $sellerupdate->address_latitude    = $request->address_latitude;     
        $sellerupdate->registerDate        = $r_date;
        $sellerupdate->access_token        = $accessToken;
        $sellerupdate->profile_step        = '1';
        $sellerupdate->is_approved         = '0';
        $sellerupdate->is_status           = '1';
        $sellerupdate->is_verified         = '1';
        $sellerupdate->city                = $request->city;
        $sellerupdate->province            = $request->province;
        $sellerupdate->device_token        = $request->device_token;
        $sellerupdate->device_type         = $request->device_type;
        $sellerupdate->dLNumber            = $request->dLNumber;  

        if($request->hasFile('businessLogo')){
          $sellerupdate->businesslogo        = $businessLogo;
        }
        if($request->hasFile('IdProof')){
          $sellerupdate->IDProof             = $IdProofimage;
        }
        $sellerupdate->save();
        
        // $title = "Registered";
        // $body_text="Congratulations! You have successfully registered with us.";
        // $tokens = $request->device_token;
        // $result =[
        //   'is_applied'=>1,
        // ];
        //  if($request->device_type == '0'){
        //       $is_result=$this->androidPushNotification($body_text, $tokens, $result);
        //  }elseif($request->device_type == '1'){
        //       $is_result=$this->iosPushNotification($body_text, $tokens,$result);
        //  }        
        //  if($is_result == '1'){
        //     $data = new Notification;
        //     $data->seller_id = $sellerupdate->id;
        //     $data->message = $body_text;
        //     $data->save();
        //  }
        $response['message']='Registered successfully';        
        $response['data'] = $sellerupdate;        
        return response()->json($response,200);
    }
    elseif ($request->serviceProviderType == 'freelancer'){
      $validations =[        
          'profileImage' => 'required|mimes:jpg,png,svg,jpeg|max:8192',
          'firstName' => 'required',
          'lastName' => 'required',
          'nickName' => 'required',
          'email' => 'required|email|unique:sellers',
          'countryCode' => 'required|numeric',
          'mobileNo' => 'required|numeric|unique:sellers',
          // 'dLNumber' => 'required',
          // 'businessLogo' => 'required|mimes:jpg,jpeg,png,svg|max:8192',
          'IdProof' => 'required|mimes:jpg,jpeg,png,svg|max:8192',
          'password' => 'required',
          'cpassword' => 'required|same:password',
          'serviceType' => 'required', 
          'address' => 'required',
          'address_longitude' =>'required',
          'address_latitude' => 'required',    
      ]; 
      $validator=Validator::make($request->all(),$validations);
      if($validator->fails())
      {
          $response['message']=$validator->errors($validator)->first();
          return response()->json($response,400);
      }

      if ($request->hasFile('profileImage')) {
           $imageprofile = $request->file('profileImage');
           $s3 = \Storage::disk('s3');
           $profileImage =  'profileimage_'.time() .'.'. $imageprofile->getClientOriginalExtension();
           $filepathprofile='Serviceprovider/' . $profileImage;
           $s3->put($filepathprofile, file_get_contents($imageprofile), 'public');
      } 
      if ($request->hasFile('businessLogo')) {
           $image = $request->file('businessLogo');
           $s3 = \Storage::disk('s3');
           $businessLogo = 'businesslogo_'.time() .'.'. $image->getClientOriginalExtension();
           $filepath='Serviceprovider/' . $businessLogo;
           $s3->put($filepath, file_get_contents($image), 'public');
      }
      if ($request->hasFile('IdProof')) {
           $imageIdProof = $request->file('IdProof');
           $s3 = \Storage::disk('s3');
           $IdProofimage = 'idproof_'.time() .'.'. $imageIdProof->getClientOriginalExtension();
           $filepathIdProof='Serviceprovider/' . $IdProofimage;
           $s3->put($filepathIdProof, file_get_contents($imageIdProof), 'public');
      }    
      $r_date = date('Y-m-d');
      $accessToken=md5(uniqid(rand()));
        $sellerupdate = New Seller;
        $sellerupdate->serviceProviderType = $request->serviceProviderType;
        $sellerupdate->businessName        = $request->businessName;
        $sellerupdate->firstName           = $request->firstName;
        $sellerupdate->lastName            = $request->lastName;
        $sellerupdate->nickName            = $request->nickName;
        $sellerupdate->email               = $request->email;
        $sellerupdate->password            = hash::make($request->password);
        $sellerupdate->countryCode         = $request->countryCode;
        $sellerupdate->mobileNo            = $request->mobileNo;
        $sellerupdate->address             = $request->address;
        $sellerupdate->serviceType         = $request->serviceType; 
        $sellerupdate->address_longitude   = $request->address_longitude;
        $sellerupdate->address_latitude    = $request->address_latitude;
        $sellerupdate->registerDate        = $r_date;
        $sellerupdate->access_token        = $accessToken;
        $sellerupdate->profile_step        = '1';
        $sellerupdate->is_approved         = '0';
        $sellerupdate->is_status           = '1';
        $sellerupdate->is_verified         = '1';
        $sellerupdate->city                = $request->city;
        $sellerupdate->province            = $request->province;
        $sellerupdate->device_token        = $request->device_token;
        $sellerupdate->device_type         = $request->device_type;
        $sellerupdate->dLNumber            = $request->dLNumber;

        if($request->hasFile('businessLogo')){
          $sellerupdate->businesslogo        = $businessLogo;
        }
        if($request->hasFile('profileImage')){
          $sellerupdate->profileImage        = $profileImage;
        }
        if($request->hasFile('IdProof')){
          $sellerupdate->IDProof             = $IdProofimage;
        }
        $sellerupdate->save();  

        // $body_text="Congratulations! You have successfully registered with us.";
        // $tokens = $request->device_token;
        // $result =[
        //   'is_applied'=>1,
        // ];
        //  if($request->device_type == '0'){
        //       $is_result=$this->androidPushNotification($body_text, $tokens, $result);
        //  }elseif($request->device_type == '1'){
        //       $is_result=$this->iosPushNotification($body_text, $tokens,$result);
        //  }
        //  if($is_result == '1'){
        //     $data = new Notification;
        //     $data->seller_id = $sellerupdate->id;
        //     $data->message = $body_text;
        //     $data->save();
        //  }
      $response['message']='Registered successfully';
      $response['data'] = $sellerupdate;        
      return response()->json($response,200);
    }
  }

  function SellerSecoundStep(Request $request){
    // dd($request->all());
    Seller::where('id', $request->ProviderId)->update([
      'transitNumber' => $request->transitNumber,
      'institutionNumber' => $request->institutionNumber,
      'accountNumber' => $request->accountNumber,
      'profile_step' => '2',
      'is_profile' => '1',
    ]);

    $data = ['transitNumber' => $request->transitNumber, 
        'institutionNumber' => $request->institutionNumber,
        'accountNumber' => $request->accountNumber
    ];
    $response['message']='Deposit information successfully save';
    $response['data'] = $data;        
    return response()->json($response,200);
  }
  
  function SellerLogin(Request $request)
  {
    if(is_numeric($request->emailNumber)){
      $validations =[        
        'emailNumber'=>'required|numeric',
        'countryCode' => 'required|numeric',        
        'password' => 'required'        
      ];         
      $validator=Validator::make($request->all(),$validations);  
      if($validator->fails())
      {
         $response['message']=$validator->errors($validator)->first();
         return response()->json($response,400);
      }
      $query = Seller::where('mobileNo',$request->emailNumber)->where('countryCode', $request->countryCode)->first();  
      if($query)
        {
        if(Hash::check($request->password,$query->password))
          {
            $accessToken=md5(uniqid(rand()));
            Seller::where('mobileNo',$request->emailNumber)->update(['access_token'=>$accessToken, 'device_token' => $request->device_token, 'device_type' => $request->device_type]);

            $seller = Seller::where('mobileNo',$request->emailNumber)->first();
            $response['message']='Login successfully';
            $response['data'] = $seller;
            return response()->json($response,200);               
          }
          else
          {
            $response['message']='Incorrect Password';            
            return response()->json($response,400);
          }
        }
        else
        {
            $response['message']='Invalid Credential';        
            return response()->json($response,400);
        }
    }else{

      $validations =[        
        'emailNumber'=>'required|email',
        'password' => 'required'        
      ];         
      $validator=Validator::make($request->all(),$validations);  
      if($validator->fails())
      {
         $response['message']=$validator->errors($validator)->first();
         return response()->json($response,400);
      }
      $query = Seller::where('email',$request->emailNumber)->first();  
      if($query)
        {
        if(Hash::check($request->password,$query->password))
          {
            $accessToken=md5(uniqid(rand()));
            Seller::where('email',$request->emailNumber)->update(['access_token'=>$accessToken, 'device_token' => $request->device_token, 'device_type' => $request->device_type]);
            $seller = Seller::where('email',$request->emailNumber)->first();
            $response['message']='Login successfully';
            $response['data'] = $seller;
            return response()->json($response,200);               
          }
          else
          {
            $response['message']='Incorrect Password';            
            return response()->json($response,400);
          }
        }
        else
        {
          $response['message']='Invalid Credential';        
          return response()->json($response,400);
        }
     }
  }

  function SellerForgotPassword(Request $request)
  {
    $validations =[          
        'mobileNo'=>'required',
        'countryCode' => 'required',        
    ];
    $validator=Validator::make($request->all(),$validations);
    if($validator->fails())
    {
        $response['message']=$validator->errors($validator)->first();
        return response()->json($response,400);
    }
    $query = Seller::where('mobileNo',$request->mobileNo)->where('countryCode', $request->countryCode)->first();
    if($query)
    {   
       if($query->is_verified == '1'){
          //$otp = rand(1000,9999);
          $otp = '1234';
          SellerOtp::where('seller_id', $query->id)->delete();
          SellerOtp::create([
            'seller_id' => $query->id,
            'otp' => $otp,
          ]);
          $response['message']='OTP sent to your mobile number';
          $response['mobileNumber'] = $query->mobileNo;      
          return response()->json($response,200);            
        }     
     }
     else
     {
         $response['message']='Invalid mobile number';      
         return response()->json($response,400);
     }
  }

  function SellerResetPassword(Request $request)
  {
    $validations = ([          
        'mobileNo'=>'required',
        'countryCode' => 'required',
        'password' => 'required',
        // 'cpassword' => 'required|same:password'       
    ]);

    $validator=Validator::make($request->all(),$validations);
    if($validator->fails())
    {
        $response['message']=$validator->errors($validator)->first();
        return response()->json($response,400);
    }
    if($request->password == $request->cpassword){
      $query = Seller::where('mobileNo',$request->mobileNo)->update(['password'=>Hash::make($request->password)]);
       if($query)
       {
       $response['message']='Password reset successful';        
       return response()->json($response,200);
       }
       else
       {
       $response['message']='Sorry! Invalid mobile number';
       return response()->json($response,400);
       }
    }else{
       $response['message']='Password and password confirmation do not match';
       return response()->json($response,400);        
    }
    
  }


  function SellerVerifyOtp(Request $request)
  {
    $validations =[        
        'mobileNo'=>'required',
        'countryCode' => 'required',
        'otp'=>'required',        
    ];
    $validator=Validator::make($request->all(),$validations);
    if($validator->fails())
    {
        $response['message']=$validator->errors($validator)->first();
        return response()->json($response,400);
    }
    $seller_id = Seller::select('id','mobileNo')->where('mobileNo',$request->mobileNo)->where('countryCode', $request->countryCode)->first();
    if($seller_id){
      $chk = SellerOtp::where('seller_id',$seller_id->id)->where('otp',$request->otp)->first();
    if($chk)
      {
           $chk1 = Seller::where('id',$seller_id->id)->update(['is_verified'=>1]);
           $response['message']='Otp Match';
           $response['mobileNo'] = $seller_id->mobileNo;
           return response()->json($response,200);
      }
      else
      {
        $response['message']='Incorrect OTP';
        return response()->json($response,400);
      }
    }else{
       $response['message']='Invalid mobile number';
       return response()->json($response,400);
    }    
  }


  function SellerResendOtp(Request $request)
    {
      $validations =[        
         'mobileNo'=>'required', 
         'countryCode' => 'required' 
      ];
      $validator=Validator::make($request->all(),$validations);  
      if($validator->fails())
      {
        $response['message']=$validator->errors($validator)->first();
        return response()->json($response,400);
      }
      $query = Seller::where('mobileNo',$request->mobileNo)->where('countryCode', $request->countryCode)->first();
      if($query){
          //$otp = rand(1000,9999);
          $otp = '1234';
          SellerOtp::where('seller_id', $query->id)->delete();
          SellerOtp::create([
            'seller_id' => $query->id, 
            'otp'=> $otp,
          ]);
          $response['message']='OTP sent to your mobile number'; 
          return response()->json($response,200);
      }else{
          $response['message']='You are not Register with us';        
          return response()->json($response,400);
      }         
    }

    function PrivacyPolicy(){
      // dd("jgtyuf");
      $data = CmsPage::where('id', '1')->first();
      return response()->json(['message'=> 'success', 'data'=>$data]);
    }

    function AboutUs(){
      $data = About::where('id', '1')->first();
      return response()->json(['message'=> 'success', 'data'=>$data]);
    }

    function Faq(){
      $data = Faq::orderBy('id', 'Desc')->get();
      return response()->json(['message'=> 'success', 'data'=>$data]);
    }

    function ContactUs(Request $request){
      
      // dd($request->UserData);
      
      // try {
      //     $listId = 'eeecc6e917';
      //     $options = [
      //       'list_id'   => $listId,
      //       'subject' => 'Myles App Query',
      //       'from_name' => $request->input('name'),
      //       'from_email' => 'info@themylesapp.ca',
      //       'to_name' => $request->input('email')
      //     ];
      //     $options = [
      //       'list_id'   => $listId,
      //       'subject' => 'mylesapp',
      //       'from_name' => 'testname',
      //       'from_email' => 'kuldeeptomer777@gmail.com',
      //       'to_name' => 'tomerkuldeep08@gmail.com',
      //     ];

      //     $content = [
      //       'html' => $request->input('role'),
      //       'text' => strip_tags($request->input('role'))
      //     ];

      //     $campaign = $this->mailchimp->campaigns->create('regular', $options, $content);
      //     $this->mailchimp->campaigns->send($campaign['id']);

      //     WebUser::create([
      //       'name' => $request->name,
      //       'email' => $request->email,
      //       'city' => $request->city,
      //       'role' => $request->role,
      //     ]);
      //     return redirect()->back()->with('message','your request send successfully we will contact you soon!');
      //   }catch (Exception $e) {          
      //     return redirect()->back()->with('message','Something went wrong!');
      //   }

      // $data = new ContactUser;
      // $data->conname = $request->UserData->firstName;
      // $data->conemail = $request->UserData->emailAddress;
      // $data->conmobileno = $request->UserData->mobileNo;
      // $data->concity = $request->UserData->city;
      // $data->conmessage = $request->message;
      // if($data->save()){
        return response()->json(['message'=> 'your request send successfully we will contact you soon!'], 200);
      // }else{
        // return response()->json(['message'=> 'something went wrong'], 400);
      // }
    }


    function AddImage(Request $request){
      // dd($request->all());
      $validations =[        
         'imagename'=>'required|mimes:jpg,png,svg,gif|max:1024',
      ];
      $validator=Validator::make($request->all(),$validations);  
      if($validator->fails())
      {
        $response['message']=$validator->errors($validator)->first();
        return response()->json($response,400);
      }

      $imageName = time().'.'.$request->imagename->getClientOriginalExtension();  
      $request->imagename->move(public_path('image'), $imageName);

      $data = new User;
      $data->profileImage = $imageName;
      if($data->save()){
        return response()->json(['message'=>'image uploded successfully'],200);
      }else{
        return response()->json(['message'=>'Something went wrong'],400);
      }

      // dd("asdsa");
    }
    

  }  