<?php

namespace App\Http\Middleware;

use Auth;
use Redirect;
use Closure;

class SellerMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Auth::guard('seller')->check())
            {
               return Redirect('login/seller');
            }
       
        return $next($request);
    }
}
