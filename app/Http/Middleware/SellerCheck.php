<?php

namespace App\Http\Middleware;

use Closure;
use DB;

class SellerCheck
{
    public function handle($request, Closure $next)
    {
        //dd('vkjsbvjk');
        $accessToken =$request->header('accessToken');
        if($accessToken){
            $check=DB::table('sellers')->where('access_token',$accessToken)->first();
            //dd($check);
            if($check){
                $request['UserData']=$check;
                return $next($request);
            }else{
                $response['message']="Invalid access token";
                return response()->json($response,401);
            }

        }else{
            $response['message']="Access Token is required";
            return response()->json($response,403);
        }
    }
}
