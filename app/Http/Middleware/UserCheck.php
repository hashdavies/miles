<?php

namespace App\Http\Middleware;

use Closure;
use DB;
class UserCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $accessToken =$request->header('accessToken');
        if($accessToken){
            $check=DB::table('users')->where('access_token',$accessToken)->first();
            if($check){
                $request['UserData']=$check;
                return $next($request);
            }else{
                $response['message']="Invalid access token";
                return response()->json($response,401);
            }

        }else{
            $response['message']="Access Token is required";
            return response()->json($response,403);
        }
    }
}
