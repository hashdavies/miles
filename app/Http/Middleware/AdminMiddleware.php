<?php

namespace App\Http\Middleware;

use Auth;
use Redirect;
use Closure;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    
    public function handle($request, Closure $next)
    {
        if(!Auth::guard('admin')->check())
            {
               return Redirect('login/admin');
            }
       
        return $next($request);
    }
}
