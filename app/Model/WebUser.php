<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class WebUser extends Model
{
    protected $guarded = ['website_user'];
    protected $table = 'website_user';
}
