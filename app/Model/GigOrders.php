<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\ServiceCategoryAddress;
use App\User;
use App\Seller\Seller;
use App\Model\PromoCode;

class GigOrders extends Model
{
    protected $guarded = ['gig_orders'];
    protected $table = 'gig_orders';

    public function UserGigAddressDetails(){
    	return $this->belongsTo(ServiceCategoryAddress::class, 'gig_id', 'id');
    }
    public function UserName(){
    	return $this->belongsTo(User::class, 'user_id', 'id');
    }
    public function SellerName(){
    	return $this->belongsTo(Seller::class, 'seller_id', 'id');
    }
    public function CouponName(){
    	return $this->belongsTo(PromoCode::class, 'coupon_discount', 'id');
    }
}
