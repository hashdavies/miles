<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AddSellerAddress extends Model
{
    protected $guarded = ['add_seller_address'];
    protected $table = 'add_seller_address';
}
