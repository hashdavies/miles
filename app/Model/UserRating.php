<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Seller;

class UserRating extends Model
{
    protected $guarded = ['user_rating'];
    protected $table = 'user_rating';

    public function UserName(){
    	return $this->belongsTo(User::class, 'User_Id', 'id');
    }

    public function SellerName(){
    	return $this->belongsTo(Seller::class, 'Seller_Id', 'id');
    }

    public function RatingUserName(){
    	return $this->hasMany(User::class, 'id', 'User_Id');
    }
    

}