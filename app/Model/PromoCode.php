<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PromoCode extends Model
{
    protected $guarded = ['promo_code'];
    protected $table = 'promo_code';
 
 	public function getCouponImageAttribute($value){
 		// dd($value);

        if(empty($value)){
            return "";
        }else{
        	// dd(\Config::get('https://mylesimages.s3.ap-south-1.amazonaws.com/Category')."/".$value);
            
        	return "https://mylesimages.s3.ap-south-1.amazonaws.com/Category"."/".$value;
             
        }
    }      
}
