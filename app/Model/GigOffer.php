<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Seller;
use App\Model\City;

class GigOffer extends Model
{
    protected $guarded = ['gig_offer'];
    protected $table = 'gig_offer';

    public function UserName(){
    	return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function SellerName(){
    	return $this->belongsTo(Seller::class, 'seller_id', 'id');
    }

    public function CityName(){
    	return $this->belongsTo(City::class, 'cityName', 'id');
    }

    
}
