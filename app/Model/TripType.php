<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TripType extends Model
{
    protected $guarded = ['user_trip_type'];
    protected $table = 'user_trip_type';

}
