<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ServiceCategoryImage extends Model
{
    protected $guarded = ['category_service_image'];
    protected $table = 'category_service_image'; 

    
    public function getServiceImageAttribute($value){
		return $value ? asset('storage/banner/'.$value) : "";
	}
}
