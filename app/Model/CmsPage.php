<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CmsPage extends Model
{
    protected $guarded = ['cms_page'];
    protected $table = 'cms_page';
}