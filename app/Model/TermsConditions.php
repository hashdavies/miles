<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TermsConditions extends Model
{
    protected $guarded = ['terms_conditions'];
    protected $table = 'terms_conditions';
}
