<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Model\ServiceCategory;
use App\Seller;
use App\Model\ServiceCategoryImage;

class ServiceCategoryAddress extends Model
{
    protected $guarded = ['category_service_address'];
    protected $table = 'category_service_address';

    public function UserName(){
    	return $this->belongsTo(User::class, 'user_id', 'id');
    }
    public function CategoryName(){
    	return $this->belongsTo(ServiceCategory::class, 'cat_id', 'id');
    } 
    public function SellerName(){
        return $this->belongsTo(Seller::class, 'Seller_ID', 'id');
    }
    public function CityName(){
    	return $this->belongsTo(City::class, 'cityName', 'id');
    }
    public function TripType(){
        return $this->belongsTo(TripType::class, 'trip_type', 'id');
    }     
    public function CaregoryImages(){
        return $this->HasMany(ServiceCategoryImage::class,'cat_id', 'cat_id');
    }    
    public function StopAddress(){
        return $this->HasMany(UserTripStop::class,'service_category_address_id', 'id');
    }
}