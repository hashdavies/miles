<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ContactUser extends Model
{
    protected $guarded = ['contact_user'];
    protected $table = 'contact_user';
}
