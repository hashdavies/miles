<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $guarded = ['user_notification'];
    protected $table = 'user_notification';
}
