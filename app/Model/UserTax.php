<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserTax extends Model
{
    protected $guarded = ['user_tax'];
    protected $table = 'user_tax';
}