<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CardDetails extends Model
{
    protected $guarded = ['card_detail'];
    protected $table = 'card_detail';
}
