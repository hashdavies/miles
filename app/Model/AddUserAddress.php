<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AddUserAddress extends Model
{
    protected $guarded = ['add_user_address'];
    protected $table = 'add_user_address';
}
