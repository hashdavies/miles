<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class About extends Model
{
    protected $guarded = ['about'];
    protected $table = 'about';
}
