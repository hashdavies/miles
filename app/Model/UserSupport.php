<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\User;

class UserSupport extends Model
{
    protected $guarded = ['user_support'];
    protected $table = 'user_support';

    public function UserName(){
    	return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
