<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\SubcategoryService;

class ServiceCategory extends Model
{
    protected $guarded = ['service_category'];
    protected $table = 'service_category';

    public function subcategoryname(){
    	return $this->belongsTo(SubcategoryService::class, 'subcategoryID', 'id'); 
    }

}
