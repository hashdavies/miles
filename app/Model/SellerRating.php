<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SellerRating extends Model
{
    protected $guarded  =['seller_rating'];
    protected $table = 'seller_rating';
}
