<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\ServiceCategoryAddress;
use App\User;
use App\Model\ServiceCategory;
use App\Seller;

class UsrRequestGig extends Model
{
    protected $guarded = ['user_request_gig'];
    protected $table = 'user_request_gig';

    public function UserGigDetail(){
    	return $this->belongsTo(ServiceCategoryAddress::class, 'User_ID', 'user_id');
    }

    public function UserName(){
    	return $this->belongsTo(User::class, 'User_ID', 'id');
    }

    public function SellerName(){
        return $this->belongsTo(Seller::class, 'Seller_ID', 'id');
    }

    public function GigDetails(){
        return $this->belongsTo(ServiceCategoryAddress::class, 'gig_id', 'id');
    }

    public function CategoryName(){
    	return $this->belongsTo(ServiceCategory::class, 'cat_id', 'id');
    }

    public function GigDetail(){
        return $this->belongsTo(ServiceCategoryAddress::class, 'gig_id', 'id');
    } 

    

}
