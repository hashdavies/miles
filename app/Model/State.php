<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    protected $guarded = ['states'];
    protected $table = 'states';
}
