<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ServiceTax extends Model
{
    protected $guarded = ['service_tax'];
    protected $table = 'service_tax';
}
