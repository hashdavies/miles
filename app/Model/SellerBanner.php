<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SellerBanner extends Model
{
    protected $guarded = ['seller_banner'];
    protected $table = 'seller_banner';
}
