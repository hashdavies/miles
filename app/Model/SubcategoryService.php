<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SubcategoryService extends Model
{
    protected $guarded = ['sub_service_category'];
    protected $table = 'sub_service_category';
}
