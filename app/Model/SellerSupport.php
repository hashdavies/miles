<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Seller;

class SellerSupport extends Model
{
    protected $guarded = ['seller_support'];
    protected $table = 'seller_support'; 

    public function SellerName(){
    	return $this->belongsTo(Seller::class,'seller_id' ,'id');
    }


}
