<?php

namespace App;

use Illuminate\Database\Eloquent\Model; 
 
class SellerOtp extends Model
{
   protected $guarded = ['seller_otps'];
   protected $table ='seller_otps';
}

