<?php

namespace App\Seller;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model; 
use Illuminate\Foundation\Auth\User as Authenticatable;

class Seller extends Model
{
    use Notifiable;

    protected $guard = 'seller';

    protected $fillable = [
        'countryCode', 'mobileNumber', 'password', 'otp', 'access_token'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

}