<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Model\PurchaseLoyalityOffer;

class User extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
        'firstName', 'lastName', 'nickName', 'countryCode', 'mobileNo', 'emailAddress', 'password', 'registerDate', 'is_verified', 'dob', 'remember', 'gender', 'access_token', 'address','address_latitude', 'address_longitude', 'status', 'province', 'city','fcm_token', 'device_type', 'device_token',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The attributes that are mass assignable.
     *
     //* @var array
     */
     //protected $guarded =[];

    /**
     * The attributes that should be hidden for arrays.
     *
    // * @var array
     */
    
    // public function UserOffer(){
    //     // return $this->belongsTo(::);
    // }

    public function Business_Hours(){
        return $this->belongsTo(BusinessHour::class, 'id', 'SellerID');
    }

    public function ActiveOfferActivated(){
        return $this->HasMany(PurchaseLoyalityOffer::class, 'Pur_User_ID', 'id');
    }

} 