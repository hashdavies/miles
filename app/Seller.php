<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Model\BusinessHour;
use App\Model\UserRating;

class Seller extends Model
{
    protected $guarded = ['sellers'];
    protected $table = 'sellers';

    public function SellerRating(){
        return $this->belongsTo(UserRating::class, 'id', 'Seller_Id');
    } 
    
}
