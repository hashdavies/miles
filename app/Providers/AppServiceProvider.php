<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Model\CmsPage;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
         ini_set("memory_limit", "100M");
        ini_set('post_max_size', '50M');
        ini_set('upload_max_filesize', '50M');
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {        
        view()->composer('*', function($view) {
            // $adminProfile = auth()->guard('admin')->user()->toArray();
            
            // dd($adminProfile);

            $CmsPageCustom = CmsPage::orderBy('id','Desc')->get();           
            $view->with(['CmsPageCustom'=> $CmsPageCustom]); 
        });
    }
}